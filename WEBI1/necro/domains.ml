module SMap = Map.Make(String)

module ABool = struct
  type t = bool * bool
  let is_bot (x, y) = not x && not y
  let check_bot v = if is_bot v then None else Some v
  let top = (true, true)
  let lub (x1, y1) (x2, y2) = (x1 || x2, y1 || y2)
  let glb (x1, y1) (x2, y2) = check_bot (x1 && x2, y1 && y2)
  let neg (x1, x2) = (x2, x1)
  let leq (x1, y1) (x2, y2) =
    ((x2 || not x1) && (y2 || not y1))
  let widen = lub
  let pp_print ff = function
    | None | Some (false, false) -> Format.fprintf ff "bot_B"
    | Some (false, true) -> Format.fprintf ff "false#"
    | Some (true, false) -> Format.fprintf ff "true#"
    | Some (true, true) -> Format.fprintf ff "top_B"
end

module Interval = struct
  type bound = Bminf | Bint of int | Binf
  type t = bound * bound

  let top = (Bminf, Binf)

  let bound_leq b1 b2 = match b1, b2 with
    | Bminf, _ | _, Binf -> true
    | _, Bminf | Binf, _ -> false
    | Bint n1, Bint n2 -> n1 <= n2

  let bound_lub b1 b2 = match b1, b2 with
    | Binf, _ | _, Binf -> Binf
    | Bminf, b | b, Bminf -> b
    | Bint n1, Bint n2 -> Bint (max n1 n2)

  let bound_glb b1 b2 = match b1, b2 with
    | Bminf, _ | _, Bminf -> Bminf
    | Binf, b | b, Binf -> b
    | Bint n1, Bint n2 -> Bint (min n1 n2)

  let bound_add b1 b2 = match b1, b2 with
    | Binf, Bminf | Bminf, Binf -> assert false
    | Binf, _ | _, Binf -> Binf
    | Bminf, _ | _, Bminf -> Bminf
    | Bint n1, Bint n2 -> Bint (n1 + n2)

  let pp_print_bound ff = function
    | Binf -> Format.fprintf ff "+inf"
    | Bminf -> Format.fprintf ff "-inf"
    | Bint n -> Format.fprintf ff "%d" n

  let is_bot (b1, b2) = not (bound_leq b1 b2)
  let check_bot v = if is_bot v then None else Some v

  let leq (x1, y1) (x2, y2) =
    bound_leq x2 x1 && bound_leq y1 y2

  let lub (x1, y1) (x2, y2) =
    (bound_glb x1 x2, bound_lub y1 y2)

  let glb (x1, y1) (x2, y2) =
    check_bot (bound_lub x1 x2, bound_glb y1 y2)

  let add (x1, y1) (x2, y2) =
    (bound_add x1 x2, bound_add y1 y2)

  let eq (x1, y1) (x2, y2) =
    let can_be_equal = (glb (x1, y1) (x2, y2)) <> None in
    let can_be_different = not (x1 = y1 && x1 = x2 && x2 = y2) in
      (can_be_equal, can_be_different)

  let widen (x1, y1) (x2, y2) =
    let (x2, y2) = lub (x1, y1) (x2, y2) in
    let nx = if bound_leq x1 x2 then x2 else Bminf in
    let ny = if bound_leq y2 y1 then y2 else Binf in
    (nx, ny)

  let pp_print ff = function
    | None -> Format.fprintf ff "bot_I"
    | Some (b1, b2) -> Format.fprintf ff "[%a, %a]" pp_print_bound b1 pp_print_bound b2
end

module AValue = struct
  type t = Interval.t option * ABool.t option

  let top = (Some Interval.top, Some ABool.top)
  let check_bot (i, b) = match i, b with None, None -> None | _ -> Some (i, b)

  let lub (i1, b1) (i2, b2) =
    let i = match i1, i2 with None, i | i, None -> i | Some i1, Some i2 -> Some (Interval.lub i1 i2) in
    let b = match b1, b2 with None, b | b, None -> b | Some b1, Some b2 -> Some (ABool.lub b1 b2) in
    (i, b)

  let glb (i1, b1) (i2, b2) =
    let i = match i1, i2 with None, _ | _, None -> None | Some i1, Some i2 -> Interval.glb i1 i2 in
    let b = match b1, b2 with None, _ | _, None -> None | Some b1, Some b2 -> ABool.glb b1 b2 in
    check_bot (i, b)

  let widen (i1, b1) (i2, b2) =
    let i = match i1, i2 with None, i | i, None -> i | Some i1, Some i2 -> Some (Interval.widen i1 i2) in
    let b = match b1, b2 with None, b | b, None -> b | Some b1, Some b2 -> Some (ABool.widen b1 b2) in
    (i, b)

  let leq (i1, b1) (i2, b2) =
    (match i1, i2 with None, _ -> true | Some _, None -> false | Some i1, Some i2 -> Interval.leq i1 i2) &&
    (match b1, b2 with None, _ -> true | Some _, None -> false | Some b1, Some b2 -> ABool.leq b1 b2)

  let from_int i = (Some i, None)
  let from_bool b = (None, Some b)

  let pp_print ff = function
    | None -> Format.fprintf ff "bot_V"
    | Some (i, b) -> Format.fprintf ff "(%a, %a)" Interval.pp_print i ABool.pp_print b
end

module AState = struct
  type t = AValue.t SMap.t

  let top = SMap.empty
  let lub s1 s2 =
    SMap.merge (fun _ x1 x2 ->
        match x1, x2 with None, x | x, None -> x | Some x1, Some x2 -> Some (AValue.lub x1 x2)
      ) s1 s2

  let leq s1 s2 =
    SMap.for_all (fun _ b -> b) (SMap.merge (fun _ x1 x2 ->
        match x1, x2 with
        | None, _ -> None
        | Some _, None -> Some false
        | Some x1, Some x2 -> Some (AValue.leq x1 x2)
      ) s1 s2)

  let widen s1 s2 =
    SMap.merge (fun _ x1 x2 ->
        match x1, x2 with
        | None, x | x, None -> x
        | Some x1, Some x2 -> Some (AValue.widen x1 x2)
      ) s1 s2

  let pp_print ff s =
    match s with
    | None -> Format.fprintf ff "bot_S"
    | Some s ->
      Format.fprintf ff "@[<v>";
      SMap.iter (fun x v -> Format.fprintf ff "%s |-> %a@," x AValue.pp_print (Some v)) s;
      Format.fprintf ff "@]"
end
