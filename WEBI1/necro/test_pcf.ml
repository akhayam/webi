open Pcf

module SMap = Map.Make(String)
module F = struct
  type ident = string
  type nonrec int = int
  type env = value SMap.t
  and value =
    | Int of int
    | Clos of ident * (ident, int) term * env
  
  let initial_env () = SMap.empty
  let print_value = function
    | Int i -> Format.printf "%d@." i
    | Clos _ -> Format.printf "<function>@."
  
  let extEnv env x v = Some (SMap.add x v env)
  let getClos = function | Int _ -> None | Clos (x, t, e) -> Some (x, t, e)
  let getEnv x env = SMap.find_opt x env
  let getInt = function | Int i -> Some i | Clos _ -> None
  let isNotZero d = if d = 0 then None else Some ()
  let isZero d = if d = 0 then Some () else None
  let mkClos x t e = Some (Clos (x, t, e))
  let mkInt d = Some (Int d)
  let plus x y = Some (x + y)
end

module I = MakeInterpreter(F)

let test t0 =
  I.print_value (I.eval (I.initial_env ()) t0)

let let_ n v body = App (Lam (n, body), v)

(*
let t0 =
  let_ "f" (Lam ("x", App (Var "x", Var "x")))
    (App (Lam ("y", Var "y"), App (Var "f", Var "f")))

let () = test t0
*)

let t0 =
  let_ "Y" (Lam ("f", let u = Lam ("x", App (Var "f", Lam ("v", App (App (Var "x", Var "x"), Var "v")))) in App (u, u)))
    (let_ "z" (App (Var "Y", (Lam ("z", Lam ("x", Var "x")))))
       (App (Var "z", Const 42)))

let () = test t0

let t1 =
  let_ "Y" (Lam ("f", let u = Lam ("x", App (Var "f", Lam ("v", App (App (Var "x", Var "x"), Var "v")))) in App (u, u)))
    (let_ "z" (App (Var "Y", (Lam ("z", Lam ("x", IfZero (Var "x", Const 0, App (Var "z", Plus (Var "x", Const (-1)))))))))
       (App (Var "z", Const 42)))

let () = test t1

let t2 =
  let_ "Y" (Lam ("f", let u = Lam ("x", App (Var "f", Lam ("v", App (App (Var "x", Var "x"), Var "v")))) in App (u, u)))
    (let_ "z" (App (Var "Y", (Lam ("z", Lam ("x", IfZero (Var "x", Const 0, App (Var "z", Plus (Var "x", Const (-1)))))))))
       (App (Var "z", Const (-42))))

let () = test t2
