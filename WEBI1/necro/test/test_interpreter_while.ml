(* while language test : needs an interpreter to be generated in "while.ml" *)

open While

module Flow = struct
  type ident = string
  type lit = int
  type vint = int
  type vbool = bool
  type value = Int of vint | Bool of vbool
  type state = (ident, value) Hashtbl.t
  let initial_state () = Hashtbl.create 29
  let litToVal lit = Some (Int lit)
  let read ident state = Hashtbl.find_opt state ident
  let isInt = function
    | Int i -> Some i
    | _ -> None
  let add a b = Some (Int (a + b))
  let eq a b = Some (Bool (a = b))
  let isBool = function
    | Bool b -> Some b
    | _ -> None
  let neg b = Some (Bool (not b))
  let write ident state value =
    let () = Hashtbl.replace state ident value in
    Some state
  let id state = Some state
  let isTrue b = if b then Some () else None
  let isFalse b = if b then None else Some ()
  let mkWhile e s = Some (While(e,s))
  let print_value = function
    | Int i -> Printf.printf "%d" i
    | Bool b -> Printf.printf "%B" b
  let print_state state =
    let f id v =
      Printf.printf "%s : " id;
      print_value v;
      print_newline()
    in
    Hashtbl.iter f state
end

module InterpWhile : (INTERPRETER with type ident = string and type lit = int) = MakeInterpreter (Flow)

open InterpWhile

let () =
  let t =
    Seq (
      Assign ("a", Const 10),
      If (
        Equal(Var "a", Const 9),
        Assign ("x", Const 1),
        Assign ("x", Const 0)
      )
    )
  in
  let s = stat (initial_state()) t in
  print_state s;

  let t =
    Seq (
      Assign ("n", Const 10),
    Seq (
      Assign ("a", Const 1),
    Seq (
      Assign ("b", Const 1),
    Seq (
      Assign ("c", Const 0),
    Seq (
      Assign ("i", Const 0),
      While (
        Bang (Equal (Var "i", Var "n")),
        Seq (
          Assign ("d", Var "b"),
        Seq (
          Assign ("b", Plus (Var "a", Var "b")),
        Seq (
          Assign ("a", Var "d"),
        Seq (
          Assign ("c", Plus (Var "c", Var "b")),
          Assign ("i", Plus (Var "i", Const 1))
        ))))
      )
    )))))
  in
  let s = stat (initial_state()) t in
  print_state s;

  let t =
    Seq (
      Assign ("over", Const 0),
    Seq (
      Assign ("fact", Const 1),
    Seq (
      Assign ("n", Const 10),
      While (
        Equal(Var "over", Const 0),
        If (
          Equal(Var "n", Const 0),
          Assign ("over", Const 1),
          Seq (
            Assign ("a", Var "fact"),
          Seq (
            Assign ("i", Const 1),
          Seq (
            While (
              Bang (Equal(Var "i", Var "n")),
              Seq (
                Assign ("fact", Plus (Var "fact", Var "a")),
                Assign ("i", Plus (Var "i", Const 1))
              )
            ),
            Assign ("n", Plus (Var "n", Const (-1)))
          )))
        )
      )
    )))
  in
  let s = stat (initial_state()) t in
  print_state s
