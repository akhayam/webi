open Stream
(* open Typechecker *)

open Generator

(* getlines ic takes all the lines available in the input channel and puts
   them in a string.
*)
let getlines ic =
  let rec f acc =
    try f ((input_line ic) :: acc) with End_of_file -> String.concat "\n" (List.rev acc)
  in f []

let report_error filename start_pos end_pos =
  let open Lexing in
  let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
  let end_col = end_pos.pos_cnum - start_pos.pos_bol + 1 in
  Format.eprintf "File %S, line %d, characters %d-%d:@." filename start_pos.pos_lnum start_col end_col

(* MAIN FUNCTION *)
let () =
  if Array.length Sys.argv = 3 then begin
    let atoms, env = Util.parse_from_file Parser.main Sys.argv.(1) in
    let () = Skeleton.check_well_typed env in
    generate_interpreter env atoms Sys.argv.(2);
    (* generate_user_input bs fs ps atoms const filt
      Sys.argv.(2) (Sys.argv.(2) ^ ".userinput") *)
  end else begin
    print_endline ("Usage : " ^ Sys.argv.(0) ^ " input_file output_file.");
    exit 1
  end
