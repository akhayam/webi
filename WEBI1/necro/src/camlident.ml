open Types
open Printing

(* maps a (char -> string) function to each char of a string and returns the
   concatenation of all the results (basically map & concat at the same time)
*)
let smap f s =
  let l = String.length s in
  let rec smap_aux acc i =
    if i = l then acc
    else smap_aux (acc ^ (f s.[i])) (i+1)
  in
  smap_aux "" 0

(* converts a constructor name to a valid OCaml constructor name *)
let get_alpha_name s =
  let f = function
    | '&' -> "And"
    | '%' -> "Percent"
    | '!' -> "Bang"
    | '$' -> "Dollar"
    | '=' -> "Equal"
    | '+' -> "Plus"
    | ':' -> "Colon"
    | '/' -> "Slash"
    | ';' -> "Semicolon"
    | '.' -> "Dot"
    | '?' -> "QMark"
    | '<' -> "Lt"
    | '>' -> "Gt"
    | '@' -> "At"
    | '#' -> "Hash"
    | '-' -> "Minus"
    | '\'' -> "Quote"
    | c -> String.make 1 c
  in
  let s' = smap f s in
  let g = function
    | '_' -> "Underscore"
    | '0' -> "Zero"
    | '1' -> "One"
    | '2' -> "Two"
    | '3' -> "Three"
    | '4' -> "Four"
    | '5' -> "Five"
    | '6' -> "Six"
    | '7' -> "Seven"
    | '8' -> "Eight"
    | '9' -> "Nine"
    | letter -> String.make 1 (Char.uppercase_ascii letter)
  in
  let r = Str.regexp "[a-z0-9_]" in
  let len = String.length s' in
  if Str.string_match r (String.sub s' 0 1) 0 then
    (g s.[0]) ^ (String.sub s' 1 (len - 1))
  else s'

(* converts a sort to 'sortname string format *)
let generic_name_of_sort s = "'" ^ (string_of_sort s)

(* converts a string to snake case (OneTwoThree -> one_two_three) *)
let get_snake_case s =
  let f c =
    let cc = Char.code c in
    if cc >= 65 && cc <= 90 then
      "_" ^ (String.make 1 (Char.lowercase_ascii c))
    else String.make 1 c
  in
  let result = smap f s in
  match result.[0] with
  | '_' -> String.sub result 1 (String.length result - 1)
  | _ -> result

let pp_sep_string s ff () = Format.fprintf ff "%s" s

let print_constructor_args args =
  match args with
  | [] -> empty
  | [arg] -> Cat [cs " "; arg]
  | _ -> make_list ~first:(cs " (") ~last:(cs ")") ~sep:(cs ", ") args

let pp_print_constructor_args f ff args =
  match args with
  | [] -> ()
  | [arg] -> Format.fprintf ff " %a" f arg
  | _ -> Format.fprintf ff " (%a)" (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") f) args

let print_type_args args =
  match args with
  | [] -> empty
  | [arg] -> Cat [arg; cs " "]
  | _ -> make_list ~first:(cs "(") ~last:(cs ") ") ~sep:(cs ", ") args

let pp_print_type_args f ff args =
  match args with
  | [] -> ()
  | [arg] -> Format.fprintf ff "%a " f arg
  | _ -> Format.fprintf ff "(%a) " (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") f) args

let print_constructor_declaration args =
  match args with
  | [] -> empty
  | [arg] -> Cat [cs " of "; arg]
  | _ -> make_list ~first:(cs " of ") ~sep:(cs " * ") args

let print_constructor_declaration_gadt args =
  match args with
  | [] -> cs " : "
  | [arg] -> Cat [cs " : "; arg; cs " -> "]
  | _ -> make_list ~first:(cs " : ") ~last:(cs " -> ") ~sep:(cs " * ") args

let print_base_generic_args (bs : flow_sort list) =
  print_type_args (List.map (fun s -> Cat [cs "'"; cs s]) bs)

let pp_print_base_generic_args (bs : flow_sort list) ff =
  pp_print_type_args (fun ff s -> Format.fprintf ff "'%s" s) ff bs

let print_base_args bs =
  print_type_args (List.map (fun s -> cs s) bs)

let print_ast_types env =
  let bs = get_term_base_types env in
  let print_targs = print_base_generic_args bs in
  let print_type_arg = function
    | Flow s -> Cat [cs "'"; cs s]
    | Program s -> Cat [print_targs; cs s]
  in
  let print_constructor_line c =
    Cat [cs "| "; cs (get_alpha_name c.cs_name); print_constructor_declaration (List.map print_type_arg c.cs_input_sorts)]
  in
  let const = get_constructors_by_sort env in
  make_list ~first:(cs "type ") ~sep:(Cat [nlc; nlc; cs "and "]) (List.map (fun (ps_name, const) ->
      Cat [print_targs; cs ps_name; cs " ="; nlc;
           make_list ~sep:nlc (List.map print_constructor_line const)]
    ) const)

let pp_print_ast_types ff env = pp_print_cl ff (print_ast_types env)
