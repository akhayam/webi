%{
    open Types

    let mkflow l = List.map (fun x -> Flow x) l
    let mkenv_sorts bfs ps =
      let m = SMap.empty in
      let m = List.fold_left (fun m x -> SMap.add x (Flow x) m) m bfs in
      let m = List.fold_left (fun m x -> SMap.add x (Program x) m) m ps in
      m

    module SSet = Set.Make(String)
    let rec identify_hooks_bone hk = function
      | Hook h -> Hook h
      | Filter { f_name ; f_inputs ; f_outputs } when SSet.mem f_name hk ->
         let iv, it = match f_inputs with [iv; it] -> (iv, it) | _ -> failwith "Wrong number of arguments to hook" in
         let ov = match f_outputs with [ov] -> ov | _ -> failwith "Hook must have only one output argument" in
         Hook { h_name = f_name ; h_annot = "" ; h_inputs = [iv] ; h_term = TVar it ; h_outputs = [ov] }
      | Filter f -> Filter f
      | Branching b -> Branching { b with b_branches = List.map (identify_hooks_skeleton hk) b.b_branches }

    and identify_hooks_skeleton hk l = List.map (identify_hooks_bone hk) l

    type decl =
      | BaseType of string
      | ProgramType of string * (string * string list) list
      | FilterDecl of string * string list * string list
      | Atom of string
      | HookDef of string * string * string * string * (string * string list * skeleton) list

    let split_decl l =
      let rec aux (a, b, c, d, e) = function
      | [] -> (a, b, c, d, e)
      | BaseType s :: l -> aux (s :: a, b, c, d, e) l
      | ProgramType (name, consts) :: l -> aux (a, (name, consts) :: b, c, d, e) l
      | FilterDecl (name, ins, outs) :: l -> aux (a, b, (name, ins, outs) :: c, d, e) l
      | Atom at :: l -> aux (a, b, c, at :: d, e) l
      | HookDef (name, tin, tterm, tout, rules) :: l -> aux (a, b, c, d, (name, tin, tterm, tout, rules) :: e) l
      in
      aux ([], [], [], [], []) (List.rev l)

    type cdecl =
      | CType of (string * typedesc)
      | CVar of var_constr_decl
      | CRuleInVar of (string * typedesc)
      | CRuleOutVar of (string * typedesc)
      | CFilter of pfilter_constr_decl
      | CHookIn of phookin_constr_decl
      | CHookOut of phookout_constr_decl
      | CRuleIn of prulein_constr_decl
      | CRuleOut of pruleout_constr_decl
      | CRuleVarIn of (string * typedesc)
      | CRuleVarOut of (string * typedesc)
      | CQuark of quark_decl

%}

// Keywords
%token ATOM
%token BRANCH
%token END
%token HOOK
%token OF
%token OR
%token TYPE
%token VAL

%token FILTER
%token FORALL
%token IN
%token JOIN
%token LET
%token LIST
%token MAP
%token OUT
%token PROGRAM_POINT
%token RULE
%token SET
%token TRUE
%token VAR

// Identifiers
%token <string> LIDENT
%token <string> UIDENT

// Operators
%token BAR
%token BARBAR
%token BARMINUSGREATER
%token CARET
%token COLON
%token COMMA
%token EQUAL
%token LESSEQUAL
%token LESSMINUS
%token MINUSGREATER
%token QUESTIONGREATER
%token SEMI
%token SLASHBACKSLASH
%token STAR

// Paired delimiters
%token LPAREN
%token RPAREN
%token LBRACE
%token RBRACE
%token LBRACK
%token RBRACK
%token LBRACKBAR
%token BARRBRACK

// Other
%token EOF

%start main
%type <string list * Types.env> main
%start main_constr
%type <Types.pcdecls> main_constr

%nonassoc let_prec forall_prec join_prec
%left SLASHBACKSLASH
%nonassoc LESSEQUAL
%nonassoc LBRACK

%%

main:
    | decls = list(decl); EOF
    {
      let base, progs, filt, atoms, hooks = split_decl decls in
      let sorts = mkenv_sorts base (List.map fst progs) in
      let type_sort x = SMap.find x sorts in
      let type_sorts = List.map type_sort in
      let const = List.flatten (List.map (fun (o, cst) -> List.map (fun (name, l) -> {
          cs_name = name ;
          cs_input_sorts = type_sorts l ;
          cs_output_sort = o
        }) cst) progs) in
      let env_consts = List.fold_left (fun m c -> SMap.add c.cs_name c m) SMap.empty const in
      let filt = List.map (fun (name, lin, lout) -> {
          fs_name = name ;
          fs_input_sorts = type_sorts lin ;
          fs_output_sorts = type_sorts lout ;
        }) filt in
      let env_filt = List.fold_left (fun m f -> SMap.add f.fs_name f m) SMap.empty filt in
      let hks = SSet.of_list (List.map (fun (name, _, _, _, _) -> name) hooks) in
      let env_hooks = List.fold_left (fun m (name, tin, tterm, tout, rules) ->
        SMap.add name {
            hd_term_sort = tterm ;
            hd_input_sorts = [type_sort tin] ;
            hd_output_sorts = [type_sort tout] ;
            hd_rules =
              List.fold_left (fun mm (c, args, s) ->
                  SMap.add c {
                      r_constructor = c ;
                      r_constructor_arguments = args ;
                      r_skeleton = identify_hooks_skeleton hks s ;
                      r_input_sort = type_sort tin ;
                      r_output_sort = type_sort tout ;
                      r_name = name ^ "_" ^ c
                    } mm
                ) SMap.empty rules
          } m
        ) SMap.empty hooks in
      let env = {
          env_sorts = sorts ;
          env_constr_signatures = env_consts ;
          env_hook_definitions = env_hooks ;
          env_filter_signatures = env_filt
        } in
      (atoms, env)
    }

decl:
    | TYPE; x = LIDENT { BaseType x }
    | TYPE; x = LIDENT; EQUAL; l = list(constr_decl) { ProgramType (x, l) }
    | f = filter { f }
    | h = hook { h }
    | a = atom { a }

constr_decl:
    | BAR; x = UIDENT { (x, []) }
    | BAR; x = UIDENT; OF; l = separated_nonempty_list(STAR, LIDENT) { (x, l) }

atom:
    | ATOM; l = list(atom_elem) { Atom (String.concat " " l) }

atom_elem:
    | x = LIDENT { x }
    | MINUSGREATER { "->" }
    | COLON { ":" }

filter:
    | VAL; name = LIDENT; COLON; lin = separated_nonempty_list(STAR, LIDENT); MINUSGREATER; lout = separated_nonempty_list(STAR, LIDENT)
    { let lin = if lin = ["unit"] then [] else lin in
      let lout = if lout = ["unit"] then [] else lout in FilterDecl (name, lin, lout) }

const_args:
    | /* empty */ { [] }
    | x = LIDENT { [x] }
    | LPAREN; l = separated_nonempty_list(COMMA, LIDENT); RPAREN { l }

rule:
    | BAR; c = UIDENT; args = const_args; MINUSGREATER; s = skeleton { (c, args, s) }

skeleton:
    | l = separated_nonempty_list(SEMI, bone) { l }

%inline bind_vars:
    | x = LIDENT; LESSMINUS { [x] }
    | LPAREN; l = separated_list(COMMA, LIDENT); RPAREN; LESSMINUS { l }
    | /* nothing */ { [] }

separated_list2(sep, X):
    | x1 = X; sep; x2 = X { [x1; x2] }
    | x = X; sep; l = separated_list2(sep, X) { x :: l }

simple_term:
    | t = UIDENT { TConstr (t, []) }
    | t = LIDENT { TVar t }
    | LPAREN; t = term; RPAREN { t }

simple_onlyterm:
    | t = UIDENT { TConstr (t, []) }
    | LPAREN; t = term; RPAREN { t }

term:
    | t = simple_term { t }
    | t = UIDENT; x = simple_term { TConstr (t, [x]) }
    | t = UIDENT; LPAREN; l = separated_list2(COMMA, term); RPAREN { TConstr (t, l) }

bone:
    | lout = bind_vars; f = LIDENT; lin = list(LIDENT)
      { Filter { f_name = f ; f_inputs = lin ; f_outputs = lout } }
    | vout = LIDENT; LESSMINUS; h = LIDENT; vin = LIDENT; t = simple_onlyterm
      { Hook { h_name = h ; h_annot = ""; h_inputs = [vin] ; h_term = t ; h_outputs = [vout] } }
    | vout = LIDENT; LESSMINUS; h = LIDENT; CARET; a = LIDENT; vin = LIDENT; t = simple_term
      { Hook { h_name = h ; h_annot = a; h_inputs = [vin] ; h_term = t ; h_outputs = [vout] } }
    | v = bind_vars; BRANCH; l = separated_list(OR, skeleton); END
      { Branching { b_branches = l ; b_outputs = v } }

hook:
    | HOOK; name = LIDENT; COLON; instate = LIDENT; MINUSGREATER; progtype = LIDENT; MINUSGREATER; outstate = LIDENT;
      EQUAL; l = list(rule)
      { HookDef (name, instate, progtype, outstate, l) }

/* Constraints */

binder:
    | LPAREN; l = separated_list(COMMA, LIDENT); RPAREN { l }
    | x = LIDENT { [x] }

value:
    | x = LIDENT { PNamedVar x }
    | m = value; LBRACK; v = value; RBRACK { PMapGet (m, v) }
    | f = LIDENT; LPAREN; l = separated_list(COMMA, value); RPAREN { PFunc (f, if l = [] then [PVUnit] else l) }
    | LET; l = binder; EQUAL; v1 = value; IN; v2 = value %prec let_prec { PVLet (v1, (l, v2)) }
    | c = constr; SLASHBACKSLASH; v = value { PVWith (c, v) }
    | LPAREN; RPAREN { PVUnit }
    | LPAREN; l = separated_list2(COMMA, value); RPAREN { PVTuple l }
    | LBRACE; l = separated_list(SEMI, value); RBRACE { PVSet l }
    | LBRACK; l = separated_list(SEMI, value); RBRACK { PVList l }
    | JOIN; l = binder; IN; v1 = value; COMMA; v2 = value %prec join_prec { PVJoin (v1, (l, v2)) }
    | LPAREN; v = value; RPAREN { v }

constr:
    | TRUE { PTrue }
    | c1 = constr; SLASHBACKSLASH; c2 = constr { PConj (c1, c2) }
    | LET; l = binder; EQUAL; v = value; IN; c = constr %prec let_prec { PCLet (v, (l, c)) }
    | v1 = value; LESSEQUAL; v2 = value { PSub (v1, v2) }
    | FORALL; l = binder; IN; v = value; COMMA; c = constr %prec forall_prec { PForall (v, (l, c)) }
    | LPAREN; c = constr; RPAREN { c }

filter_constr_decl:
    | FILTER; f = LIDENT; LPAREN; l = separated_list(COMMA, LIDENT); RPAREN; EQUAL; v = value
      { { pfc_name = f ; pfc_value = (l, v) } }

hookin_constr_decl:
    | HOOK; IN; name = LIDENT; a = maybe_annot; LPAREN; l = separated_list(COMMA, LIDENT); RPAREN; EQUAL; v = value
      { { phi_name = name ; phi_annot = a ; phi_value = (l, v) } }

hookout_constr_decl:
    | HOOK; OUT; name = LIDENT; a = maybe_annot; LPAREN; l = separated_list(COMMA, LIDENT); RPAREN; EQUAL; v = value
      { { pho_name = name ; pho_annot = a ; pho_value = (l, v) } }

rulein_constr_decl:
    | RULE; IN; name = LIDENT; LPAREN; l = separated_list(COMMA, LIDENT); RPAREN; EQUAL; v = value
      { { pri_name = name ; pri_value = (l, v) } }

ruleout_constr_decl:
    | RULE; OUT; name = LIDENT; LPAREN; l = separated_list(COMMA, LIDENT); RPAREN; EQUAL; v = value
      { { pro_name = name ; pro_value = (l, v) } }

rulein_var_decl:
    | RULE; IN; VAR; name = LIDENT; COLON; t = typedesc
      { (name, t) }

ruleout_var_decl:
    | RULE; OUT; VAR; name = LIDENT; COLON; t = typedesc
      { (name, t) }


simple_typedesc:
    | LPAREN; t = typedesc; RPAREN { t }
    | x = LIDENT { BaseType x }
    | t = simple_typedesc; SET { SetType t }
    | t = simple_typedesc; LIST { ListType t }
    | LPAREN; t1 = typedesc; COMMA; t2 = typedesc; RPAREN; MAP { MapType (t1, t2) }
    | PROGRAM_POINT { Program_point TopType }
    | t = simple_typedesc; PROGRAM_POINT { Program_point t }

typedesc:
    | t = simple_typedesc { t }
    | l = separated_list2(STAR, simple_typedesc) { TupleType l }

typ_decl:
    | TYPE; name = LIDENT; EQUAL; t = typedesc { (name, t) }

quark_decl:
    | VAL; f = LIDENT; COLON; l = separated_nonempty_list (STAR, simple_typedesc); MINUSGREATER; r = typedesc
      { { q_name = f ; q_inputs = l ; q_output = r } }

var_constr_decl:
    | VAR; x = LIDENT; COLON; t = typedesc { { vc_name = x ; vc_type = t } }

maybe_annot:
    | /* nothing */ { "" }
    | CARET; x = LIDENT { x }

cdecl:
    | t = typ_decl { CType t }
    | f = filter_constr_decl { CFilter f }
    | q = quark_decl { CQuark q }
    | v = var_constr_decl { CVar v }
    | v = rulein_var_decl { CRuleInVar v }
    | v = ruleout_var_decl { CRuleOutVar v }
    | h = hookin_constr_decl { CHookIn h }
    | h = hookout_constr_decl { CHookOut h }
    | r = rulein_constr_decl { CRuleIn r }
    | r = ruleout_constr_decl { CRuleOut r }

main_constr:
    | l = list(cdecl); EOF
      {
        let ts = List.of_seq (Seq.filter_map (function CType t -> Some t | _ -> None) (List.to_seq l)) in
        let vs = List.of_seq (Seq.filter_map (function CVar v -> Some v | _ -> None) (List.to_seq l)) in
        let rivs = List.of_seq (Seq.filter_map (function CRuleInVar v -> Some v | _ -> None) (List.to_seq l)) in
        let rovs = List.of_seq (Seq.filter_map (function CRuleOutVar v -> Some v | _ -> None) (List.to_seq l)) in
        let qs = List.of_seq (Seq.filter_map (function CQuark q -> Some q | _ -> None) (List.to_seq l)) in
        let fs = List.of_seq (Seq.filter_map (function CFilter f -> Some f | _ -> None) (List.to_seq l)) in
        let his = List.of_seq (Seq.filter_map (function CHookIn h -> Some h | _ -> None) (List.to_seq l)) in
        let hos = List.of_seq (Seq.filter_map (function CHookOut h -> Some h | _ -> None) (List.to_seq l)) in
        let ris = List.of_seq (Seq.filter_map (function CRuleIn r -> Some r | _ -> None) (List.to_seq l)) in
        let ros = List.of_seq (Seq.filter_map (function CRuleOut r -> Some r | _ -> None) (List.to_seq l)) in
        {
          pc_types = ts ;
          pc_vars = vs ;
          pc_rulein_vars = rivs ;
          pc_ruleout_vars = rovs ;
          pc_quarks = qs ;
          pc_filters = fs ;
          pc_hookins = his ;
          pc_hookouts = hos ;
          pc_ruleins = ris ;
          pc_ruleouts = ros ;
        }
      }
