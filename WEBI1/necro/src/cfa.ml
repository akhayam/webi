open Types
open Skeleton
open Camlident

type 'a lambda = string list * 'a

type path = int list

type 'a with_type = {
  desc : 'a ;
  typ : typedesc ;
}

type lvalue_desc =
  | LMapGet of lvalue * value
  | LNamedVar of string

and lvalue = lvalue_desc with_type

and value_desc =
  | LocalVar of string
  | MapGet of value * value
  | NamedVar of string
  (* | ConstantPath of path *)
  | Func of string * value list
  | VLet of value * value lambda
  | VWith of constr * value
  | VTuple of value list
  | VSet of value list
  | VList of value list
  (* join_{args in v1} v2 *)
  | VJoin of value * value lambda
  (* join_{_ in v1} v2 = if empty v1 then bot else v2 *)
  | VIsEmpty of value * value
  | VUnit

and value = value_desc with_type

and constr =
  | True
  | Conj of constr * constr
  | CLet of value * constr lambda
  (* | Pred of string * value list * lvalue list *)
  | Sub of value * lvalue
  (* forall_{args in v} c *)
  | Forall of value * constr lambda
  (* forall_{_ in v} c = if empty v then c *)
  | CEmpty of value * constr

type dpath = string * value option

type deps =
  | DBase of dpath list
  | DLet of value * deps lambda
  | DForall of value * deps lambda
  | DEmpty of value * deps
  | DConj of deps * deps

let fresh =
  let c = ref 0 in
  fun name ->
    incr c; "__" ^ name ^ "_" ^ (string_of_int !c)

type tcenv = {
  tcenv_types : typedesc SMap.t ;
  tcenv_vars : (typedesc * bool) SMap.t ;
  tcenv_rulevars : (typedesc * typedesc) SMap.t ;
  tcenv_funcs : quark_decl SMap.t ;
}

type filter_constr_decl = {
  fc_name : string ;
  fc_value : value lambda ;
}

type hookin_constr_decl = {
  hi_name : string ;
  hi_annot : string ;
  hi_value : value lambda ;
}

type hookout_constr_decl = {
  ho_name : string ;
  ho_annot : string ;
  ho_value : value lambda ;
}

type rulein_constr_decl = {
  ri_name : string ;
  ri_value : value lambda ;
}

type ruleout_constr_decl = {
  ro_name : string ;
  ro_value : value lambda ;
}


type cenv = {
  ce_tce : tcenv ;
  ce_tenv : env ;
  ce_filts : filter_constr_decl SMap.t ;
  ce_hookins : hookin_constr_decl SMap.t ;
  ce_hookouts : hookout_constr_decl SMap.t ;
  ce_ruleins : rulein_constr_decl SMap.t ;
  ce_ruleouts : ruleout_constr_decl SMap.t ;
}

let rec canonize_type env t =
  match t with
  | BaseType s when SMap.mem s env.tcenv_types -> canonize_type env (SMap.find s env.tcenv_types)
  | _ -> t

let rec check_eqtype env t1 t2 =
  match t1, t2 with
  | SetType t1, SetType t2
  | ListType t1, ListType t2 -> check_eqtype env t1 t2
  | BaseType s1, BaseType s2 when s1 = s2 -> ()
  | TopType, TopType -> ()
  | BottomType, BottomType -> ()
  | TupleType l1, TupleType l2 when List.length l1 = List.length l2 ->
    List.iter2 (check_eqtype env) l1 l2
  | MapType (t1k, t1v), MapType (t2k, t2v) ->
    check_eqtype env t1k t2k; check_eqtype env t1v t2v
  | Program_point t1, Program_point t2 -> check_eqtype env t1 t2
  | BaseType s1, BaseType s2 when SMap.mem s1 env.tcenv_types && SMap.mem s2 env.tcenv_types ->
    check_eqtype env (canonize_type env t1) (canonize_type env t2)
  | BaseType s1, _ when SMap.mem s1 env.tcenv_types ->
    check_eqtype env (canonize_type env t1) t2
  | _, BaseType s2 when SMap.mem s2 env.tcenv_types ->
    check_eqtype env t1 (canonize_type env t2)
  | _ -> failwith "Incompatible types"

let rec check_subtype env t1 t2 =
  match t1, t2 with
  | _, TopType -> ()
  | BottomType, _ -> ()
  | SetType t1, SetType t2
  | ListType t1, ListType t2 -> check_subtype env t1 t2
  | BaseType s1, BaseType s2 when s1 = s2 -> ()
  | TupleType l1, TupleType l2 when List.length l1 = List.length l2 ->
    List.iter2 (check_subtype env) l1 l2
  | MapType (t1k, t1v), MapType (t2k, t2v) ->
    check_eqtype env t1k t2k; check_subtype env t1v t2v
  | Program_point t1, Program_point t2 -> check_subtype env t1 t2
  | BaseType s1, BaseType s2 when SMap.mem s1 env.tcenv_types && SMap.mem s2 env.tcenv_types ->
    check_subtype env (canonize_type env t1) (canonize_type env t2)
  | BaseType s1, _ when SMap.mem s1 env.tcenv_types ->
    check_subtype env (canonize_type env t1) t2
  | _, BaseType s2 when SMap.mem s2 env.tcenv_types ->
    check_subtype env t1 (canonize_type env t2)
  | _ -> failwith "Incompatible types"

exception TypeUnificationError

let rec unify_types env t1 t2 =
  match t1, t2 with
  | TopType, _ | _, TopType -> TopType
  | BottomType, t | t, BottomType -> t
  | SetType t1, SetType t2 -> SetType (unify_types env t1 t2)
  | ListType t1, ListType t2 -> ListType (unify_types env t1 t2)
  | BaseType s1, BaseType s2 when s1 = s2 -> t1
  | TupleType l1, TupleType l2 when List.length l1 = List.length l2 ->
    TupleType (List.map2 (unify_types env) l1 l2)
  | MapType (t1k, t1v), MapType (t2k, t2v) ->
    check_eqtype env t1k t2k; MapType (t1k, unify_types env t1v t2v)
  | Program_point t1, Program_point t2 ->
    (try Program_point (unify_types env t1 t2)
     with TypeUnificationError -> Program_point TopType)
  | BaseType s1, BaseType s2 when SMap.mem s1 env.tcenv_types && SMap.mem s2 env.tcenv_types ->
    unify_types env (canonize_type env t1) (canonize_type env t2)
  | BaseType s1, _ when SMap.mem s1 env.tcenv_types ->
    unify_types env (canonize_type env t1) t2
  | _, BaseType s2 when SMap.mem s2 env.tcenv_types ->
    unify_types env t1 (canonize_type env t2)
  | _ -> failwith "Incompatible types"

let unify_types_list env l = List.fold_left (unify_types env) BottomType l

let tcenv_get_var env s =
  try SMap.find s env.tcenv_vars with Not_found -> failwith ("Variable not found in env: " ^ s)

let tcenv_get_func env s =
  try SMap.find s env.tcenv_funcs with Not_found -> failwith ("Function not found in env: " ^ s)

let tcenv_bind_vars env vars t =
  let t = canonize_type env t in
  match vars with
  | [] ->
    if t <> BaseType "unit" then
      failwith "Type error: unit pattern used with non-unit type";
    env
  | [x] -> { env with tcenv_vars = SMap.add x (t, true) env.tcenv_vars }
  | l ->
    let lt =
      match t with
      | TupleType lt -> lt
      | _ -> failwith "Type error: non-tuple type used with multiple variable binding"
    in
    if List.length l <> List.length lt then failwith "Type error: variable binding with incorrect number of arguments";
    let nvars = List.fold_left2 (fun nv x t -> SMap.add x (t, true) nv) env.tcenv_vars l lt in
    { env with tcenv_vars = nvars }

let type_lambda type_val vt env (vars, v) = (vars, type_val (tcenv_bind_vars env vars vt) v)
let lambda_type (_, v) = v.typ

let iterator_type env t = match canonize_type env t with
  | SetType t | ListType t -> t
  | MapType (t1, t2) -> TupleType [t1; t2]
  | _ -> failwith "Type error: incorrect type for iteration"

let rec type_down_prop_lvalue env t lv = match lv.desc with
  | LNamedVar _ -> { desc = lv.desc ; typ = t }
  | LMapGet (lv1, v1) ->
    { desc = LMapGet (type_down_prop_lvalue env (MapType (v1.typ, t)) lv1, v1) ; typ = t }

let rec type_down_prop_value env t v = match v.desc with
  | NamedVar _ | LocalVar _ | VUnit | Func _ -> { desc = v.desc ; typ = t }
  | VTuple l ->
    (match canonize_type env t with
     | TupleType tl ->
       assert (List.length l = List.length tl);
       { desc = VTuple (List.map2 (type_down_prop_value env) tl l) ; typ = t }
     | _ -> assert false)
  | VSet l ->
    (match canonize_type env t with
     | SetType t1 -> { desc = VSet (List.map (type_down_prop_value env t1) l) ; typ = t }
     | _ -> assert false)
  | VList l ->
    (match canonize_type env t with
     | ListType t1 -> { desc = VList (List.map (type_down_prop_value env t1) l) ; typ = t }
     | _ -> assert false)
  | VWith (c, v) -> { desc = VWith (c, type_down_prop_value env t v) ; typ = t }
  | MapGet (v1, v2) ->
    { desc = MapGet (type_down_prop_value env (MapType (v2.typ, t)) v1, v2) ; typ = t }
  | VLet (v1, (args, body)) ->
    { desc = VLet (v1, (args, type_down_prop_value env t body)) ; typ = t }
  | VJoin (v1, (args, body)) ->
    { desc = VJoin (v1, (args, type_down_prop_value env t body)) ; typ = t }
  | VIsEmpty (v1, body) ->
    { desc = VIsEmpty (v1, type_down_prop_value env t body) ; typ = t }

let make_mapget env v1 v2 =
  let (t1, t2) = match canonize_type env v1.typ with MapType (t1, t2) -> (t1, t2) | _ -> failwith "Type error: map expected" in
  check_subtype env v2.typ t1;
  let v2 = type_down_prop_value env t1 v2 in
  { desc = MapGet (v1, v2) ; typ = t2 }

let make_lmapget env v1 v2 =
  let (t1, t2) = match canonize_type env v1.typ with MapType (t1, t2) -> (t1, t2) | _ -> failwith "Type error: map expected" in
  check_subtype env v2.typ t1;
  let v2 = type_down_prop_value env t1 v2 in
  { desc = LMapGet (v1, v2) ; typ = t2 }

let rec type_lvalue env = function
  | PNamedVar s ->
    let t, is_local = tcenv_get_var env s in
    if is_local then failwith "Local variable used in lvalue position";
    { desc = LNamedVar s ; typ = t }
  | PMapGet (lv, v) ->
    let lv = type_lvalue env lv in
    let v = type_value env v in
    make_lmapget env lv v
  | _ -> failwith "Incorrect term in lvalue position"

and type_value env = function
  | PNamedVar s ->
    let t, is_local = tcenv_get_var env s in
    { desc = if is_local then LocalVar s else NamedVar s ; typ = t }
  | PMapGet (m, v) ->
    let m = type_value env m in
    let v = type_value env v in
    make_mapget env m v
  | PFunc (f, args) ->
    let args = List.map (type_value env) args in
    let args, typ =
      if f = "subterm" then begin
        let arg = match args with [arg] -> arg | _ -> failwith "Incorrect number of arguments" in
        match canonize_type env arg.typ with
        | Program_point t -> args, t
        | _ -> failwith "Incompatible type"
      end else begin
        let q = tcenv_get_func env f in
        if List.length args <> List.length q.q_inputs then failwith "Incorrect number of arguments";
        List.iter2 (fun arg t -> check_subtype env arg.typ t) args q.q_inputs;
        let args = List.map2 (type_down_prop_value env) q.q_inputs args in
        args, q.q_output
      end
    in
    { desc = Func (f, args) ; typ = typ }
  | PVLet (v, body) ->
    let v = type_value env v in
    let body = type_lambda type_value v.typ env body in
    { desc = VLet (v, body) ; typ = lambda_type body }
  | PVWith (c, v) ->
    let c = type_constr env c in
    let v = type_value env v in
    { desc = VWith (c, v) ; typ = v.typ }
  | PVTuple l ->
    let l = List.map (type_value env) l in
    { desc = VTuple l ; typ = TupleType (List.map (fun v -> v.typ) l) }
  | PVSet l ->
    let l = List.map (type_value env) l in
    { desc = VSet l ; typ = SetType (unify_types_list env (List.map (fun x -> x.typ) l)) }
  | PVList l ->
    let l = List.map (type_value env) l in
    { desc = VList l ; typ = ListType (unify_types_list env (List.map (fun x -> x.typ) l)) }
  | PVJoin (v, body) ->
    let v = type_value env v in
    let body = type_lambda type_value (iterator_type env v.typ) env body in
    { desc = VJoin (v, body) ; typ = lambda_type body }
  | PVUnit -> { desc = VUnit ; typ = BaseType "unit" }

and type_constr env = function
  | PTrue -> True
  | PConj (c1, c2) -> Conj (type_constr env c1, type_constr env c2)
  | PCLet (v, body) ->
    let v = type_value env v in
    let body = type_lambda type_constr v.typ env body in
    CLet (v, body)
  | PSub (v1, v2) ->
    let v1 = type_value env v1 in
    let v2 = type_lvalue env v2 in
    check_eqtype env v1.typ v2.typ;
    Sub (v1, v2)
  | PForall (v, body) ->
    let v = type_value env v in
    Forall (v, type_lambda type_constr (iterator_type env v.typ) env body)


let var_appears_lambda var_appears v (args, body) =
  if List.mem v args then
    0
  else
    var_appears v body

let rec var_appears_lvalue var lv = match lv.desc with
  | LNamedVar _ -> 0
  | LMapGet (lv, v) -> var_appears_lvalue var lv + var_appears_value var v

and var_appears_value var v = match v.desc with
  | NamedVar _ | VUnit -> 0
  | MapGet (v1, v2) -> var_appears_value var v1 + var_appears_value var v2
  | LocalVar var1 -> if var = var1 then 1 else 0
  | VTuple l | VSet l | VList l | Func (_, l) -> List.fold_left (+) 0 (List.map (var_appears_value var) l)
  | VWith (c, v) -> var_appears_constr var c + var_appears_value var v
  | VLet (v1, body) | VJoin (v1, body) -> var_appears_value var v1 + var_appears_lambda var_appears_value var body
  | VIsEmpty (v1, body) -> var_appears_value var v1 + var_appears_value var body

and var_appears_constr var c = match c with
  | True -> 0
  | Conj (c1, c2) -> var_appears_constr var c1 + var_appears_constr var c2
  | Sub (v1, v2) -> var_appears_value var v1 + var_appears_lvalue var v2
  | CLet (v1, body) | Forall (v1, body) -> var_appears_value var v1 + var_appears_lambda var_appears_constr var body
  | CEmpty (v1, body) -> var_appears_value var v1 + var_appears_constr var body

let rec var_appears_dep var d = match d with
  | DBase l -> List.fold_left (+) 0 (List.map (fun (_, v) -> match v with Some v -> var_appears_value var v | _ -> 0) l)
  | DConj (d1, d2) -> var_appears_dep var d1 + var_appears_dep var d2
  | DLet (v1, body) | DForall (v1, body) -> var_appears_value var v1 + var_appears_lambda var_appears_dep var body
  | DEmpty (v1, body) -> var_appears_value var v1 + var_appears_dep var body

let over_approx_dep_empty = true

module SSet = Set.Make(String)
let fv_lambda fv (args, body) =
  List.fold_left (fun s v -> SSet.remove v s) (fv body) args

let rec fv_lvalue lv = match lv.desc with
  | LNamedVar _ -> SSet.empty
  | LMapGet (lv, v) -> SSet.union (fv_lvalue lv) (fv_value v)

and fv_value v = match v.desc with
  | NamedVar _ | VUnit -> SSet.empty
  | MapGet (v1, v2) -> SSet.union (fv_value v1) (fv_value v2)
  | LocalVar var1 -> SSet.singleton var1
  | VTuple l | VSet l | VList l | Func (_, l) ->
    List.fold_left SSet.union SSet.empty (List.map fv_value l)
  | VWith (c, v) ->
    SSet.union (fv_constr c) (fv_value v)
  | VLet (v1, body) | VJoin (v1, body) ->
    SSet.union (fv_value v1) (fv_lambda fv_value body)
  | VIsEmpty (v1, body) -> SSet.union (fv_value v1) (fv_value body)

and fv_constr c = match c with
  | True -> SSet.empty
  | Conj (c1, c2) -> SSet.union (fv_constr c1) (fv_constr c2)
  | Sub (v1, v2) -> SSet.union (fv_value v1) (fv_lvalue v2)
  | CLet (v1, body) | Forall (v1, body) -> SSet.union (fv_value v1) (fv_lambda fv_constr body)
  | CEmpty (v1, body) -> SSet.union (fv_value v1) (fv_constr body)

let rec fv_dep d = match d with
  | DBase l ->
    List.fold_left SSet.union SSet.empty
      (List.map (fun (_, v) -> match v with Some v -> fv_value v | None -> SSet.empty) l)
  | DConj (d1, d2) -> SSet.union (fv_dep d1) (fv_dep d2)
  | DLet (v1, body) | DForall (v1, body) ->
    SSet.union (fv_value v1) (fv_lambda fv_dep body)
  | DEmpty (v1, body) -> SSet.union (fv_value v1) (fv_dep body)


let lift_with_lambda liftf (args, v) =
  let v, c = liftf v in
  ((args, v), (List.map (fun c1 -> (args, c1)) c))

let rec lift_with_shallow_value v = match v.desc with
  | NamedVar _ | LocalVar _ | VUnit -> (v, [])
  | MapGet (v1, v2) ->
    let v1, c1 = lift_with_shallow_value v1 in
    let v2, c2 = lift_with_shallow_value v2 in
    ({ desc = MapGet (v1, v2) ; typ = v.typ }, c1 @ c2)
  | VWith (c, v) ->
    let v, c1 = lift_with_shallow_value v in
    (v, c :: c1)
  | VTuple l ->
    let l, c = lift_with_shallow_value_list l in
    ({ desc = VTuple l ; typ = v.typ }, c)
  | VSet l ->
    let l, c = lift_with_shallow_value_list l in
    ({ desc = VSet l ; typ = v.typ }, c)
  | VList l ->
    let l, c = lift_with_shallow_value_list l in
    ({ desc = VList l ; typ = v.typ }, c)
  | Func (f, l) ->
    let l, c = lift_with_shallow_value_list l in
    ({ desc = Func (f, l) ; typ = v.typ }, c)
  | VLet (v1, body) ->
    let v1, c1 = lift_with_shallow_value v1 in
    let body, c2 = lift_with_lambda lift_with_shallow_value body in
    ({ desc = VLet (v1, body) ; typ = v.typ }, c1 @ (List.map (fun c -> CLet (v1, c)) c2))
  | VJoin (v1, body) ->
    let v1, c1 = lift_with_shallow_value v1 in
    let body, c2 = lift_with_lambda lift_with_shallow_value body in
    ({ desc = VJoin (v1, body) ; typ = v.typ }, c1 @ (List.map (fun c -> Forall (v1, c)) c2))
  | VIsEmpty (v1, body) ->
    let v1, c1 = lift_with_shallow_value v1 in
    let body, c2 = lift_with_shallow_value body in
    ({ desc = VIsEmpty (v1, body) ; typ = v.typ}, c1 @ (List.map (fun c -> CEmpty (v1, c)) c1))

and lift_with_shallow_value_list l =
  let l = List.map lift_with_shallow_value l in
  (List.map fst l, List.flatten (List.map snd l))


let rec filter_with_value v = match v.desc with
  | NamedVar _ | LocalVar _ | VUnit -> v
  | MapGet (v1, v2) ->
    let v1 = filter_with_value v1 in
    let v2 = filter_with_value v2 in
    { desc = MapGet (v1, v2) ; typ = v.typ }
  | VTuple l ->
    { desc = VTuple (List.map filter_with_value l) ; typ = v.typ }
  | VSet l ->
    { desc = VSet (List.map filter_with_value l) ; typ = v.typ }
  | VList l ->
    { desc = VList (List.map filter_with_value l) ; typ = v.typ }
  | Func (f, l) ->
    { desc = Func (f, List.map filter_with_value l) ; typ = v.typ }
  | VWith (_, v) -> filter_with_value v
  | VLet (v1, (args, body)) ->
    { desc = VLet (filter_with_value v1, (args, filter_with_value body)) ; typ = v.typ }
  | VJoin (v1, (args, body)) ->
    { desc = VJoin (filter_with_value v1, (args, filter_with_value body)) ; typ = v.typ }
  | VIsEmpty (v1, body) ->
    { desc = VIsEmpty (filter_with_value v1, filter_with_value body) ; typ = v.typ }

let make_vwith_list v c =
  match c with
  | [] -> v
  | c1 :: c -> { desc = VWith (List.fold_left (fun c1 c2 -> Conj (c1, c2)) c1 c, v) ; typ = v.typ }


let find_var_name fv n =
  (* let rec find i = let n = n ^ "_" ^ string_of_int i in if SSet.mem n fv then find (i + 1) else n in
     find 1 *)
  fresh n

let subst_lambda sf subst fv (args, body) =
  let nargs, nsubst, nfv = List.fold_left (fun (nargs, subst, fv) arg ->
      if SSet.mem arg fv then
        let narg = find_var_name fv arg in
        (narg :: nargs, SMap.add arg (LocalVar narg) subst, SSet.add narg fv)
      else if SMap.mem arg subst then
        (arg :: nargs, SMap.remove arg subst, fv)
      else
        (arg :: nargs, subst, fv)
    ) ([], subst, fv) (List.rev args)
  in
  (nargs, sf nsubst nfv body)

let alpha_convert sf fv (args, body) =
  subst_lambda sf SMap.empty fv (args, body)

let rec make_vlet v (args, body) =
  match v.desc with
  | VTuple l when List.length args > 1 ->
    assert (List.length l = List.length args);
    let args = List.map (fun x -> (x, fresh x)) args in
    let body =
      List.fold_left2 (fun body (arg, narg) av ->
          make_vlet ({ desc = LocalVar narg ; typ = av.typ }) ([arg], body)
        ) body args l
    in
    let body =
      List.fold_left2 (fun body (arg, narg) av ->
          make_vlet av ([narg], body)) body args l
    in
    body
  | LocalVar v1 when List.length args = 1 ->
    let arg = List.hd args in
    subst_value (SMap.singleton arg (LocalVar v1)) (SSet.singleton v1) body
  | _ ->
    if List.length args = 1 && var_appears_value (List.hd args) body = 1 then
      let v, c = lift_with_shallow_value v in
      make_vwith_list (subst_value (SMap.singleton (List.hd args) v.desc) (fv_value v) body) c
    else if List.for_all (fun var -> var_appears_value var body = 0) args then
      let _, c = lift_with_shallow_value v in
      make_vwith_list body c
    else
      { desc = VLet (v, (args, body)) ; typ = body.typ }

and make_clet v (args, body) =
  match v.desc with
  | VTuple l when List.length args > 1 ->
    assert (List.length l = List.length args);
    let args = List.map (fun x -> (x, fresh x)) args in
    let body =
      List.fold_left2 (fun body (arg, narg) av ->
          make_clet ({ desc = LocalVar narg ; typ = av.typ }) ([arg], body)
        ) body args l
    in
    let body =
      List.fold_left2 (fun body (arg, narg) av ->
          make_clet av ([narg], body)) body args l
    in
    body
  | LocalVar v1 when List.length args = 1 ->
    let arg = List.hd args in
    subst_constr (SMap.singleton arg (LocalVar v1)) (SSet.singleton v1) body
  | _ ->
    if List.length args = 1 && var_appears_constr (List.hd args) body = 1 then
      let v, c = lift_with_shallow_value v in
      List.fold_left (fun c1 c2 -> Conj (c1, c2))
        (subst_constr (SMap.singleton (List.hd args) v.desc) (fv_value v) body) c
    else if List.for_all (fun var -> var_appears_constr var body = 0) args then
      let _, c = lift_with_shallow_value v in
      List.fold_left (fun c1 c2 -> Conj (c1, c2)) body c
    else
      CLet (v, (args, body))


and subst_value subst fv v = match v.desc with
  | NamedVar _ | VUnit -> v
  | LocalVar name ->
    { desc = (try SMap.find name subst with Not_found -> v.desc) ; typ = v.typ }
  | MapGet (v1, v2) -> { desc = MapGet (subst_value subst fv v1, subst_value subst fv v2) ; typ = v.typ }
  | VWith (c, v1) -> { desc = VWith (subst_constr subst fv c, subst_value subst fv v1) ; typ = v.typ }
  | VTuple l -> { desc = VTuple (List.map (subst_value subst fv) l) ; typ = v.typ }
  | VSet l -> { desc = VSet (List.map (subst_value subst fv) l) ; typ = v.typ }
  | VList l -> { desc = VList (List.map (subst_value subst fv) l) ; typ = v.typ }
  | Func (f, l) -> { desc = Func (f, List.map (subst_value subst fv) l) ; typ = v.typ }
  | VLet (v1, body) ->
    make_vlet (subst_value subst fv v1) (subst_lambda subst_value subst fv body)
  | VJoin (v1, body) ->
    make_join (subst_value subst fv v1) (subst_lambda subst_value subst fv body)
  | VIsEmpty (v1, body) ->
    make_visempty (subst_value subst fv v1) (subst_value subst fv body)

and subst_constr subst fv c = match c with
  | True -> True
  | Conj (c1, c2) -> Conj (subst_constr subst fv c1, subst_constr subst fv c2)
  | CLet (v1, body) -> CLet (v1, subst_lambda subst_constr subst fv body)
  | Forall (v1, body) -> Forall (v1, subst_lambda subst_constr subst fv body)
  | Sub (v1, v2) -> Sub (subst_value subst fv v1, subst_lvalue subst fv v2)
  | CEmpty (v1, body) -> CEmpty (subst_value subst fv v1, subst_constr subst fv body)

and subst_lvalue subst fv lv = match lv.desc with
  | LNamedVar _ -> lv
  | LMapGet (lv1, v2) -> { desc = LMapGet (subst_lvalue subst fv lv1, subst_value subst fv v2) ; typ = lv.typ }

and lift_with_lvalue lv = match lv.desc with
  | LNamedVar _ -> (lv, [])
  | LMapGet (lv1, v1) ->
    let lv1, c1 = lift_with_lvalue lv1 in
    let v1, c2 = lift_with_value v1 in
    ({ desc = LMapGet (lv1, v1) ; typ = lv.typ }, c1 @ c2)

and lift_with_value v = match v.desc with
  | NamedVar _ | LocalVar _ | VUnit -> (v, [])
  | MapGet (v1, v2) ->
    let v1, c1 = lift_with_value v1 in
    let v2, c2 = lift_with_value v2 in
    ({ desc = MapGet (v1, v2) ; typ = v.typ }, c1 @ c2)
  | VWith (c, v) ->
    let v, c1 = lift_with_value v in
    (v, lift_with_constr c @ c1)
  | VTuple l ->
    let l, c = lift_with_value_list l in
    ({ desc = VTuple l ; typ = v.typ }, c)
  | VSet l ->
    let l, c = lift_with_value_list l in
    ({ desc = VSet l ; typ = v.typ }, c)
  | VList l ->
    let l, c = lift_with_value_list l in
    ({ desc = VList l ; typ = v.typ }, c)
  | Func (f, l) ->
    let l, c = lift_with_value_list l in
    ({ desc = Func (f, l) ; typ = v.typ }, c)
  | VLet (v1, body) ->
    let v1, c1 = lift_with_value v1 in
    let body, c2 = lift_with_lambda lift_with_value body in
    (make_vlet v1 body, c1 @ (List.map (fun c -> make_clet v1 c) c2))
  | VJoin (v1, body) ->
    let v1, c1 = lift_with_value v1 in
    let body, c2 = lift_with_lambda lift_with_value body in
    (make_join v1 body, c1 @ (List.map (fun c -> Forall (v1, c)) c2))
  | VIsEmpty (v1, body) ->
    let v1, c1 = lift_with_value v1 in
    let body, c2 = lift_with_value body in
    (make_visempty v1 body, c1 @ (List.map (fun c -> CEmpty (v1, c)) c2))

and lift_with_value_list l =
  let l = List.map lift_with_value l in
  (List.map fst l, List.flatten (List.map snd l))

and lift_with_constr c = match c with
  | True -> []
  | Conj (c1, c2) ->
    let cl1 = lift_with_constr c1 in
    let cl2 = lift_with_constr c2 in
    cl1 @ cl2
  | Sub (v1, v2) ->
    let v1, cl1 = lift_with_value v1 in
    let v2, cl2 = lift_with_lvalue v2 in
    Sub (v1, v2) :: cl1 @ cl2
  | Forall (v, (args, c2)) ->
    let v, cl1 = lift_with_value v in
    let cl2 = lift_with_constr c2 in
    cl1 @ List.map (fun c -> Forall (v, (args, c))) cl2
  | CLet (v, (args, c2)) ->
    let v, cl1 = lift_with_value v in
    let cl2 = lift_with_constr c2 in
    cl1 @ List.map (fun c -> make_clet v (args, c)) cl2
  | CEmpty (v, body) ->
    let v, cl1 = lift_with_value v in
    let cl2 = lift_with_constr body in
    cl1 @ List.map (fun c -> CEmpty (v, body)) cl2

and make_with c v = { desc = VWith (c, v) ; typ = v.typ }

and make_tuple_type = function
  | [] -> BaseType "unit"
  | [t] -> t
  | l -> TupleType l

and make_tuple l =
  { desc =
      (match l with
       | [] -> VUnit
       | [x] -> x.desc
       | l -> VTuple l) ;
    typ = make_tuple_type (List.map (fun x -> x.typ) l)
  }

and never_empty v = match v.desc with
  | LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> false
  | VWith (_, v) -> never_empty v
  | VLet (_, (_, body)) -> never_empty body
  | VJoin (v1, (_, body)) | VIsEmpty (v1, body) -> never_empty v1 && never_empty body
  | VSet l | VList l -> l <> []

and not_empty_cond v = match v.desc with
  | LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> v
  | VWith (c, v1) -> make_with c (not_empty_cond v1)
  | VLet (v1, (args, body)) -> make_vlet v1 (args, not_empty_cond body)
  | VJoin (v1, (args, body)) ->
    if never_empty body then not_empty_cond v1 else v
  | VIsEmpty (v1, body) ->
    if never_empty body then not_empty_cond v1 else v
  | VSet l | VList l -> v

and always_empty v = match v.desc with
  | LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> false
  | VWith (_, v) -> always_empty v
  | VLet (_, (_, body)) -> always_empty body
  | VJoin (v1, (_, body)) | VIsEmpty (v1, body) -> always_empty v1 || always_empty body
  | VSet l | VList l -> l = []

and try_select_fields_in is_set fields v = match v.desc with
  | LocalVar _ | NamedVar _ | VUnit | VTuple _ | Func _ | MapGet _ -> None
  | VWith (c, v) ->
    (match try_select_fields_in is_set fields v with
     | None -> None
     | Some v -> Some (make_with c v))
  | VLet (v1, (args, body)) ->
    (match try_select_fields_in is_set fields body with
     | None -> None
     | Some body -> Some (make_vlet v1 (args, body)))
  | VJoin (v1, (args, body)) ->
    (match try_select_fields_in is_set fields body with
     | None -> None
     | Some body -> Some (make_join_gen is_set v1 (args, body)))
  | VIsEmpty (v1, body) ->
    (match try_select_fields_in is_set fields body with
     | None -> None
     | Some body -> Some (make_visempty v1 body)
    )
  | VSet l ->
    if not is_set then None else begin
      let l = List.map (try_select_fields fields) l in
      if List.for_all (fun x -> x <> None) l then
        let l = List.map (function None -> assert false | Some x -> x) l in
        Some { desc = VSet l ; typ = SetType (match l with [] -> BottomType | x :: _ -> x.typ) }
      else
        None
    end
  | VList l ->
    let l = List.map (try_select_fields fields) l in
    if List.for_all (fun x -> x <> None) l then
      let l = List.map (function None -> assert false | Some x -> x) l in
      Some { desc = VList l ; typ = ListType (match l with [] -> BottomType | x :: _ -> x.typ) }
    else
      None

and try_select_fields fields v = match v.desc with
  | LocalVar _ | NamedVar _ | VUnit | Func _ | MapGet _ | VJoin _ | VSet _ | VList _ | VIsEmpty _ -> None
  | VWith (c, v) ->
    (match try_select_fields fields v with
     | None -> None
     | Some v -> Some (make_with c v))
  | VLet (v1, (args, body)) ->
    (match try_select_fields fields body with
     | None -> None
     | Some body -> Some (make_vlet v1 (args, body)))
  | VTuple l ->
    let _, _, nl, cs = List.fold_left (fun (i, fds, nl, cs) vi ->
        match fds with
        | j :: fds when i = j -> (i + 1, fds, vi :: nl, cs)
        | _ -> (i + 1, fds, nl, snd (lift_with_shallow_value vi) @ cs)
      ) (0, fields, [], []) l
    in
    let nl = List.rev nl in
    Some (make_vwith_list (make_tuple nl) cs)

and make_join1 is_set v1 (args, v2) =
  match v1.desc with
  | VList [v] -> make_vlet v (args, v2)
  | VSet [v] when is_set -> make_vlet v (args, v2)
  | VList [] | VSet [] ->
    { desc = (match v2.typ with ListType _ -> VList [] | SetType _ -> VSet [] | _ -> VJoin (v1, (args, v2))) ;
      typ = v2.typ }
  | _ -> { desc = VJoin (v1, (args, v2)) ; typ = v2.typ }

and make_join_gen is_set v1 (args, v2) =
  let ar = List.mapi (fun i x -> (i, x)) args in
  let fields1 = List.filter (fun (_, x) -> var_appears_value x v2 > 0) ar in
  if fields1 = [] then
    make_visempty v1 v2
  else if List.length args = 1 then
    make_join1 is_set v1 (args, v2)
  else
    match try_select_fields_in is_set (List.map fst fields1) v1 with
    | None -> make_join1 is_set v1 (args, v2)
    | Some v1 -> make_join1 is_set v1 (List.map snd fields1, v2)

and make_join v1 v2 = make_join_gen false v1 v2

and make_visempty v1 v2 =
  if never_empty v1 then v2 else { desc = VIsEmpty (not_empty_cond v1, v2) ; typ = v2.typ }

let make_list l =
  assert (l <> []);
  { desc = VList l ; typ = ListType (List.hd l).typ }

let make_dconj x y =
  match x, y with
  | DBase l1, DBase l2 -> DBase (l1 @ l2)
  | DBase [], _ -> y
  | _, DBase [] -> x
  | _ -> DConj (x, y)

let rec make_dlet v (args, d) =
  match v.desc with
  | VTuple l when List.length args > 1 ->
    assert (List.length l = List.length args);
    let args = List.map (fun x -> (x, fresh x)) args in
    let d =
      List.fold_left2 (fun d (arg, narg) av ->
          make_dlet ({ desc = LocalVar narg ; typ = av.typ }) ([arg], d)
        ) d args l
    in
    let d =
      List.fold_left2 (fun d (arg, narg) av ->
          make_dlet av ([narg], d)) d args l
    in
    d
  | _ ->
    if List.length args = 1 && var_appears_dep (List.hd args) d = 1 then
      subst_dep (SMap.singleton (List.hd args) v.desc) (fv_value v) d
    else if List.for_all (fun var -> var_appears_dep var d = 0) args then
      d
    else
      DLet (v, (args, d))

and subst_dep subst fv d = match d with
  | DBase l -> DBase (List.map (fun (name, v) -> match v with None -> (name, v) | Some v -> (name, Some (subst_value subst fv v))) l)
  | DConj (d1, d2) -> DConj (subst_dep subst fv d1, subst_dep subst fv d2)
  | DLet (v, body) -> make_dlet (subst_value subst fv v) (subst_lambda subst_dep subst fv body)
  | DForall (v, body) -> make_dforall (subst_value subst fv v) (subst_lambda subst_dep subst fv body)
  | DEmpty (v, body) -> make_dempty (subst_value subst fv v) (subst_dep subst fv body)

and make_dforall1 v1 (args, d) =
  match v1.desc with
  | VList [v] | VSet [v] -> make_dlet v (args, d)
  | VList [] | VSet [] -> DBase []
  | VJoin (v2, (args2, body)) ->
    let args2, body = alpha_convert subst_value (fv_dep d) (args2, body) in
    make_dforall v2 (args2, make_dforall body (args, d))
  | _ -> DForall (v1, (args, d))

and make_dforall v (args, d) =
  if d = DBase [] then DBase [] else begin
    let ar = List.mapi (fun i x -> (i, x)) args in
    let fields1 = List.filter (fun (_, x) -> var_appears_dep x d > 0) ar in
    if fields1 = [] then
      make_dempty v d
    else if List.length args = 1 then
      make_dforall1 v (args, d)
    else
      match try_select_fields_in true (List.map fst fields1) v with
      | None -> make_dforall1 v (args, d)
      | Some v1 -> make_dforall1 v1 (List.map snd fields1, d)
  end

and make_dempty v d =
  if never_empty v then
    d
  else if always_empty v then
    DBase []
  else if over_approx_dep_empty then
    d
  else match v.desc with
    | VJoin (v2, (args2, body)) ->
      let args2, body = alpha_convert subst_value (fv_dep d) (args2, body) in
      make_dforall v2 (args2, make_dempty body d)
    | _ -> DEmpty (not_empty_cond v, d)

let rec deps_lvalue_in lv = match lv.desc with
  | LNamedVar x -> DBase []
  | LMapGet (lv, v) ->
    make_dconj (deps_lvalue_in lv) (deps_value_in v)

and deps_value_in v = match v.desc with
  | NamedVar x -> DBase [(x, None)]
  | MapGet ({ desc = NamedVar x }, v) ->
    make_dconj (deps_value_in v) (DBase [(x, Some v)])
  | MapGet (v1, v2) ->
    make_dconj (deps_value_in v1) (deps_value_in v2)
  | VUnit -> DBase []
  | LocalVar _ -> DBase []
  | VSet l | VList l | Func (_, l) | VTuple l ->
    List.fold_left (fun acc v -> make_dconj acc (deps_value_in v)) (DBase []) l
  | VWith (c, v) ->
    make_dconj (deps_constr_in c) (deps_value_in v)
  | VLet (v, (args, body)) ->
    let d = deps_value_in v in
    make_dconj d (make_dlet (filter_with_value v) (args, deps_value_in body))
  | VJoin (v, (args, body)) ->
    let d = deps_value_in v in
    make_dconj d (make_dforall (filter_with_value v) (args, deps_value_in body))
  | VIsEmpty (v, body) ->
    let d = deps_value_in v in
    make_dconj d (make_dempty (filter_with_value v) (deps_value_in body))

and deps_constr_in c = match c with
  | True -> DBase []
  | Conj (c1, c2) -> make_dconj (deps_constr_in c1) (deps_constr_in c2)
  | Sub (v1, v2) ->
    let d1 = deps_value_in v1 in
    let d2 = deps_lvalue_in v2 in
    make_dconj d1 d2
  | Forall (v, (args, c2)) ->
    let d = deps_value_in v in
    make_dconj d (make_dforall (filter_with_value v) (args, deps_constr_in c2))
  | CLet (v, (args, c2)) ->
    let d = deps_value_in v in
    make_dconj d (make_dlet (filter_with_value v) (args, deps_constr_in c2))
  | CEmpty (v, c2) ->
    let d = deps_value_in v in
    make_dconj d (make_dempty (filter_with_value v) (deps_constr_in c2))


let rec deps_lvalue_out lv = match lv.desc with
  | LNamedVar x -> DBase [(x, None)]
  | LMapGet ({ desc = LNamedVar x }, v) -> make_dconj (DBase [(x, Some v)]) (deps_value_out v)
  | LMapGet (lv, v) ->
    make_dconj (deps_lvalue_out lv) (deps_value_out v)

and deps_value_out v = match v.desc with
  | NamedVar x -> DBase []
  | MapGet (v1, v2) -> make_dconj (deps_value_out v1) (deps_value_out v2)
  | VUnit -> DBase []
  | LocalVar _ -> DBase []
  | VSet l | VList l | Func (_, l) | VTuple l ->
    List.fold_left (fun acc v -> make_dconj acc (deps_value_out v)) (DBase []) l
  | VWith (c, v) ->
    make_dconj (deps_constr_out c) (deps_value_out v)
  | VLet (v, (args, body)) ->
    let d = deps_value_out v in
    make_dconj d (make_dlet (filter_with_value v) (args, deps_value_out body))
  | VJoin (v, (args, body)) ->
    let d = deps_value_out v in
    make_dconj d (make_dforall (filter_with_value v) (args, deps_value_out body))
  | VIsEmpty (v, body) ->
    let d = deps_value_out v in
    make_dconj d (make_dempty (filter_with_value v) (deps_value_out body))

and deps_constr_out c = match c with
  | True -> DBase []
  | Conj (c1, c2) -> make_dconj (deps_constr_out c1) (deps_constr_out c2)
  | Sub (v1, v2) ->
    let d1 = deps_value_out v1 in
    let d2 = deps_lvalue_out v2 in
    make_dconj d1 d2
  | Forall (v, (args, c2)) ->
    let d = deps_value_out v in
    make_dconj d (make_dforall (filter_with_value v) (args, deps_constr_out c2))
  | CLet (v, (args, c2)) ->
    let d = deps_value_out v in
    make_dconj d (make_dlet (filter_with_value v) (args, deps_constr_out c2))
  | CEmpty (v, body) ->
    let d = deps_value_out v in
    make_dconj d (make_dempty (filter_with_value v) (deps_constr_out body))

let rec deps_deps d = match d with
  | DConj (d1, d2) -> make_dconj (deps_deps d1) (deps_deps d2)
  | DLet (v, (args, body)) ->
    make_dconj (deps_value_in v) (make_dlet (filter_with_value v) (args, deps_deps body))
  | DForall (v, (args, body)) ->
    make_dconj (deps_value_in v) (make_dforall (filter_with_value v) (args, deps_deps body))
  | DEmpty (v, body) ->
    make_dconj (deps_value_in v) (make_dempty (filter_with_value v) (deps_deps body))
  | DBase l ->
    List.fold_left make_dconj (DBase []) (List.map (function _, None -> DBase [] | _, Some v -> deps_value_in v) l)

let make_type tenv s =
  match s with
  | Program s -> SetType (Program_point (BaseType s))
  | Flow s -> if is_base_type tenv s then SetType (Program_point (BaseType s)) else BaseType s

let make_filter_type tenv f =
  let f = SMap.find f tenv.env_filter_signatures in
  let it = TupleType (Program_point TopType :: BaseType "globalcontext" :: BaseType "localcontext" ::
                      List.map (make_type tenv) f.fs_input_sorts)
  in
  let ot = ListType (make_tuple_type (BaseType "localcontext" :: List.map (make_type tenv) f.fs_output_sorts)) in
  (it, ot)

let make_hook_intype tenv env hname =
  let h = SMap.find hname tenv.env_hook_definitions in
  let it = TupleType (
      Program_point TopType :: BaseType "globalcontext" :: BaseType "localcontext" ::
      (List.map (make_type tenv) h.hd_input_sorts) @ [Program_point (BaseType h.hd_term_sort)])
  in
  let ot = ListType (TupleType [BaseType "globalcontext"; fst (SMap.find hname env.tcenv_rulevars)]) in
  (it, ot)

let make_hook_outtype tenv env hname =
  let h = SMap.find hname tenv.env_hook_definitions in
  let it = TupleType [
      Program_point TopType; BaseType "globalcontext"; BaseType "localcontext";
      snd (SMap.find hname env.tcenv_rulevars)]
  in
  let ot = ListType (TupleType (BaseType "localcontext" :: List.map (make_type tenv) h.hd_output_sorts)) in
  (it, ot)

let make_rule_intype tenv env hname =
  let h = SMap.find hname tenv.env_hook_definitions in
  let it = TupleType [
      Program_point (BaseType h.hd_term_sort); BaseType "globalcontext";
      fst (SMap.find hname env.tcenv_rulevars)]
  in
  let ot = ListType (TupleType (BaseType "localcontext" :: List.map (make_type tenv) h.hd_input_sorts)) in
  (it, ot)

let make_rule_outtype tenv env hname =
  let h = SMap.find hname tenv.env_hook_definitions in
  let it = TupleType (
      Program_point (BaseType h.hd_term_sort) :: BaseType "globalcontext" :: BaseType "localcontext" ::
      List.map (make_type tenv) h.hd_output_sorts)
  in
  let ot = snd (SMap.find hname env.tcenv_rulevars) in
  (it, ot)

let type_fc tenv env fc = {
  fc_name = fc.pfc_name ;
  fc_value =
    let it, ot = make_filter_type tenv fc.pfc_name in
    let v = type_lambda type_value it env fc.pfc_value in
    check_subtype env (lambda_type v) ot;
    let (args, body) = v in
    let v = (args, type_down_prop_value env ot body) in
    v
}

let type_hi tenv env hi = {
  hi_name = hi.phi_name ;
  hi_annot = hi.phi_annot ;
  hi_value =
    let it, ot = make_hook_intype tenv env hi.phi_name in
    let v = type_lambda type_value it env hi.phi_value in
    check_subtype env (lambda_type v) ot;
    let (args, body) = v in
    let v = (args, type_down_prop_value env ot body) in
    v
}

let type_ho tenv env ho = {
  ho_name = ho.pho_name ;
  ho_annot = ho.pho_annot ;
  ho_value =
    let it, ot = make_hook_outtype tenv env ho.pho_name in
    let v = type_lambda type_value it env ho.pho_value in
    check_subtype env (lambda_type v) ot;
    let (args, body) = v in
    let v = (args, type_down_prop_value env ot body) in
    v
}

let type_ri tenv env ri = {
  ri_name = ri.pri_name ;
  ri_value =
    let it, ot = make_rule_intype tenv env ri.pri_name in
    let v = type_lambda type_value it env ri.pri_value in
    check_subtype env (lambda_type v) ot;
    let (args, body) = v in
    let v = (args, type_down_prop_value env ot body) in
    v
}

let type_ro tenv env ro = {
  ro_name = ro.pro_name ;
  ro_value =
    let it, ot = make_rule_outtype tenv env ro.pro_name in
    let v = type_lambda type_value it env ro.pro_value in
    check_subtype env (lambda_type v) ot;
    let (args, body) = v in
    let v = (args, type_down_prop_value env ot body) in
    v
}


let make_tcenv decls =
  let rulevars = SMap.merge (fun _ v1 v2 -> match v1, v2 with Some v1, Some v2 -> Some (v1, v2) | _ -> assert false)
      (List.fold_left (fun m (name, t) -> SMap.add name t m) SMap.empty decls.pc_rulein_vars)
      (List.fold_left (fun m (name, t) -> SMap.add name t m) SMap.empty decls.pc_ruleout_vars)
  in
  {
    tcenv_types = List.fold_left (fun m (name, t) -> SMap.add name t m) SMap.empty decls.pc_types ;
    tcenv_vars = List.fold_left (fun m v -> SMap.add v.vc_name (v.vc_type, false) m) SMap.empty decls.pc_vars ;
    tcenv_rulevars = rulevars ;
    tcenv_funcs = List.fold_left (fun m q -> SMap.add q.q_name q m) SMap.empty decls.pc_quarks ;
  }

let make_cenv tenv tcenv decls = {
  ce_tce = tcenv ;
  ce_tenv = tenv ;
  ce_filts = List.fold_left (fun m f -> SMap.add f.pfc_name (type_fc tenv tcenv f) m) SMap.empty decls.pc_filters ;
  ce_hookins = List.fold_left (fun m h -> SMap.add (h.phi_name ^ "^" ^ h.phi_annot) (type_hi tenv tcenv h) m) SMap.empty decls.pc_hookins ;
  ce_hookouts = List.fold_left (fun m h -> SMap.add (h.pho_name ^ "^" ^ h.pho_annot) (type_ho tenv tcenv h) m) SMap.empty decls.pc_hookouts ;
  ce_ruleins = List.fold_left (fun m r -> SMap.add r.pri_name (type_ri tenv tcenv r) m) SMap.empty decls.pc_ruleins ;
  ce_ruleouts = List.fold_left (fun m r -> SMap.add r.pro_name (type_ro tenv tcenv r) m) SMap.empty decls.pc_ruleouts ;
}

let globalcontext = BaseType "globalcontext"
let localcontext = BaseType "localcontext"
let pp_var_name = "_pp"
let pp_var = { desc = LocalVar pp_var_name ; typ = Program_point TopType }
let sigmas_var = { desc = LocalVar "_sigmas" ; typ = globalcontext }
let sigma_var_name = "_sigma"
let sigma_var = { desc = LocalVar sigma_var_name ; typ = localcontext }

let hook_in_var hook_name ptyp typ = {
  desc = NamedVar ("_in_" ^ hook_name) ;
  typ = MapType (TupleType [ptyp; globalcontext], typ) ;
}
let hook_out_var hook_name ptyp typ = {
  desc = NamedVar ("_out_" ^ hook_name) ;
  typ = MapType (TupleType [ptyp; globalcontext], typ) ;
}
let hook_in_lvar hook_name ptyp typ = {
  desc = LNamedVar ("_in_" ^ hook_name) ;
  typ = MapType (TupleType [ptyp; globalcontext], typ) ;
}
let hook_out_lvar hook_name ptyp typ = {
  desc = LNamedVar ("_out_" ^ hook_name) ;
  typ = MapType (TupleType [ptyp; globalcontext], typ) ;
}

let mkvar v typ = { desc = LocalVar v ; typ = typ }
let make_var cenv tenv v =
  mkvar v (make_type cenv.ce_tenv (VMap.find v tenv))

let cstr_cfa_interp_base cenv t0 = {
  empty_interp = (fun (tenv, c) -> c) ;
  hook_interp = (fun h (tenv, c) ->
      let ix = h.h_name ^ "^" ^ h.h_annot in
      let hic = SMap.find ix cenv.ce_hookins in
      let hoc = SMap.find ix cenv.ce_hookouts in
      let hdef = SMap.find h.h_name cenv.ce_tenv.env_hook_definitions in
      let itype, otype = SMap.find h.h_name cenv.ce_tce.tcenv_rulevars in
      let ttype = Program_point (BaseType hdef.hd_term_sort) in
      let pp1_var = mkvar "_pp1" ttype in
      let sigmas1_var = mkvar "_sigmas1" globalcontext in
      let in1_var = mkvar "_in1" itype in
      let ein = make_vlet
          (make_tuple (pp_var :: sigmas_var :: sigma_var :: List.map (make_var cenv tenv) h.h_inputs @ [pp1_var]))
          hic.hi_value
      in
      let term = match h.h_term with
        | TVar x -> make_var cenv tenv x
        | TConstr (cname, _) as t when t = t0 ->
          let tp = Program_point (BaseType (SMap.find cname cenv.ce_tenv.env_constr_signatures).cs_output_sort) in
          { desc = VSet [{ desc = LocalVar pp_var_name ; typ = tp }] ; typ = SetType tp }
        | _ -> assert false
      in
      let r = make_join term (["_pp1"],
          make_join ein (["_sigmas1"; "_in1"],
            make_with
              (Sub (in1_var,
                    make_lmapget cenv.ce_tce (hook_in_lvar h.h_name ttype itype) (make_tuple [pp1_var; sigmas1_var]))
              )
              (make_mapget cenv.ce_tce (hook_out_var h.h_name ttype otype) (make_tuple [pp1_var; sigmas1_var]))
          )
        )
      in
      let eout = make_vlet (make_tuple [pp_var; sigmas_var; sigma_var; r]) hoc.ho_value in
      fun hole ->
        c (make_join eout ((sigma_var_name :: h.h_outputs), hole))
    ) ;
  filter_interp = (fun f (tenv, c) ->
      let fc = SMap.find f.f_name cenv.ce_filts in
      let e = make_vlet
          (make_tuple (pp_var :: sigmas_var :: sigma_var :: List.map (make_var cenv tenv) f.f_inputs))
          fc.fc_value
      in
      let ov = f.f_outputs in
      fun hole -> c (make_join e (sigma_var_name :: ov, hole))
    ) ;
  merge_interp = (fun vars outs (tenv, c) ->
      let mt tenv1 =
        let res = make_tuple (sigma_var :: (List.map (make_var cenv tenv1) vars)) in
        { desc = VList [res] ; typ = ListType res.typ }
      in
      let louts = List.map (fun (tenv1, c1) -> c1 (mt tenv1)) outs in
      let t = (List.hd louts).typ in
      fun hole -> c (make_join (make_join (make_list louts) (["_l"], mkvar "_l" t)) (sigma_var_name :: vars, hole))
    ) ;
  before_merge_interp = (fun _ hole -> hole) ;
}

let cstr_cfa_interp cenv t0 =
  add_interpretation (typing_interp cenv.ce_tenv) (cstr_cfa_interp_base cenv t0)

let cfa_rule_constraints cenv name r =
  let init = (make_init_env cenv.ce_tenv r, fun hole -> hole) in
  let t0 = TConstr (r.r_constructor, List.map (fun v -> TVar v) r.r_constructor_arguments) in
  let (tenv, c) = interpret_skeleton (cstr_cfa_interp cenv t0) init r.r_skeleton in
  let h = SMap.find name cenv.ce_tenv.env_hook_definitions in
  let ttype = Program_point (BaseType h.hd_term_sort) in
  let ppvar = { desc = LocalVar pp_var_name ; typ = ttype } in
  let res = make_vlet (make_tuple [ppvar; sigmas_var; sigma_var; make_var cenv tenv "x_o"])
      (SMap.find name cenv.ce_ruleouts).ro_value in
  let c = c res in
  let l = make_vlet
      (make_tuple [ppvar; sigmas_var;
                   (make_mapget cenv.ce_tce (hook_in_var name ttype (fst (SMap.find name cenv.ce_tce.tcenv_rulevars)))
                      (make_tuple [ppvar; sigmas_var]))])
      (SMap.find name cenv.ce_ruleins).ri_value in
  let c = make_join l ([sigma_var_name; "x_s"], c) in
  Sub (c, make_lmapget cenv.ce_tce
         (hook_out_lvar name ttype (snd (SMap.find name cenv.ce_tce.tcenv_rulevars)))
         (make_tuple [{ desc = LocalVar pp_var_name ; typ = ttype }; sigmas_var]))

(* Type definitions *)
(* We need to generate a module for each type that is used as map key or set element *)
(* Let us be conservative and generate a module for type *)

let rec list_compare comp l1 l2 = match l1, l2 with
  | [], [] -> 0
  | [], _ -> -1
  | _, [] -> 1
  | x1 :: l1, x2 :: l2 -> let c = comp x1 x2 in if c = 0 then list_compare comp l1 l2 else c

let rec compare_typedesc env t1 t2 =
  let t1 = canonize_type env t1 in
  let t2 = canonize_type env t2 in
  match t1, t2 with
  | TopType, _ | _, TopType | BottomType, _ | _, BottomType -> assert false
  | Program_point _, Program_point _ -> 0 (* All program points are extracted to int lists, and are equal *)
  | Program_point _, _ -> -1
  | _, Program_point _ -> 1
  | BaseType s1, BaseType s2 -> compare s1 s2
  | BaseType _, _ -> -1
  | _, BaseType _ -> 1
  | ListType t1, ListType t2 -> compare_typedesc env t1 t2
  | ListType _, _ -> -1
  | _, ListType _ -> 1
  | TupleType l1, TupleType l2 -> list_compare (compare_typedesc env) l1 l2
  | TupleType _, _ -> -1
  | _, TupleType _ -> 1
  | SetType t1, SetType t2 -> compare_typedesc env t1 t2
  | SetType _, _ -> -1
  | _, SetType _ -> 1
  | MapType (t1a, t1b), MapType (t2a, t2b) ->
    let c = compare_typedesc env t1a t2a in
    if c = 0 then compare_typedesc env t1b t2b else c

let typedesc_env_ref = ref None
let setup_typedesc_env x =
  assert (!typedesc_env_ref = None);
  typedesc_env_ref := Some x
let clean_typedesc_env () =
  assert (!typedesc_env_ref <> None);
  typedesc_env_ref := None

let compare_typedesc_with_ref x y =
  match !typedesc_env_ref with None -> assert false | Some env -> compare_typedesc env x y
module TMap = Map.Make(struct type t = typedesc let compare = compare_typedesc_with_ref end)
module TSet = Set.Make(struct type t = typedesc let compare = compare_typedesc_with_ref end)

let rec raw_type_to_name = function
  | BaseType s -> get_alpha_name s
  | ListType t -> "List_" ^ raw_type_to_name t
  | SetType t -> "Set_" ^ raw_type_to_name t
  | TupleType l -> "Tuple" ^ (string_of_int (List.length l)) ^ "_" ^ String.concat "_" (List.map raw_type_to_name l)
  | MapType (t1, t2) -> "Map_" ^ raw_type_to_name t1 ^ "_" ^ raw_type_to_name t2
  | TopType | BottomType -> assert false
  | Program_point _ -> "PP"

let type_arguments env t =
  match canonize_type env t with
  | ListType t -> [t]
  | SetType t -> [t]
  | MapType (t1, t2) -> [t1; t2]
  | TupleType l -> l
  | TopType | BottomType -> assert false
  | Program_point _ | BaseType _ -> []

let index_subtypes env roots : (typedesc list * string TMap.t) =
  let names = ref TMap.empty in
  let l = ref [] in
  let rec dfs t =
    if not (TMap.mem t !names) then begin
      names := TMap.add t (raw_type_to_name t) !names;
      List.iter dfs (type_arguments env t);
      l := t :: !l
    end
  in
  List.iter dfs roots;
  (List.rev !l, !names)

type needed_features = {
  ft_compare : bool ;
  ft_join : bool ;
}

let compute_needed_features env join_roots iterorder : needed_features TMap.t =
  let nf = ref TMap.empty in
  let find_nf t = try TMap.find t !nf with Not_found ->
    (nf := TMap.add t { ft_compare = false ; ft_join = false } !nf; { ft_compare = false ; ft_join = false }) in
  let add_nf t f =
    let fo = find_nf t in
    nf := TMap.add t { ft_compare = f.ft_compare || fo.ft_compare ; ft_join = f.ft_join || fo.ft_join } !nf
  in
  let compute t =
    let { ft_compare ; ft_join } = find_nf t in
    match canonize_type env t with
    | ListType t1 -> add_nf t1 { ft_compare = ft_compare ; ft_join = false }
    | SetType t1 -> add_nf t1 { ft_compare = true ; ft_join = false }
    | MapType (t1, t2) ->
      add_nf t1 { ft_compare = true ; ft_join = false };
      add_nf t2 { ft_compare = ft_compare ; ft_join = ft_join }
    | TupleType l ->
      List.iter (fun ts -> add_nf ts { ft_compare = ft_compare ; ft_join = ft_join }) l
    | TopType | BottomType -> assert false
    | Program_point _ -> assert (not ft_join);
    | BaseType _ -> ()
  in
  List.iter (fun t -> add_nf t { ft_compare = false ; ft_join = true }) join_roots;
  List.iter compute (List.rev iterorder);
  !nf

let rec compute_joins_lvalue lv = match lv.desc with
  | LNamedVar _ -> TSet.empty
  | LMapGet (lv, v) -> TSet.union (compute_joins_lvalue lv) (compute_joins_value v)

and compute_joins_value v = match v.desc with
  | NamedVar _ | LocalVar _ | VUnit -> TSet.empty
  | MapGet (v1, v2) -> TSet.union (compute_joins_value v1) (compute_joins_value v2)
  | VTuple l | VSet l | VList l | Func (_, l) ->
    List.fold_left TSet.union TSet.empty (List.map compute_joins_value l)
  | VWith (c, v) -> TSet.union (compute_joins_constr c) (compute_joins_value v)
  | VLet (v1, (_, v2)) -> TSet.union (compute_joins_value v1) (compute_joins_value v2)
  | VJoin (v1, (_, v2)) | VIsEmpty (v1, v2) ->
    TSet.add v.typ (TSet.union (compute_joins_value v1) (compute_joins_value v2))

and compute_joins_constr = function
  | True -> TSet.empty
  | Conj (c1, c2) -> TSet.union (compute_joins_constr c1) (compute_joins_constr c2)
  | CLet (v, (_, c)) | Forall (v, (_, c)) | CEmpty (v, c) ->
    TSet.union (compute_joins_value v) (compute_joins_constr c)
  | Sub (v1, v2) -> TSet.add v2.typ (TSet.union (compute_joins_value v1) (compute_joins_lvalue v2))

let rec compute_joins_dep = function
  | DBase l ->
    List.fold_left TSet.union TSet.empty (
      List.map (function (_, None) -> TSet.empty | (_, Some v) -> compute_joins_value v) l)
  | DConj (d1, d2) -> TSet.union (compute_joins_dep d1) (compute_joins_dep d2)
  | DLet (v, (_, d)) | DForall (v, (_, d)) | DEmpty (v, d) ->
    TSet.union (compute_joins_value v) (compute_joins_dep d)


let rec compute_roots_lvalue lv = match lv.desc with
  | LNamedVar _ -> TSet.empty
  | LMapGet (lv, v) -> TSet.add lv.typ (TSet.union (compute_roots_lvalue lv) (compute_roots_value v))

and compute_roots_value v = match v.desc with
  | NamedVar _ | LocalVar _ | VUnit -> TSet.empty
  | MapGet (v1, v2) -> TSet.add v1.typ (TSet.union (compute_roots_value v1) (compute_roots_value v2))
  | VTuple l | VSet l | VList l | Func (_, l) ->
    List.fold_left TSet.union TSet.empty (List.map compute_roots_value l)
  | VWith (c, v) -> TSet.union (compute_roots_constr c) (compute_roots_value v)
  | VLet (v1, (_, v2)) -> TSet.union (compute_roots_value v1) (compute_roots_value v2)
  | VJoin (v1, (_, v2)) | VIsEmpty (v1, v2) ->
    TSet.add v1.typ (TSet.add v2.typ (TSet.union (compute_roots_value v1) (compute_roots_value v2)))

and compute_roots_constr = function
  | True -> TSet.empty
  | Conj (c1, c2) -> TSet.union (compute_roots_constr c1) (compute_roots_constr c2)
  | CLet (v, (_, c)) -> TSet.union (compute_roots_value v) (compute_roots_constr c)
  | Forall (v, (_, c)) | CEmpty (v, c) ->
    TSet.add v.typ (TSet.union (compute_roots_value v) (compute_roots_constr c))
  | Sub (v1, v2) -> TSet.add v2.typ (TSet.union (compute_roots_value v1) (compute_roots_lvalue v2))

let rec compute_roots_dep = function
  | DBase l ->
    List.fold_left TSet.union TSet.empty (
      List.map (function (_, None) -> TSet.empty | (_, Some v) -> compute_roots_value v) l)
  | DConj (d1, d2) -> TSet.union (compute_roots_dep d1) (compute_roots_dep d2)
  | DLet (v, (_, d)) -> TSet.union (compute_roots_value v) (compute_roots_dep d)
  | DForall (v, (_, d)) | DEmpty (v, d) ->
    TSet.add v.typ (TSet.union (compute_roots_value v) (compute_roots_dep d))

let print_module_def env names nfall ff (t, nf) =
  let name = TMap.find t names in
  Format.fprintf ff "@[<v 2>module %s = struct" name;
  (match canonize_type env t with
   | ListType t1 ->
     let name1 = TMap.find t1 names in
     Format.fprintf ff "@,type t = %s.t list" name1;
     Format.fprintf ff "@,let print ff (l : t) = Format.fprintf ff \"[%%a]\" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff \"; \") %s.print) l" name1;
     Format.fprintf ff "@,let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l";
     if nf.ft_compare then
       Format.fprintf ff "@,let compare : t -> t -> int = compare_lists %s.compare" name1;
     if nf.ft_join then begin
       Format.fprintf ff "@,let join (l1 : t) (l2 : t) : t = l1 %@ l2@,let update (l1 : t) (l2 : t) needs_join : (t * bool) = assert false@,let bot : t = []"
     end
   | SetType t1 ->
     let name1 = TMap.find t1 names in
     Format.fprintf ff "@,module M = Set.Make(%s)@,type t = M.t" name1;
     Format.fprintf ff "@,let print ff (s : t) = Format.fprintf ff \"%@[<hv 2>{%@,%%a%@;<0 -2>}%@]\" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff \";%@ \") %s.print) (M.elements s)" name1;
     Format.fprintf ff "@,let fold f init (s : t) = M.fold f s init";
     if nf.ft_compare then
       Format.fprintf ff "@,let compare : t -> t -> int = M.compare";
     if nf.ft_join then
       Format.fprintf ff "@,let join (s1 : t) (s2 : t) : t = M.union s1 s2@,let update (s1 : t) (s2 : t) needs_join : (t * bool) = (M.union s1 s2, M.subset s2 s1)@,let bot : t = M.empty"
   | MapType (t1, t2) ->
     let name1, name2 = TMap.find t1 names, TMap.find t2 names in
     Format.fprintf ff "@,module M = Map.Make(%s)@,type t = %s.t M.t" name1 name2;
     Format.fprintf ff "@,let print ff (m : t) = Format.fprintf ff \"%@[<hv 2>{|%@,%%a%@;<0 -2>|}%@]\" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff \";%@ \") (fun ff (k, v) -> Format.fprintf ff \"%%a |-> %%a\" %s.print k %s.print v)) (M.bindings m)" name1 name2;
     Format.fprintf ff "@,let fold f init (m : t) = M.fold (fun k x acc -> f (k, x) acc) m init";
     Format.fprintf ff "@,let find x (m : t) = ";
     if (TMap.find t2 nfall).ft_join then
       Format.fprintf ff "try M.find x m with Not_found -> %s.bot" name2
     else if (match t2 with MapType _ -> true | _ -> false) then
       Format.fprintf ff "try M.find x m with Not_found -> %s.M.empty" name2
     else
       Format.fprintf ff "M.find x m";
     Format.fprintf ff "@,let update_elt x f (m : t option) needs_join =@,  match m with@,  | None -> let (y, stable) = f None needs_join in (M.singleton x y, stable)@,  | Some m -> let (y, stable) = f (M.find_opt x m) needs_join in (M.add x y m, stable)";
     if nf.ft_compare then
       Format.fprintf ff "@,let compare : t -> t -> int = M.compare %s.compare" name2;
     if nf.ft_join then begin
       Format.fprintf ff "@,let join (m1 : t) (m2 : t) : t = M.merge (fun _ v1 v2 -> match v1, v2 with None, None -> None | None, Some v | Some v, None -> Some v | Some v1, Some v2 -> Some (%s.join v1 v2)) m1 m2" name2;
       Format.fprintf ff "@,let bot : t = M.empty";
       Format.fprintf ff "@,let update (m1 : t) (m2 : t) needs_join : (t * bool) =\
                          @,  let stable = ref true in\
                          @,  let m = M.merge (fun _ v1 v2 -> match v1, v2 with None, None -> None | None, Some v -> stable := false; Some v | Some v, None -> Some v | Some v1, Some v2 -> let v, st = %s.update v1 v2 needs_join in stable := !stable && st; Some v) m1 m2 in\
                          @,  (m, !stable)" name2
     end
   | TupleType l ->
     let lnames = List.map (fun t -> TMap.find t names) l in
     Format.fprintf ff "@,type t = %a"
                      (Format.pp_print_list ~pp_sep:(pp_sep_string " * ") (fun ff s -> Format.fprintf ff "%s.t" s))
                      lnames;
     let il = List.mapi (fun i x -> (i, x)) lnames in
     Format.fprintf ff "@,let print ff ((%a) : t) = Format.fprintf ff \"(%a)\" %a"
       (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, _) -> Format.fprintf ff "x%d" i)) il
       (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff _ -> Format.fprintf ff "%%a")) il
       (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (fun ff (i, x) -> Format.fprintf ff "%s.print x%d" x i)) il;
     if nf.ft_compare then begin
       Format.fprintf ff "@,@[<v 2>let compare ((%a) : t) ((%a) : t) : int =@,"
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, _) -> Format.fprintf ff "x%d" i)) il
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, _) -> Format.fprintf ff "y%d" i)) il;
       List.iter (fun (i, x) -> Format.fprintf ff "let c = %s.compare x%d y%d in@,if c <> 0 then c else@," x i i) il;
       Format.fprintf ff "c@]"
     end;
     if nf.ft_join then begin
       Format.fprintf ff "@,let join ((%a) : t) ((%a) : t) : t = (%a)"
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, _) -> Format.fprintf ff "x%d" i)) il
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, _) -> Format.fprintf ff "y%d" i)) il
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ")
            (fun ff (i, x) -> Format.fprintf ff "%s.join x%d y%d" x i i))
         il;
       Format.fprintf ff "@,let update ((%a) : t) ((%a) : t) needs_join : (t * bool) =@,%a@,((%a), %a)"
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, _) -> Format.fprintf ff "x%d" i)) il
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, _) -> Format.fprintf ff "y%d" i)) il
         (Format.pp_print_list (fun ff (i, x) ->
              Format.fprintf ff "  let (z%d, u%d) = %s.update x%d y%d needs_join in" i i x i i)) il
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (i, x) -> Format.fprintf ff "z%d" i)) il
         (Format.pp_print_list ~pp_sep:(pp_sep_string " && ") (fun ff (i, x) -> Format.fprintf ff "u%d" i)) il;
       Format.fprintf ff "@,let bot : t = (%a)"
         (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (fun ff (_, x) -> Format.fprintf ff "%s.bot" x)) il
     end
   | TopType | BottomType -> assert false
   | Program_point _ ->
     Format.fprintf ff "@,type t = int list";
     Format.fprintf ff "@,let print ff (p : t) = pp_pp_ppmap !pp_default_map ff p";
     if nf.ft_compare then
       Format.fprintf ff "@,let compare : t -> t -> int = compare";
     assert (not nf.ft_join);
   | BaseType name ->
     assert (name = "unit");
     Format.fprintf ff "@,type t = unit";
     Format.fprintf ff "@,let print ff (() : t) = Format.fprintf ff \"()\"";
     if nf.ft_compare then
       Format.fprintf ff "@,let compare : t -> t -> int = compare";
     if nf.ft_join then begin
       Format.fprintf ff "@,let join (() : t) (() : t) : t = ()";
       Format.fprintf ff "@,let update (() : t) (() : t) (needs_join : bool) : t * bool = ((), true)";
       Format.fprintf ff "@,let bot : t = ()"
     end
  );
  Format.fprintf ff "@]@,end"

(* Interpretation of an lvalue: function of type (lv option -> bool -> lv * bool) -> unit which updates the store given
   a function that updates the lvalue
*)
let rec pp_lvalue cenv names ff lv = match lv.desc with
  | LNamedVar x -> Format.fprintf ff "(Store.%s_set _store)" x
  | LMapGet ({ desc = LNamedVar x }, v1) ->
    Format.fprintf ff "(fun _f -> Store.%s_set _store %a _f)" x (pp_value cenv names) v1
  | LMapGet (lv1, v1) ->
    Format.fprintf ff "(fun _f -> %a (%s.update_elt %a _f))"
      (pp_lvalue cenv names) lv1
      (TMap.find lv1.typ names)
      (pp_value cenv names) v1

and pp_value cenv names ff v = match v.desc with
  | NamedVar x -> Format.fprintf ff "(Store.%s_get _store)" x
  | LocalVar x -> Format.fprintf ff "%s" x
  | MapGet ({ desc = NamedVar x }, v2) ->
    Format.fprintf ff "(Store.%s_get _store %a)" x (pp_value cenv names) v2
  | MapGet (v1, v2) ->
    Format.fprintf ff "(%s.find %a %a)"
      (TMap.find v1.typ names)
      (pp_value cenv names) v2
      (pp_value cenv names) v1
  | VUnit -> Format.fprintf ff "()"
  | VTuple l -> Format.fprintf ff "(%a)" (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (pp_value cenv names)) l
  | VList l -> Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(pp_sep_string "; ") (pp_value cenv names)) l
  | VSet l ->
    Format.fprintf ff "(%s.M.of_list [%a])"
      (TMap.find v.typ names)
      (Format.pp_print_list ~pp_sep:(pp_sep_string "; ") (pp_value cenv names)) l
  | Func (f, l) ->
    if f = "subterm" then
      let tn = match canonize_type cenv.ce_tce v.typ with BaseType tn -> tn | _ -> assert false in
      Format.fprintf ff "(subterm_%s _t0 %a)"
        tn
        (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (pp_value cenv names)) l
    else
      Format.fprintf ff "(%s %a)" f (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (pp_value cenv names)) l
  | VLet (v1, (l, v2)) ->
    Format.fprintf ff "(let (%a) = %a in %a)"
      (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") Format.pp_print_string) l
      (pp_value cenv names) v1 (pp_value cenv names) v2
  | VWith (c, v) ->
    Format.fprintf ff "(%a; %a)" (pp_constr cenv names) c (pp_value cenv names) v
  | VJoin (v1, (l, v2)) ->
    let name1 = TMap.find v1.typ names in
    let name2 = TMap.find v2.typ names in
    Format.fprintf ff "(%s.fold (fun (%a) -> %s.join %a) %s.bot %a)"
      name1 (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") Format.pp_print_string) l
      name2 (pp_value cenv names) v2 name2 (pp_value cenv names) v1
  | VIsEmpty (v1, v2) ->
    let name1 = TMap.find v1.typ names in
    let name2 = TMap.find v2.typ names in
    Format.fprintf ff "(if %s.fold (fun _ _ -> true) false %a then %a else %s.bot)"
      name1 (pp_value cenv names) v1 (pp_value cenv names) v2 name2

and pp_constr cenv names ff = function
  | True -> Format.fprintf ff "()"
  | Conj (c1, c2) -> Format.fprintf ff "%a; %a" (pp_constr cenv names) c1 (pp_constr cenv names) c2
  | CLet (v, (l, c)) ->
    Format.fprintf ff "(let (%a) = %a in %a)"
      (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") Format.pp_print_string) l
      (pp_value cenv names) v (pp_constr cenv names) c
  | Forall (v, (l, c)) ->
    let name = TMap.find v.typ names in
    Format.fprintf ff "(%s.fold (fun (%a) () -> %a) () %a)"
      name
      (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") Format.pp_print_string) l
      (pp_constr cenv names) c
      (pp_value cenv names) v
  | CEmpty (v, c) ->
    let name = TMap.find v.typ names in
    Format.fprintf ff "(if %s.fold (fun _ _ -> true) false %a then %a)"
      name
      (pp_value cenv names) v
      (pp_constr cenv names) c
  | Sub (v, lv) ->
    Format.fprintf ff "(let _v, _lv = %a, %a in _lv (make_update %s.update _v))"
      (pp_value cenv names) v (pp_lvalue cenv names) lv (TMap.find lv.typ names)


let rec pp_dep cenv names ff d = match d with
  | DBase l ->
    Format.fprintf ff "(LocationSet.of_list [%a])" (Format.pp_print_list ~pp_sep:(pp_sep_string "; ") (fun ff (name, v) ->
        Format.fprintf ff "D_%s" name;
        match v with Some v -> Format.fprintf ff " (%a)" (pp_value cenv names) v | None -> ()
      )) l
  | DConj (d1, d2) ->
    Format.fprintf ff "(LocationSet.union %a %a)" (pp_dep cenv names) d1 (pp_dep cenv names) d2
  | DLet (v, (l, d)) ->
    Format.fprintf ff "(let (%a) = %a in %a)"
      (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") Format.pp_print_string) l
      (pp_value cenv names) v (pp_dep cenv names) d
  | DForall (v, (l, d)) ->
    let name = TMap.find v.typ names in
    Format.fprintf ff "(%s.fold (fun (%a) -> LocationSet.union %a) LocationSet.empty %a)"
      name
      (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") Format.pp_print_string) l
      (pp_dep cenv names) d
      (pp_value cenv names) v
  | DEmpty (v, d) ->
    let name = TMap.find v.typ names in
    Format.fprintf ff "(if %s.fold (fun _ _ -> true) false %a then %a else LocationSet.empty)"
      name
      (pp_value cenv names) v
      (pp_dep cenv names) d


let pp_types cenv names nf iterorder ff =
  let base, notbase =
    List.partition (fun t -> match canonize_type cenv.ce_tce t with BaseType name -> name <> "unit" | _ -> false) iterorder in
  Format.fprintf ff "@[<v 2>module type BASETYPES = sig@,";
  Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,") (fun ff t ->
      let name = TMap.find t names in
      let nf = TMap.find t nf in
      Format.fprintf ff "@[<v 2>module %s : sig" name;
      Format.fprintf ff "@,type t@,val print : Format.formatter -> t -> unit";
      if nf.ft_compare then
        Format.fprintf ff "@,val compare : t -> t -> int";
      if nf.ft_join then
        Format.fprintf ff "@,val join : t -> t -> t@,val update : t -> t -> join_type -> t * bool@,val bot : t";
      Format.fprintf ff "@]@,end"
    ) ff base;
  Format.fprintf ff "@]@,end@,@,";
  Format.fprintf ff "@[<v 2>module MakeTypes(B : BASETYPES) = struct@,open B@,";
  Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,")
    (fun ff t -> print_module_def cenv.ce_tce names nf ff (t, TMap.find t nf)) ff notbase;
  (* let base_program_types =
    List.map (fun t -> (BaseType t)) (get_term_program_types cenv.ce_tenv @ get_term_base_types cenv.ce_tenv)
  in *)
  Format.fprintf ff "@,@,@[<v 2>module type ABSTRACT = sig@,%a@]@,end"
    (Format.pp_print_list (fun ff f -> f ff))
       (List.map (fun (_, q) ff ->
            Format.fprintf ff "val %s : %a%a" q.q_name
              (Format.pp_print_list ~pp_sep:(pp_sep_string "") (fun ff t -> Format.fprintf ff "%s.t -> " (TMap.find t names))) q.q_inputs
              (fun ff t -> Format.fprintf ff "%s.t" (TMap.find t names)) q.q_output)
        (SMap.bindings cenv.ce_tce.tcenv_funcs)(* @
        (List.map (fun t ff ->
             let name = TMap.find t names in
             Format.fprintf ff "val subterm_%s : program -> PP.t -> %s.t" name name)
                                                  (List.filter (fun t -> TMap.mem t names) base_program_types)) *)
      );
  Format.fprintf ff "@]@,end"

(*
let pp_constraint_type ff =
  Format.fprintf ff
    "type cstr = {\
   @,  c_id : int ;\
   @,  c_run : unit -> unit ;\
   @,  mutable c_deps_in : LocationSet.t ;\
   @,  mutable c_deps_out : LocationSet.t ;\
   @,  mutable c_deps_deps : LocationSet.t ;\
   @,  c_recompute : unit -> unit ;\
   @,}\
   @,\
   @,module CstrSet = Set.Make(struct type t = cstr let compare x y = compare x.c_id y.c_id end)\
   @,\
   @,let cstr_cur_id = ref 0\
   @,let make_cstr run deps =\
   @,  let id = (incr cstr_cur_id; !cstr_cur_id) in\
   @,  let (din, dout, dd) = deps () in\
   @,  let rec res = {\
   @,    c_id = id ;\
   @,    c_run = run ;\
   @,    c_deps_in = din ;\
   @,    c_deps_out = dout ;\
   @,    c_deps_deps = dd ;\
   @,    c_recompute = (fun () -> let (din, dout, dd) = deps () in res.c_deps_in <- din; res.c_deps_out <- dout; res.c_deps_deps <- dd) ;\
   @,  } in res"
*)

let pp_constraint_type ff =
  Format.fprintf ff
    "type cstr = {\
   @,  c_run : unit -> unit ;\
   @,  c_deps_in : LocationSet.t ;\
   @,  c_deps_out : LocationSet.t ;\
   @,  c_deps_deps : LocationSet.t ;\
   @,}"


let pp_store cenv names tps ff =
  Format.fprintf ff
    "@[<v 2>module Store = struct\
     @,@[<v 2>type t = {\
     @,mutable _stable : bool ;\
     @,mutable _widen_points : LocationSet.t ;\
     @,mutable _deps_stable : bool ;\
     @,mutable _deps_deps : LocationSet.t ;@,";
  Format.pp_print_list (fun ff (name, t) ->
      Format.fprintf ff "mutable %s : %s.t ;" name (TMap.find t names)) ff tps;
  Format.fprintf ff "@]@,}@,@,";
  Format.fprintf ff "@[<v 2>let create_store () = {@,_stable = true ;@,_widen_points = LocationSet.empty ;@,_deps_stable = true ;@,_deps_deps = LocationSet.empty ;@,%a@]@,}@,@,"
    (Format.pp_print_list (fun ff (name, t) ->
      match canonize_type cenv.ce_tce t with
      | MapType _ -> Format.fprintf ff "%s = %s.M.empty ;" name (TMap.find t names)
      | _ -> Format.fprintf ff "%s = %s.bot ;" name (TMap.find t names)
    )) tps;
  Format.fprintf ff "let print ff (s : t) = Format.fprintf ff \"%@[<v 2>{%@,%a%@]%@,}\" %a@,@,"
    (Format.pp_print_list ~pp_sep:(pp_sep_string "@,") (fun ff _ -> Format.fprintf ff "%%a")) tps
    (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (fun ff (name, t) ->
         match canonize_type cenv.ce_tce t with
         | MapType (t1, t2) -> Format.fprintf ff "(fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff \"%s[%%a] = %%a ;\" %s.print k %s.print v) ff (%s.M.bindings m)) s.%s" name (TMap.find t1 names) (TMap.find t2 names) (TMap.find t names) name
         | _ -> Format.fprintf ff "(fun ff -> Format.fprintf ff \"%s = %%a ;\" %s.print) s.%s" name (TMap.find t names) name
       )
    ) tps;
  Format.pp_print_list (fun ff (name, t) ->
      match canonize_type cenv.ce_tce t with
      | MapType _ ->
        let joint ff =
          Format.fprintf ff "(if LocationSet.mem (D_%s elt) store._widen_points then JTWiden else JTJoin)" name
        in
        Format.fprintf ff "let %s_get store elt = %s.find elt store.%s@," name (TMap.find t names) name;
        Format.fprintf ff "let %s_set store elt f =" name;
        if (String.length name >= 4 && name.[0] = '_' && name.[1] = 'i' && name.[2] = 'n' && name.[3] = '_') then
          Format.fprintf ff "@,  if not (%s.M.mem elt store.%s) then store._deps_stable <- false;" (TMap.find t names) name;
        Format.fprintf ff
          "@,  let (x, stable) = %s.update_elt elt f (Some store.%s) %t in\
           @,  store.%s <- x;\
           @,  store._stable <- store._stable && stable;\
           @,  if LocationSet.mem (D_%s elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable"
          (TMap.find t names) name joint name name;
      | _ ->
        let joint ff =
          Format.fprintf ff "(if LocationSet.mem D_%s store._widen_points then JTWiden else JTJoin)" name
        in
        Format.fprintf ff "let %s_get store = store.%s@," name name;
        Format.fprintf ff
          "let %s_set store f =\
         @,  let (x, stable) = f (Some store.%s) %t in\
         @,  store.%s <- x;\
         @,  store._stable <- store._stable && stable;\
         @,  if LocationSet.mem D_%s store._deps_deps then store._deps_stable <- store._deps_stable && stable"
        name name joint name name) ff tps;
  Format.fprintf ff "@]@,end"

let pp_locations cenv names tps ff =
  Format.fprintf ff "@[<v 2>type location =@,";
  Format.pp_print_list (fun ff (name, t) ->
      Format.fprintf ff "| D_%s" name;
      match canonize_type cenv.ce_tce t with
      | MapType (key, _) -> Format.fprintf ff " of %s.t" (TMap.find key names)
      | _ -> ()
    ) ff tps;
  Format.fprintf ff "@]@,@,";
  Format.fprintf ff "@[<v 2>module Location = struct@,type t = location@,@[<v 2>let compare x y = match x, y with";
  let rec print = function
    | [] -> ()
    | (name, t) :: l ->
      let keytype = match canonize_type cenv.ce_tce t with MapType (key, _) -> Some key | _ -> None in
      (match keytype with
       | None -> Format.fprintf ff "@,| D_%s, D_%s -> 0" name name
       | Some t -> Format.fprintf ff "@,| D_%s a, D_%s b -> %s.compare a b" name name (TMap.find t names));
      let anyt = if keytype = None then "D_" ^ name else "D_" ^ name ^ " _" in
      if l <> [] then Format.fprintf ff "@,| %s, _ -> -1@,| _, %s -> 1" anyt anyt;
      print l
  in print tps;
  Format.fprintf ff "@]@]@,end@,\
                     @,module LocationSet = Set.Make(Location)\
                     @,module LocationMap = Map.Make(Location)\
                     @,@,%t" pp_constraint_type

let pp_cstr_def cenv names ff (r, cstr) =
  Format.fprintf ff "@[<v 2>let constraints_%s _store _t0 _pp _sigmas =@," r.r_name;
  List.iteri (fun i x -> Format.fprintf ff "let %s = Set_PP.M.singleton (%d :: _pp) in@," x i)
    r.r_constructor_arguments;
  Format.fprintf ff "@[<v 2>[@,%a@]@,]@]"
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@,")
       (fun ff (cstr, din, dout, dd) ->
          Format.fprintf ff "@[<v 2>{@,c_run = (fun () -> %a) ;\
                                     @,c_deps_in = (%a) ;\
                                     @,c_deps_out = (%a) ;\
                                     @,c_deps_deps = (%a) ;@]@,}"
            (pp_constr cenv names) cstr (pp_dep cenv names) din (pp_dep cenv names) dout (pp_dep cenv names) dd))
    cstr

let pp_print_subterm cenv names ff =
  let env = cenv.ce_tenv in
  let const = get_constructors_by_sort env in
  let ps = get_term_program_types env in
  let bs = get_term_base_types env in
  let pp_print_line ps_name sub_term ff c =
    Format.fprintf ff "| %s%a -> (match b with%t | _ -> assert false)"
      (get_alpha_name c.cs_name)
      (pp_print_constructor_args (fun ff i -> Format.fprintf ff "x_t%d" i)) (List.mapi (fun i _ -> i) c.cs_input_sorts)
      (fun ff -> List.iteri (fun i is ->
           Format.fprintf ff " | %d -> " i;
           match is with
           | Program is_name ->
             Format.fprintf ff "subterm_%s_%s x_t%d pp" is_name sub_term i
           | Flow is_name ->
             if is_name = sub_term then
               Format.fprintf ff "if pp = [] then x_t%d else assert false" i
             else
               Format.fprintf ff "assert false")
           c.cs_input_sorts)
  in
  let pp_print_ps sub_term ff (ps_name, const) =
    Format.fprintf ff "subterm_%s_%s term pp =@,match pp with@,| [] -> %t@,@[<v 2>| b :: pp -> match term with@,%a@]"
      ps_name sub_term
      (fun ff -> if ps_name = sub_term then Format.fprintf ff "term" else Format.fprintf ff "assert false")
      (Format.pp_print_list (pp_print_line ps_name sub_term)) const
  in
  let prog_type = match canonize_type cenv.ce_tce (BaseType "program") with BaseType t -> t | _ -> assert false in
  let pp_print_subs ff sub_term =
    Format.fprintf ff "@[<v>@[<v 2>let rec %a@]@,@,%a"
      (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@]@,@,@[<v 2>and ") (pp_print_ps sub_term))
      const
      (Format.pp_print_list (fun ff ps_name ->
           Format.fprintf ff "let subterm_%s_%s term pp = subterm_%s_%s term (List.rev pp)"
             ps_name sub_term ps_name sub_term))
      ps;
      Format.fprintf ff "@,@,let subterm_%s = subterm_%s_%s" sub_term prog_type sub_term;
    Format.fprintf ff "@]"
  in
  Format.fprintf ff "@[<v>%a@]"
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,") pp_print_subs) (ps @ bs)

let pp_print_gent ff env =
  let const = get_constructors_by_sort env in
  let ps = get_term_program_types env in
  let bs = get_term_base_types env in
  let pp_print_fargs ff = Format.pp_print_list ~pp_sep:(pp_sep_string " ")
      (fun ff s -> Format.fprintf ff "f_%s" s) ff (ps @ bs)
  in
  Format.fprintf ff "@[<v>@[<v 2>let rec %a@]@]"
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@]@,@,@[<v 2>and ")
       (fun ff (ps_name, const) ->
          Format.fprintf ff "gent_%s %t pp term =@,match term with@,%a" ps_name pp_print_fargs
            (Format.pp_print_list (fun ff c ->
                 Format.fprintf ff "| %s%a -> f_%s pp term%a"
                   (get_alpha_name c.cs_name)
                   (pp_print_constructor_args (fun ff i -> Format.fprintf ff "x_t%d" i))
                   (List.mapi (fun i _ -> i) c.cs_input_sorts)
                   ps_name
                   (Format.pp_print_list ~pp_sep:(pp_sep_string "") (fun ff (is, i) ->
                        match is with
                        | Program is_name ->
                          Format.fprintf ff "; gent_%s %t (%d :: pp) x_t%d" is_name pp_print_fargs i i
                        | Flow is_name ->
                          Format.fprintf ff "; f_%s (%d :: pp) x_t%d" is_name i i
                      ))
                   (List.mapi (fun i is -> (is, i)) c.cs_input_sorts)
               )) const
       )
    ) const


let pp_print_printterm ff env =
  let const = get_constructors_by_sort env in
  let bs = get_term_base_types env in
  let pp_print_fargs ff = Format.pp_print_list ~pp_sep:(pp_sep_string " ")
      (fun ff s -> Format.fprintf ff "pp_%s" s) ff bs
  in
  let pp_print_fill_map ff =
    let ps = get_term_program_types env in
    Format.fprintf ff "let fill_default_ppmap gent t0 =\
                       @,  let c = ref 0 in\
                       @,  pp_default_map := PPMap.empty;\
                       @,  gent %a [] t0"
      (Format.pp_print_list ~pp_sep:(pp_sep_string " ")
         (fun ff _ -> Format.fprintf ff
             "(fun pp _ -> pp_default_map := PPMap.add pp (incr c; \"l\" ^ string_of_int !c) !pp_default_map)"))
      (ps @ bs)
  in
  Format.fprintf ff "@[<v>module PPMap = Map.Make(struct type t = int list let compare = compare end)\
                     @,let pp_default_map = ref PPMap.empty\
                     @,let empty_ppmap () = pp_default_map := PPMap.empty\
                     @,%t\
                     @,let pp_pp_ppmap ppmap ff pp = match PPMap.find pp ppmap with\
                     @,| s -> Format.fprintf ff \"%%s\" s\
                     @,| exception Not_found -> Format.fprintf ff \"[%%a]\" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff \"; \") Format.pp_print_int) pp\
                     @,@,@[<v 2>let rec %a@]@]"
    pp_print_fill_map
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@]@,@,@[<v 2>and ")
       (fun ff (ps_name, const) ->
          Format.fprintf ff "pp_%s %t pp ppmap ff term =@,match term with@,%a" ps_name pp_print_fargs
            (Format.pp_print_list (fun ff c ->
                 Format.fprintf ff "| %s%a -> Format.fprintf ff \"%@[<hv 2>(%s%a%@;<0 -2>)^%%a%@]\" %a(pp_pp_ppmap ppmap) pp"
                   (get_alpha_name c.cs_name)
                   (pp_print_constructor_args (fun ff i -> Format.fprintf ff "x_t%d" i))
                   (List.mapi (fun i _ -> i) c.cs_input_sorts)
                   (get_alpha_name c.cs_name)
                   (pp_print_constructor_args (fun ff _ -> Format.fprintf ff "%@,%%a")) c.cs_input_sorts
                   (Format.pp_print_list ~pp_sep:(pp_sep_string "") (fun ff (is, i) ->
                        match is with
                        | Program is_name ->
                          Format.fprintf ff "(pp_%s %t (%d :: pp) ppmap) x_t%d " is_name pp_print_fargs i i
                        | Flow is_name ->
                          Format.fprintf ff "(fun ff x -> Format.fprintf ff \"%%a^%%a\" pp_%s x (pp_pp_ppmap ppmap) (%d :: pp)) x_t%d " is_name i i
                      ))
                   (List.mapi (fun i is -> (is, i)) c.cs_input_sorts)
               )) const
       )
    ) const

let pp_print_constraint_dispatch cenv names ff =
  let pp_print_rule_line ff {r_name ; r_constructor ; r_constructor_arguments} =
    let ca = r_constructor_arguments in
    Format.fprintf ff "| %s%a -> constraints_%s store t0 pp sigmas"
      (get_alpha_name r_constructor)
      (pp_print_constructor_args Format.pp_print_string) ca
      r_name
  in
  let rules = List.map (fun (name, h) ->
      (name, h, List.map snd (SMap.bindings h.hd_rules))) (SMap.bindings cenv.ce_tenv.env_hook_definitions) in
  Format.fprintf ff "@[<v>%a@]"
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,")
       (fun ff (h_name, h, rules) ->
          let ityp = fst (SMap.find h_name cenv.ce_tce.tcenv_rulevars) in
          Format.fprintf ff "@[<v 2>let constraints_%s store t0 =@,@[<v 2>let f pp sigmas = match subterm_%s t0 pp with@,%a@]@,in@," h_name h.hd_term_sort
            (Format.pp_print_list pp_print_rule_line) rules;
          Format.fprintf ff "%s.fold (fun ((pp, sigmas), _) acc -> f pp sigmas @@ acc) [] (store._in_%s)"
            (TMap.find (MapType (TupleType [Program_point TopType; globalcontext], ityp)) names)
            h_name;
          Format.fprintf ff "@]"
       )
    ) rules

let pp_gen_gen_constr ff cenv =
  Format.fprintf ff "let get_constraints store t0 = %a"
    (Format.pp_print_list ~pp_sep:(pp_sep_string " @ ")
       (fun ff (name, _) ->
          Format.fprintf ff "constraints_%s store t0" name
    )) (SMap.bindings cenv.ce_tenv.env_hook_definitions);
  Format.fprintf ff
    "@,@,@[<v 2>let sort_constraints cstrs =\
     @,let cstrs = Array.of_list cstrs in\
     @,let init_vars = ref LocationMap.empty in\
     @,Array.iteri (fun i c ->\
     @,  LocationSet.iter (fun v ->\
     @,    init_vars := LocationMap.add v (i :: (try LocationMap.find v !init_vars with Not_found -> [])) !init_vars\
     @,  ) c.c_deps_in\
     @,) cstrs;\
     @,let seen = Array.make (Array.length cstrs) 0 in\
     @,let wid = ref LocationSet.empty in\
     @,let sorted = ref [] in\
     @,let rec dfs v i =\
     @,  match seen.(i) with\
     @,  | 0 ->\
     @,    seen.(i) <- 1;\
     @,    let c = cstrs.(i) in\
     @,    LocationSet.iter (fun v ->\
     @,      let nxt = try LocationMap.find v !init_vars with Not_found -> [] in\
     @,      List.iter (dfs (Some v)) nxt\
     @,    ) c.c_deps_out;\
     @,    seen.(i) <- 2;\
     @,    sorted := c :: !sorted\
     @,  | 1 -> (match v with None -> assert false | Some v -> wid := LocationSet.add v !wid)\
     @,  | _ -> ()\
     @,in\
     @,for i = 0 to Array.length cstrs - 1 do\
     @,  dfs None i\
     @,done;\
     @,(!sorted, !wid)\
     @]";
  Format.fprintf ff
    "@,@,@[<v 2>let run_constraints store t0 =\
     @,let open Store in\
     @,while not store._stable do\
     @,  store._stable <- true;\
     @,  store._deps_stable <- true;\
     @,  store._widen_points <- LocationSet.empty;\
     @,  let cstr, widen = sort_constraints (get_constraints store t0) in\
     @,  store._deps_deps <- List.fold_left LocationSet.union LocationSet.empty (List.map (fun c -> c.c_deps_deps) cstr);\
     @,  List.iter (fun c -> c.c_run ()) cstr;\
     @,  store._widen_points <- widen;\
     @,  while store._deps_stable && not store._stable do\
     @,    store._stable <- true;\
     @,    List.iter (fun c -> c.c_run ()) cstr\
     @,  done\
     @,done\
     @]"



let pp_cstr_defs cenv names tps ff cstrs =
  Format.fprintf ff "@[<v 2>module MakeCstr(B : BASETYPES)(A : MakeTypes(B).ABSTRACT) = struct@,include B@,include MakeTypes(B)@,include A@,%t@,@,%t@,@," (pp_locations cenv names tps) (pp_store cenv names tps);
  Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,") (pp_cstr_def cenv names) ff cstrs;
  Format.printf "@,@,%t@,@,%a" (pp_print_constraint_dispatch cenv names) pp_gen_gen_constr cenv;
  Format.fprintf ff "@]@,end"

let () =
  let atoms, env = Util.parse_from_file Parser.main Sys.argv.(1) in
  let decls = Util.parse_from_file Parser.main_constr Sys.argv.(2) in
  let tcenv = make_tcenv decls in
  setup_typedesc_env tcenv;
  let cenv = make_cenv env tcenv decls in
  let () = Skeleton.check_well_typed env in
  let all_rules = List.flatten (List.map (fun (hname, hd) -> List.map (fun (_, x) -> (hname, x)) (SMap.bindings hd.hd_rules)) (SMap.bindings env.env_hook_definitions)) in
  let cstrs = List.map (fun (hname, r) ->
      let cstr = lift_with_constr (cfa_rule_constraints cenv hname r) in
      let cstr = List.map (fun cstr ->
          let cin = deps_constr_in cstr in
          let cout = deps_constr_out cstr in
          (cstr, cin, cout, make_dconj (deps_deps cin) (deps_deps cout))
        ) cstr
      in
      (r, cstr)) all_rules
  in
  let all_cstrs = List.flatten (List.map snd cstrs) in
  let joins = List.fold_left TSet.union TSet.empty (List.map (fun (cstr, din, dout, dd) ->
      TSet.union (compute_joins_constr cstr)
        (TSet.union (compute_joins_dep dd)
           (TSet.union (compute_joins_dep din) (compute_joins_dep dout)))) all_cstrs)
  in
  let roots = List.fold_left TSet.union TSet.empty (List.map (fun (cstr, din, dout, dd) ->
      TSet.union (compute_roots_constr cstr)
        (TSet.union (compute_roots_dep dd)
           (TSet.union (compute_roots_dep din) (compute_roots_dep dout)))) all_cstrs)
  in
  let roots =
    List.map (fun (name, _) -> BaseType name)
      (List.filter (fun (name, _) -> name <> "program") (SMap.bindings tcenv.tcenv_types))
    @ List.flatten (List.map (fun (_, q) -> q.q_output :: q.q_inputs) (SMap.bindings tcenv.tcenv_funcs))
    @ List.flatten (List.map (fun (_, (t1, t2)) -> [t1; t2]) (SMap.bindings tcenv.tcenv_rulevars))
    @ List.map (fun (_, (t, _)) -> t) (SMap.bindings tcenv.tcenv_vars)
    @ TSet.elements roots in
  let iterorder, names = index_subtypes tcenv roots in
  let nf = compute_needed_features tcenv (TSet.elements joins) iterorder in
  Format.printf "@[<v>type join_type = JTJoin | JTWiden@,let make_update f v old jt = match old with None -> (v, false) | Some v1 -> f v1 v jt@,@,%a@,@,%t@,@,%a@,@,%a@,@,%t"
    pp_print_ast_types env
    (pp_print_subterm cenv names)
    pp_print_gent env
    pp_print_printterm env
    (pp_types cenv names nf iterorder);
  let addvars = List.map (fun (name, (t, _)) -> (name, t)) (SMap.bindings tcenv.tcenv_vars) in
  let hin_vars = List.map (fun (hname, hd) ->
      ("_in_" ^ hname,
         MapType (TupleType [Program_point (BaseType hd.hd_term_sort); globalcontext],
                  fst (SMap.find hname tcenv.tcenv_rulevars))))
      (SMap.bindings env.env_hook_definitions) in
  let hout_vars = List.map (fun (hname, hd) ->
      ("_out_" ^ hname,
       MapType (TupleType [Program_point (BaseType hd.hd_term_sort); globalcontext],
                snd (SMap.find hname tcenv.tcenv_rulevars))))
      (SMap.bindings env.env_hook_definitions) in
  Format.printf "@,@,%a" (pp_cstr_defs cenv names (hin_vars @ hout_vars @ addvars)) cstrs;
  Format.printf "@]@.";
  clean_typedesc_env ()
