open Types
open Skeleton
open Camlident
open Printing

type cvar = string
type path = int list

type gconstraint =
  | GCSub of cvar * path * cvar * path
  | GCFilter of string * cvar list * cvar list

let print_list_suffixed s printer ff l =
  List.iter (fun x -> Format.fprintf ff "%a%s" printer x s) l

let print_list_prefixed s printer ff l =
  List.iter (fun x -> Format.fprintf ff "%s%a" s printer x) l

let print_intlist_suffixed s =
  print_list_suffixed s Format.pp_print_int

let print_path_prefix = print_intlist_suffixed " :: "

let print_gconstraint ff = function
  | GCSub (var1, path1, var2, path2) ->
    Format.fprintf ff "CSub ((%S, %app), (%S, %app))"
      var1 print_path_prefix path1 var2 print_path_prefix path2
  | GCFilter (f, invars, outvars) ->
    Format.fprintf ff "CFilter (%s%a%a)" f
      (print_list_prefixed " " (fun ff s -> Format.fprintf ff "(%S, pp)" s)) invars
      (print_list_prefixed " " (fun ff s -> Format.fprintf ff "(%S, pp)" s)) outvars

let get_subterm t path =
  let rec aux t path =
    match path with
    | [] -> t
    | x :: path -> match t with
      | TVar _ -> raise (Invalid_argument "can't take subterm of var")
      | TConstr (_, l) -> aux (List.nth l x) path
  in aux t (List.rev path)

let get_path t subt =
  let exception Found of path in
  let rec iter t path =
    if t = subt then raise (Found path);
    match t with
    | TVar _ -> ()
    | TConstr (_, l) -> List.iteri (fun i t -> iter t (i :: path)) l
  in
  match iter t [] with
  | () -> raise Not_found
  | exception Found path -> path

let empty_cstrgen _ = []

let hook_cstrgen {h_name ; h_input ; h_term ; h_output} t0 tenv =
  let subp = get_path t0 h_term in
  let cstr1 = GCSub (h_input, [], "x_s", subp) in
  let cstr2 = GCSub ("x_o", subp, h_output, []) in
  [cstr1 ; cstr2]

let filter_cstrgen {f_name ; f_inputs ; f_outputs} _ =
  [GCFilter (f_name, f_inputs, f_outputs)]

let merge_cstrgen vars _ _ = []

let cstrgen_base_interp t0 =
  {
    empty_interp = empty_cstrgen ;
    hook_interp = (fun h -> hook_cstrgen h t0) ;
    filter_interp = filter_cstrgen ;
    merge_interp = merge_cstrgen ;
    before_merge_interp = (fun _ -> []) ;
  }

let cstrgen_interp env t0 =
  accumulating_interpretation
    (combine_interpretation (typing_interp env) (cstrgen_base_interp t0))

let generate_rule_constraints env ff ({r_name ; r_constructor ; r_constructor_arguments ; r_skeleton} as r) =
  (* let ca = List.map string_of_var r_constructor_arguments in *)
  let ca = r_constructor_arguments in
  Format.fprintf ff "@[<v 2>let constraints_%s pp%a = @,[@[<hov>" r_name
    (print_list_prefixed " " Format.pp_print_string) ca;
  let t0 = TConstr (r_constructor, List.map (fun v -> TVar v) ca) in
  (* let init = ((make_init_env filt r, t0), []) in *)
  let init = (make_init_env env r, []) in
  let (tenv, cstrs) = interpret_skeleton (cstrgen_interp env t0) init r_skeleton in
  (* let env = get_env_map env in *)
  VMap.iter (fun v s -> Format.fprintf ff "CSort ((%S, pp), %S);@ " v (string_of_sort s)) tenv;
  let is = SMap.find r_constructor env.env_constr_signatures in
  List.iter2 (fun a at -> Format.fprintf ff "CEq_%s ((%S, pp), %s);@ " (string_of_sort at) a a) ca is.cs_input_sorts;
  List.iter (fun c -> Format.fprintf ff "%a;@ " print_gconstraint c) cstrs;
  Format.fprintf ff "@]]@]@,@,"


type gconstraint_fs =
  | GCFSSub of cvar * cvar * path * cvar * path
  | GCFSFlag of cvar * path * cvar * path
  | GCFSFlagTest of cvar * cvar * cvar
  | GCFSFilter of cvar * cvar * string * cvar list * cvar list

let print_gconstraint_fs ff = function
  | GCFSSub (flowin, var1, path1, var2, path2) ->
    Format.fprintf ff "CFSSub ((%S, pp), (%S, %app), (%S, %app))"
      flowin var1 (print_intlist_suffixed " :: ") path1 var2 (print_intlist_suffixed " :: ") path2
  | GCFSFlag (flowin, pathin, flowout, pathout) ->
    Format.fprintf ff "CFSFlag ((%S, %app), (%S, %app))"
      flowin (print_intlist_suffixed " :: ") pathin flowout (print_intlist_suffixed " :: ") pathout
  | GCFSFlagTest (flowin, testvar, flowout) ->
    Format.fprintf ff "CFSFlagTest ((%S, pp), (%S, pp), (%S, pp))"
      flowin testvar flowout
  | GCFSFilter (flowin, flowout, f, invars, outvars) ->
    Format.fprintf ff "CFSFilter (%s (%S, pp) (%S, pp)%a%a)" f flowin flowout
      (print_list_prefixed " " (fun ff s -> Format.fprintf ff "(%S, pp)" s)) invars
      (print_list_prefixed " " (fun ff s -> Format.fprintf ff "(%S, pp)" s)) outvars

let fv_counter = ref 0
let fv_list = ref []
let reset_fv_list () = fv_list := []
let get_new_freevar () =
  let z = "flow" ^ (string_of_int !fv_counter) in
  incr fv_counter;
  fv_list := z :: !fv_list;
  z

let freevar_generator_interp = {
  empty_interp = (fun x -> x) ;
  hook_interp = (fun _ _ -> get_new_freevar ()) ;
  filter_interp = (fun _ _ -> get_new_freevar ()) ;
  merge_interp = (fun _ _ _ -> get_new_freevar ()) ;
  before_merge_interp = (fun x -> x) ;
}

let empty_cstrgen_fs _ = []

let hook_cstrgen_fs t0 {h_input ; h_term ; h_output} ((flowin, env), (flowout, _)) =
  let subp = get_path t0 h_term in
  let cstr1 = GCFSSub (flowin, h_input, [], "x_s", subp) in
  let cstr2 = GCFSSub (flowin, "x_o", subp, h_output, []) in
  let cstr3 = GCFSFlagTest (flowin, h_output, flowout) in
  let cstr4 = GCFSFlag (flowin, [], "inflag", subp) in
  [cstr1 ; cstr2 ; cstr3 ; cstr4]

let filter_cstrgen_fs {f_name ; f_inputs ; f_outputs} ((flowin, _), (flowout, _)) =
  [GCFSFilter (flowin, flowout, f_name, f_inputs, f_outputs)]

let merge_cstrgen_fs vars outs (_, (flowout, _)) =
  List.map (fun (flowin, _) -> GCFSFlag (flowin, [], flowout, [])) outs

let cstrgen_fs_base_interp t0 =
  {
    empty_interp = empty_cstrgen_fs ;
    hook_interp = hook_cstrgen_fs t0 ;
    filter_interp = filter_cstrgen_fs ;
    merge_interp = merge_cstrgen_fs ;
    before_merge_interp = (fun x -> []) ;
  }

let cstrgen_fs_interp env t0 =
  accumulating_interpretation
    (combine_interpretation_inout
       (interpretation_product freevar_generator_interp (typing_interp env))
       (cstrgen_fs_base_interp t0))

let generate_rule_constraints_fs env ff ({r_name ; r_constructor ; r_constructor_arguments ; r_skeleton} as r) =
  let ca = r_constructor_arguments in
  Format.fprintf ff "@[<v 2>let constraints_%s pp%a = @,[@[<hov>" r_name
    (print_list_prefixed " " Format.pp_print_string) ca;
  let t0 = TConstr (r_constructor, List.map (fun v -> TVar v) ca) in
  fv_list := ["inflag"];
  let init = (("inflag", make_init_env env r), []) in
  let ((_, tenv), cstrs) = interpret_skeleton (cstrgen_fs_interp env t0) init r_skeleton in
  let fvl = !fv_list in
  (* let env = get_env_map env in *)
  List.iter (fun v -> Format.fprintf ff "CFSSort ((%S, pp), \"flag\");@ " v) fvl;
  VMap.iter (fun v s -> Format.fprintf ff "CFSSort ((%S, pp), %S);@ " v (string_of_sort s)) tenv;
  let is = SMap.find r_constructor env.env_constr_signatures in
  List.iter2 (fun a at -> Format.fprintf ff "CFSEq_%s ((%S, pp), %s);@ " (string_of_sort at) a a) ca is.cs_input_sorts;
  List.iter (fun c -> Format.fprintf ff "%a;@ " print_gconstraint_fs c) cstrs;
  Format.fprintf ff "@]]@]@,@,"


type gcfa_var =
  | CVar of cvar * path
  | WVar of path option
  | IVar of path option

type gconstraint_cfa =
  | GCFASub of gcfa_var * gcfa_var
  | GCFAForallSub of gcfa_var * gcfa_var * gcfa_var
  | GCFAFilter of string * gcfa_var list * gcfa_var list

let print_gcfa_var ff = function
  | CVar (var, path) -> Format.fprintf ff "CVar (%S, %app)" var (print_intlist_suffixed " :: ") path
  | WVar None -> Format.fprintf ff "WVar None"
  | WVar (Some path) -> Format.fprintf ff "WVar (Some %app)" (print_intlist_suffixed " :: ") path
  | IVar None -> Format.fprintf ff "IVar None"
  | IVar (Some path) -> Format.fprintf ff "IVar (Some %app)" (print_intlist_suffixed " :: ") path


let print_gconstraint_cfa ff = function
  | GCFASub (var1, var2) ->
    Format.fprintf ff "CFASub (%a, %a)" print_gcfa_var var1 print_gcfa_var var2
  | GCFAForallSub (quantify, var1, var2) ->
    Format.fprintf ff "CFAForallSub (%a, %a, %a)"
      print_gcfa_var quantify print_gcfa_var var1 print_gcfa_var var2
  | GCFAFilter (f, invars, outvars) ->
    Format.fprintf ff "CFAFilter (%s%a%a)" f
      (print_list_prefixed " " print_gcfa_var) invars
      (print_list_prefixed " " print_gcfa_var) outvars

let empty_cstrgen_cfa _ = []

let hook_cstrgen_cfa t0 {h_input ; h_term ; h_output} ((flowin, tenv), (flowout, _)) =
  let subp = get_path t0 h_term in
  let cstr1 = GCFSSub (flowin, h_input, [], "x_s", subp) in
  let cstr2 = GCFSSub (flowin, "x_o", subp, h_output, []) in
  let cstr3 = GCFSFlagTest (flowin, h_output, flowout) in
  let cstr4 = GCFSFlag (flowin, [], "inflag", subp) in
  [cstr1 ; cstr2 ; cstr3 ; cstr4]

let filter_cstrgen_cfa {f_name ; f_inputs ; f_outputs} ((flowin, _), (flowout, _)) =
  [GCFSFilter (flowin, flowout, f_name, f_inputs, f_outputs)]

let merge_cstrgen_cfa vars outs (_, (flowout, _)) =
  List.map (fun (flowin, _) -> GCFSFlag (flowin, [], flowout, [])) outs

let cstrgen_cfa_base_interp t0 =
  {
    empty_interp = empty_cstrgen_cfa ;
    hook_interp = hook_cstrgen_cfa t0 ;
    filter_interp = filter_cstrgen_cfa ;
    merge_interp = merge_cstrgen_cfa ;
    before_merge_interp = (fun _ -> []) ;
  }

let cstrgen_cfa_interp env t0 =
  accumulating_interpretation
    (combine_interpretation_inout
       (interpretation_product freevar_generator_interp
          (typing_interp env))
       (cstrgen_cfa_base_interp t0))

(*
let generate_rule_constraints_cfa ff (filt, ({r_name ; constructor ; constructor_arguments ; skeleton} as r)) =
  let ca = List.map string_of_var constructor_arguments in
  Format.fprintf ff "@[<v 2>let constraints_%s pp%a = @,[@[<hov>" r_name
    (print_list_prefixed " " Format.pp_print_string) ca;
  let t0 = Cterm (constructor, List.map (fun v -> Vterm v) ca) in
  fv_list := ["inflag"];
  let init = (("inflag", (make_init_env filt r, t0)), []) in
  let ((_, (env, _)), cstrs) = interpret_skeleton cstrgen_cfa_interp init skeleton in
  let fvl = !fv_list in
  let env = get_env_map env in
  List.iter (fun v -> Format.fprintf ff "CFSSort ((%S, pp), \"flag\");@ " v) fvl;
  VMap.iter (fun v s -> Format.fprintf ff "CFSSort ((%S, pp), %S);@ " (string_of_var v) (string_of_sort s)) env;
  List.iter2 (fun a at -> Format.fprintf ff "CFSEq_%s ((%S, pp), %s);@ " (string_of_sort at) a a) ca constructor.c_input_sorts;
  List.iter (fun c -> Format.fprintf ff "%a;@ " print_gconstraint_fs c) cstrs;
  Format.fprintf ff "@]]@]@,@,"
*)










let pp_print_constraint_dispatch ff env =
  let pp_print_rule_line ff {r_name ; r_constructor ; r_constructor_arguments} =
    let ca = r_constructor_arguments in
    Format.fprintf ff "| %s%a -> constraints_%s pp %a"
      (get_alpha_name r_constructor)
      (pp_print_constructor_args Format.pp_print_string) ca
      r_name
      (Format.pp_print_list ~pp_sep:(pp_sep_string " ") Format.pp_print_string) ca
  in
  let rules = List.map (fun (name, h) ->
      (name, List.map snd (SMap.bindings h.hd_rules))) (SMap.bindings env.env_hook_definitions) in
  Format.fprintf ff "@[<v>%a@,@]"
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,")
       (fun ff (ps_name, rules) ->
          Format.fprintf ff "@[<v 2>let constraints_%s pp term =@,match term with@,%a@]" ps_name
            (Format.pp_print_list pp_print_rule_line) rules
       )
    ) rules

let generate_run ff env =
  let ps = get_term_program_types env in
  Format.fprintf ff
    "@[<v 2>\
     let generate_constraints gent program =@,\
       let c = ref [] in@,\
       gent %a[] program;@,\
       !c\
     @]"
    (print_list_suffixed " " (fun ff s -> Format.fprintf ff "(fun pp term -> c := constraints_%s pp term %@ !c)" s)) ps

let pp_print_constraint_type ff env =
  let bs = get_term_base_types env in
  let ps = get_term_program_types env in
  let pp_print_targs = pp_print_base_generic_args bs in
  Format.fprintf ff
    "@[<v>\
    type %tcstr =@,\
    | CSort of pvar * string@,\
    | CFilter of filter_constraint@,\
    | CSub of pvar * pvar@,\
    %a@,\
    %a@]@,@,"
    pp_print_targs
    (Format.pp_print_list (fun ff s -> Format.fprintf ff "| CEq_%s of pvar * '%s" s s)) bs
    (Format.pp_print_list (fun ff s -> Format.fprintf ff "| CEq_%s of pvar * %t%s" s pp_print_targs s)) ps

let pp_print_constraint_fs_type ff env =
  let bs = get_term_base_types env in
  let ps = get_term_program_types env in
  let pp_print_targs = pp_print_base_generic_args bs in
  Format.fprintf ff
    "@[<v>\
    type %tcstr =@,\
    | CFSSort of pvar * string@,\
    | CFSFilter of filter_constraint@,\
    | CFSSub of pvar * pvar * pvar@,\
    | CFSFlag of pvar * pvar@,\
    | CFSFlagTest of pvar * pvar * pvar@,\
    %a@,\
    %a@]@,@,"
    pp_print_targs
    (Format.pp_print_list (fun ff s -> Format.fprintf ff "| CFSEq_%s of pvar * '%s" s s)) bs
    (Format.pp_print_list (fun ff s -> Format.fprintf ff "| CFSEq_%s of pvar * %t%s" s pp_print_targs s)) ps

let pp_print_gent_signature ff env =
  let bs = get_term_base_types env in
  let ps = get_term_program_types env in
  let pp_print_targs = pp_print_base_generic_args bs in
  Format.fprintf ff "%a" (Format.pp_print_list (fun ff s ->
      Format.fprintf ff "val gent_%s : %a -> path -> %t%s -> unit" s
        (Format.pp_print_list ~pp_sep:(pp_sep_string " -> ")
           (fun ff s -> Format.fprintf ff "(path -> %t%s -> unit)" pp_print_targs s))
        ps pp_print_targs s
    )) ps

let pp_print_primitives_signature ff env =
  Format.fprintf ff
    "@[<v 2>\
    module type PRIM = sig@,\
      type filter_constraint@,";
  Format.fprintf ff "%a@," (Format.pp_print_list (fun ff (_, {fs_name; fs_input_sorts; fs_output_sorts}) ->
      Format.fprintf ff "val %s : %a -> filter_constraint" fs_name
        (Format.pp_print_list ~pp_sep:(pp_sep_string " -> ") (fun ff _ -> Format.fprintf ff "pvar"))
        (fs_input_sorts @ fs_output_sorts))) (SMap.bindings env.env_filter_signatures);
  Format.fprintf ff "%a@]@,end@,@," pp_print_gent_signature env

let pp_print_primitives_signature_fs ff env =
  Format.fprintf ff
    "@[<v 2>\
    module type PRIMFS = sig@,\
      type filter_constraint@,";
  Format.fprintf ff "%a@," (Format.pp_print_list (fun ff (_, {fs_name; fs_input_sorts; fs_output_sorts}) ->
      Format.fprintf ff "val %s : pvar -> pvar -> %a -> filter_constraint" fs_name
        (Format.pp_print_list ~pp_sep:(pp_sep_string " -> ") (fun ff _ -> Format.fprintf ff "pvar"))
        (fs_input_sorts @ fs_output_sorts))) (SMap.bindings env.env_filter_signatures);
  Format.fprintf ff "%a@]@,end@,@," pp_print_gent_signature env

let pp_print_gent ff env =
  let const = get_constructors_by_sort env in
  let ps = get_term_program_types env in
  let pp_print_fargs ff = Format.pp_print_list ~pp_sep:(pp_sep_string " ")
      (fun ff s -> Format.fprintf ff "f_%s" s) ff ps
  in
  Format.fprintf ff "@[<v>@[<v 2>let rec %a@]@]"
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@]@,@,@[<v 2>and ")
       (fun ff (ps_name, const) ->
          Format.fprintf ff "gent_%s %t pp term =@,match term with@,%a" ps_name pp_print_fargs
            (Format.pp_print_list (fun ff c ->
                 Format.fprintf ff "| %s%a -> f_%s pp term%a"
                   (get_alpha_name c.cs_name)
                   (pp_print_constructor_args (fun ff i -> Format.fprintf ff "x_t%d" i))
                   (List.mapi (fun i _ -> i) c.cs_input_sorts)
                   ps_name
                   (Format.pp_print_list ~pp_sep:(pp_sep_string "") (fun ff (is, i) ->
                        match is with
                        | Program is_name ->
                          Format.fprintf ff "; gent_%s %t (%d :: pp) x_t%d" is_name pp_print_fargs i i
                        | _ -> ()
                      ))
                   (List.mapi (fun i is -> (is, i)) c.cs_input_sorts)
               )) const
       )
    ) const

let pp_print_abstract_module ff env =
  let bs = get_term_base_types env in
  let fs =
    env.env_sorts
    |> SMap.to_seq
    |> Seq.filter_map (fun (_, s) -> match s with | Flow s -> if List.mem s bs then None else Some s | _ -> None)
    |> List.of_seq |> List.sort compare
  in
  Format.fprintf ff "@[<v 2>module type ABSTRACT = sig@,";
  Format.fprintf ff "%a@,%a@,@,"
    (Format.pp_print_list (fun ff s ->
         Format.fprintf ff "module %s : PRINTABLE" (get_alpha_name s))) bs
    (Format.pp_print_list (fun ff s ->
         Format.fprintf ff "module %s : DOMAIN" (get_alpha_name s))) fs;
  let pp_print_product_type printer ff l =
    match l with
    | [] -> Format.fprintf ff "unit"
    | [x] -> Format.fprintf ff "%a" printer x
    | _ -> Format.fprintf ff "(%a)" (Format.pp_print_list ~pp_sep:(pp_sep_string " * ") printer) l
  in
  Format.fprintf ff "%a"
    (Format.pp_print_list (fun ff {fs_name; fs_input_sorts; fs_output_sorts} ->
         Format.fprintf ff "val %s : %a -> %a option"
           fs_name
           (Format.pp_print_list ~pp_sep:(pp_sep_string " -> ")
              (fun ff s -> Format.fprintf ff "%s.t" (get_alpha_name (string_of_sort s))))
           fs_input_sorts
           (pp_print_product_type
              (fun ff s -> Format.fprintf ff "%s.t" (get_alpha_name (string_of_sort s))))
           fs_output_sorts
       )) (List.map snd (SMap.bindings env.env_filter_signatures));
  Format.fprintf ff "@,@,%a@]@,end" pp_print_gent_signature env

let pp_print_makestore_functor ff (name, bss, fss) =
  Format.fprintf ff "@[<v 2>module %s (A : ABSTRACT) = struct@,open A@," name;
  Format.fprintf ff "type t = %a"
    (Format.pp_print_list ~pp_sep:(fun ff () -> ()) (fun ff s ->
         Format.fprintf ff "%s.t PMap.t * " (get_alpha_name s)
       )) bss;
  Format.fprintf ff "%aPSet.t@,@,"
    (Format.pp_print_list ~pp_sep:(fun ff () -> ()) (fun ff s ->
         Format.fprintf ff "%s.t option PMap.t * " (get_alpha_name s)
       )) fss;
  Format.fprintf ff "let empty wid = (%awid)@,@,"
    (Format.pp_print_list ~pp_sep:(fun ff () -> ()) (fun ff _ ->
         Format.fprintf ff "PMap.empty, "
       )) (bss @ fss);
  let pp_print_t_pat ff (pref, wid) =
    Format.fprintf ff "((%a%s) : t)"
      (Format.pp_print_list ~pp_sep:(fun ff () -> ()) (fun ff s ->
           Format.fprintf ff "%s_%s, " pref s
         )) (bss @ fss) wid
  in
  Format.fprintf ff "%a@," (Format.pp_print_list (fun ff s ->
      Format.fprintf ff "let get_%s v %a = Some (PMap.find v env_%s)@," s pp_print_t_pat ("env", "_") s;
      Format.fprintf ff "let set_%s v x %a =@,  let env_%s = match x with None -> env_%s | Some x -> PMap.add v x env_%s in@,  %a"
        s pp_print_t_pat ("env", "wid") s s s pp_print_t_pat ("env", "wid")
    )) bss;
  Format.fprintf ff "%a@,@," (Format.pp_print_list (fun ff s ->
      Format.fprintf ff "let get_%s v %a = default None v env_%s@,"
        s pp_print_t_pat ("env", "_") s;
      Format.fprintf ff "let set_%s v x %a =@,  let env_%s = add_wid v x env_%s wid %s.lub %s.widen in@,  %a"
        s pp_print_t_pat ("env", "wid") s s (get_alpha_name s) (get_alpha_name s) pp_print_t_pat ("env", "wid")
    )) fss;
  Format.fprintf ff "@[<v 2>let leq %a %a =@,%a@]@,@,"
    pp_print_t_pat ("env1", "_") pp_print_t_pat ("env2", "_")
    (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff " &&@,") (fun ff s ->
         Format.fprintf ff "env_leq env1_%s env2_%s %s.leq" s s (get_alpha_name s)
       )) fss;
  Format.fprintf ff "@[<v 2>let pp_print ff %a =@,%a@]"
    pp_print_t_pat ("env", "_")
    (Format.pp_print_list (fun ff s ->
         Format.fprintf ff "env_print ff \"%%a |-> %%a%@,\" env_%s %s.pp_print;" s (get_alpha_name s)
       )) (bss @ fss);
  Format.fprintf ff "@]@,end"

let pp_print_makeprim_functor ff env =
  let ps = get_term_program_types env in
  Format.fprintf ff
    "@[<v 2>module MakePrim (A : ABSTRACT) = struct@,\
     module Store = MakeStore(A)@,\
     open A@,\
     @,\
     type filter_constraint = string * pvar list * pvar list * (Store.t -> Store.t)@,@,";
  Format.fprintf ff "%a" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,") (fun ff filt ->
      let name = filt.fs_name in
      let inv = List.mapi (fun i s -> (i, string_of_sort s)) filt.fs_input_sorts in
      let outv = List.mapi (fun i s -> (i, string_of_sort s)) filt.fs_output_sorts in
      let pp_var_name s ff (vn, _) = Format.fprintf ff "%s%d" s vn in
      Format.fprintf ff "let %s %a %a =@,"
        name
        (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (pp_var_name "in")) inv
        (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (pp_var_name "out")) outv;
      Format.fprintf ff "  (%S, [%a], [%a], fun store ->@,"
        name
        (Format.pp_print_list ~pp_sep:(pp_sep_string "; ") (pp_var_name "in")) inv
        (Format.pp_print_list ~pp_sep:(pp_sep_string "; ") (pp_var_name "out")) outv;
      Format.fprintf ff "    @[<v>let open Store in@,";
      Format.fprintf ff "match bind%d %s %a with@,| None -> store@,| @[<v>Some (%a) ->@,"
        (List.length inv) name
        (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (fun ff (vn, s) ->
             Format.fprintf ff "(get_%s in%d store)" s vn)) inv
        (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (pp_var_name "res")) outv;
      Format.fprintf ff "%astore)@]@]" (Format.pp_print_list ~pp_sep:(pp_sep_string "") (fun ff (vn, s) ->
          Format.fprintf ff "set_%s out%d (Some res%d) %@%@@," s vn vn
        )) outv
    )) (List.map snd (SMap.bindings env.env_filter_signatures));
  Format.fprintf ff "@,@,%a@]@,end" (Format.pp_print_list (fun ff s ->
      Format.fprintf ff "let gent_%s = gent_%s" s s
    )) ps

let pp_print_makeprim_functor_fs ff env =
  let ps = get_term_program_types env in
  Format.fprintf ff
    "@[<v 2>module MakePrimFs (A : ABSTRACT) = struct@,\
     module Store = MakeStoreFs(A)@,\
     open A@,\
     @,\
     type filter_constraint = string * pvar list * pvar list * (Store.t -> Store.t)@,@,";
  Format.fprintf ff "%a" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "@,@,") (fun ff filt ->
      let name = filt.fs_name in
      let inv = List.mapi (fun i s -> (i, string_of_sort s)) filt.fs_input_sorts in
      let outv = List.mapi (fun i s -> (i, string_of_sort s)) filt.fs_output_sorts in
      let pp_var_name s ff (vn, _) = Format.fprintf ff "%s%d" s vn in
      Format.fprintf ff "let %s inflag outflag %a %a =@,"
        name
        (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (pp_var_name "in")) inv
        (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (pp_var_name "out")) outv;
      Format.fprintf ff "  (%S, inflag :: [%a], outflag :: [%a], fun store ->@,"
        name
        (Format.pp_print_list ~pp_sep:(pp_sep_string "; ") (pp_var_name "in")) inv
        (Format.pp_print_list ~pp_sep:(pp_sep_string "; ") (pp_var_name "out")) outv;
      Format.fprintf ff "    @[<v>let open Store in@,";
      Format.fprintf ff "if (get_flag inflag store) = None then store else@,";
      Format.fprintf ff "match bind%d %s %a with@,| None -> store@,| @[<v>Some (%a) ->@,"
        (List.length inv) name
        (Format.pp_print_list ~pp_sep:(pp_sep_string " ") (fun ff (vn, s) ->
             Format.fprintf ff "(get_%s in%d store)" s vn)) inv
        (Format.pp_print_list ~pp_sep:(pp_sep_string ", ") (pp_var_name "res")) outv;
      Format.fprintf ff "set_flag outflag (Some ()) %@%@@,";
      Format.fprintf ff "%astore)@]@]" (Format.pp_print_list ~pp_sep:(pp_sep_string "") (fun ff (vn, s) ->
          Format.fprintf ff "set_%s out%d (Some res%d) %@%@@," s vn vn
        )) outv
    )) (List.map snd (SMap.bindings env.env_filter_signatures));
  Format.fprintf ff "@,@,%a@]@,end" (Format.pp_print_list (fun ff s ->
      Format.fprintf ff "let gent_%s = gent_%s" s s
    )) ps


let pp_print_constraints_module ff env =
  Format.fprintf ff "@[<v 2>module Constraints (P : PRIM) = struct@,include P@,";
  Format.fprintf ff "%a" pp_print_constraint_type env;
  let rules = List.flatten (List.map (fun (_, h) ->
      List.map snd (SMap.bindings h.hd_rules)) (SMap.bindings env.env_hook_definitions)) in
  List.iter (Format.fprintf ff "%a" (generate_rule_constraints env)) rules;
  Format.fprintf ff "%a@," pp_print_constraint_dispatch env;
  Format.fprintf ff "%a" generate_run env;
  Format.fprintf ff "@]@,end"

let pp_print_constraints_module_fs ff env =
  Format.fprintf ff "@[<v 2>module ConstraintsFs (P : PRIMFS) = struct@,include P@,";
  Format.fprintf ff "%a" pp_print_constraint_fs_type env;
  let rules = List.flatten (List.map (fun (_, h) ->
      List.map snd (SMap.bindings h.hd_rules)) (SMap.bindings env.env_hook_definitions)) in
  List.iter (Format.fprintf ff "%a" (generate_rule_constraints_fs env)) rules;
  Format.fprintf ff "%a@," pp_print_constraint_dispatch env;
  Format.fprintf ff "%a" generate_run env;
  Format.fprintf ff "@]@,end"

let generate_constraints env atoms =
  let bss = get_term_base_types env in
  let fss =
    env.env_sorts
    |> SMap.to_seq
    |> Seq.filter_map (fun (_, s) -> match s with | Flow s -> if List.mem s bss then None else Some s | _ -> None)
    |> List.of_seq |> List.sort compare
  in
  Format.printf "@[<v>open Cstr@,@,";
  Format.printf "%a@," pp_print_ast_types env;
  Format.printf "%a@,@," pp_print_gent env;
  Format.printf "%a@,@," pp_print_abstract_module env;

  Format.printf "%a" pp_print_primitives_signature env;
  Format.printf "%a@,@," pp_print_constraints_module env;
  Format.printf "%a@,@," pp_print_makestore_functor ("MakeStore", bss, fss);
  Format.printf "%a@,@," pp_print_makeprim_functor env;

  Format.printf "%a" pp_print_primitives_signature_fs env;
  Format.printf "%a@,@," pp_print_constraints_module_fs env;
  Format.printf "%a@,@," pp_print_makestore_functor ("MakeStoreFs", bss, fss @ ["flag"]);
  Format.printf "%a" pp_print_makeprim_functor_fs env;

  Format.printf "@]@."

let getlines ic =
  let rec f acc =
    try f ((input_line ic) :: acc) with End_of_file -> String.concat "\n" (List.rev acc)
  in f []

let report_error filename start_pos end_pos =
  let open Lexing in
  let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
  let end_col = end_pos.pos_cnum - start_pos.pos_bol + 1 in
  Format.eprintf "File %S, line %d, characters %d-%d:@." filename start_pos.pos_lnum start_col end_col

let () =
  let atoms, env = Util.parse_from_file Parser.main Sys.argv.(1) in
  let () = Skeleton.check_well_typed env in
  generate_constraints env atoms

type program_point
type 'a set = Set
type ('a, 'b) map = Map
type globalcontext
type localcontext
type value

type 'a typedesc =
  | BaseType : string -> value typedesc
  | Localcontext : localcontext typedesc
  | Globalcontext : globalcontext typedesc
  | ProgramPoint : program_point typedesc
  | SetType : 'a typedesc -> 'a set typedesc
  | MapType : 'a typedesc * 'b typedesc -> ('a, 'b) map typedesc

type ('a, 'b) eq =
  | Eq : ('a, 'a) eq

let rec print_typedesc : type a. _ -> a typedesc -> unit = fun ff -> function
  | BaseType s -> Format.fprintf ff "%s" s
  | Localcontext -> Format.fprintf ff "localcontext"
  | Globalcontext -> Format.fprintf ff "globalcontext"
  | ProgramPoint -> Format.fprintf ff "pp"
  | SetType t -> Format.fprintf ff "%a set" print_typedesc t
  | MapType (t1, t2) -> Format.fprintf ff "(%a, %a) map" print_typedesc t1 print_typedesc t2

let rec check_type_eq : type a b. a typedesc -> b typedesc -> (a, b) eq = fun t1 t2 ->
  match t1, t2 with
  | BaseType s1, BaseType s2 -> if s1 = s2 then Eq else failwith "Incompatible base types"
  | Localcontext, Localcontext -> Eq
  | Globalcontext, Globalcontext -> Eq
  | ProgramPoint, ProgramPoint -> Eq
  | SetType t1, SetType t2 -> (match check_type_eq t1 t2 with Eq -> Eq)
  | MapType (k1, v1), MapType (k2, v2) -> (match check_type_eq k1 k2, check_type_eq v1 v2 with Eq, Eq -> Eq)
  | _ -> failwith "Incompatible types"

type ('a, 'b) with_type = {
  desc : 'a ;
  typ : 'b typedesc
}

(*
type 'a genlvalue_desc =
  | ProgramVar : string * path -> (globalcontext, (localcontext, value) map) map genlvalue_desc
  | ProgramVarPPS : string * path -> (globalcontext, (localcontext, program_point set) map) map genlvalue_desc
  | MapGet : ('a, 'b) map genlvalue * 'a genvalue -> 'b genlvalue_desc
  | InputVar : (program_point, (globalcontext, value) map) map genlvalue_desc
  | OutputVar : (program_point, (globalcontext, (localcontext, value) map) map) map genlvalue_desc
  | OtherVar : string -> value genlvalue_desc

and 'a genlvalue = 'a genlvalue_desc with_type

and 'a genvalue_desc =
  | LValue : 'a genlvalue -> 'a genvalue_desc
  | Function : string * genvalue_EBB list -> value genvalue_desc
  | LocalVar : string -> 'a genvalue_desc
  | VLet : 'a genvalue * ('a genvalue -> 'b genvalue) -> 'b genvalue_desc
  | ProgramPoint : path -> program_point genvalue_desc

and 'a genvalue = 'a genvalue_desc with_type

and genvalue_EBB =
  | EBBgen : 'a genvalue -> genvalue_EBB

and genlvalue_EBB =
  | EBBlgen : 'a genlvalue -> genlvalue_EBB
*)

type ('a, 'b) typecons = Typecons
type typenil = Typenil

type 'a typelist_desc =
  | TypeNil : typenil typelist_desc
  | TypeCons : 'a typedesc * 'b typelist_desc -> ('a, 'b) typecons typelist_desc

let rec check_typelist_eq : type a b. a typelist_desc -> b typelist_desc -> (a, b) eq = fun l1 l2 ->
  match l1, l2 with
  | TypeNil, TypeNil -> Eq
  | TypeCons (t1, l1), TypeCons (t2, l2) -> (match check_type_eq t1 t2, check_typelist_eq l1 l2 with Eq, Eq -> Eq)
  | _ -> failwith "List lengths are incompatible"

type ('a, 'b) func_desc = string * 'a typelist_desc * 'b typedesc
type ('a, 'b) predicate_desc = string * 'a typelist_desc * 'b typelist_desc

type 'a genlvalue_desc =
  | ProgramVar : string * path -> (globalcontext, (localcontext, 'a) map) map genlvalue_desc
  | MapGet : ('a, 'b) map genlvalue * 'a genvalue -> 'b genlvalue_desc
  | InputVar : string -> (program_point, (globalcontext, 'a) map) map genlvalue_desc
  | OutputVar : string -> (program_point, (globalcontext, (localcontext, 'a) map) map) map genlvalue_desc
  | OtherVar : string -> 'a genlvalue_desc

and 'a genlvalue = ('a genlvalue_desc, 'a) with_type

and 'a genvalue_list =
  | VNil : typenil genvalue_list
  | VCons : 'a genvalue * 'b genvalue_list -> ('a, 'b) typecons genvalue_list

and 'a genvalue_desc =
  | LValue : 'a genlvalue -> 'a genvalue_desc
  | Function : ('a, 'b) func_desc * 'a genvalue_list -> 'b genvalue_desc
  | LocalVar : string -> 'a genvalue_desc
  | VLet : 'a genvalue * ('a genvalue -> 'b genvalue) -> 'b genvalue_desc

and 'a genvalue = ('a genvalue_desc, 'a) with_type

type 'a genlvalue_list =
  | LNil : typenil genlvalue_list
  | LCons : 'a genlvalue * 'b genlvalue_list -> ('a, 'b) typecons genlvalue_list

(*
type genconstraint =
  | True : genconstraint
  | Conj : genconstraint * genconstraint -> genconstraint
  | Forall : 'a set genvalue * ('a genvalue -> genconstraint) -> genconstraint
  | MapForall : ('a, 'b) map genvalue * ('a genvalue -> 'b genvalue -> genconstraint) -> genconstraint
  | Predicate : string * genvalue_EBB list * genlvalue_EBB list -> genconstraint
  | CLet : 'a genvalue * ('a genvalue -> genconstraint) -> genconstraint
*)

type ('a, 'b) static_map_description =
  | EmptyStaticMap : ('a, 'b) static_map_description
  | SingletonStaticMap : 'a genvalue * 'b -> ('a, 'b) static_map_description
  | Join2StaticMap : ('a, 'b) static_map_description * ('a, 'b) static_map_description -> ('a, 'b) static_map_description
  | JoinStaticMap : 'c set genvalue * ('c genvalue -> ('a, 'b) static_map_description) -> ('a, 'b) static_map_description
  | LetStaticMap : 'c genvalue * ('c genvalue -> ('a, 'b) static_map_description) -> ('a, 'b) static_map_description

type genconstraint =
  | True
  | Conj : genconstraint * genconstraint -> genconstraint
  | Forall : 'a set genvalue * ('a genvalue -> genconstraint) -> genconstraint
  | MapForall : ('a, 'b) map genvalue * ('a genvalue -> 'b genvalue -> genconstraint) -> genconstraint
  | Predicate : ('a, 'b) predicate_desc * 'a genvalue_list * 'b genlvalue_list -> genconstraint
  | CLet : 'a genvalue * ('a genvalue -> genconstraint) -> genconstraint

(*
type 'a static_map_description =
  | EmptyStaticMap
  | SingletonStaticMap of genvalue * 'a
  | Join2StaticMap of 'a static_map_description * 'a static_map_description
  | JoinStaticMap of genvalue * (genvalue -> 'a static_map_description)
  | LetStaticMap of genvalue * (genvalue -> 'a static_map_description)
*)

type 'a static_set_description =
  | EmptyStaticSet : 'a static_set_description
  | SingletonStaticSet : 'a genvalue -> 'a static_set_description
  | Join2StaticSet : 'a static_set_description * 'a static_set_description -> 'a static_set_description
  | JoinStaticSet : 'b set genvalue * ('b genvalue -> 'a static_set_description) -> 'a static_set_description
  | LetStaticSet : 'b genvalue * ('b genvalue -> 'a static_set_description) -> 'a static_set_description

(*
type static_set_description =
  | EmptyStaticSet
  | SingletonStaticSet of genvalue
  | Join2StaticSet of static_set_description * static_set_description
  | JoinStaticSet of genvalue * (genvalue -> static_set_description)
  | LetStaticSet of genvalue * (genvalue -> static_set_description)
*)

let rec static_map_forall (sm : ('a, 'b) static_map_description) (f : 'a genvalue -> 'b -> genconstraint) : genconstraint =
  (* let rec static_map_forall (sm : 'a static_map_description) (f : genvalue -> 'a -> genconstraint) : genconstraint = *)
  match sm with
  | EmptyStaticMap -> True
  | SingletonStaticMap (a, b) -> CLet (a, fun x -> f x b)
  | Join2StaticMap (sm1, sm2) -> Conj (static_map_forall sm1 f, static_map_forall sm2 f)
  | JoinStaticMap (x, fx) -> Forall (x, fun v -> static_map_forall (fx v) f)
  | LetStaticMap (x, fx) -> CLet (x, fun v -> static_map_forall (fx v) f)

let rec static_set_forall (s : 'a static_set_description) (f : 'a genvalue -> genconstraint) : genconstraint =
  (* let rec static_set_forall (s : static_set_description) (f : genvalue -> genconstraint) : genconstraint = *)
  match s with
  | EmptyStaticSet -> True
  | SingletonStaticSet a -> CLet (a, f)
  | Join2StaticSet (s1, s2) -> Conj (static_set_forall s1 f, static_set_forall s2 f)
  | JoinStaticSet (x, fx) -> Forall (x, fun v -> static_set_forall (fx v) f)
  | LetStaticSet (x, fx) -> CLet (x, fun v -> static_set_forall (fx v) f)


type ('a, 'b) filter_constraint =
  globalcontext genvalue -> localcontext genvalue -> 'a genvalue_list ->
    (localcontext, 'b genlvalue_list -> genconstraint) static_map_description
(*
type filter_constraint = genvalue -> genvalue -> genvalue list -> (genlvalue list -> genconstraint) static_map_description
*)

let globalcontext_type = Globalcontext
let localcontext_type = Localcontext
let flag_type = BaseType "flag"
let pp_type = ProgramPoint
let pps_type = SetType ProgramPoint


let mapget (map : _ genlvalue) arg =
  let MapType (t1, t2) = map.typ in
  assert (arg.typ = t1);
  { desc = MapGet (map, arg) ; typ = t2 }


type genvalue_ebb = VEBB : 'a genvalue -> genvalue_ebb
type genlvalue_ebb = LEBB : 'a genlvalue -> genlvalue_ebb

type typedesc_ebb = TEBB : 'a typedesc -> typedesc_ebb
(* type typelist_ebb = TEBB : 'a typelist_desc -> typelist_ebb *)
type func_desc_ebb = FEBB : ('a, 'b) func_desc -> func_desc_ebb
type predicate_desc_ebb = PEBB : ('a, 'b) predicate_desc -> predicate_desc_ebb
type type_value_ebb = TVEBB : 'a typedesc * (genvalue_ebb SMap.t -> 'a genvalue) -> type_value_ebb
type type_lvalue_ebb = TLEBB : 'a typedesc * (genvalue_ebb SMap.t -> 'a genlvalue) -> type_lvalue_ebb

type 'a type_value_list =
  | TVLNil : typenil type_value_list
  | TVLCons : (genvalue_ebb SMap.t -> 'a genvalue) * 'b type_value_list -> ('a, 'b) typecons type_value_list
type type_value_list_ebb = TVLEBB : 'a typelist_desc * 'a type_value_list -> type_value_list_ebb
type 'a type_lvalue_list =
  | TLLNil : typenil type_lvalue_list
  | TLLCons : (genvalue_ebb SMap.t -> 'a genlvalue) * 'b type_lvalue_list -> ('a, 'b) typecons type_lvalue_list
type type_lvalue_list_ebb = TLLEBB : 'a typelist_desc * 'a type_lvalue_list -> type_lvalue_list_ebb

type tenv = {
  tenv_local_vars : typedesc_ebb SMap.t ;
  tenv_fun_descs : func_desc_ebb SMap.t ;
  tenv_pred_descs : predicate_desc_ebb SMap.t ;
  tenv_extra_vars : typedesc_ebb SMap.t ;
}

let rec type_value_list_from_list = function
  | [] -> TVLEBB (TypeNil, TVLNil)
  | TVEBB (t, v) :: l ->
    let TVLEBB (tl, vl) = type_value_list_from_list l in
    TVLEBB (TypeCons (t, tl), TVLCons (v, vl))

let rec type_value_list_to_value_list : type a. _ -> a type_value_list -> a genvalue_list = fun env -> function
  | TVLNil -> VNil
  | TVLCons (f, l) -> VCons (f env, type_value_list_to_value_list env l)

let rec type_lvalue_list_from_list = function
  | [] -> TLLEBB (TypeNil, TLLNil)
  | TLEBB (t, v) :: l ->
    let TLLEBB (tl, vl) = type_lvalue_list_from_list l in
    TLLEBB (TypeCons (t, tl), TLLCons (v, vl))

let rec type_lvalue_list_to_lvalue_list : type a. _ -> a type_lvalue_list -> a genlvalue_list = fun env -> function
  | TLLNil -> LNil
  | TLLCons (f, l) -> LCons (f env, type_lvalue_list_to_lvalue_list env l)

let add_to_tenv tenv name tx = { tenv with tenv_local_vars = SMap.add name (TEBB tx) tenv.tenv_local_vars }

(*
let rec type_lvalue : _ -> plvalue -> type_lvalue_ebb = fun tenv -> function
  | PLVar x ->
    if SMap.mem x tenv.tenv_local_vars then failwith "Local var used as lvalue"
    else if SMap.mem x tenv.tenv_extra_vars then
      let (TEBB t) = SMap.find x tenv.tenv_extra_vars in
      TLEBB (t, fun env -> { desc = OtherVar x ; typ = t })
    else
      failwith "Unknown variable"
  | PLMapGet (m, x) ->
    let (TLEBB (tm, m)) = type_lvalue tenv m in
    let (TVEBB (tx, x)) = type_value tenv x in
    (match tm with
     | MapType (t1, t2) ->
       (match check_type_eq tx t1 with
        Eq -> TLEBB (t2, fun env -> mapget (m env) (x env)))
     | _ -> failwith "Type error")

and type_value : _ -> pvalue -> type_value_ebb = fun tenv -> function
  | PLValue (PLVar x) when SMap.mem x tenv.tenv_local_vars ->
    let (TEBB tx) = SMap.find x tenv.tenv_local_vars in
    TVEBB (tx, fun env -> let (VEBB v) = SMap.find x env in match check_type_eq tx v.typ with Eq -> v)
  | PLValue lv ->
    let (TLEBB (tv, v)) = type_lvalue tenv lv in
    TVEBB (tv, fun env -> { desc = LValue (v env) ; typ = tv })
  | PFun (fname, args) ->
    let (FEBB ((_, targs, t) as f)) = SMap.find fname tenv.tenv_fun_descs in
    let (TVLEBB (targs2, largs)) = type_value_list_from_list (List.map (type_value tenv) args) in
    let Eq = check_typelist_eq targs targs2 in
    TVEBB (t, fun env -> { desc = Function (f, type_value_list_to_value_list env largs) ; typ = t })
  | PLet (name, v, body) ->
    let (TVEBB (tx, x)) = type_value tenv v in
    let (TVEBB (tb, b)) = type_value (add_to_tenv tenv name tx) body in
    TVEBB (tb, (fun env -> { desc = VLet (x env, fun xv -> b (SMap.add name (VEBB xv) env)) ; typ = tb }))

let rec type_constraint : _ -> pconstraint -> _ -> genconstraint = fun tenv -> function
  | PTrue -> (fun env -> True)
  | PConj (c1, c2) ->
    let c1 = type_constraint tenv c1 in
    let c2 = type_constraint tenv c2 in
    (fun env -> Conj (c1 env, c2 env))
  | PForall (name, v, c) ->
    let (TVEBB (ts, s)) = type_value tenv v in
    (match ts with
     | SetType tx ->
       let c = type_constraint (add_to_tenv tenv name tx) c in
       (fun env -> Forall (s env, fun xv -> c (SMap.add name (VEBB xv) env)))
     | _ -> failwith "Type error: set type expected")
  | PMapForall (name1, name2, v, c) ->
    let (TVEBB (tm, m)) = type_value tenv v in
    (match tm with
     | MapType (t1, t2) ->
       let c = type_constraint (add_to_tenv (add_to_tenv tenv name1 t1) name2 t2) c in
       (fun env -> MapForall (m env, fun kv xv -> c (SMap.add name2 (VEBB xv) (SMap.add name1 (VEBB kv) env))))
     | _ -> failwith "Type error: map type expected"
    )
  | PPredicate (pname, args1, args2) ->
    let (PEBB ((_, tp1, tp2) as p)) = SMap.find pname tenv.tenv_pred_descs in
    let (TVLEBB (targs1, largs1)) = type_value_list_from_list (List.map (type_value tenv) args1) in
    let (TLLEBB (targs2, largs2)) = type_lvalue_list_from_list (List.map (type_lvalue tenv) args2) in
    let Eq = check_typelist_eq tp1 targs1 in
    let Eq = check_typelist_eq tp2 targs2 in
    (fun env -> Predicate (p, type_value_list_to_value_list env largs1, type_lvalue_list_to_lvalue_list env largs2))
  | PLetC (name, v, c) ->
    let (TVEBB (tx, x)) = type_value tenv v in
    let c = type_constraint (add_to_tenv tenv name tx) c in
    (fun env -> CLet (x env, fun xv -> c (SMap.add name (VEBB xv) env)))
*)

let pv pp (name, tp) : _ genlvalue = { desc = ProgramVar (name, pp) ; typ = MapType (globalcontext_type, MapType (localcontext_type, tp)) }
let pv_c pp g (name, tp) : _ genlvalue = mapget (pv pp (name, tp)) g
let pv_cc pp g l (name, tp) : _ genlvalue = mapget (pv_c pp g (name, tp)) l

let setflag_prec : (typenil, (value, typenil) typecons) predicate_desc = ("setflag", TypeNil, TypeCons (flag_type, TypeNil))
let set_flag v = Predicate (setflag_prec, VNil, LCons (v, LNil))

let lvalue v : _ genvalue = { desc = LValue v ; typ = v.typ }

let rec type_name : type a. a typedesc -> string = fun t ->
  match t with
  | BaseType s -> s
  | SetType t -> type_name t ^ "_set"
  | MapType (t1, t2) -> type_name t1 ^ "_" ^ type_name t2 ^ "_map"
  | Localcontext -> "localcontext"
  | Globalcontext -> "globalcontext"
  | ProgramPoint -> "pp"

let subset_predicate t = ("subset_" ^ type_name t, TypeCons (t, TypeNil), TypeCons (t, TypeNil))
let mksubset v lv =
  assert (v.typ = lv.typ);
  Predicate (subset_predicate v.typ, VCons (v, VNil), LCons (lv, LNil))

type 'a vardesc_list =
  | VDNil : typenil vardesc_list
  | VDCons : string * 'a typedesc * 'b vardesc_list -> ('a, 'b) typecons vardesc_list

let rec lvalue_list : type a. a genlvalue_list -> a genvalue_list = function
  | LNil -> VNil
  | LCons (lv, l) -> VCons (lvalue lv, lvalue_list l)

let rec variable_lvalues : type a. _ -> _ -> _ -> a vardesc_list -> a genlvalue_list = fun pp sigmas sigma -> function
  | VDNil -> LNil
  | VDCons (name, t, l) ->
    let v = pv_cc pp sigmas sigma (name, t) in
    LCons (v, variable_lvalues pp sigmas sigma l)

let make_filter_constraint pp flag oflag (inputs : 'a vardesc_list) (outputs : 'b vardesc_list) (filter_func : ('a, 'b) filter_constraint) defined =
  MapForall (lvalue (pv pp (flag, flag_type)), fun sigmas vflag ->
  MapForall (vflag, fun sigma _ ->
    let vr = pv_cc pp sigmas in
    static_map_forall
      (filter_func sigmas sigma (lvalue_list (variable_lvalues pp sigmas sigma inputs)))
      (fun sigma1 f ->
         Conj (
           f (variable_lvalues pp sigmas sigma1 outputs),
           List.fold_left (fun c v -> Conj (c, mksubset (lvalue (vr sigma v)) (vr sigma1 v))) (set_flag (vr sigma1 oflag)) defined
         )
      )
  ))

type 'a hook_in_constraint =
  path -> globalcontext genvalue -> localcontext genvalue -> 'a genvalue -> program_point genvalue -> globalcontext static_set_description
type ('a, 'b) hook_out_constraint =
  path -> globalcontext genvalue -> localcontext genvalue -> localcontext genvalue -> 'a genvalue -> program_point genvalue -> 'b genvalue -> localcontext static_set_description

(*
type hook_in_constraint =
  path -> genvalue -> genvalue -> genvalue -> genvalue -> static_set_description
type hook_out_constraint =
  path -> genvalue -> genvalue -> genvalue -> genvalue -> genvalue -> genvalue -> static_set_description
*)
let make_hook_constraint pp flag oflag input term term_type hook_intype hook_outtype output (hookin : 'a hook_in_constraint) (hookout : ('a, 'b) hook_out_constraint) defined =
  MapForall (lvalue (pv pp (flag, flag_type)), fun sigmas vflag ->
  MapForall (vflag, fun sigma _ ->
    let vr sigma v = pv_cc pp sigmas sigma v in
    Forall (lvalue (vr sigma (term, pps_type)), fun pp1 ->
      static_set_forall (hookin pp sigmas sigma (lvalue (vr sigma input)) pp1)
        (fun sigmas1 ->
           Conj (
             mksubset (lvalue (vr sigma input)) (mapget (mapget { desc = InputVar term_type ; typ = MapType (pp_type, MapType (globalcontext_type, hook_intype)) } pp1) sigmas1),
             MapForall (lvalue (mapget (mapget { desc = OutputVar term_type ; typ = MapType (pp_type, MapType (globalcontext_type, MapType (localcontext_type, hook_outtype))) } pp1) sigmas1), fun sigma1 out ->
                 static_set_forall (hookout pp sigmas sigma sigma1 (lvalue (vr sigma input)) pp1 out) (fun sigma2 ->
                     Conj (
                       mksubset out (vr sigma2 output),
                     List.fold_left (fun c v -> Conj (c, (mksubset (lvalue (vr sigma v)) (vr sigma2 v)))) (set_flag (vr sigma2 oflag)) defined
                   )
           )
        ))))))


let fresh =
  let c = ref 0 in
  fun () -> incr c; "v" ^ (string_of_int !c)

let rec value_list_to_list : type c. c genvalue_list -> genvalue_ebb list = function
  | VNil -> []
  | VCons (v, l) -> VEBB v :: value_list_to_list l

let rec lvalue_list_to_list : type c. c genlvalue_list -> genlvalue_ebb list = function
  | LNil -> []
  | LCons (v, l) -> LEBB v :: lvalue_list_to_list l


let rec print_lvalue_desc : type a. _ -> a genlvalue_desc -> unit = fun ff v ->
  match v with
  | ProgramVar (s, pp) -> Format.fprintf ff "ProgramVar (%S, %a[])" s (print_intlist_suffixed " :: ") pp
  | MapGet (v1, v2) -> Format.fprintf ff "MapGet (%a, %a)" print_lvalue v1 print_value v2
  | InputVar s -> Format.fprintf ff "InputVar %S" s
  | OutputVar s -> Format.fprintf ff "OutputVar %S" s
  | OtherVar s -> Format.fprintf ff "OtherVar %S" s

and print_value_desc : type a. _ -> a genvalue_desc -> unit = fun ff v ->
  match v with
  | LValue lv -> Format.fprintf ff "LValue (%a)" print_lvalue lv
  | Function ((f, _, _), args) -> Format.fprintf ff "Function_%s%a" f (pp_print_constructor_args (fun ff (VEBB v) -> print_value ff v)) (value_list_to_list args)
  | VLet (v, body) ->
    let name = fresh () in
    Format.fprintf ff "VLet (%a, fun %s -> %a)" print_value v name print_value (body { desc = (LocalVar name) ; typ = v.typ })
  | LocalVar s -> Format.fprintf ff "%s" s

and print_lvalue : type a. _ -> a genlvalue -> unit = fun ff v -> print_lvalue_desc ff v.desc
and print_value : type a . _ -> a genvalue -> unit = fun ff v -> print_value_desc ff v.desc

(*
and print_value_ebb ff v = match v with EBBgen v -> print_value ff v
and print_lvalue_ebb ff v = match v with EBBlgen v -> print_lvalue ff v
*)


let rec print_constraint ff cstr =
  match cstr with
  | True -> Format.fprintf ff "True"
  | Conj (c1, c2) -> Format.fprintf ff "Conj (%a, %a)" print_constraint c1 print_constraint c2
  | Forall (s, f) ->
    let name = fresh () in
    let SetType t = s.typ in
    Format.fprintf ff "Forall (%a, fun %s -> %a)" print_value s name print_constraint (f { desc = LocalVar name ; typ = t })
  | MapForall (m, f) ->
    let name1 = fresh () in
    let name2 = fresh () in
    let MapType (t1, t2) = m.typ in
    Format.fprintf ff "MapForall (%a, fun %s %s -> %a)" print_value m name1 name2 print_constraint (f { desc = LocalVar name1 ; typ = t1 } { desc = LocalVar name2 ; typ = t2 })
  | Predicate ((name, _, _), arg1, arg2) ->
    Format.fprintf ff "Predicate_%s%a" name (pp_print_constructor_args (fun ff f -> f ff))
      (List.map (fun (VEBB x) ff -> print_value ff x) (value_list_to_list arg1) @ List.map (fun (LEBB x) ff -> print_lvalue ff x) (lvalue_list_to_list arg2))
  | CLet (v, body) ->
    let name = fresh () in
    Format.fprintf ff "CLet (%a, fun %s -> %a)" print_value v name print_constraint (body { desc = LocalVar name ; typ = v.typ })


let rec print_lvalue_c : type a. _ -> a genlvalue -> unit = fun ff v ->
  match v.desc with
  | ProgramVar (s, pp) -> Format.fprintf ff "(ProgramVar (%S, %a[]))" s (print_intlist_suffixed " :: ") pp
  | MapGet (v1, v2) -> Format.fprintf ff "(MapGet (%a, %a))" print_lvalue_c v1 print_value_c v2
  | InputVar s -> Format.fprintf ff "InputVar_%s" s
  | OutputVar s -> Format.fprintf ff "OutputVar_%s" s
  | OtherVar s -> Format.fprintf ff "OtherVar_%s" s

and print_value_c : type a. _ -> a genvalue -> unit = fun ff v ->
  match v.desc with
  | LValue lv -> Format.fprintf ff "(compute_lvalue %a)" print_lvalue_c lv
  | Function ((f, _, _), args) -> Format.fprintf ff "(function_%s%a)" f (print_list_prefixed " " (fun ff (VEBB x) -> print_value_c ff x)) (value_list_to_list args)
  | VLet (v, body) ->
    let name = fresh () in
    Format.fprintf ff "(@[<v>let %s = %a in@ %a@])" name print_value_c v print_value_c (body { desc = LocalVar name ; typ = v.typ })
  | LocalVar s -> Format.fprintf ff "%s" s

(*
and print_value_c_ebb ff v = match v with EBBgen v -> print_value_c ff v
and print_lvalue_c_ebb ff v = match v with EBBlgen v -> print_lvalue_c ff v
*)

let rec print_constraint_c ff cstr =
  match cstr with
  | True -> Format.fprintf ff "()"
  | Conj (c1, c2) -> Format.fprintf ff "@[<v 1>(%a);@,(%a)@]" print_constraint_c c1 print_constraint_c c2
  | Forall (s, f) ->
    let name = fresh () in
    let SetType t = s.typ in
    Format.fprintf ff "@[<v 2>forall %a (fun %s ->@ %a)@]" print_value_c s name print_constraint_c (f { desc = LocalVar name ; typ = t })
  | MapForall (m, f) ->
    let name1 = fresh () in
    let name2 = fresh () in
    let MapType (t1, t2) = m.typ in
    Format.fprintf ff "@[<v 2>mapforall %a (fun %s %s ->@ %a)@]" print_value_c m name1 name2 print_constraint_c (f { desc = LocalVar name1 ; typ = t1 } { desc = LocalVar name2 ; typ = t2 })
  | Predicate ((name, _, _), arg1, arg2) ->
    Format.fprintf ff "predicate_%s%a%a" name
      (print_list_prefixed " " (fun ff (VEBB x) -> print_value_c ff x)) (value_list_to_list arg1)
      (print_list_prefixed " " (fun ff (LEBB x) -> print_lvalue_c ff x)) (lvalue_list_to_list arg2)
  | CLet (v, body) ->
    let name = fresh () in
    Format.fprintf ff "@[<v>let %s = %a in@ %a@]" name print_value_c v print_constraint_c (body { desc = LocalVar name ; typ = v.typ })

let rec value_depends_on : type a. string -> a genvalue -> bool = fun localvar v ->
  match v.desc with
  | LValue lv -> lvalue_depends_on localvar lv
  | Function (f, args) -> List.exists (fun (VEBB x) -> value_depends_on localvar x) (value_list_to_list args)
  | VLet (v, body) ->
    let name = fresh () in
    let vv = { desc = LocalVar name ; typ = v.typ } in
    value_depends_on localvar (body vv) || (value_depends_on localvar v && value_depends_on name (body vv))
  | LocalVar name -> localvar = name

and lvalue_depends_on : type a. string -> a genlvalue -> bool = fun localvar lv ->
  match lv.desc with
  | MapGet (lv1, v2) -> lvalue_depends_on localvar lv1 || value_depends_on localvar v2
  | _ -> false

let rec addr_depends_on : type a. string -> a genvalue -> bool = fun localvar v ->
  match v.desc with
  | LValue lv -> lvalue_depends_on localvar lv
  | Function (f, args) -> List.exists (fun (VEBB x) -> addr_depends_on localvar x) (value_list_to_list args)
  | VLet (v, body) ->
    let name = fresh () in
    let vv = { desc = LocalVar name ; typ = v.typ } in
    addr_depends_on localvar (body vv) || addr_depends_on localvar v || (value_depends_on localvar v && addr_depends_on name (body vv))
  | LocalVar name -> localvar = name


let rec deps_depend_on localvar cstr =
  match cstr with
  | True -> false
  | Conj (c1, c2) -> deps_depend_on localvar c1 || deps_depend_on localvar c2
  | Forall (s, f) ->
    let name = fresh () in
    let SetType t = s.typ in
    let v = { desc = LocalVar name ; typ = t } in
    deps_depend_on localvar (f v) || (value_depends_on localvar s && deps_depend_on name (f v))
  | MapForall (m, f) ->
    let name1 = fresh () in
    let name2 = fresh () in
    let MapType (t1, t2) = m.typ in
    let v1, v2 = { desc = LocalVar name1 ; typ = t1 }, { desc = LocalVar name2 ; typ = t2 } in
    deps_depend_on localvar (f v1 v2) || (value_depends_on localvar m && (deps_depend_on name1 (f v1 v2) || deps_depend_on name2 (f v1 v2)))
  | Predicate (name, arg1, arg2) ->
    List.exists (fun (VEBB x) -> addr_depends_on localvar x) (value_list_to_list arg1) ||
    List.exists (fun (LEBB x) -> lvalue_depends_on localvar x) (lvalue_list_to_list arg2)
  | CLet (v, f) ->
    let name = fresh () in
    let vv = { desc = LocalVar name ; typ = v.typ } in
    deps_depend_on localvar (f vv) || (value_depends_on localvar v && deps_depend_on name (f vv))

let rec print_constraint_cdep ff cstr =
  match cstr with
  | True -> Format.fprintf ff "[]"
  | Conj (c1, c2) -> Format.fprintf ff "(%a) @@ (%a)" print_constraint_cdep c1 print_constraint_cdep c2
  | Forall (s, f) ->
    let name = fresh () in
    let SetType t = s.typ in
    Format.fprintf ff "@[<v 2>foralldeps %a (fun %s ->@ %a)@]" print_value_c s name print_constraint_cdep (f { desc = LocalVar name ; typ = t })
  | MapForall (m, f) ->
    let name1 = fresh () in
    let name2 = fresh () in
    let MapType (t1, t2) = m.typ in
    Format.fprintf ff "@[<v 2>mapforalldeps %a (fun %s %s ->@ %a)@]" print_value_c m name1 name2 print_constraint_cdep (f { desc = LocalVar name1 ; typ = t1 } { desc = LocalVar name2 ; typ = t2 })
  | Predicate (name, arg1, arg2) ->
    Format.fprintf ff "[(%a[], %a[])]"
      (print_list_suffixed "::" (fun ff (VEBB x) -> print_value_c ff x)) (value_list_to_list arg1)
      (print_list_suffixed "::" (fun ff (LEBB x) -> print_lvalue_c ff x)) (lvalue_list_to_list arg2)
  | CLet (v, body) ->
    let name = fresh () in
    Format.fprintf ff "@[<v>let %s = %a in@ %a@]" name print_value_c v print_constraint_cdep (body { desc = LocalVar name ; typ = v.typ })

