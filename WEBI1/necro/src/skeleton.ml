open Types

type ('a, 'b, 'c, 'd) interpretation =
  {
    empty_interp : 'a -> 'd ;
    hook_interp : hook -> 'a -> 'c ;
    filter_interp : filter -> 'a -> 'c ;
    merge_interp : var list -> 'b list -> 'a -> 'c ;
    before_merge_interp : 'a -> 'c ;
  }

type ('a, 'b) executable_interpretation = ('a, 'b, 'a, 'b) interpretation

let rec interpret_bone (interp : ('a, 'b) executable_interpretation) s = function
  | Hook h -> interp.hook_interp h s
  | Filter f -> interp.filter_interp f s
  | Branching {b_branches; b_outputs} ->
    let o = List.map (interpret_skeleton interp (interp.before_merge_interp s)) b_branches in
    interp.merge_interp b_outputs o s

and interpret_skeleton interp s = function
  | [] -> interp.empty_interp s
  | b :: bs -> interpret_skeleton interp (interpret_bone interp s b) bs

let add_interpretation interp ainterp =
  {
    empty_interp = (fun (st, st1) -> (interp.empty_interp st, ainterp.empty_interp (st, st1))) ;
    hook_interp = (fun h (st, st1) -> (interp.hook_interp h st, ainterp.hook_interp h (st, st1))) ;
    filter_interp = (fun f (st, st1) -> (interp.filter_interp f st, ainterp.filter_interp f (st, st1))) ;
    merge_interp = (fun vars outs (st, st1) ->
        (interp.merge_interp vars (List.map fst outs) st), ainterp.merge_interp vars outs (st, st1)) ;
    before_merge_interp = (fun (st, st1) -> (interp.before_merge_interp st, ainterp.before_merge_interp (st, st1))) ;
  }

let combine_interpretation interp1 interp2 =
  {
    empty_interp = (fun st -> interp1.empty_interp st, interp2.empty_interp st) ;
    hook_interp = (fun h st -> (interp1.hook_interp h st, interp2.hook_interp h st)) ;
    filter_interp = (fun f st -> (interp1.filter_interp f st, interp2.filter_interp f st)) ;
    merge_interp = (fun vars outs st ->
        (interp1.merge_interp vars outs st, interp2.merge_interp vars outs st)) ;
    before_merge_interp = (fun st -> (interp1.before_merge_interp st, interp2.before_merge_interp st))
  }

let combine_interpretation_inout interp1 interp2 =
  {
    empty_interp = (fun st -> let st1 = interp1.empty_interp st in (st1, interp2.empty_interp (st, st1))) ;
    hook_interp = (fun h st -> let st1 = interp1.hook_interp h st in (st1, interp2.hook_interp h (st, st1))) ;
    filter_interp = (fun f st -> let st1 = interp1.filter_interp f st in (st1, interp2.filter_interp f (st, st1))) ;
    merge_interp = (fun vars outs st ->
        let st1 = interp1.merge_interp vars outs st in
        (st1, interp2.merge_interp vars outs (st, st1))) ;
    before_merge_interp = (fun st ->
        let st1 = interp1.before_merge_interp st in
        (st1, interp2.before_merge_interp (st, st1))) ;
  }


let interpretation_product interp1 interp2 =
  {
    empty_interp = (fun (st1, st2) -> interp1.empty_interp st1, interp2.empty_interp st2) ;
    hook_interp = (fun h (st1, st2) -> interp1.hook_interp h st1, interp2.hook_interp h st2) ;
    filter_interp = (fun f (st1, st2) -> interp1.filter_interp f st1, interp2.filter_interp f st2) ;
    merge_interp = (fun vars outs (st1, st2) ->
        (interp1.merge_interp vars (List.map fst outs) st1, interp2.merge_interp vars (List.map snd outs) st2)) ;
    before_merge_interp = (fun (st1, st2) -> interp1.before_merge_interp st1, interp2.before_merge_interp st2) ;
  }

(*
let constant_interpretation =
  {
    empty_interp = (fun x -> x) ;
    hook_interp = (fun h x -> x) ;
    filter_interp = (fun f x -> x) ;
    merge_interp = (fun _ _ x -> x) ;
    before_merge_interp = (fun x -> x) ;
  }
*)

let rec take n = function
  | [] -> []
  | x :: l -> if n = 0 then [] else x :: take (n - 1) l

let accumulating_interpretation interp =
  {
    empty_interp = (fun (st, rest) ->
        let (st, add) = interp.empty_interp st in (st, add @ rest)) ;
    hook_interp = (fun h (st, rest) ->
        let (st, add) = interp.hook_interp h st in (st, add @ rest)) ;
    filter_interp = (fun f (st, rest) ->
        let (st, add) = interp.filter_interp f st in (st, add @ rest)) ;
    merge_interp = (fun vars outs (st, rest) ->
        let (st, add) = interp.merge_interp vars (List.map fst outs) st in
        let souts = List.map snd outs in
        (st, add @ List.concat souts @ rest)) ;
    before_merge_interp = (fun (st, _) -> (st, [])) ;
  }

let get_accumulated result = snd result


(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)
(* =====              Typing interpretation              ===== *)
(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

let option_get = function Some x -> x | None -> raise (Invalid_argument "option_get")

module Var = struct type t = var let compare = compare end
module VMap = Map.Make(Var)

let check_type got expected =
  if got <> expected then
    failwith (Format.sprintf "Invalid type: got '%s', expected '%s'" (string_of_sort got) (string_of_sort expected))

let get_env_term_sort env tenv term =
  match term with
  | TVar v -> (match VMap.find v tenv with Program s -> s | _ -> assert false)
  | TConstr (c, _) -> (SMap.find c env.env_constr_signatures).cs_output_sort

let add_env_sort tenv var sort =
  VMap.add var sort tenv

let add_filter_outs env f tenv =
  let name = f.f_name in
  let fs = SMap.find name env.env_filter_signatures in
  List.iter2 (fun v vt -> check_type (VMap.find v tenv) vt) f.f_inputs fs.fs_input_sorts;
  let nenv = List.fold_left2 (fun tenv v vt -> VMap.add v vt tenv) tenv f.f_outputs fs.fs_output_sorts in
  nenv

let merge_envs env vars tenvs tenv =
  if tenvs = [] then begin
    if vars = [] then
      tenv
    else
      failwith "Empty branchings which produce variables are not permitted"
  end else begin
    let tenv1 = List.hd tenvs in
    let nenv = List.fold_left (fun tenv key ->
        VMap.add key (VMap.find key tenv1) tenv)
        tenv vars in
    let () = List.iter (fun tenv2 ->
        List.iter (fun v -> check_type (VMap.find v tenv2) (VMap.find v nenv)) vars
      ) tenvs in
    let _ = List.fold_left (fun allenvs tenv2 ->
        VMap.merge (fun v t1 t2 ->
            if VMap.mem v nenv then
              t1
            else match t1, t2 with
              | None, None -> None
              | None, Some t | Some t, None -> Some t
              | Some t1, Some t2 -> failwith ("The variable '" ^ v ^ "'was defined in multiple branches")
          ) allenvs tenv2
      ) tenv tenvs in
    nenv
  end

let make_init_env env r =
  let constr = SMap.find r.r_constructor env.env_constr_signatures in
  let tenv =
    List.fold_left2 (fun tenv v vt -> VMap.add v vt tenv)
      (VMap.singleton "x_s" r.r_input_sort)
      r.r_constructor_arguments constr.cs_input_sorts
  in
  tenv

let get_env_map tenv = tenv

let typing_interp env = {
  empty_interp = (fun tenv -> tenv) ;
  hook_interp = (fun h tenv ->
      let hs = SMap.find h.h_name env.env_hook_definitions in
      List.iter2 (fun v vt -> check_type (VMap.find v tenv) vt) h.h_inputs hs.hd_input_sorts;
      check_type (Program (get_env_term_sort env tenv h.h_term)) (Program hs.hd_term_sort);
      List.fold_left2 add_env_sort tenv h.h_outputs hs.hd_output_sorts) ;
  filter_interp = add_filter_outs env ;
  merge_interp = merge_envs env ;
  before_merge_interp = (fun x -> x) ;
}

let check_well_typed env =
  SMap.iter (fun _ h ->
      SMap.iter (fun _ r ->
          let tenv = make_init_env env r in
          let tenv1 = interpret_skeleton (typing_interp env) tenv r.r_skeleton in
          check_type (VMap.find "x_o" tenv1) r.r_output_sort
        ) h.hd_rules) env.env_hook_definitions
