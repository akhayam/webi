open Cstr
open While_cstr
open Domains

module Abstract = struct
  module Lit = struct include Interval let pp_print ff x = pp_print ff (Some x) end
  module Ident = struct type t = string let pp_print = Format.pp_print_string end
  module Vint = Interval
  module Vbool = ABool
  module Value = AValue
  module State = AState

  let litToVal x = Some (Value.from_int x)
  let read x state = try Some (SMap.find x state) with Not_found -> Some (Value.top)
  let isInt = fst
  let isBool = snd
  let add x1 x2 = Some (Value.from_int (Vint.add x1 x2))
  let eq x1 x2 = Some (Value.from_bool (Vint.eq x1 x2))
  let neg x = Some (Value.from_bool (Vbool.neg x))
  let write x st v = Some (SMap.add x v st)
  let id v = Some v
  let isTrue (ct, cf) = if ct then Some () else None
  let isFalse (ct, cf) = if cf then Some () else None

  let gent_stat = gent_stat
  let gent_expr = gent_expr
end

open Abstract

(*
module Store = MakeStore(Abstract)
module CGen = Constraints(MakePrim(Abstract))
open CGen

let initial_node = ("x_s", [])
let initial_store wid cstrs =
  let st = ref (Store.set_state initial_node (Some AState.top) (Store.empty wid)) in
  List.iter (function
      | CEq_expr (v, x) -> ()
      | CEq_stat (v, x) -> ()
      | CEq_ident (v, x) -> st := Store.set_ident v (Some x) !st
      | CEq_lit (v, x) -> st := Store.set_lit v (Some x) !st
      | _ -> ()
    ) cstrs;
  !st

let get_sorts cstrs =
  let sorts = ref PMap.empty in
  List.iter (function
      | CSort (p, s) -> sorts := PMap.add p s !sorts
      | _ -> ()
    ) cstrs;
  !sorts

let sort_updater = function
  | "lit" -> Store.(make_updater get_lit set_lit)
  | "ident" -> Store.(make_updater get_ident set_ident)
  | "vint" -> Store.(make_updater get_vint set_vint)
  | "vbool" -> Store.(make_updater get_vbool set_vbool)
  | "value" -> Store.(make_updater get_value set_value)
  | "state" -> Store.(make_updater get_state set_state)
  | _ -> assert false

let collect_upds cstrs sorts =
  let result = ref [] in
  List.iter (function
      | CFilter upd -> result := upd :: !result
      | CSub (v1, v2) ->
        result := ("", [v1], [v2], sort_updater (PMap.find v1 sorts) v1 v2) :: !result
      | _ -> ()
    ) cstrs;
  !result

let rec iterate store fs =
  let nstore = List.fold_left (fun store f -> f store) store fs in
  if Store.leq nstore store then
    store
  else
    iterate nstore fs

let solve cstrs =
  let sorts = get_sorts cstrs in
  let upds = collect_upds cstrs sorts in
  let widen_points, upds = sort_upds upds in
  let store = initial_store widen_points cstrs in
  iterate store (List.map (fun (_, _, _, f) -> f) upds)
*)

module Store = MakeStoreFs(Abstract)
module CGen = ConstraintsFs(MakePrimFs(Abstract))
open CGen

let initial_node = ("x_s", [])
let initial_flag = ("inflag", [])
let initial_store wid cstrs =
  let st = ref (
      Store.set_flag initial_flag (Some ()) @@
      Store.set_state initial_node (Some AState.top) @@
      Store.empty wid)
  in
  List.iter (function
      | CFSEq_expr (v, x) -> ()
      | CFSEq_stat (v, x) -> ()
      | CFSEq_ident (v, x) -> st := Store.set_ident v (Some x) !st
      | CFSEq_lit (v, x) -> st := Store.set_lit v (Some x) !st
      | _ -> ()
    ) cstrs;
  !st

let get_sorts cstrs =
  let sorts = ref PMap.empty in
  List.iter (function
      | CFSSort (p, s) -> sorts := PMap.add p s !sorts
      | _ -> ()
    ) cstrs;
  !sorts

let sort_updater = function
  | "lit" -> Store.(make_updater get_lit set_lit)
  | "ident" -> Store.(make_updater get_ident set_ident)
  | "vint" -> Store.(make_updater get_vint set_vint)
  | "vbool" -> Store.(make_updater get_vbool set_vbool)
  | "value" -> Store.(make_updater get_value set_value)
  | "state" -> Store.(make_updater get_state set_state)
  | "flag" -> Store.(make_updater get_flag set_flag)
  | _ -> assert false

let sort_tester = function
  | "lit" -> fun x store -> Store.get_lit x store = None
  | "ident" -> fun x store -> Store.get_ident x store = None
  | "vint" -> fun x store -> Store.get_vint x store = None
  | "vbool" -> fun x store -> Store.get_vbool x store = None
  | "value" -> fun x store -> Store.get_value x store = None
  | "state" -> fun x store -> Store.get_state x store = None
  | "flag" -> fun x store -> Store.get_flag x store = None
  | _ -> assert false

let collect_upds cstrs sorts =
  let result = ref [] in
  List.iter (function
      | CFSFilter upd -> result := upd :: !result
      | CFSSub (flag, v1, v2) ->
        let upd = sort_updater (PMap.find v1 sorts) v1 v2 in
        result := ("", [flag; v1], [v2], fun store -> if Store.get_flag flag store = None then store else upd store) :: !result
      | CFSFlag (inflag, outflag) ->
        result := ("", [inflag], [outflag], Store.(make_updater get_flag set_flag) inflag outflag) :: !result
      | CFSFlagTest (inflag, test, outflag) ->
        let tst = sort_tester (PMap.find test sorts) test in
        result := ("", [inflag; test], [outflag], fun store -> if Store.get_flag inflag store = None || tst store then store else Store.set_flag outflag (Some ()) store) :: !result
      | _ -> ()
    ) cstrs;
  !result

let rec iterate store fs =
  let nstore = List.fold_left (fun store f -> f store) store fs in
  if Store.leq nstore store then
    store
  else
    iterate nstore fs

let solve cstrs =
  let sorts = get_sorts cstrs in
  let upds = collect_upds cstrs sorts in
  let widen_points, upds = sort_upds upds in
  let store = initial_store widen_points cstrs in
  iterate store (List.map (fun (_, _, _, f) -> f) upds)

let rec pp_print_expr ff = function
  | Const d -> Format.fprintf ff "Const %a" Interval.pp_print (Some d)
  | Var x -> Format.fprintf ff "Var %S" x
  | Plus (x1, x2) -> Format.fprintf ff "Plus (%a, %a)" pp_print_expr x1 pp_print_expr x2
  | EqualEqual (x1, x2) -> Format.fprintf ff "EqualEqual (%a, %a)" pp_print_expr x1 pp_print_expr x2
  | Bang x -> Format.fprintf ff "Bang (%a)" pp_print_expr x

and pp_print_stat ff = function
  | Skip -> Format.fprintf ff "Skip"
  | ColonEqual (x1, x2) -> Format.fprintf ff "ColonEqual (%S, %a)" x1 pp_print_expr x2
  | SemicolonSemicolon (x1, x2) -> Format.fprintf ff "SemicolonSemicolon (%a, %a)" pp_print_stat x1 pp_print_stat x2
  | If (x1, x2, x3) -> Format.fprintf ff "If (%a, %a, %a)" pp_print_expr x1 pp_print_stat x2 pp_print_stat x3
  | While (x1, x2) -> Format.fprintf ff "While (%a, %a)" pp_print_expr x1 pp_print_stat x2

let print_fc ff (filter, inv, outv, _) =
  Format.fprintf ff "(%a) = %s(%a)" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ", ") print_pvar) outv filter (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ", ") print_pvar) inv

(*
let print_constraint ff = function
  | CSort (v, s) -> Format.fprintf ff "[%a : %s]" print_pvar v s
  | CFilter fc -> Format.fprintf ff "[%a]" print_fc fc
  | CSub (v1, v2) -> Format.fprintf ff "[%a <= %a]" print_pvar v1 print_pvar v2
  | CEq_ident (v, x) -> Format.fprintf ff "[%a = %s]" print_pvar v x
  | CEq_lit (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v Interval.pp_print (Some x)
  | CEq_expr (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v pp_print_expr x
  | CEq_stat (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v pp_print_stat x
*)

let print_constraint_fs ff = function
  | CFSSort (v, s) -> Format.fprintf ff "[%a : %s]" print_pvar v s
  | CFSFilter fc -> Format.fprintf ff "[%a]" print_fc fc
  | CFSSub (inflag, v1, v2) -> Format.fprintf ff "[%a -> %a <= %a]" print_pvar inflag print_pvar v1 print_pvar v2
  | CFSFlag (inflag, outflag) -> Format.fprintf ff "[%a -> %a]" print_pvar inflag print_pvar outflag
  | CFSFlagTest (inflag, test, outflag) -> Format.fprintf ff "[%a /\\ %a <> bot -> %a]" print_pvar inflag print_pvar test print_pvar outflag
  | CFSEq_ident (v, x) -> Format.fprintf ff "[%a = %s]" print_pvar v x
  | CFSEq_lit (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v Interval.pp_print (Some x)
  | CFSEq_expr (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v pp_print_expr x
  | CFSEq_stat (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v pp_print_stat x


let m x = Interval.(Bint x, Bint x)
(*let t = SemicolonSemicolon (ColonEqual ("x", Const (m 10)), While (Bang (EqualEqual (Var "x", Const (m 0))), ColonEqual ("x", Plus (Var "x", Const (m (-1))))))*)
let t = If (EqualEqual (Const (m 0), Const (m 0)), (ColonEqual ("x", Const (m 42))), (ColonEqual ("x", Const (m 17))))
let cstrs = generate_constraints gent_stat t
(*let () = List.iter (fun c -> Format.printf "%a@." print_constraint c) cstrs*)
let () = List.iter (fun c -> Format.printf "%a@." print_constraint_fs c) cstrs
let () = Format.printf "========@."
let solved = solve cstrs
let () = Format.printf "%a@." Store.pp_print solved

