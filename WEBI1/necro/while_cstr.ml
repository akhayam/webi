open Cstr

type ('ident, 'lit) stat =
| Skip
| ColonEqual of 'ident * ('ident, 'lit) expr
| SemicolonSemicolon of ('ident, 'lit) stat * ('ident, 'lit) stat
| If of ('ident, 'lit) expr * ('ident, 'lit) stat * ('ident, 'lit) stat
| While of ('ident, 'lit) expr * ('ident, 'lit) stat

and ('ident, 'lit) expr =
| Const of 'lit
| Var of 'ident
| Plus of ('ident, 'lit) expr * ('ident, 'lit) expr
| EqualEqual of ('ident, 'lit) expr * ('ident, 'lit) expr
| Bang of ('ident, 'lit) expr

let rec gent_stat f_expr f_stat pp term =
  match term with
  | Skip -> f_stat pp term
  | ColonEqual (x_t0, x_t1) -> f_stat pp term; gent_expr f_expr f_stat (1 :: pp) x_t1
  | SemicolonSemicolon (x_t0, x_t1) -> f_stat pp term; gent_stat f_expr f_stat (0 :: pp) x_t0; gent_stat f_expr f_stat (1 :: pp) x_t1
  | If (x_t0, x_t1, x_t2) -> f_stat pp term; gent_expr f_expr f_stat (0 :: pp) x_t0; gent_stat f_expr f_stat (1 :: pp) x_t1; gent_stat f_expr f_stat (2 :: pp) x_t2
  | While (x_t0, x_t1) -> f_stat pp term; gent_expr f_expr f_stat (0 :: pp) x_t0; gent_stat f_expr f_stat (1 :: pp) x_t1

and gent_expr f_expr f_stat pp term =
  match term with
  | Const x_t0 -> f_expr pp term
  | Var x_t0 -> f_expr pp term
  | Plus (x_t0, x_t1) -> f_expr pp term; gent_expr f_expr f_stat (0 :: pp) x_t0; gent_expr f_expr f_stat (1 :: pp) x_t1
  | EqualEqual (x_t0, x_t1) -> f_expr pp term; gent_expr f_expr f_stat (0 :: pp) x_t0; gent_expr f_expr f_stat (1 :: pp) x_t1
  | Bang x_t0 -> f_expr pp term; gent_expr f_expr f_stat (0 :: pp) x_t0

module type ABSTRACT = sig
  module Ident : PRINTABLE
  module Lit : PRINTABLE
  module State : DOMAIN
  module Value : DOMAIN
  module Vint : DOMAIN
  module Vbool : DOMAIN
  
  val litToVal : Lit.t -> Value.t option
  val read : Ident.t -> State.t -> Value.t option
  val isInt : Value.t -> Vint.t option
  val add : Vint.t -> Vint.t -> Value.t option
  val eq : Vint.t -> Vint.t -> Value.t option
  val isBool : Value.t -> Vbool.t option
  val neg : Vbool.t -> Value.t option
  val write : Ident.t -> State.t -> Value.t -> State.t option
  val id : State.t -> State.t option
  val isTrue : Vbool.t -> unit option
  val isFalse : Vbool.t -> unit option
  
  val gent_expr : (path -> ('ident, 'lit) expr -> unit) -> (path -> ('ident, 'lit) stat -> unit) -> path -> ('ident, 'lit) expr -> unit
  val gent_stat : (path -> ('ident, 'lit) expr -> unit) -> (path -> ('ident, 'lit) stat -> unit) -> path -> ('ident, 'lit) stat -> unit
end

module type PRIM = sig
  type filter_constraint
  val litToVal : pvar -> pvar -> filter_constraint
  val read : pvar -> pvar -> pvar -> filter_constraint
  val isInt : pvar -> pvar -> filter_constraint
  val add : pvar -> pvar -> pvar -> filter_constraint
  val eq : pvar -> pvar -> pvar -> filter_constraint
  val isBool : pvar -> pvar -> filter_constraint
  val neg : pvar -> pvar -> filter_constraint
  val write : pvar -> pvar -> pvar -> pvar -> filter_constraint
  val id : pvar -> pvar -> filter_constraint
  val isTrue : pvar -> filter_constraint
  val isFalse : pvar -> filter_constraint
  val gent_expr : (path -> ('ident, 'lit) expr -> unit) -> (path -> ('ident, 'lit) stat -> unit) -> path -> ('ident, 'lit) expr -> unit
  val gent_stat : (path -> ('ident, 'lit) expr -> unit) -> (path -> ('ident, 'lit) stat -> unit) -> path -> ('ident, 'lit) stat -> unit
end

module Constraints (P : PRIM) = struct
  include P
  type ('ident, 'lit) cstr =
  | CSort of pvar * string
  | CFilter of filter_constraint
  | CSub of pvar * pvar
  | CEq_ident of pvar * 'ident
  | CEq_lit of pvar * 'lit
  | CEq_expr of pvar * ('ident, 'lit) expr
  | CEq_stat of pvar * ('ident, 'lit) stat
  
  let constraints_lit_int pp x_t = 
    [CSort (("x_t", pp), "lit"); CSort (("x_o", pp), "value");
     CSort (("x_s", pp), "state"); CEq_lit (("x_t", pp), x_t);
     CFilter (litToVal ("x_t", pp) ("x_o", pp)); ]
  
  let constraints_var pp x_t = 
    [CSort (("x_t", pp), "ident"); CSort (("x_o", pp), "value");
     CSort (("x_s", pp), "state"); CEq_ident (("x_t", pp), x_t);
     CFilter (read ("x_t", pp) ("x_s", pp) ("x_o", pp)); ]
  
  let constraints_add pp x_t1 x_t2 = 
    [CSort (("x_t1", pp), "expr"); CSort (("x_t2", pp), "expr");
     CSort (("x_f1", pp), "value"); CSort (("x_f1'", pp), "vint");
     CSort (("x_f2", pp), "value"); CSort (("x_f2'", pp), "vint");
     CSort (("x_o", pp), "value"); CSort (("x_s", pp), "state");
     CEq_expr (("x_t1", pp), x_t1); CEq_expr (("x_t2", pp), x_t2);
     CFilter (add ("x_f1'", pp) ("x_f2'", pp) ("x_o", pp));
     CFilter (isInt ("x_f2", pp) ("x_f2'", pp));
     CSub (("x_s", pp), ("x_s", 1 :: pp));
     CSub (("x_o", 1 :: pp), ("x_f2", pp));
     CFilter (isInt ("x_f1", pp) ("x_f1'", pp));
     CSub (("x_s", pp), ("x_s", 0 :: pp));
     CSub (("x_o", 0 :: pp), ("x_f1", pp)); ]
  
  let constraints_eq pp x_t1 x_t2 = 
    [CSort (("x_t1", pp), "expr"); CSort (("x_t2", pp), "expr");
     CSort (("x_f1", pp), "value"); CSort (("x_f1'", pp), "vint");
     CSort (("x_f2", pp), "value"); CSort (("x_f2'", pp), "vint");
     CSort (("x_o", pp), "value"); CSort (("x_s", pp), "state");
     CEq_expr (("x_t1", pp), x_t1); CEq_expr (("x_t2", pp), x_t2);
     CFilter (eq ("x_f1'", pp) ("x_f2'", pp) ("x_o", pp));
     CFilter (isInt ("x_f2", pp) ("x_f2'", pp));
     CSub (("x_s", pp), ("x_s", 1 :: pp));
     CSub (("x_o", 1 :: pp), ("x_f2", pp));
     CFilter (isInt ("x_f1", pp) ("x_f1'", pp));
     CSub (("x_s", pp), ("x_s", 0 :: pp));
     CSub (("x_o", 0 :: pp), ("x_f1", pp)); ]
  
  let constraints_neg pp x_t = 
    [CSort (("x_t", pp), "expr"); CSort (("x_f1", pp), "value");
     CSort (("x_f1'", pp), "vbool"); CSort (("x_o", pp), "value");
     CSort (("x_s", pp), "state"); CEq_expr (("x_t", pp), x_t);
     CFilter (neg ("x_f1'", pp) ("x_o", pp));
     CFilter (isBool ("x_f1", pp) ("x_f1'", pp));
     CSub (("x_s", pp), ("x_s", 0 :: pp));
     CSub (("x_o", 0 :: pp), ("x_f1", pp)); ]
  
  let constraints_skip pp = 
    [CSort (("x_o", pp), "state"); CSort (("x_s", pp), "state");
     CFilter (id ("x_s", pp) ("x_o", pp)); ]
  
  let constraints_asn pp x_t1 x_t2 = 
    [CSort (("x_t1", pp), "ident"); CSort (("x_t2", pp), "expr");
     CSort (("x_f2", pp), "value"); CSort (("x_o", pp), "state");
     CSort (("x_s", pp), "state"); CEq_ident (("x_t1", pp), x_t1);
     CEq_expr (("x_t2", pp), x_t2);
     CFilter (write ("x_t1", pp) ("x_s", pp) ("x_f2", pp) ("x_o", pp));
     CSub (("x_s", pp), ("x_s", 1 :: pp));
     CSub (("x_o", 1 :: pp), ("x_f2", pp)); ]
  
  let constraints_seq pp x_t1 x_t2 = 
    [CSort (("x_t1", pp), "stat"); CSort (("x_t2", pp), "stat");
     CSort (("x_f1", pp), "state"); CSort (("x_o", pp), "state");
     CSort (("x_s", pp), "state"); CEq_stat (("x_t1", pp), x_t1);
     CEq_stat (("x_t2", pp), x_t2); CSub (("x_f1", pp), ("x_s", 1 :: pp));
     CSub (("x_o", 1 :: pp), ("x_o", pp));
     CSub (("x_s", pp), ("x_s", 0 :: pp));
     CSub (("x_o", 0 :: pp), ("x_f1", pp)); ]
  
  let constraints_if pp x_t1 x_t2 x_t3 = 
    [CSort (("x_t1", pp), "expr"); CSort (("x_t2", pp), "stat");
     CSort (("x_t3", pp), "stat"); CSort (("x_f1", pp), "value");
     CSort (("x_f1'", pp), "vbool"); CSort (("x_o", pp), "state");
     CSort (("x_s", pp), "state"); CEq_expr (("x_t1", pp), x_t1);
     CEq_stat (("x_t2", pp), x_t2); CEq_stat (("x_t3", pp), x_t3);
     CSub (("x_s", pp), ("x_s", 1 :: pp));
     CSub (("x_o", 1 :: pp), ("x_o", pp)); CFilter (isTrue ("x_f1'", pp));
     CSub (("x_s", pp), ("x_s", 2 :: pp));
     CSub (("x_o", 2 :: pp), ("x_o", pp)); CFilter (isFalse ("x_f1'", pp));
     CFilter (isBool ("x_f1", pp) ("x_f1'", pp));
     CSub (("x_s", pp), ("x_s", 0 :: pp));
     CSub (("x_o", 0 :: pp), ("x_f1", pp)); ]
  
  let constraints_while pp x_t1 x_t2 = 
    [CSort (("x_t1", pp), "expr"); CSort (("x_t2", pp), "stat");
     CSort (("x_f1", pp), "value"); CSort (("x_f1'", pp), "vbool");
     CSort (("x_f2", pp), "state"); CSort (("x_o", pp), "state");
     CSort (("x_s", pp), "state"); CEq_expr (("x_t1", pp), x_t1);
     CEq_stat (("x_t2", pp), x_t2); CSub (("x_f2", pp), ("x_s", pp));
     CSub (("x_o", pp), ("x_o", pp)); CSub (("x_s", pp), ("x_s", 1 :: pp));
     CSub (("x_o", 1 :: pp), ("x_f2", pp)); CFilter (isTrue ("x_f1'", pp));
     CFilter (id ("x_s", pp) ("x_o", pp)); CFilter (isFalse ("x_f1'", pp));
     CFilter (isBool ("x_f1", pp) ("x_f1'", pp));
     CSub (("x_s", pp), ("x_s", 0 :: pp));
     CSub (("x_o", 0 :: pp), ("x_f1", pp)); ]
  
  let constraints_stat pp term =
    match term with
    | Skip -> constraints_skip pp 
    | ColonEqual (x_t1, x_t2) -> constraints_asn pp x_t1 x_t2
    | SemicolonSemicolon (x_t1, x_t2) -> constraints_seq pp x_t1 x_t2
    | If (x_t1, x_t2, x_t3) -> constraints_if pp x_t1 x_t2 x_t3
    | While (x_t1, x_t2) -> constraints_while pp x_t1 x_t2
  
  let constraints_expr pp term =
    match term with
    | Const x_t -> constraints_lit_int pp x_t
    | Var x_t -> constraints_var pp x_t
    | Plus (x_t1, x_t2) -> constraints_add pp x_t1 x_t2
    | EqualEqual (x_t1, x_t2) -> constraints_eq pp x_t1 x_t2
    | Bang x_t -> constraints_neg pp x_t
  
  let generate_constraints gent program =
    let c = ref [] in
    gent (fun pp term -> c := constraints_expr pp term @ !c) (fun pp term -> c := constraints_stat pp term @ !c) [] program;
    !c
end

module MakeStore (A : ABSTRACT) = struct
  open A
  type t = Ident.t PMap.t * Lit.t PMap.t * State.t option PMap.t * Value.t option PMap.t * Vint.t option PMap.t * Vbool.t option PMap.t * PSet.t
  
  let empty wid = (PMap.empty, PMap.empty, PMap.empty, PMap.empty, PMap.empty, PMap.empty, wid)
  
  let get_ident v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, _) : t) = Some (PMap.find v env_ident)
  let set_ident v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t) =
    let env_ident = match x with None -> env_ident | Some x -> PMap.add v x env_ident in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t)
  let get_lit v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, _) : t) = Some (PMap.find v env_lit)
  let set_lit v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t) =
    let env_lit = match x with None -> env_lit | Some x -> PMap.add v x env_lit in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t)
  let get_state v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, _) : t) = default None v env_state
  let set_state v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t) =
    let env_state = add_wid v x env_state wid State.lub State.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t)
  let get_value v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, _) : t) = default None v env_value
  let set_value v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t) =
    let env_value = add_wid v x env_value wid Value.lub Value.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t)
  let get_vint v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, _) : t) = default None v env_vint
  let set_vint v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t) =
    let env_vint = add_wid v x env_vint wid Vint.lub Vint.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t)
  let get_vbool v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, _) : t) = default None v env_vbool
  let set_vbool v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t) =
    let env_vbool = add_wid v x env_vbool wid Vbool.lub Vbool.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, wid) : t)
  
  let leq ((env1_ident, env1_lit, env1_state, env1_value, env1_vint, env1_vbool, _) : t) ((env2_ident, env2_lit, env2_state, env2_value, env2_vint, env2_vbool, _) : t) =
    env_leq env1_state env2_state State.leq &&
    env_leq env1_value env2_value Value.leq &&
    env_leq env1_vint env2_vint Vint.leq &&
    env_leq env1_vbool env2_vbool Vbool.leq
  
  let pp_print ff ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, _) : t) =
    env_print ff "%a |-> %a@," env_ident Ident.pp_print;
    env_print ff "%a |-> %a@," env_lit Lit.pp_print;
    env_print ff "%a |-> %a@," env_state State.pp_print;
    env_print ff "%a |-> %a@," env_value Value.pp_print;
    env_print ff "%a |-> %a@," env_vint Vint.pp_print;
    env_print ff "%a |-> %a@," env_vbool Vbool.pp_print;
end

module MakePrim (A : ABSTRACT) = struct
  module Store = MakeStore(A)
  open A
  
  type filter_constraint = string * pvar list * pvar list * (Store.t -> Store.t)
  
  let litToVal in0 out0 =
    ("litToVal", [in0], [out0], fun store ->
      let open Store in
      match bind1 litToVal (get_lit in0 store) with
      | None -> store
      | Some (res0) ->
        set_value out0 (Some res0) @@
        store)
  
  let read in0 in1 out0 =
    ("read", [in0; in1], [out0], fun store ->
      let open Store in
      match bind2 read (get_ident in0 store) (get_state in1 store) with
      | None -> store
      | Some (res0) ->
        set_value out0 (Some res0) @@
        store)
  
  let isInt in0 out0 =
    ("isInt", [in0], [out0], fun store ->
      let open Store in
      match bind1 isInt (get_value in0 store) with
      | None -> store
      | Some (res0) ->
        set_vint out0 (Some res0) @@
        store)
  
  let add in0 in1 out0 =
    ("add", [in0; in1], [out0], fun store ->
      let open Store in
      match bind2 add (get_vint in0 store) (get_vint in1 store) with
      | None -> store
      | Some (res0) ->
        set_value out0 (Some res0) @@
        store)
  
  let eq in0 in1 out0 =
    ("eq", [in0; in1], [out0], fun store ->
      let open Store in
      match bind2 eq (get_vint in0 store) (get_vint in1 store) with
      | None -> store
      | Some (res0) ->
        set_value out0 (Some res0) @@
        store)
  
  let isBool in0 out0 =
    ("isBool", [in0], [out0], fun store ->
      let open Store in
      match bind1 isBool (get_value in0 store) with
      | None -> store
      | Some (res0) ->
        set_vbool out0 (Some res0) @@
        store)
  
  let neg in0 out0 =
    ("neg", [in0], [out0], fun store ->
      let open Store in
      match bind1 neg (get_vbool in0 store) with
      | None -> store
      | Some (res0) ->
        set_value out0 (Some res0) @@
        store)
  
  let write in0 in1 in2 out0 =
    ("write", [in0; in1; in2], [out0], fun store ->
      let open Store in
      match bind3 write (get_ident in0 store) (get_state in1 store) (get_value in2 store) with
      | None -> store
      | Some (res0) ->
        set_state out0 (Some res0) @@
        store)
  
  let id in0 out0 =
    ("id", [in0], [out0], fun store ->
      let open Store in
      match bind1 id (get_state in0 store) with
      | None -> store
      | Some (res0) ->
        set_state out0 (Some res0) @@
        store)
  
  let isTrue in0  =
    ("isTrue", [in0], [], fun store ->
      let open Store in
      match bind1 isTrue (get_vbool in0 store) with
      | None -> store
      | Some () ->
        store)
  
  let isFalse in0  =
    ("isFalse", [in0], [], fun store ->
      let open Store in
      match bind1 isFalse (get_vbool in0 store) with
      | None -> store
      | Some () ->
        store)
  
  let gent_expr = gent_expr
  let gent_stat = gent_stat
end

module type PRIMFS = sig
  type filter_constraint
  val litToVal : pvar -> pvar -> pvar -> pvar -> filter_constraint
  val read : pvar -> pvar -> pvar -> pvar -> pvar -> filter_constraint
  val isInt : pvar -> pvar -> pvar -> pvar -> filter_constraint
  val add : pvar -> pvar -> pvar -> pvar -> pvar -> filter_constraint
  val eq : pvar -> pvar -> pvar -> pvar -> pvar -> filter_constraint
  val isBool : pvar -> pvar -> pvar -> pvar -> filter_constraint
  val neg : pvar -> pvar -> pvar -> pvar -> filter_constraint
  val write : pvar -> pvar -> pvar -> pvar -> pvar -> pvar -> filter_constraint
  val id : pvar -> pvar -> pvar -> pvar -> filter_constraint
  val isTrue : pvar -> pvar -> pvar -> filter_constraint
  val isFalse : pvar -> pvar -> pvar -> filter_constraint
  val gent_expr : (path -> ('ident, 'lit) expr -> unit) -> (path -> ('ident, 'lit) stat -> unit) -> path -> ('ident, 'lit) expr -> unit
  val gent_stat : (path -> ('ident, 'lit) expr -> unit) -> (path -> ('ident, 'lit) stat -> unit) -> path -> ('ident, 'lit) stat -> unit
end

module ConstraintsFs (P : PRIMFS) = struct
  include P
  type ('ident, 'lit) cstr =
  | CFSSort of pvar * string
  | CFSFilter of filter_constraint
  | CFSSub of pvar * pvar * pvar
  | CFSFlag of pvar * pvar
  | CFSFlagTest of pvar * pvar * pvar
  | CFSEq_ident of pvar * 'ident
  | CFSEq_lit of pvar * 'lit
  | CFSEq_expr of pvar * ('ident, 'lit) expr
  | CFSEq_stat of pvar * ('ident, 'lit) stat
  
  let constraints_lit_int pp x_t = 
    [CFSSort (("flow0", pp), "flag"); CFSSort (("inflag", pp), "flag");
     CFSSort (("x_t", pp), "lit"); CFSSort (("x_o", pp), "value");
     CFSSort (("x_s", pp), "state"); CFSEq_lit (("x_t", pp), x_t);
     CFSFilter (litToVal ("inflag", pp) ("flow0", pp) ("x_t", pp) ("x_o", pp));
     ]
  
  let constraints_var pp x_t = 
    [CFSSort (("flow1", pp), "flag"); CFSSort (("inflag", pp), "flag");
     CFSSort (("x_t", pp), "ident"); CFSSort (("x_o", pp), "value");
     CFSSort (("x_s", pp), "state"); CFSEq_ident (("x_t", pp), x_t);
     CFSFilter (read ("inflag", pp) ("flow1", pp) ("x_t", pp) ("x_s", pp) ("x_o", pp));
     ]
  
  let constraints_add pp x_t1 x_t2 = 
    [CFSSort (("flow6", pp), "flag"); CFSSort (("flow5", pp), "flag");
     CFSSort (("flow4", pp), "flag"); CFSSort (("flow3", pp), "flag");
     CFSSort (("flow2", pp), "flag"); CFSSort (("inflag", pp), "flag");
     CFSSort (("x_t1", pp), "expr"); CFSSort (("x_t2", pp), "expr");
     CFSSort (("x_f1", pp), "value"); CFSSort (("x_f1'", pp), "vint");
     CFSSort (("x_f2", pp), "value"); CFSSort (("x_f2'", pp), "vint");
     CFSSort (("x_o", pp), "value"); CFSSort (("x_s", pp), "state");
     CFSEq_expr (("x_t1", pp), x_t1); CFSEq_expr (("x_t2", pp), x_t2);
     CFSFilter (add ("flow5", pp) ("flow6", pp) ("x_f1'", pp) ("x_f2'", pp) ("x_o", pp));
     CFSFilter (isInt ("flow4", pp) ("flow5", pp) ("x_f2", pp) ("x_f2'", pp));
     CFSSub (("flow3", pp), ("x_s", pp), ("x_s", 1 :: pp));
     CFSSub (("flow3", pp), ("x_o", 1 :: pp), ("x_f2", pp));
     CFSFlagTest (("flow3", pp), ("x_f2", pp), ("flow4", pp));
     CFSFlag (("flow3", pp), ("inflag", 1 :: pp));
     CFSFilter (isInt ("flow2", pp) ("flow3", pp) ("x_f1", pp) ("x_f1'", pp));
     CFSSub (("inflag", pp), ("x_s", pp), ("x_s", 0 :: pp));
     CFSSub (("inflag", pp), ("x_o", 0 :: pp), ("x_f1", pp));
     CFSFlagTest (("inflag", pp), ("x_f1", pp), ("flow2", pp));
     CFSFlag (("inflag", pp), ("inflag", 0 :: pp)); ]
  
  let constraints_eq pp x_t1 x_t2 = 
    [CFSSort (("flow11", pp), "flag"); CFSSort (("flow10", pp), "flag");
     CFSSort (("flow9", pp), "flag"); CFSSort (("flow8", pp), "flag");
     CFSSort (("flow7", pp), "flag"); CFSSort (("inflag", pp), "flag");
     CFSSort (("x_t1", pp), "expr"); CFSSort (("x_t2", pp), "expr");
     CFSSort (("x_f1", pp), "value"); CFSSort (("x_f1'", pp), "vint");
     CFSSort (("x_f2", pp), "value"); CFSSort (("x_f2'", pp), "vint");
     CFSSort (("x_o", pp), "value"); CFSSort (("x_s", pp), "state");
     CFSEq_expr (("x_t1", pp), x_t1); CFSEq_expr (("x_t2", pp), x_t2);
     CFSFilter (eq ("flow10", pp) ("flow11", pp) ("x_f1'", pp) ("x_f2'", pp) ("x_o", pp));
     CFSFilter (isInt ("flow9", pp) ("flow10", pp) ("x_f2", pp) ("x_f2'", pp));
     CFSSub (("flow8", pp), ("x_s", pp), ("x_s", 1 :: pp));
     CFSSub (("flow8", pp), ("x_o", 1 :: pp), ("x_f2", pp));
     CFSFlagTest (("flow8", pp), ("x_f2", pp), ("flow9", pp));
     CFSFlag (("flow8", pp), ("inflag", 1 :: pp));
     CFSFilter (isInt ("flow7", pp) ("flow8", pp) ("x_f1", pp) ("x_f1'", pp));
     CFSSub (("inflag", pp), ("x_s", pp), ("x_s", 0 :: pp));
     CFSSub (("inflag", pp), ("x_o", 0 :: pp), ("x_f1", pp));
     CFSFlagTest (("inflag", pp), ("x_f1", pp), ("flow7", pp));
     CFSFlag (("inflag", pp), ("inflag", 0 :: pp)); ]
  
  let constraints_neg pp x_t = 
    [CFSSort (("flow14", pp), "flag"); CFSSort (("flow13", pp), "flag");
     CFSSort (("flow12", pp), "flag"); CFSSort (("inflag", pp), "flag");
     CFSSort (("x_t", pp), "expr"); CFSSort (("x_f1", pp), "value");
     CFSSort (("x_f1'", pp), "vbool"); CFSSort (("x_o", pp), "value");
     CFSSort (("x_s", pp), "state"); CFSEq_expr (("x_t", pp), x_t);
     CFSFilter (neg ("flow13", pp) ("flow14", pp) ("x_f1'", pp) ("x_o", pp));
     CFSFilter (isBool ("flow12", pp) ("flow13", pp) ("x_f1", pp) ("x_f1'", pp));
     CFSSub (("inflag", pp), ("x_s", pp), ("x_s", 0 :: pp));
     CFSSub (("inflag", pp), ("x_o", 0 :: pp), ("x_f1", pp));
     CFSFlagTest (("inflag", pp), ("x_f1", pp), ("flow12", pp));
     CFSFlag (("inflag", pp), ("inflag", 0 :: pp)); ]
  
  let constraints_skip pp = 
    [CFSSort (("flow15", pp), "flag"); CFSSort (("inflag", pp), "flag");
     CFSSort (("x_o", pp), "state"); CFSSort (("x_s", pp), "state");
     CFSFilter (id ("inflag", pp) ("flow15", pp) ("x_s", pp) ("x_o", pp)); ]
  
  let constraints_asn pp x_t1 x_t2 = 
    [CFSSort (("flow17", pp), "flag"); CFSSort (("flow16", pp), "flag");
     CFSSort (("inflag", pp), "flag"); CFSSort (("x_t1", pp), "ident");
     CFSSort (("x_t2", pp), "expr"); CFSSort (("x_f2", pp), "value");
     CFSSort (("x_o", pp), "state"); CFSSort (("x_s", pp), "state");
     CFSEq_ident (("x_t1", pp), x_t1); CFSEq_expr (("x_t2", pp), x_t2);
     CFSFilter (write ("flow16", pp) ("flow17", pp) ("x_t1", pp) ("x_s", pp) ("x_f2", pp) ("x_o", pp));
     CFSSub (("inflag", pp), ("x_s", pp), ("x_s", 1 :: pp));
     CFSSub (("inflag", pp), ("x_o", 1 :: pp), ("x_f2", pp));
     CFSFlagTest (("inflag", pp), ("x_f2", pp), ("flow16", pp));
     CFSFlag (("inflag", pp), ("inflag", 1 :: pp)); ]
  
  let constraints_seq pp x_t1 x_t2 = 
    [CFSSort (("flow19", pp), "flag"); CFSSort (("flow18", pp), "flag");
     CFSSort (("inflag", pp), "flag"); CFSSort (("x_t1", pp), "stat");
     CFSSort (("x_t2", pp), "stat"); CFSSort (("x_f1", pp), "state");
     CFSSort (("x_o", pp), "state"); CFSSort (("x_s", pp), "state");
     CFSEq_stat (("x_t1", pp), x_t1); CFSEq_stat (("x_t2", pp), x_t2);
     CFSSub (("flow18", pp), ("x_f1", pp), ("x_s", 1 :: pp));
     CFSSub (("flow18", pp), ("x_o", 1 :: pp), ("x_o", pp));
     CFSFlagTest (("flow18", pp), ("x_o", pp), ("flow19", pp));
     CFSFlag (("flow18", pp), ("inflag", 1 :: pp));
     CFSSub (("inflag", pp), ("x_s", pp), ("x_s", 0 :: pp));
     CFSSub (("inflag", pp), ("x_o", 0 :: pp), ("x_f1", pp));
     CFSFlagTest (("inflag", pp), ("x_f1", pp), ("flow18", pp));
     CFSFlag (("inflag", pp), ("inflag", 0 :: pp)); ]
  
  let constraints_if pp x_t1 x_t2 x_t3 = 
    [CFSSort (("flow26", pp), "flag"); CFSSort (("flow25", pp), "flag");
     CFSSort (("flow24", pp), "flag"); CFSSort (("flow23", pp), "flag");
     CFSSort (("flow22", pp), "flag"); CFSSort (("flow21", pp), "flag");
     CFSSort (("flow20", pp), "flag"); CFSSort (("inflag", pp), "flag");
     CFSSort (("x_t1", pp), "expr"); CFSSort (("x_t2", pp), "stat");
     CFSSort (("x_t3", pp), "stat"); CFSSort (("x_f1", pp), "value");
     CFSSort (("x_f1'", pp), "vbool"); CFSSort (("x_o", pp), "state");
     CFSSort (("x_s", pp), "state"); CFSEq_expr (("x_t1", pp), x_t1);
     CFSEq_stat (("x_t2", pp), x_t2); CFSEq_stat (("x_t3", pp), x_t3);
     CFSFlag (("flow23", pp), ("flow26", pp));
     CFSFlag (("flow25", pp), ("flow26", pp));
     CFSSub (("flow22", pp), ("x_s", pp), ("x_s", 1 :: pp));
     CFSSub (("flow22", pp), ("x_o", 1 :: pp), ("x_o", pp));
     CFSFlagTest (("flow22", pp), ("x_o", pp), ("flow23", pp));
     CFSFlag (("flow22", pp), ("inflag", 1 :: pp));
     CFSFilter (isTrue ("flow21", pp) ("flow22", pp) ("x_f1'", pp));
     CFSSub (("flow24", pp), ("x_s", pp), ("x_s", 2 :: pp));
     CFSSub (("flow24", pp), ("x_o", 2 :: pp), ("x_o", pp));
     CFSFlagTest (("flow24", pp), ("x_o", pp), ("flow25", pp));
     CFSFlag (("flow24", pp), ("inflag", 2 :: pp));
     CFSFilter (isFalse ("flow21", pp) ("flow24", pp) ("x_f1'", pp));
     CFSFilter (isBool ("flow20", pp) ("flow21", pp) ("x_f1", pp) ("x_f1'", pp));
     CFSSub (("inflag", pp), ("x_s", pp), ("x_s", 0 :: pp));
     CFSSub (("inflag", pp), ("x_o", 0 :: pp), ("x_f1", pp));
     CFSFlagTest (("inflag", pp), ("x_f1", pp), ("flow20", pp));
     CFSFlag (("inflag", pp), ("inflag", 0 :: pp)); ]
  
  let constraints_while pp x_t1 x_t2 = 
    [CFSSort (("flow34", pp), "flag"); CFSSort (("flow33", pp), "flag");
     CFSSort (("flow32", pp), "flag"); CFSSort (("flow31", pp), "flag");
     CFSSort (("flow30", pp), "flag"); CFSSort (("flow29", pp), "flag");
     CFSSort (("flow28", pp), "flag"); CFSSort (("flow27", pp), "flag");
     CFSSort (("inflag", pp), "flag"); CFSSort (("x_t1", pp), "expr");
     CFSSort (("x_t2", pp), "stat"); CFSSort (("x_f1", pp), "value");
     CFSSort (("x_f1'", pp), "vbool"); CFSSort (("x_f2", pp), "state");
     CFSSort (("x_o", pp), "state"); CFSSort (("x_s", pp), "state");
     CFSEq_expr (("x_t1", pp), x_t1); CFSEq_stat (("x_t2", pp), x_t2);
     CFSFlag (("flow31", pp), ("flow34", pp));
     CFSFlag (("flow33", pp), ("flow34", pp));
     CFSSub (("flow30", pp), ("x_f2", pp), ("x_s", pp));
     CFSSub (("flow30", pp), ("x_o", pp), ("x_o", pp));
     CFSFlagTest (("flow30", pp), ("x_o", pp), ("flow31", pp));
     CFSFlag (("flow30", pp), ("inflag", pp));
     CFSSub (("flow29", pp), ("x_s", pp), ("x_s", 1 :: pp));
     CFSSub (("flow29", pp), ("x_o", 1 :: pp), ("x_f2", pp));
     CFSFlagTest (("flow29", pp), ("x_f2", pp), ("flow30", pp));
     CFSFlag (("flow29", pp), ("inflag", 1 :: pp));
     CFSFilter (isTrue ("flow28", pp) ("flow29", pp) ("x_f1'", pp));
     CFSFilter (id ("flow32", pp) ("flow33", pp) ("x_s", pp) ("x_o", pp));
     CFSFilter (isFalse ("flow28", pp) ("flow32", pp) ("x_f1'", pp));
     CFSFilter (isBool ("flow27", pp) ("flow28", pp) ("x_f1", pp) ("x_f1'", pp));
     CFSSub (("inflag", pp), ("x_s", pp), ("x_s", 0 :: pp));
     CFSSub (("inflag", pp), ("x_o", 0 :: pp), ("x_f1", pp));
     CFSFlagTest (("inflag", pp), ("x_f1", pp), ("flow27", pp));
     CFSFlag (("inflag", pp), ("inflag", 0 :: pp)); ]
  
  let constraints_stat pp term =
    match term with
    | Skip -> constraints_skip pp 
    | ColonEqual (x_t1, x_t2) -> constraints_asn pp x_t1 x_t2
    | SemicolonSemicolon (x_t1, x_t2) -> constraints_seq pp x_t1 x_t2
    | If (x_t1, x_t2, x_t3) -> constraints_if pp x_t1 x_t2 x_t3
    | While (x_t1, x_t2) -> constraints_while pp x_t1 x_t2
  
  let constraints_expr pp term =
    match term with
    | Const x_t -> constraints_lit_int pp x_t
    | Var x_t -> constraints_var pp x_t
    | Plus (x_t1, x_t2) -> constraints_add pp x_t1 x_t2
    | EqualEqual (x_t1, x_t2) -> constraints_eq pp x_t1 x_t2
    | Bang x_t -> constraints_neg pp x_t
  
  let generate_constraints gent program =
    let c = ref [] in
    gent (fun pp term -> c := constraints_expr pp term @ !c) (fun pp term -> c := constraints_stat pp term @ !c) [] program;
    !c
end

module MakeStoreFs (A : ABSTRACT) = struct
  open A
  type t = Ident.t PMap.t * Lit.t PMap.t * State.t option PMap.t * Value.t option PMap.t * Vint.t option PMap.t * Vbool.t option PMap.t * Flag.t option PMap.t * PSet.t
  
  let empty wid = (PMap.empty, PMap.empty, PMap.empty, PMap.empty, PMap.empty, PMap.empty, PMap.empty, wid)
  
  let get_ident v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) = Some (PMap.find v env_ident)
  let set_ident v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t) =
    let env_ident = match x with None -> env_ident | Some x -> PMap.add v x env_ident in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t)
  let get_lit v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) = Some (PMap.find v env_lit)
  let set_lit v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t) =
    let env_lit = match x with None -> env_lit | Some x -> PMap.add v x env_lit in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t)
  let get_state v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) = default None v env_state
  let set_state v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t) =
    let env_state = add_wid v x env_state wid State.lub State.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t)
  let get_value v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) = default None v env_value
  let set_value v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t) =
    let env_value = add_wid v x env_value wid Value.lub Value.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t)
  let get_vint v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) = default None v env_vint
  let set_vint v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t) =
    let env_vint = add_wid v x env_vint wid Vint.lub Vint.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t)
  let get_vbool v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) = default None v env_vbool
  let set_vbool v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t) =
    let env_vbool = add_wid v x env_vbool wid Vbool.lub Vbool.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t)
  let get_flag v ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) = default None v env_flag
  let set_flag v x ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t) =
    let env_flag = add_wid v x env_flag wid Flag.lub Flag.widen in
    ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, wid) : t)
  
  let leq ((env1_ident, env1_lit, env1_state, env1_value, env1_vint, env1_vbool, env1_flag, _) : t) ((env2_ident, env2_lit, env2_state, env2_value, env2_vint, env2_vbool, env2_flag, _) : t) =
    env_leq env1_state env2_state State.leq &&
    env_leq env1_value env2_value Value.leq &&
    env_leq env1_vint env2_vint Vint.leq &&
    env_leq env1_vbool env2_vbool Vbool.leq &&
    env_leq env1_flag env2_flag Flag.leq
  
  let pp_print ff ((env_ident, env_lit, env_state, env_value, env_vint, env_vbool, env_flag, _) : t) =
    env_print ff "%a |-> %a@," env_ident Ident.pp_print;
    env_print ff "%a |-> %a@," env_lit Lit.pp_print;
    env_print ff "%a |-> %a@," env_state State.pp_print;
    env_print ff "%a |-> %a@," env_value Value.pp_print;
    env_print ff "%a |-> %a@," env_vint Vint.pp_print;
    env_print ff "%a |-> %a@," env_vbool Vbool.pp_print;
    env_print ff "%a |-> %a@," env_flag Flag.pp_print;
end

module MakePrimFs (A : ABSTRACT) = struct
  module Store = MakeStoreFs(A)
  open A
  
  type filter_constraint = string * pvar list * pvar list * (Store.t -> Store.t)
  
  let litToVal inflag outflag in0 out0 =
    ("litToVal", inflag :: [in0], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind1 litToVal (get_lit in0 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_value out0 (Some res0) @@
        store)
  
  let read inflag outflag in0 in1 out0 =
    ("read", inflag :: [in0; in1], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind2 read (get_ident in0 store) (get_state in1 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_value out0 (Some res0) @@
        store)
  
  let isInt inflag outflag in0 out0 =
    ("isInt", inflag :: [in0], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind1 isInt (get_value in0 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_vint out0 (Some res0) @@
        store)
  
  let add inflag outflag in0 in1 out0 =
    ("add", inflag :: [in0; in1], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind2 add (get_vint in0 store) (get_vint in1 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_value out0 (Some res0) @@
        store)
  
  let eq inflag outflag in0 in1 out0 =
    ("eq", inflag :: [in0; in1], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind2 eq (get_vint in0 store) (get_vint in1 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_value out0 (Some res0) @@
        store)
  
  let isBool inflag outflag in0 out0 =
    ("isBool", inflag :: [in0], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind1 isBool (get_value in0 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_vbool out0 (Some res0) @@
        store)
  
  let neg inflag outflag in0 out0 =
    ("neg", inflag :: [in0], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind1 neg (get_vbool in0 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_value out0 (Some res0) @@
        store)
  
  let write inflag outflag in0 in1 in2 out0 =
    ("write", inflag :: [in0; in1; in2], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind3 write (get_ident in0 store) (get_state in1 store) (get_value in2 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_state out0 (Some res0) @@
        store)
  
  let id inflag outflag in0 out0 =
    ("id", inflag :: [in0], outflag :: [out0], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind1 id (get_state in0 store) with
      | None -> store
      | Some (res0) ->
        set_flag outflag (Some ()) @@
        set_state out0 (Some res0) @@
        store)
  
  let isTrue inflag outflag in0  =
    ("isTrue", inflag :: [in0], outflag :: [], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind1 isTrue (get_vbool in0 store) with
      | None -> store
      | Some () ->
        set_flag outflag (Some ()) @@
        store)
  
  let isFalse inflag outflag in0  =
    ("isFalse", inflag :: [in0], outflag :: [], fun store ->
      let open Store in
      if (get_flag inflag store) = None then store else
      match bind1 isFalse (get_vbool in0 store) with
      | None -> store
      | Some () ->
        set_flag outflag (Some ()) @@
        store)
  
  let gent_expr = gent_expr
  let gent_stat = gent_stat
end
