type path = int list
type var = string
type pvar = var * path

let print_pvar ff (name, path) =
  List.iter (fun d -> Format.fprintf ff "%d-" (d + 1)) (List.rev path); Format.fprintf ff "%s" name

module PVar = struct
  type t = pvar
  let compare = compare
  let pp_print = print_pvar
end
module PMap = Map.Make(PVar)
module PSet = Set.Make(PVar)

module type PRINTABLE = sig
  type t
  val pp_print : Format.formatter -> t -> unit
end

module type DOMAIN = sig
  type t
  val lub : t -> t -> t
  val widen : t -> t -> t
  val leq : t -> t -> bool
  val pp_print : Format.formatter -> t option -> unit
end

module Flag = struct
  type t = unit
  let top = ()
  let lub () () = ()
  let glb () () = Some ()
  let leq () () = true
  let widen = lub
  let pp_print ff = function None -> Format.fprintf ff "bot" | Some () -> Format.fprintf ff "top"
end

let default def x env =
  try PMap.find x env with Not_found -> def

let add_wid v x env wid lubf widf =
  match x with
  | None -> env
  | Some x ->
    match PMap.find v env with
    | exception Not_found -> PMap.add v (Some x) env
    | None -> PMap.add v (Some x) env
    | Some y ->
      if PSet.mem v wid then
        PMap.add v (Some (widf y x)) env
      else
        PMap.add v (Some (lubf y x)) env

let env_leq env1 env2 leq =
  PMap.for_all (fun _ b -> b) (PMap.merge (fun _ x1 x2 ->
      match x1, x2 with
      | None, _ | Some None, _ -> None
      | Some _, None | Some _, Some None -> Some false
      | Some (Some x1), Some (Some x2) -> Some (leq x1 x2)
    ) env1 env2)

let env_print ff fmt env printer =
  Format.fprintf ff "@[<v>";
  PMap.iter (fun x v -> Format.fprintf ff fmt print_pvar x printer v) env;
  Format.fprintf ff "@]@."

let make_updater getf setf v1 v2 store =
  setf v2 (getf v1 store) store

let sort_upds upds =
  let upds = Array.of_list upds in
  let init_vars = ref PMap.empty in
  Array.iteri (fun i (__, start, _, _) ->
      List.iter (fun v ->
          init_vars := PMap.add v (i :: (try PMap.find v !init_vars with Not_found -> [])) !init_vars
        ) start
    ) upds;
  let seen = Array.make (Array.length upds) 0 in
  let wid = ref PSet.empty in
  let sorted = ref [] in
  let rec dfs v i =
    match seen.(i) with
    | 0 ->
      seen.(i) <- 1;
      let ((_, _, dest, _) as c) = upds.(i) in
      List.iter (fun v ->
          let nxt = try PMap.find v !init_vars with Not_found -> [] in
          List.iter (dfs v) nxt
        ) dest;
      seen.(i) <- 2;
      sorted := c :: !sorted
    | 1 -> wid := PSet.add v !wid
    | _ -> ()
  in
  for i = 0 to Array.length upds - 1 do
    dfs ("", []) i
  done;
  (!wid, !sorted)

let noneS none = fun _ -> none
let bindS bind none f = function None -> none | Some x -> bind (f x)
let none0 = None
let none1 x = noneS none0 x
let none2 x = noneS none1 x
let none3 x = noneS none2 x
let none4 x = noneS none3 x
let none5 x = noneS none4 x
let none6 x = noneS none5 x
let none7 x = noneS none6 x
let none8 x = noneS none7 x

let bind0 f = f
let bind1 f = bindS bind0 none0 f
let bind2 f = bindS bind1 none1 f
let bind3 f = bindS bind2 none2 f
let bind4 f = bindS bind3 none3 f
let bind5 f = bindS bind4 none4 f
let bind6 f = bindS bind5 none5 f
let bind7 f = bindS bind6 none6 f
let bind8 f = bindS bind7 none7 f
let bind9 f = bindS bind8 none8 f
