open Cstr
open While_cstr

module Prim = struct
  type filter_constraint = string * pvar list * pvar list
  let litToVal v1 v2 = ("litToVal", [v1], [v2])
  let read v1 v2 v3 = ("read", [v1; v2], [v3])
  let isInt v1 v2 = ("isInt", [v1], [v2])
  let add v1 v2 v3 = ("add", [v1; v2], [v3])
  let eq v1 v2 v3 = ("eq", [v1; v2], [v3])
  let isBool v1 v2 = ("isBool", [v1], [v2])
  let neg v1 v2 = ("neg", [v1], [v2])
  let write v1 v2 v3 v4 = ("write", [v1; v2; v3], [v4])
  let id v1 v2 = ("id", [v1], [v2])
  let isTrue v1 = ("isTrue", [v1], [])
  let isFalse v1 = ("isFalse", [v1], [])

  let gent_expr = gent_expr
  let gent_stat = gent_stat
end

module CGen = Constraints(Prim)
open CGen

let rec pp_print_expr ff = function
  | Const d -> Format.fprintf ff "Const %d" d
  | Var x -> Format.fprintf ff "Var %S" x
  | Plus (x1, x2) -> Format.fprintf ff "Plus (%a, %a)" pp_print_expr x1 pp_print_expr x2
  | EqualEqual (x1, x2) -> Format.fprintf ff "EqualEqual (%a, %a)" pp_print_expr x1 pp_print_expr x2
  | Bang x -> Format.fprintf ff "Bang (%a)" pp_print_expr x

and pp_print_stat ff = function
  | Skip -> Format.fprintf ff "Skip"
  | ColonEqual (x1, x2) -> Format.fprintf ff "ColonEqual (%S, %a)" x1 pp_print_expr x2
  | SemicolonSemicolon (x1, x2) -> Format.fprintf ff "SemicolonSemicolon (%a, %a)" pp_print_stat x1 pp_print_stat x2
  | If (x1, x2, x3) -> Format.fprintf ff "If (%a, %a, %a)" pp_print_expr x1 pp_print_stat x2 pp_print_stat x3
  | While (x1, x2) -> Format.fprintf ff "While (%a, %a)" pp_print_expr x1 pp_print_stat x2

let print_fc ff (filter, inv, outv) =
  Format.fprintf ff "(%a) = %s(%a)" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ", ") print_pvar) outv filter (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ", ") print_pvar) inv

let print_constraint ff = function
  | CSort (v, s) -> Format.fprintf ff "[%a : %s]" print_pvar v s
  | CFilter fc -> Format.fprintf ff "[%a]" print_fc fc
  | CSub (v1, v2) -> Format.fprintf ff "[%a <= %a]" print_pvar v1 print_pvar v2
  | CEq_ident (v, x) -> Format.fprintf ff "[%a = %s]" print_pvar v x
  | CEq_lit (v, x) -> Format.fprintf ff "[%a = %d]" print_pvar v x
  | CEq_expr (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v pp_print_expr x
  | CEq_stat (v, x) -> Format.fprintf ff "[%a = %a]" print_pvar v pp_print_stat x

let t = While (Bang (EqualEqual (Var "x", Const 0)), ColonEqual ("x", Plus (Var "x", Const (-1))))
let cstrs = generate_constraints gent_stat t
let () = List.iter (fun c -> Format.printf "%a@." print_constraint c) cstrs
