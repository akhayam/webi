open Lambda_cfa

module BaseTypes = struct
  module Ident = struct
    type t = string
    let print ff s = Format.fprintf ff "%s" s
    let compare = compare
  end

  module Globalcontext = struct
    type t = int list list
    let print ff s = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") (pp_pp_ppmap !pp_default_map)) s
    let compare = compare
  end
end

let k = 1
let rec lastk k l =
  match k, l with
  | 0, _ -> []
  | _, [] -> []
  | k, x :: l -> x :: lastk (k - 1) l

module Abstract : MakeTypes(BaseTypes).ABSTRACT = struct
  module X = MakeTypes(BaseTypes)
  open X

  let envset env x v = X.Env1.M.add x v env
  let framepush gc pp = lastk k (pp :: gc)
end

module Cstr = MakeCstr(BaseTypes)(Abstract)

let make_store () =
  let init_env = Cstr.Env.M.singleton Cstr.Env1.M.empty in
  let store = Cstr.Store.create_store () in
  let () = Cstr.Store._in_eval_set store ([], [])
      (function None -> fun _ -> (init_env, false) | Some env -> Cstr.Env.update init_env env) in
  store


let test t0 =
  fill_default_ppmap gent_lterm t0;
  let store = make_store () in
  let () = Cstr.run_constraints store t0 in
  Format.printf "%a@." (pp_lterm Format.pp_print_string [] !pp_default_map) t0;
  Format.printf "%a@." Cstr.Store.print store

let let_ n v body = App (Lam (n, body), v)

let t0 =
  let_ "f" (Lam ("x", App (Var "x", Var "x")))
    (App (Lam ("y", Var "y"), App (Var "f", Var "f")))

let () = test t0
