(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)
(* =====   Memory representation of semantics elements   ===== *)
(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

module SMap = Map.Make(String)

(*
   A sort is either a program sort, which is the sort of terms made with a
   constructor, or a flow sort (or base sort in the paper), which are all
   other sorts: the sort of values when interpreting the semantics, or the
   sort of subterms used only as inputs of constructors.
*)

type flow_sort = string
type program_sort = string
type sort =
  | Flow of flow_sort
  | Program of program_sort

let string_of_sort = function
  | Program s -> s
  | Flow s -> s

(*
   The signature of a constructor is its name, the sorts of all its arguments, and
   the sort of the term that is produced, which is always a program sort.
*)
type constructor_signature = {
  cs_name : string ;
  cs_input_sorts : sort list ;
  cs_output_sort : program_sort ;
}

(*
   The signature of a filter is its name, the sort of its inputs, and the sort of its outputs.
*)
type filter_signature = {
  fs_name : string ;
  fs_input_sorts : sort list ;
  fs_output_sorts : sort list ;
}

type var = string

type term =
  | TVar of string
  | TConstr of string * term list

type hook = {
  h_name : string ;
  h_annot : string ;
  h_inputs : var list ;
  h_term : term ;
  h_outputs : var list ;
}

type filter = {
  f_name : string ;
  f_inputs : var list ;
  f_outputs : var list ;
}

type bone =
  | Hook of hook
  | Filter of filter
  | Branching of branching

and skeleton = bone list

and branching = {
  b_branches : skeleton list ;
  b_outputs : var list ;
}

type rule = {
  r_constructor : string ;
  r_constructor_arguments : var list ;
  r_skeleton : skeleton ;
  r_input_sort : sort ;
  r_output_sort : sort ;
  r_name : string ;
}

(*
   The definition of a hook is the sort of the term it executes (which is always a program sort),
   the sort of its input and the sort of its output, as well as the various rules for it
   (a map from the constructor name to the rule)
*)
type hook_definition = {
  hd_term_sort : program_sort ;
  hd_input_sorts : sort list ;
  hd_output_sorts : sort list ;
  hd_rules : rule SMap.t ;
}


(*
   The environment maps sort names to their description, constructor names to their signatures,
   hooks to their signatures, and filters to their signatures.
   Note that, for now, hooks are identified to the program sort of the term they execute.
*)
type env = {
  env_sorts : sort SMap.t ;
  env_constr_signatures : constructor_signature SMap.t ;
  env_hook_definitions : hook_definition SMap.t ;
  env_filter_signatures : filter_signature SMap.t ;
}

let separate_by_sort f l =
  let t = Hashtbl.create 17 in
  List.iter (fun x -> let i = f x in Hashtbl.replace t i (x :: try Hashtbl.find t i with Not_found -> [])) l;
  Hashtbl.to_seq t
  |> Seq.map (fun (i, l) -> (i, List.rev l))
  |> List.of_seq

let get_constructors_by_sort env =
  separate_by_sort (fun c -> c.cs_output_sort) (List.map snd (SMap.bindings env.env_constr_signatures))

let get_rules_by_sort env rules =
  separate_by_sort (fun r -> (SMap.find r.r_constructor env.env_constr_signatures).cs_output_sort) rules

let is_base_type env s =
  SMap.exists (fun _ c -> List.exists (fun s1 -> s1 = Flow s) c.cs_input_sorts) env.env_constr_signatures

(* Returns all flow types that can appear as arguments of a term *)
let get_term_base_types env =
  let t = Hashtbl.create 17 in
  SMap.iter (fun _ c ->
      List.iter (fun s -> match s with Flow s -> Hashtbl.replace t s () | _ -> ()) c.cs_input_sorts
    ) env.env_constr_signatures;
  Hashtbl.to_seq_keys t |> List.of_seq |> List.sort compare

let get_term_program_types env =
  env.env_sorts
  |> SMap.to_seq
  |> Seq.filter_map (fun (_, s) -> match s with Program s -> Some s | _ -> None)
  |> List.of_seq
  |> List.sort compare

(*
type plvalue =
  | PLVar of string
  | PLMapGet of plvalue * pvalue

and pvalue =
  | PLValue of plvalue
  | PFun of string * pvalue list
  | PLet of string * pvalue * pvalue

type pconstraint =
  | PTrue
  | PConj of pconstraint * pconstraint
  | PForall of string * pvalue * pconstraint
  | PMapForall of string * string * pvalue * pconstraint
  | PPredicate of string * pvalue list * plvalue list
  | PLetC of string * pvalue * pconstraint

type 'a pstatic_map =
  | PBaseStaticMap of (pvalue * 'a) list
  | PJoin2StaticMap of 'a pstatic_map * 'a pstatic_map
  | PJoinStaticMap of string * pvalue * 'a pstatic_map
  | PLetStaticMap of string * pvalue * 'a pstatic_map

type 'a pstatic_set =
  | PBaseStaticSet of 'a list
  | PJoin2StaticSet of 'a pstatic_set * 'a pstatic_set
  | PJoinStaticSet of string * pvalue * 'a pstatic_set
  | PLetStaticSet of string * pvalue * 'a pstatic_set
*)

type typedesc =
  | MapType of typedesc * typedesc
  | SetType of typedesc
  | ListType of typedesc
  | BaseType of string
  | TupleType of typedesc list
  | Program_point of typedesc
  | TopType
  | BottomType

type quark_decl = {
  q_name : string ;
  q_inputs : typedesc list ;
  q_output : typedesc ;
}

type 'a lambda = string list * 'a
type pvalue =
  | PNamedVar of string
  | PMapGet of pvalue * pvalue
  | PFunc of string * pvalue list
  | PVLet of pvalue * pvalue lambda
  | PVWith of pconstr * pvalue
  | PVTuple of pvalue list
  | PVSet of pvalue list
  | PVList of pvalue list
  | PVJoin of pvalue * pvalue lambda
  | PVUnit

and pconstr =
  | PTrue
  | PConj of pconstr * pconstr
  | PCLet of pvalue * pconstr lambda
  | PSub of pvalue * pvalue
  | PForall of pvalue * pconstr lambda

type pfilter_constr_decl = {
  pfc_name : string ;
  pfc_value : pvalue lambda ;
}

type phookin_constr_decl = {
  phi_name : string ;
  phi_annot : string ;
  phi_value : pvalue lambda ;
}

type phookout_constr_decl = {
  pho_name : string ;
  pho_annot : string ;
  pho_value : pvalue lambda ;
}

type prulein_constr_decl = {
  pri_name : string ;
  pri_value : pvalue lambda ;
}

type pruleout_constr_decl = {
  pro_name : string ;
  pro_value : pvalue lambda ;
}

type var_constr_decl = {
  vc_name : string ;
  vc_type : typedesc ;
}

type pcdecls = {
  pc_types : (string * typedesc) list ;
  pc_vars : var_constr_decl list ;
  pc_rulein_vars : (string * typedesc) list ;
  pc_ruleout_vars : (string * typedesc) list ;
  pc_quarks : quark_decl list ;
  pc_filters : pfilter_constr_decl list ;
  pc_hookins : phookin_constr_decl list ;
  pc_hookouts : phookout_constr_decl list ;
  pc_ruleins : prulein_constr_decl list ;
  pc_ruleouts : pruleout_constr_decl list ;
}
