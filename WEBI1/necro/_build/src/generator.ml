open Types
open Skeleton
open Camlident
open Printing

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)
(* =====          Parameters & useful functions          ===== *)
(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

(* gets a filename's radix *)
let without_extension s =
  let r = Str.regexp "\\." in
  let i =
    try Str.search_backward r s (String.length s - 1)
    with Not_found -> String.length s
  in
  String.sub s 0 i

(* prints the sort's name with "type " before it *)
let print_sort_type_declaration = function
  | Flow s -> Cat [cs "type "; cs s]
  | _ -> assert false

let rec print_term = function
  | TVar v -> cs v
  | TConstr (c, tl) ->
    Cat [cs "("; cs (get_alpha_name c);
         print_constructor_args (List.map print_term tl); cs ")"]

let print_var v = cs v

let empty_interpgen (tenv, res) =
  res

(* turns a hook into a let [output] = [name] [state] [term] in OCaml statement *)
let print_hook h =
  Cat [make_list ~first:(cs "let (") ~last:(cs ") = ") ~sep:(cs ", ") (List.map print_var h.h_outputs);
       cs h.h_name; cs " ";
       make_list ~sep:(cs " ") (List.map print_var h.h_inputs); cs " "; print_term h.h_term; cs " in"]

let hook_interpgen env h (tenv, res) =
  (* let sort = get_env_term_sort env tenv h.h_term in *)
  Cat [res; nlc; print_hook h]

(* turns a filter into a pattern matching (if None, fails, otherwise returns
   the filter's output values)
*)
let print_filter f =
  Cat [
    make_list ~first:(cs "let (") ~last:(cs ") = match ") ~sep:(cs ", ") (List.map print_var f.f_outputs);
    cs f.f_name; cs " ";
    make_list ~sep:(cs " ") (List.map print_var f.f_inputs); cs " with"; nlc;
    cs "| None -> raise Branch_fail"; nlc; cs "| Some result -> result"; nlc;
    cs "in"
  ]

let filter_interpgen f (tenv, res) =
  Cat [res; nlc; print_filter f]

(* A branching will become an OCaml block of the following form :
   let [expected_branching_outputs] =
    try [branch1] with Branch_fail ->
    try [branch2] with Branch_fail ->
    ...
    try [lastbranch] with Branch_fail -> raise Branch_fail
*)
let print_branching env vars outs =
  let outputs = make_list ~first:(cs "(") ~last:(cs ")") ~sep:(cs ", ") (List.map print_var vars) in
  Cat [
    cs "let "; outputs; cs " ="; Base [indent; nl];
    make_list (List.map (fun body ->
        Cat [cs "try"; Base [indent; nl]; body; nlc; outputs; Base[dedent; nl]; cs "with Branch_fail ->"; nlc]
      ) outs);
    cs "raise Branch_fail"; Base [dedent; nl]; cs "in"
  ]

let merge_interpgen env vars outs (tenv, res) =
  Cat [res; nlc; print_branching env vars (List.map snd outs)]

let interpgen_interp env =
  add_interpretation (typing_interp env) {
    empty_interp = empty_interpgen ;
    hook_interp = hook_interpgen env ;
    filter_interp = filter_interpgen ;
    merge_interp = merge_interpgen env ;
    before_merge_interp = (fun _ -> Cat []) ;
  }

(* A rule will be translated as :
   eval_rule_name [args] state =
     {process every bone, returning x_o at the end}
   The initial "let rec" or "and" declaration is omitted,
   as it is added when gluing all declarations together.
*)
let print_rule env r =
  let prefix = Cat [
      cs "__eval_"; cs r.r_name; cs " ";
      make_list ~sep:(cs " ") (List.map print_var r.r_constructor_arguments); cs " x_s ="; Base [indent]]
  in
  let init_env = make_init_env env r in
  let init_state = (init_env, prefix) in
  let (_, result) = interpret_skeleton (interpgen_interp env) init_state r.r_skeleton in
  Cat [result; nlc; cs "x_o"; Base [dedent]]

(* Print the dispatching function for a hook.
   As for pp_print_rule, the initial "and" is omitted.
*)
let print_dispatch (name, rules) =
  let prefix = Cat [cs name; cs " x_s = function"] in
  let body = make_list ~sep:nlc (List.map (fun {r_name; r_constructor; r_constructor_arguments} ->
      Cat [cs "| ";
           cs (get_alpha_name r_constructor);
           print_constructor_args (List.map print_var r_constructor_arguments);
           cs " -> __eval_";
           cs r_name;
           make_list ~first:(cs " ") ~sep:(cs " ") ~last:(cs " ") (List.map print_var r_constructor_arguments);
           cs " x_s"
          ]
    ) rules)
  in
  Cat [prefix; nlc; body]

(* writes the mutually-recursive eval functions *)
let print_eval env =
  let all_rules = List.flatten
      (List.map (fun (_, h) -> List.map snd (SMap.bindings h.hd_rules)) (SMap.bindings env.env_hook_definitions))
  in
  let disp = List.map (fun (name, h) ->
      (name, List.map snd (SMap.bindings h.hd_rules))) (SMap.bindings env.env_hook_definitions)
  in
  make_list
    ~first:(cs "let rec ") ~sep:(Cat [nlc; nlc; cs "and "]) ~last:(Cat [nlc; nlc])
    (List.map (print_rule env) all_rules @ List.map print_dispatch disp)

(* finds all the sorts used as output types in hooks *)
let get_output_sorts env =
  List.sort_uniq compare
    (List.flatten
       (List.map (fun (_, h) -> h.hd_output_sorts) (SMap.bindings env.env_hook_definitions)))

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)
(* =====                    Term type                    ===== *)
(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

(* type x *)
let print_base_flow_sorts_types env =
  let ts =
    env.env_sorts
    |> SMap.to_seq
    |> Seq.filter_map (fun (_, s) -> match s with Flow _ -> Some s | _ -> None)
    |> List.of_seq
  in
  make_list ~sep:nlc (List.map print_sort_type_declaration ts)

(* val atom : x -> y *)
let print_atoms_signatures env atoms =
  Cat [
    make_list (List.map (fun s -> Cat [cs "val "; cs s; nlc]) atoms);
    make_list ~sep:nlc
      (List.map (fun s ->
           let s = string_of_sort s in Cat [cs "val print_"; cs s; cs " : "; cs s; cs " -> unit"])
          (get_output_sorts env))
  ]

let print_type targs = function
  | Program s -> Cat [targs; cs s]
  | Flow s -> cs s

(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)
(* =====                Filter Signatures                ===== *)
(* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== *)

(* generates the correct filter signature from the filter structure *)
let print_filter_sig targs { fs_name; fs_input_sorts; fs_output_sorts } =
  let os =
    match fs_output_sorts with
    | [] -> Cat [cs "unit"]
    | [output] -> print_type targs output
    | outputs ->
      make_list ~first:(cs "(") ~last:(cs ")") ~sep:(cs " * ") (List.map (print_type targs) outputs)
  in
  Cat [
    cs "val "; cs fs_name; cs " : ";
    make_list ~sep:(cs " -> ") (List.map (print_type targs) fs_input_sorts);
    cs " -> "; os; cs " option"
  ]

(* same for a list of filters *)
let print_filter_signatures env =
  let targs = print_base_args (get_term_base_types env) in
  make_list ~sep:nlc (List.map (fun (_, f) -> print_filter_sig targs f) (SMap.bindings env.env_filter_signatures))

(* writes the FLOW module type *)
let print_flow_module_type env atoms =
  Cat [
    cs "module type FLOW = sig"; Base [indent; nl];
    print_base_flow_sorts_types env; nlc; nlc;
    print_atoms_signatures env atoms; nlc; nlc;
    print_filter_signatures env; Base [dedent; nl]; cs "end"
  ]

(* writes the INTERPRETER module type *)
let print_interpreter_module_type env atoms =
  let targs = print_base_args (get_term_base_types env) in
  let os h =
    match h.hd_output_sorts with
    | [] -> Cat [cs "unit"]
    | [output] -> print_type targs output
    | outputs ->
      make_list ~first:(cs "(") ~last:(cs ")") ~sep:(cs " * ") (List.map (print_type targs) outputs)
  in
  Cat [
    cs "module type INTERPRETER = sig"; Base [indent; nl];
    print_base_flow_sorts_types env; nlc; nlc;
    print_atoms_signatures env atoms; nlc; nlc;
    make_list ~sep:nlc (List.map (fun (name, h) ->
        Cat [cs "val "; cs name; cs " : ";
             make_list ~sep:(cs " -> ") (List.map (print_type targs) h.hd_input_sorts);
             cs " -> "; print_type targs (Program h.hd_term_sort);
             cs " -> "; os h
            ]
      ) (SMap.bindings env.env_hook_definitions));
    Base [dedent; nl]; cs "end"
  ]

(* writes the MakeInterpreter functor *)
let print_makeinterpreter_functor env =
  let ts =
    env.env_sorts
    |> SMap.to_seq
    |> Seq.filter_map (fun (_, s) -> match s with Flow s -> Some s | _ -> None)
    |> List.of_seq
  in
  Cat [
    cs "module MakeInterpreter (F : FLOW) : (INTERPRETER with ";
    make_list ~sep:(cs " and ") (List.map (fun s -> Cat [cs "type "; cs s; cs " = F."; cs s]) ts);
    cs ") = struct"; Base [indent; nl];
    cs "include F"; nlc; nlc;
    print_eval env; Base [dedent; nl]; cs "end";
  ]

(* writes the interpreter generator modules to an output file *)
let generate_interpreter env atoms filename =
  let oc = open_out filename in
  let ff = Format.formatter_of_out_channel oc in
  let r = Cat [
      cs "exception Branch_fail"; nlc; nlc;
      print_ast_types env; nlc; nlc;
      print_flow_module_type env atoms; nlc; nlc;
      print_interpreter_module_type env atoms; nlc; nlc;
      print_makeinterpreter_functor env;
    ]
  in
  Format.fprintf ff "%a@." pp_print_cl r;
  close_out oc
