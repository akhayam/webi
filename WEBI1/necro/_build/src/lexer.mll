{
  open Parser
  open Lexing

  exception Lexing_error of string

  let kw = [
    "atom", ATOM;
    "branch", BRANCH;
    "end", END;
    "hook", HOOK;
    "of", OF;
    "or", OR;
    "type", TYPE;
    "val", VAL;

    "filter", FILTER;
    "forall", FORALL;
    "in", IN;
    "join", JOIN;
    "let", LET;
    "list", LIST;
    "map", MAP;
    "out", OUT;
    "program_point", PROGRAM_POINT;
    "rule", RULE;
    "set", SET;
    "True", TRUE;
    "var", VAR;
  ]

  let keywords = Hashtbl.create (List.length kw)
  let () = List.iter (fun (a, b) -> Hashtbl.add keywords a b) kw

  let newline lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <- { pos with pos_lnum = pos.pos_lnum + 1 ; pos_bol = pos.pos_cnum }
}

let alpha = ['a'-'z'] | ['A'-'Z']
let ident_car = alpha | '_' | '\'' | ['0'-'9']
let lident = ( ['a'-'z'] ident_car* ) | ( '_' ident_car* )
let uident = ['A'-'Z'] ident_car*
let whitespace = [' ' '\t']

rule token = parse
  | whitespace+ { token lexbuf }
  | "(*"        { comment lexbuf; token lexbuf }
  | "\n"        { (* let is_bol = lexbuf.lex_curr_p.pos_cnum = lexbuf.lex_curr_p.pos_bol + 1 in newline lexbuf; if is_bol then token lexbuf else NEWLINE *) newline lexbuf; token lexbuf }
  (* Operators *)
  | "|"         { BAR }
  | "||"        { BARBAR }
  | "|->"       { BARMINUSGREATER }
  | "^"         { CARET }
  | ":"         { COLON }
  | ","         { COMMA }
  | "="         { EQUAL }
  | "<-"        { LESSMINUS }
  | "<="        { LESSEQUAL }
  (* | "-"         { MINUS } *)
  | "->"        { MINUSGREATER }
  | "?>"        { QUESTIONGREATER }
  | ";"         { SEMI }
  | "/\\"       { SLASHBACKSLASH }
  | "*"         { STAR }
  (* Paired delimiters *)
  | "("         { LPAREN }
  | ")"         { RPAREN }
  | "{"         { LBRACE }
  | "}"         { RBRACE }
  | "["         { LBRACK }
  | "]"         { RBRACK }
  | "[|"        { LBRACKBAR }
  | "|]"        { BARRBRACK }
  | lident as s { try Hashtbl.find keywords s with Not_found -> LIDENT s }
  | uident as s { try Hashtbl.find keywords s with Not_found -> UIDENT s }
  | eof         { EOF }

and comment = parse
  | "\n" { newline lexbuf; comment lexbuf }
  | "*)" { () }
  | "(*" { comment lexbuf; comment lexbuf }
  | _    { comment lexbuf }
  | eof  { raise (Lexing_error "Unterminated comment") }
