
(* The type of tokens. *)

type token = 
  | VAR
  | VAL
  | UIDENT of (string)
  | TYPE
  | TRUE
  | STAR
  | SLASHBACKSLASH
  | SET
  | SEMI
  | RULE
  | RPAREN
  | RBRACK
  | RBRACE
  | QUESTIONGREATER
  | PROGRAM_POINT
  | OUT
  | OR
  | OF
  | MINUSGREATER
  | MAP
  | LPAREN
  | LIST
  | LIDENT of (string)
  | LET
  | LESSMINUS
  | LESSEQUAL
  | LBRACKBAR
  | LBRACK
  | LBRACE
  | JOIN
  | IN
  | HOOK
  | FORALL
  | FILTER
  | EQUAL
  | EOF
  | END
  | COMMA
  | COLON
  | CARET
  | BRANCH
  | BARRBRACK
  | BARMINUSGREATER
  | BARBAR
  | BAR
  | ATOM

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val main_constr: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Types.pcdecls)

val main: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (string list * Types.env)
