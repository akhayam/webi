
module MenhirBasics = struct
  
  exception Error
  
  type token = 
    | VAR
    | VAL
    | UIDENT of (
# 83 "src/parser.mly"
       (string)
# 13 "src/parser.ml"
  )
    | TYPE
    | TRUE
    | STAR
    | SLASHBACKSLASH
    | SET
    | SEMI
    | RULE
    | RPAREN
    | RBRACK
    | RBRACE
    | QUESTIONGREATER
    | PROGRAM_POINT
    | OUT
    | OR
    | OF
    | MINUSGREATER
    | MAP
    | LPAREN
    | LIST
    | LIDENT of (
# 82 "src/parser.mly"
       (string)
# 37 "src/parser.ml"
  )
    | LET
    | LESSMINUS
    | LESSEQUAL
    | LBRACKBAR
    | LBRACK
    | LBRACE
    | JOIN
    | IN
    | HOOK
    | FORALL
    | FILTER
    | EQUAL
    | EOF
    | END
    | COMMA
    | COLON
    | CARET
    | BRANCH
    | BARRBRACK
    | BARMINUSGREATER
    | BARBAR
    | BAR
    | ATOM
  
end

include MenhirBasics

let _eRR =
  MenhirBasics.Error

type _menhir_env = {
  _menhir_lexer: Lexing.lexbuf -> token;
  _menhir_lexbuf: Lexing.lexbuf;
  _menhir_token: token;
  mutable _menhir_error: bool
}

and _menhir_state = 
  | MenhirState275
  | MenhirState260
  | MenhirState257
  | MenhirState253
  | MenhirState250
  | MenhirState248
  | MenhirState245
  | MenhirState242
  | MenhirState238
  | MenhirState234
  | MenhirState231
  | MenhirState228
  | MenhirState217
  | MenhirState210
  | MenhirState206
  | MenhirState198
  | MenhirState195
  | MenhirState191
  | MenhirState188
  | MenhirState184
  | MenhirState182
  | MenhirState180
  | MenhirState178
  | MenhirState177
  | MenhirState175
  | MenhirState174
  | MenhirState173
  | MenhirState172
  | MenhirState167
  | MenhirState166
  | MenhirState165
  | MenhirState162
  | MenhirState160
  | MenhirState157
  | MenhirState154
  | MenhirState148
  | MenhirState144
  | MenhirState141
  | MenhirState139
  | MenhirState129
  | MenhirState124
  | MenhirState120
  | MenhirState118
  | MenhirState115
  | MenhirState112
  | MenhirState105
  | MenhirState100
  | MenhirState97
  | MenhirState86
  | MenhirState82
  | MenhirState80
  | MenhirState79
  | MenhirState76
  | MenhirState65
  | MenhirState59
  | MenhirState58
  | MenhirState56
  | MenhirState55
  | MenhirState53
  | MenhirState52
  | MenhirState50
  | MenhirState49
  | MenhirState46
  | MenhirState45
  | MenhirState40
  | MenhirState39
  | MenhirState33
  | MenhirState31
  | MenhirState28
  | MenhirState18
  | MenhirState15
  | MenhirState12
  | MenhirState8
  | MenhirState5
  | MenhirState3
  | MenhirState0

# 1 "src/parser.mly"
  
    open Types

    let mkflow l = List.map (fun x -> Flow x) l
    let mkenv_sorts bfs ps =
      let m = SMap.empty in
      let m = List.fold_left (fun m x -> SMap.add x (Flow x) m) m bfs in
      let m = List.fold_left (fun m x -> SMap.add x (Program x) m) m ps in
      m

    module SSet = Set.Make(String)
    let rec identify_hooks_bone hk = function
      | Hook h -> Hook h
      | Filter { f_name ; f_inputs ; f_outputs } when SSet.mem f_name hk ->
         let iv, it = match f_inputs with [iv; it] -> (iv, it) | _ -> failwith "Wrong number of arguments to hook" in
         let ov = match f_outputs with [ov] -> ov | _ -> failwith "Hook must have only one output argument" in
         Hook { h_name = f_name ; h_annot = "" ; h_inputs = [iv] ; h_term = TVar it ; h_outputs = [ov] }
      | Filter f -> Filter f
      | Branching b -> Branching { b with b_branches = List.map (identify_hooks_skeleton hk) b.b_branches }

    and identify_hooks_skeleton hk l = List.map (identify_hooks_bone hk) l

    type decl =
      | BaseType of string
      | ProgramType of string * (string * string list) list
      | FilterDecl of string * string list * string list
      | Atom of string
      | HookDef of string * string * string * string * (string * string list * skeleton) list

    let split_decl l =
      let rec aux (a, b, c, d, e) = function
      | [] -> (a, b, c, d, e)
      | BaseType s :: l -> aux (s :: a, b, c, d, e) l
      | ProgramType (name, consts) :: l -> aux (a, (name, consts) :: b, c, d, e) l
      | FilterDecl (name, ins, outs) :: l -> aux (a, b, (name, ins, outs) :: c, d, e) l
      | Atom at :: l -> aux (a, b, c, at :: d, e) l
      | HookDef (name, tin, tterm, tout, rules) :: l -> aux (a, b, c, d, (name, tin, tterm, tout, rules) :: e) l
      in
      aux ([], [], [], [], []) (List.rev l)

    type cdecl =
      | CType of (string * typedesc)
      | CVar of var_constr_decl
      | CRuleInVar of (string * typedesc)
      | CRuleOutVar of (string * typedesc)
      | CFilter of pfilter_constr_decl
      | CHookIn of phookin_constr_decl
      | CHookOut of phookout_constr_decl
      | CRuleIn of prulein_constr_decl
      | CRuleOut of pruleout_constr_decl
      | CRuleVarIn of (string * typedesc)
      | CRuleVarOut of (string * typedesc)
      | CQuark of quark_decl


# 211 "src/parser.ml"

let rec _menhir_goto_separated_nonempty_list_OR_skeleton_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.skeleton list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState82 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Types.skeleton list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Types.skeleton))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.skeleton list) = 
# 243 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 225 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_OR_skeleton_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState49 | MenhirState79 | MenhirState80 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Types.skeleton list)) = _v in
        let _v : (Types.skeleton list) = 
# 144 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x )
# 235 "src/parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_OR_skeleton__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_separated_list2_COMMA_term_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.term list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState65 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (Types.term))), _, (l : (Types.term list))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.term list) = 
# 215 "src/parser.mly"
                                              ( x :: l )
# 253 "src/parser.ml"
         in
        _menhir_goto_separated_list2_COMMA_term_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState58 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (t : (
# 83 "src/parser.mly"
       (string)
# 268 "src/parser.ml"
            ))), _), _, (l : (Types.term list))) = _menhir_stack in
            let _4 = () in
            let _2 = () in
            let _v : (Types.term) = 
# 229 "src/parser.mly"
                                                                   ( TConstr (t, l) )
# 275 "src/parser.ml"
             in
            _menhir_goto_term _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_run65 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.term) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
    | LPAREN ->
        _menhir_run59 _menhir_env (Obj.magic _menhir_stack) MenhirState65
    | UIDENT _v ->
        _menhir_run56 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState65

and _menhir_run62 : _menhir_env -> ('ttv_tail * _menhir_state) * _menhir_state * (Types.term) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let ((_menhir_stack, _menhir_s), _, (t : (Types.term))) = _menhir_stack in
    let _3 = () in
    let _1 = () in
    let _v : (Types.term) = 
# 220 "src/parser.mly"
                               ( t )
# 313 "src/parser.ml"
     in
    _menhir_goto_simple_term _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_separated_nonempty_list_SEMI_bone_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.skeleton) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState39 | MenhirState49 | MenhirState79 | MenhirState80 | MenhirState82 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (l : (Types.skeleton)) = _v in
        let _v : (Types.skeleton) = 
# 206 "src/parser.mly"
                                              ( l )
# 327 "src/parser.ml"
         in
        let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
        (match _menhir_s with
        | MenhirState49 | MenhirState79 | MenhirState82 | MenhirState80 ->
            let _menhir_stack = Obj.magic _menhir_stack in
            assert (not _menhir_env._menhir_error);
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | OR ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | BRANCH ->
                    _menhir_run80 _menhir_env (Obj.magic _menhir_stack) MenhirState82
                | LIDENT _v ->
                    _menhir_run50 _menhir_env (Obj.magic _menhir_stack) MenhirState82 _v
                | LPAREN ->
                    _menhir_run40 _menhir_env (Obj.magic _menhir_stack) MenhirState82
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState82)
            | END ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, (x : (Types.skeleton))) = _menhir_stack in
                let _v : (Types.skeleton list) = 
# 241 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [ x ] )
# 357 "src/parser.ml"
                 in
                _menhir_goto_separated_nonempty_list_OR_skeleton_ _menhir_env _menhir_stack _menhir_s _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | MenhirState39 ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), (c : (
# 83 "src/parser.mly"
       (string)
# 372 "src/parser.ml"
            ))), (args : (Types.var list))), _, (s : (Types.skeleton))) = _menhir_stack in
            let _4 = () in
            let _1 = () in
            let _v : (string * Types.var list * Types.skeleton) = 
# 203 "src/parser.mly"
                                                                     ( (c, args, s) )
# 379 "src/parser.ml"
             in
            let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
            let _menhir_stack = Obj.magic _menhir_stack in
            assert (not _menhir_env._menhir_error);
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BAR ->
                _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState97
            | ATOM | EOF | HOOK | TYPE | VAL ->
                _menhir_reduce57 _menhir_env (Obj.magic _menhir_stack) MenhirState97
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState97)
        | _ ->
            _menhir_fail ())
    | MenhirState86 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Types.skeleton)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Types.bone))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.skeleton) = 
# 243 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 405 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_SEMI_bone_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_term : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.term) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState59 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            _menhir_run62 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState58 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN ->
            _menhir_run62 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState65 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            _menhir_run65 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (x1 : (Types.term))), _, (x2 : (Types.term))) = _menhir_stack in
            let _2 = () in
            let _v : (Types.term list) = 
# 214 "src/parser.mly"
                          ( [x1; x2] )
# 457 "src/parser.ml"
             in
            _menhir_goto_separated_list2_COMMA_term_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState55 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (t : (Types.term))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.term) = 
# 224 "src/parser.mly"
                               ( t )
# 481 "src/parser.ml"
             in
            _menhir_goto_simple_onlyterm _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_run188 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.pconstr) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FORALL ->
        _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState188
    | JOIN ->
        _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState188
    | LBRACE ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState188
    | LBRACK ->
        _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState188
    | LET ->
        _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState188
    | LIDENT _v ->
        _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState188 _v
    | LPAREN ->
        _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState188
    | TRUE ->
        _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState188
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState188

and _menhir_goto_separated_list2_COMMA_value_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.pvalue list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState217 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (Types.pvalue))), _, (l : (Types.pvalue list))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.pvalue list) = 
# 215 "src/parser.mly"
                                              ( x :: l )
# 531 "src/parser.ml"
         in
        _menhir_goto_separated_list2_COMMA_value_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState162 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (l : (Types.pvalue list))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pvalue) = 
# 259 "src/parser.mly"
                                                        ( PVTuple l )
# 549 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_run217 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.pvalue) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FORALL ->
        _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState217
    | JOIN ->
        _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState217
    | LBRACE ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState217
    | LBRACK ->
        _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState217
    | LET ->
        _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState217
    | LIDENT _v ->
        _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState217 _v
    | LPAREN ->
        _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState217
    | TRUE ->
        _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState217
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState217

and _menhir_goto_separated_nonempty_list_COMMA_value_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.pvalue list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState210 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Types.pvalue list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Types.pvalue))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.pvalue list) = 
# 243 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 599 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_COMMA_value_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState165 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Types.pvalue list)) = _v in
        let _v : (Types.pvalue list) = 
# 144 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x )
# 609 "src/parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_COMMA_value__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_separated_nonempty_list_SEMI_value_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.pvalue list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState198 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Types.pvalue list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Types.pvalue))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.pvalue list) = 
# 243 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 627 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_SEMI_value_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState173 | MenhirState174 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Types.pvalue list)) = _v in
        let _v : (Types.pvalue list) = 
# 144 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x )
# 637 "src/parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMI_value__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run182 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.pvalue) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FORALL ->
        _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState182
    | JOIN ->
        _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState182
    | LBRACE ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState182
    | LBRACK ->
        _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState182
    | LET ->
        _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState182
    | LIDENT _v ->
        _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState182 _v
    | LPAREN ->
        _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState182
    | TRUE ->
        _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState182
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState182

and _menhir_run184 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.pvalue) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FORALL ->
        _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState184
    | JOIN ->
        _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState184
    | LBRACE ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState184
    | LBRACK ->
        _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState184
    | LET ->
        _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState184
    | LIDENT _v ->
        _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState184 _v
    | LPAREN ->
        _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState184
    | TRUE ->
        _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState184
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState184

and _menhir_goto_loption_separated_nonempty_list_SEMI_value__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.pvalue list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState174 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RBRACE ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (xs : (Types.pvalue list))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pvalue) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 714 "src/parser.ml"
             in
            
# 260 "src/parser.mly"
                                                      ( PVSet l )
# 719 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState173 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RBRACK ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (xs : (Types.pvalue list))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pvalue) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 743 "src/parser.ml"
             in
            
# 261 "src/parser.mly"
                                                      ( PVList l )
# 748 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_bone : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.bone) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | SEMI ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BRANCH ->
            _menhir_run80 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | LIDENT _v ->
            _menhir_run50 _menhir_env (Obj.magic _menhir_stack) MenhirState86 _v
        | LPAREN ->
            _menhir_run40 _menhir_env (Obj.magic _menhir_stack) MenhirState86
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState86)
    | ATOM | BAR | END | EOF | HOOK | OR | TYPE | VAL ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, (x : (Types.bone))) = _menhir_stack in
        let _v : (Types.skeleton) = 
# 241 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [ x ] )
# 788 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_SEMI_bone_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_reduce99 : _menhir_env -> 'ttv_tail * _menhir_state * (
# 83 "src/parser.mly"
       (string)
# 801 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let (_menhir_stack, _menhir_s, (t : (
# 83 "src/parser.mly"
       (string)
# 807 "src/parser.ml"
    ))) = _menhir_stack in
    let _v : (Types.term) = 
# 218 "src/parser.mly"
                 ( TConstr (t, []) )
# 812 "src/parser.ml"
     in
    _menhir_goto_simple_term _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_simple_term : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.term) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState55 | MenhirState58 | MenhirState65 | MenhirState59 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (t : (Types.term)) = _v in
        let _v : (Types.term) = 
# 227 "src/parser.mly"
                      ( t )
# 826 "src/parser.ml"
         in
        _menhir_goto_term _menhir_env _menhir_stack _menhir_s _v
    | MenhirState56 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Types.term)) = _v in
        let (_menhir_stack, _menhir_s, (t : (
# 83 "src/parser.mly"
       (string)
# 836 "src/parser.ml"
        ))) = _menhir_stack in
        let _v : (Types.term) = 
# 228 "src/parser.mly"
                                  ( TConstr (t, [x]) )
# 841 "src/parser.ml"
         in
        _menhir_goto_term _menhir_env _menhir_stack _menhir_s _v
    | MenhirState76 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (t : (Types.term)) = _v in
        let ((((((_menhir_stack, _menhir_s, (vout : (
# 82 "src/parser.mly"
       (string)
# 851 "src/parser.ml"
        ))), _), (h : (
# 82 "src/parser.mly"
       (string)
# 855 "src/parser.ml"
        ))), _), (a : (
# 82 "src/parser.mly"
       (string)
# 859 "src/parser.ml"
        ))), (vin : (
# 82 "src/parser.mly"
       (string)
# 863 "src/parser.ml"
        ))) = _menhir_stack in
        let _4 = () in
        let _2 = () in
        let _v : (Types.bone) = 
# 237 "src/parser.mly"
      ( Hook { h_name = h ; h_annot = a; h_inputs = [vin] ; h_term = t ; h_outputs = [vout] } )
# 870 "src/parser.ml"
         in
        _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_cdecl : _menhir_env -> 'ttv_tail -> _menhir_state -> (cdecl) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FILTER ->
        _menhir_run255 _menhir_env (Obj.magic _menhir_stack) MenhirState275
    | HOOK ->
        _menhir_run236 _menhir_env (Obj.magic _menhir_stack) MenhirState275
    | RULE ->
        _menhir_run150 _menhir_env (Obj.magic _menhir_stack) MenhirState275
    | TYPE ->
        _menhir_run146 _menhir_env (Obj.magic _menhir_stack) MenhirState275
    | VAL ->
        _menhir_run137 _menhir_env (Obj.magic _menhir_stack) MenhirState275
    | VAR ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState275
    | EOF ->
        _menhir_reduce51 _menhir_env (Obj.magic _menhir_stack) MenhirState275
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState275

and _menhir_goto_constr : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.pconstr) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState260 | MenhirState253 | MenhirState245 | MenhirState234 | MenhirState160 | MenhirState217 | MenhirState165 | MenhirState210 | MenhirState172 | MenhirState173 | MenhirState174 | MenhirState198 | MenhirState177 | MenhirState195 | MenhirState180 | MenhirState182 | MenhirState184 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SLASHBACKSLASH ->
            _menhir_run188 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState188 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (c1 : (Types.pconstr))), _, (c2 : (Types.pconstr))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.pconstr) = 
# 267 "src/parser.mly"
                                               ( PConj (c1, c2) )
# 927 "src/parser.ml"
         in
        _menhir_goto_constr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState191 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SLASHBACKSLASH ->
            _menhir_run188 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), _, (l : (Types.var list))), _, (v : (Types.pvalue))), _, (c : (Types.pconstr))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pconstr) = 
# 270 "src/parser.mly"
                                                                             ( PForall (v, (l, c)) )
# 946 "src/parser.ml"
             in
            _menhir_goto_constr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState206 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SLASHBACKSLASH ->
            _menhir_run188 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), _, (l : (Types.var list))), _, (v : (Types.pvalue))), _, (c : (Types.pconstr))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pconstr) = 
# 268 "src/parser.mly"
                                                                       ( PCLet (v, (l, c)) )
# 971 "src/parser.ml"
             in
            _menhir_goto_constr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState162 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (c : (Types.pconstr))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pconstr) = 
# 271 "src/parser.mly"
                                 ( c )
# 995 "src/parser.ml"
             in
            _menhir_goto_constr _menhir_env _menhir_stack _menhir_s _v
        | SLASHBACKSLASH ->
            _menhir_run188 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_value : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.pvalue) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState180 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState191
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState191
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState191
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState191
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState191
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState191 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState191
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState191
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState191)
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState182 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN | SLASHBACKSLASH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (v1 : (Types.pvalue))), _, (v2 : (Types.pvalue))) = _menhir_stack in
            let _2 = () in
            let _v : (Types.pconstr) = 
# 269 "src/parser.mly"
                                        ( PSub (v1, v2) )
# 1069 "src/parser.ml"
             in
            _menhir_goto_constr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState184 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | RBRACK ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (m : (Types.pvalue))), _, (v : (Types.pvalue))) = _menhir_stack in
            let _4 = () in
            let _2 = () in
            let _v : (Types.pvalue) = 
# 254 "src/parser.mly"
                                           ( PMapGet (m, v) )
# 1097 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState188 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | EOF | FILTER | HOOK | IN | RBRACE | RBRACK | RPAREN | RULE | SEMI | SLASHBACKSLASH | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (c : (Types.pconstr))), _, (v : (Types.pvalue))) = _menhir_stack in
            let _2 = () in
            let _v : (Types.pvalue) = 
# 257 "src/parser.mly"
                                            ( PVWith (c, v) )
# 1122 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState191 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState177 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState195
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState195
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState195
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState195
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState195
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState195 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState195
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState195
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState195)
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState195 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | EOF | FILTER | HOOK | IN | RBRACE | RBRACK | RPAREN | RULE | SEMI | SLASHBACKSLASH | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), _, (l : (Types.var list))), _, (v1 : (Types.pvalue))), _, (v2 : (Types.pvalue))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pvalue) = 
# 262 "src/parser.mly"
                                                                          ( PVJoin (v1, (l, v2)) )
# 1204 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState173 | MenhirState198 | MenhirState174 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | SEMI ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState198
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState198
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState198
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState198
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState198
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState198 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState198
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState198
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState198)
        | RBRACE | RBRACK ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (Types.pvalue))) = _menhir_stack in
            let _v : (Types.pvalue list) = 
# 241 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [ x ] )
# 1253 "src/parser.ml"
             in
            _menhir_goto_separated_nonempty_list_SEMI_value_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState172 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState206
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState206
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState206
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState206
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState206
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState206 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState206
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState206
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState206)
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState206 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | EOF | FILTER | HOOK | IN | RBRACE | RBRACK | RPAREN | RULE | SEMI | SLASHBACKSLASH | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), _, (l : (Types.var list))), _, (v1 : (Types.pvalue))), _, (v2 : (Types.pvalue))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pvalue) = 
# 256 "src/parser.mly"
                                                                        ( PVLet (v1, (l, v2)) )
# 1320 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState210 | MenhirState165 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState210
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState210
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState210
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState210
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState210
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState210 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState210
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState210
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState210)
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (Types.pvalue))) = _menhir_stack in
            let _v : (Types.pvalue list) = 
# 241 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [ x ] )
# 1369 "src/parser.ml"
             in
            _menhir_goto_separated_nonempty_list_COMMA_value_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState162 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            _menhir_run217 _menhir_env (Obj.magic _menhir_stack)
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (v : (Types.pvalue))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pvalue) = 
# 263 "src/parser.mly"
                                ( v )
# 1399 "src/parser.ml"
             in
            _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState217 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            _menhir_run217 _menhir_env (Obj.magic _menhir_stack)
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (x1 : (Types.pvalue))), _, (x2 : (Types.pvalue))) = _menhir_stack in
            let _2 = () in
            let _v : (Types.pvalue list) = 
# 214 "src/parser.mly"
                          ( [x1; x2] )
# 1426 "src/parser.ml"
             in
            _menhir_goto_separated_list2_COMMA_value_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState160 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | EOF | FILTER | HOOK | RULE | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 1449 "src/parser.ml"
            ))), _, (xs : (Types.var list))), _, (v : (Types.pvalue))) = _menhir_stack in
            let _7 = () in
            let _6 = () in
            let _4 = () in
            let _2 = () in
            let _1 = () in
            let _v : (Types.pruleout_constr_decl) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1459 "src/parser.ml"
             in
            
# 291 "src/parser.mly"
      ( { pro_name = name ; pro_value = (l, v) } )
# 1464 "src/parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (r : (Types.pruleout_constr_decl)) = _v in
            let _v : (cdecl) = 
# 339 "src/parser.mly"
                              ( CRuleOut r )
# 1472 "src/parser.ml"
             in
            _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState234 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | EOF | FILTER | HOOK | RULE | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 1495 "src/parser.ml"
            ))), _, (xs : (Types.var list))), _, (v : (Types.pvalue))) = _menhir_stack in
            let _7 = () in
            let _6 = () in
            let _4 = () in
            let _2 = () in
            let _1 = () in
            let _v : (Types.prulein_constr_decl) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1505 "src/parser.ml"
             in
            
# 287 "src/parser.mly"
      ( { pri_name = name ; pri_value = (l, v) } )
# 1510 "src/parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (r : (Types.prulein_constr_decl)) = _v in
            let _v : (cdecl) = 
# 338 "src/parser.mly"
                             ( CRuleIn r )
# 1518 "src/parser.ml"
             in
            _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState245 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | EOF | FILTER | HOOK | RULE | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 1541 "src/parser.ml"
            ))), _, (a : (string))), _, (xs : (Types.var list))), _, (v : (Types.pvalue))) = _menhir_stack in
            let _8 = () in
            let _7 = () in
            let _5 = () in
            let _2 = () in
            let _1 = () in
            let _v : (Types.phookout_constr_decl) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1551 "src/parser.ml"
             in
            
# 283 "src/parser.mly"
      ( { pho_name = name ; pho_annot = a ; pho_value = (l, v) } )
# 1556 "src/parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (h : (Types.phookout_constr_decl)) = _v in
            let _v : (cdecl) = 
# 337 "src/parser.mly"
                              ( CHookOut h )
# 1564 "src/parser.ml"
             in
            _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState253 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | EOF | FILTER | HOOK | RULE | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 1587 "src/parser.ml"
            ))), _, (a : (string))), _, (xs : (Types.var list))), _, (v : (Types.pvalue))) = _menhir_stack in
            let _8 = () in
            let _7 = () in
            let _5 = () in
            let _2 = () in
            let _1 = () in
            let _v : (Types.phookin_constr_decl) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1597 "src/parser.ml"
             in
            
# 279 "src/parser.mly"
      ( { phi_name = name ; phi_annot = a ; phi_value = (l, v) } )
# 1602 "src/parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (h : (Types.phookin_constr_decl)) = _v in
            let _v : (cdecl) = 
# 336 "src/parser.mly"
                             ( CHookIn h )
# 1610 "src/parser.ml"
             in
            _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState260 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LBRACK ->
            _menhir_run184 _menhir_env (Obj.magic _menhir_stack)
        | LESSEQUAL ->
            _menhir_run182 _menhir_env (Obj.magic _menhir_stack)
        | EOF | FILTER | HOOK | RULE | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((((_menhir_stack, _menhir_s), (f : (
# 82 "src/parser.mly"
       (string)
# 1633 "src/parser.ml"
            ))), _, (xs : (Types.var list))), _, (v : (Types.pvalue))) = _menhir_stack in
            let _6 = () in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Types.pfilter_constr_decl) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1642 "src/parser.ml"
             in
            
# 275 "src/parser.mly"
      ( { pfc_name = f ; pfc_value = (l, v) } )
# 1647 "src/parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (f : (Types.pfilter_constr_decl)) = _v in
            let _v : (cdecl) = 
# 331 "src/parser.mly"
                             ( CFilter f )
# 1655 "src/parser.ml"
             in
            _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_loption_separated_nonempty_list_COMMA_value__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.pvalue list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | RPAREN ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (f : (
# 82 "src/parser.mly"
       (string)
# 1681 "src/parser.ml"
        ))), _, (xs : (Types.pvalue list))) = _menhir_stack in
        let _4 = () in
        let _2 = () in
        let _v : (Types.pvalue) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1688 "src/parser.ml"
         in
        
# 255 "src/parser.mly"
                                                                   ( PFunc (f, if l = [] then [PVUnit] else l) )
# 1693 "src/parser.ml"
         in
        _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_reduce65 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (Types.pvalue list) = 
# 142 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 1708 "src/parser.ml"
     in
    _menhir_goto_loption_separated_nonempty_list_SEMI_value__ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run167 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState167 _v
    | RPAREN ->
        _menhir_reduce59 _menhir_env (Obj.magic _menhir_stack) MenhirState167
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState167

and _menhir_run170 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 1730 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (x : (
# 82 "src/parser.mly"
       (string)
# 1738 "src/parser.ml"
    )) = _v in
    let _v : (Types.var list) = 
# 250 "src/parser.mly"
                 ( [x] )
# 1743 "src/parser.ml"
     in
    _menhir_goto_binder _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_list_LIDENT_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.var list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState53 | MenhirState46 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Types.var list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 1757 "src/parser.ml"
        ))) = _menhir_stack in
        let _v : (Types.var list) = 
# 213 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 1762 "src/parser.ml"
         in
        _menhir_goto_list_LIDENT_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState45 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (lin : (Types.var list)) = _v in
        let (((_menhir_stack, _menhir_s), _, (xs : (Types.var list))), (f : (
# 82 "src/parser.mly"
       (string)
# 1772 "src/parser.ml"
        ))) = _menhir_stack in
        let _4 = () in
        let _3 = () in
        let _1 = () in
        let _v : (Types.bone) = let lout =
          let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1781 "src/parser.ml"
           in
          
# 210 "src/parser.mly"
                                                                   ( l )
# 1786 "src/parser.ml"
          
        in
        
# 233 "src/parser.mly"
      ( Filter { f_name = f ; f_inputs = lin ; f_outputs = lout } )
# 1792 "src/parser.ml"
         in
        _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v
    | MenhirState52 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (lin : (Types.var list)) = _v in
        let (((_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 1802 "src/parser.ml"
        ))), _), (f : (
# 82 "src/parser.mly"
       (string)
# 1806 "src/parser.ml"
        ))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.bone) = let lout = 
# 209 "src/parser.mly"
                            ( [x] )
# 1812 "src/parser.ml"
         in
        
# 233 "src/parser.mly"
      ( Filter { f_name = f ; f_inputs = lin ; f_outputs = lout } )
# 1817 "src/parser.ml"
         in
        _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v
    | MenhirState50 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (lin : (Types.var list)) = _v in
        let (_menhir_stack, _menhir_s, (f : (
# 82 "src/parser.mly"
       (string)
# 1827 "src/parser.ml"
        ))) = _menhir_stack in
        let _v : (Types.bone) = let lout = 
# 211 "src/parser.mly"
                    ( [] )
# 1832 "src/parser.ml"
         in
        
# 233 "src/parser.mly"
      ( Filter { f_name = f ; f_inputs = lin ; f_outputs = lout } )
# 1837 "src/parser.ml"
         in
        _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_loption_separated_nonempty_list_OR_skeleton__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.skeleton list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState80 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | END ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (xs : (Types.skeleton list))) = _menhir_stack in
            let _4 = () in
            let _2 = () in
            let _v : (Types.bone) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1862 "src/parser.ml"
             in
            let v = 
# 211 "src/parser.mly"
                    ( [] )
# 1867 "src/parser.ml"
             in
            
# 239 "src/parser.mly"
      ( Branching { b_branches = l ; b_outputs = v } )
# 1872 "src/parser.ml"
             in
            _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState79 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | END ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 1893 "src/parser.ml"
            ))), _), _, (xs : (Types.skeleton list))) = _menhir_stack in
            let _4 = () in
            let _2 = () in
            let _2_inlined1 = () in
            let _v : (Types.bone) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1901 "src/parser.ml"
             in
            let v =
              let _2 = _2_inlined1 in
              
# 209 "src/parser.mly"
                            ( [x] )
# 1908 "src/parser.ml"
              
            in
            
# 239 "src/parser.mly"
      ( Branching { b_branches = l ; b_outputs = v } )
# 1914 "src/parser.ml"
             in
            _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState49 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | END ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s), _, (xs : (Types.var list))), _, (xs_inlined1 : (Types.skeleton list))) = _menhir_stack in
            let _4 = () in
            let _2 = () in
            let _4_inlined1 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Types.bone) = let l =
              let xs = xs_inlined1 in
              
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1943 "src/parser.ml"
              
            in
            let v =
              let _4 = _4_inlined1 in
              let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 1951 "src/parser.ml"
               in
              
# 210 "src/parser.mly"
                                                                   ( l )
# 1956 "src/parser.ml"
              
            in
            
# 239 "src/parser.mly"
      ( Branching { b_branches = l ; b_outputs = v } )
# 1962 "src/parser.ml"
             in
            _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_simple_onlyterm : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.term) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = Obj.magic _menhir_stack in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (t : (Types.term)) = _v in
    let ((((_menhir_stack, _menhir_s, (vout : (
# 82 "src/parser.mly"
       (string)
# 1982 "src/parser.ml"
    ))), _), (h : (
# 82 "src/parser.mly"
       (string)
# 1986 "src/parser.ml"
    ))), _, (vin : (
# 82 "src/parser.mly"
       (string)
# 1990 "src/parser.ml"
    ))) = _menhir_stack in
    let _2 = () in
    let _v : (Types.bone) = 
# 235 "src/parser.mly"
      ( Hook { h_name = h ; h_annot = ""; h_inputs = [vin] ; h_term = t ; h_outputs = [vout] } )
# 1996 "src/parser.ml"
     in
    _menhir_goto_bone _menhir_env _menhir_stack _menhir_s _v

and _menhir_run56 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 83 "src/parser.mly"
       (string)
# 2003 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState56 _v
    | LPAREN ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_s = MenhirState56 in
        let _menhir_stack = (_menhir_stack, _menhir_s) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState58 _v
        | LPAREN ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack) MenhirState58
        | UIDENT _v ->
            _menhir_run56 _menhir_env (Obj.magic _menhir_stack) MenhirState58 _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState58)
    | UIDENT _v ->
        _menhir_run57 _menhir_env (Obj.magic _menhir_stack) MenhirState56 _v
    | COMMA | RPAREN ->
        _menhir_reduce99 _menhir_env (Obj.magic _menhir_stack)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState56

and _menhir_run57 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 83 "src/parser.mly"
       (string)
# 2041 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    _menhir_reduce99 _menhir_env (Obj.magic _menhir_stack)

and _menhir_run59 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
    | LPAREN ->
        _menhir_run59 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | UIDENT _v ->
        _menhir_run56 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState59

and _menhir_run60 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 2068 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (t : (
# 82 "src/parser.mly"
       (string)
# 2076 "src/parser.ml"
    )) = _v in
    let _v : (Types.term) = 
# 219 "src/parser.mly"
                 ( TVar t )
# 2081 "src/parser.ml"
     in
    _menhir_goto_simple_term _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_separated_nonempty_list_STAR_simple_typedesc_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.typedesc list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState141 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (Types.typedesc))), _, (xs : (Types.typedesc list))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.typedesc list) = 
# 243 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 2097 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_STAR_simple_typedesc_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState139 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | MINUSGREATER ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState144 _v
            | LPAREN ->
                _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState144
            | PROGRAM_POINT ->
                _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState144
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState144)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_separated_list2_STAR_simple_typedesc_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.typedesc list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState129 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (l : (Types.typedesc list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Types.typedesc))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.typedesc list) = 
# 215 "src/parser.mly"
                                              ( x :: l )
# 2141 "src/parser.ml"
         in
        _menhir_goto_separated_list2_STAR_simple_typedesc_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState228 | MenhirState154 | MenhirState148 | MenhirState144 | MenhirState118 | MenhirState120 | MenhirState124 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (l : (Types.typedesc list)) = _v in
        let _v : (Types.typedesc) = 
# 313 "src/parser.mly"
                                                 ( TupleType l )
# 2151 "src/parser.ml"
         in
        _menhir_goto_typedesc _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_typedesc : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.typedesc) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState120 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState124 _v
            | LPAREN ->
                _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState124
            | PROGRAM_POINT ->
                _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState124
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState124)
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (t : (Types.typedesc))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.typedesc) = 
# 303 "src/parser.mly"
                                   ( t )
# 2191 "src/parser.ml"
             in
            _menhir_goto_simple_typedesc _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState124 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | MAP ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _menhir_stack = Obj.magic _menhir_stack in
                let (((_menhir_stack, _menhir_s), _, (t1 : (Types.typedesc))), _, (t2 : (Types.typedesc))) = _menhir_stack in
                let _6 = () in
                let _5 = () in
                let _3 = () in
                let _1 = () in
                let _v : (Types.typedesc) = 
# 307 "src/parser.mly"
                                                               ( MapType (t1, t2) )
# 2222 "src/parser.ml"
                 in
                _menhir_goto_simple_typedesc _menhir_env _menhir_stack _menhir_s _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState118 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), (x : (
# 82 "src/parser.mly"
       (string)
# 2243 "src/parser.ml"
        ))), _, (t : (Types.typedesc))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (Types.var_constr_decl) = 
# 323 "src/parser.mly"
                                           ( { vc_name = x ; vc_type = t } )
# 2250 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (v : (Types.var_constr_decl)) = _v in
        let _v : (cdecl) = 
# 333 "src/parser.mly"
                          ( CVar v )
# 2258 "src/parser.ml"
         in
        _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
    | MenhirState144 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((((_menhir_stack, _menhir_s), (f : (
# 82 "src/parser.mly"
       (string)
# 2267 "src/parser.ml"
        ))), _, (l : (Types.typedesc list))), _, (r : (Types.typedesc))) = _menhir_stack in
        let _5 = () in
        let _3 = () in
        let _1 = () in
        let _v : (Types.quark_decl) = 
# 320 "src/parser.mly"
      ( { q_name = f ; q_inputs = l ; q_output = r } )
# 2275 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (q : (Types.quark_decl)) = _v in
        let _v : (cdecl) = 
# 332 "src/parser.mly"
                     ( CQuark q )
# 2283 "src/parser.ml"
         in
        _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
    | MenhirState148 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 2292 "src/parser.ml"
        ))), _, (t : (Types.typedesc))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (string * Types.typedesc) = 
# 316 "src/parser.mly"
                                               ( (name, t) )
# 2299 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (t : (string * Types.typedesc)) = _v in
        let _v : (cdecl) = 
# 330 "src/parser.mly"
                   ( CType t )
# 2307 "src/parser.ml"
         in
        _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
    | MenhirState154 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 2316 "src/parser.ml"
        ))), _, (t : (Types.typedesc))) = _menhir_stack in
        let _5 = () in
        let _3 = () in
        let _2 = () in
        let _1 = () in
        let _v : (string * Types.typedesc) = 
# 299 "src/parser.mly"
      ( (name, t) )
# 2325 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (v : (string * Types.typedesc)) = _v in
        let _v : (cdecl) = 
# 335 "src/parser.mly"
                           ( CRuleOutVar v )
# 2333 "src/parser.ml"
         in
        _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
    | MenhirState228 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 2342 "src/parser.ml"
        ))), _, (t : (Types.typedesc))) = _menhir_stack in
        let _5 = () in
        let _3 = () in
        let _2 = () in
        let _1 = () in
        let _v : (string * Types.typedesc) = 
# 295 "src/parser.mly"
      ( (name, t) )
# 2351 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (v : (string * Types.typedesc)) = _v in
        let _v : (cdecl) = 
# 334 "src/parser.mly"
                          ( CRuleInVar v )
# 2359 "src/parser.ml"
         in
        _menhir_goto_cdecl _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run129 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.typedesc) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState129 _v
    | LPAREN ->
        _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | PROGRAM_POINT ->
        _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState129

and _menhir_run131 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.typedesc) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (_menhir_stack, _menhir_s, (t : (Types.typedesc))) = _menhir_stack in
    let _2 = () in
    let _v : (Types.typedesc) = 
# 305 "src/parser.mly"
                               ( SetType t )
# 2390 "src/parser.ml"
     in
    _menhir_goto_simple_typedesc _menhir_env _menhir_stack _menhir_s _v

and _menhir_run132 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.typedesc) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (_menhir_stack, _menhir_s, (t : (Types.typedesc))) = _menhir_stack in
    let _2 = () in
    let _v : (Types.typedesc) = 
# 309 "src/parser.mly"
                                         ( Program_point t )
# 2403 "src/parser.ml"
     in
    _menhir_goto_simple_typedesc _menhir_env _menhir_stack _menhir_s _v

and _menhir_run133 : _menhir_env -> 'ttv_tail * _menhir_state * (Types.typedesc) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (_menhir_stack, _menhir_s, (t : (Types.typedesc))) = _menhir_stack in
    let _2 = () in
    let _v : (Types.typedesc) = 
# 306 "src/parser.mly"
                                ( ListType t )
# 2416 "src/parser.ml"
     in
    _menhir_goto_simple_typedesc _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_binder : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.var list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState166 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EQUAL ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState172
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState172
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState172
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState172
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState172
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState172 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState172
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState172
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState172)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState175 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState177
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState177
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState177
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState177
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState177
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState177 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState177
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState177
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState177)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState178 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FORALL ->
                _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState180
            | JOIN ->
                _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState180
            | LBRACE ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState180
            | LBRACK ->
                _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState180
            | LET ->
                _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState180
            | LIDENT _v ->
                _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState180 _v
            | LPAREN ->
                _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState180
            | TRUE ->
                _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState180
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState180)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_run161 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (Types.pconstr) = 
# 266 "src/parser.mly"
           ( PTrue )
# 2543 "src/parser.ml"
     in
    _menhir_goto_constr _menhir_env _menhir_stack _menhir_s _v

and _menhir_run162 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FORALL ->
        _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState162
    | JOIN ->
        _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState162
    | LBRACE ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState162
    | LBRACK ->
        _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState162
    | LET ->
        _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState162
    | LIDENT _v ->
        _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState162 _v
    | LPAREN ->
        _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState162
    | RPAREN ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_s = MenhirState162 in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        let _2 = () in
        let _1 = () in
        let _v : (Types.pvalue) = 
# 258 "src/parser.mly"
                     ( PVUnit )
# 2578 "src/parser.ml"
         in
        _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
    | TRUE ->
        _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState162
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState162

and _menhir_run164 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 2591 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LPAREN ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | FORALL ->
            _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState165
        | JOIN ->
            _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState165
        | LBRACE ->
            _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState165
        | LBRACK ->
            _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState165
        | LET ->
            _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState165
        | LIDENT _v ->
            _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState165 _v
        | LPAREN ->
            _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState165
        | TRUE ->
            _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState165
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState165 in
            let _v : (Types.pvalue list) = 
# 142 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 2625 "src/parser.ml"
             in
            _menhir_goto_loption_separated_nonempty_list_COMMA_value__ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState165)
    | COMMA | EOF | FILTER | HOOK | IN | LBRACK | LESSEQUAL | RBRACE | RBRACK | RPAREN | RULE | SEMI | SLASHBACKSLASH | TYPE | VAL | VAR ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 2637 "src/parser.ml"
        ))) = _menhir_stack in
        let _v : (Types.pvalue) = 
# 253 "src/parser.mly"
                 ( PNamedVar x )
# 2642 "src/parser.ml"
         in
        _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run166 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run170 _menhir_env (Obj.magic _menhir_stack) MenhirState166 _v
    | LPAREN ->
        _menhir_run167 _menhir_env (Obj.magic _menhir_stack) MenhirState166
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState166

and _menhir_run173 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FORALL ->
        _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | JOIN ->
        _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | LBRACE ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | LBRACK ->
        _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | LET ->
        _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | LIDENT _v ->
        _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState173 _v
    | LPAREN ->
        _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | TRUE ->
        _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | RBRACK ->
        _menhir_reduce65 _menhir_env (Obj.magic _menhir_stack) MenhirState173
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState173

and _menhir_run174 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FORALL ->
        _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | JOIN ->
        _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | LBRACE ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | LBRACK ->
        _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | LET ->
        _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | LIDENT _v ->
        _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState174 _v
    | LPAREN ->
        _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | TRUE ->
        _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | RBRACE ->
        _menhir_reduce65 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState174

and _menhir_run175 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run170 _menhir_env (Obj.magic _menhir_stack) MenhirState175 _v
    | LPAREN ->
        _menhir_run167 _menhir_env (Obj.magic _menhir_stack) MenhirState175
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState175

and _menhir_run178 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run170 _menhir_env (Obj.magic _menhir_stack) MenhirState178 _v
    | LPAREN ->
        _menhir_run167 _menhir_env (Obj.magic _menhir_stack) MenhirState178
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState178

and _menhir_reduce47 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (Types.var list) = 
# 211 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 2760 "src/parser.ml"
     in
    _menhir_goto_list_LIDENT_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run46 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 2767 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run46 _menhir_env (Obj.magic _menhir_stack) MenhirState46 _v
    | ATOM | BAR | END | EOF | HOOK | OR | SEMI | TYPE | VAL ->
        _menhir_reduce47 _menhir_env (Obj.magic _menhir_stack) MenhirState46
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState46

and _menhir_reduce63 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (Types.skeleton list) = 
# 142 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 2788 "src/parser.ml"
     in
    _menhir_goto_loption_separated_nonempty_list_OR_skeleton__ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run40 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState40 _v
    | RPAREN ->
        _menhir_reduce59 _menhir_env (Obj.magic _menhir_stack) MenhirState40
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState40

and _menhir_run50 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 2810 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LESSMINUS ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_s = MenhirState50 in
        let _menhir_stack = (_menhir_stack, _menhir_s) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BRANCH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BRANCH ->
                _menhir_run80 _menhir_env (Obj.magic _menhir_stack) MenhirState79
            | LIDENT _v ->
                _menhir_run50 _menhir_env (Obj.magic _menhir_stack) MenhirState79 _v
            | LPAREN ->
                _menhir_run40 _menhir_env (Obj.magic _menhir_stack) MenhirState79
            | END ->
                _menhir_reduce63 _menhir_env (Obj.magic _menhir_stack) MenhirState79
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState79)
        | LIDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CARET ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_s = MenhirState52 in
                let _menhir_stack = (_menhir_stack, _menhir_s) in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | LIDENT _v ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_stack = (_menhir_stack, _v) in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | LIDENT _v ->
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let _menhir_stack = (_menhir_stack, _v) in
                        let _menhir_env = _menhir_discard _menhir_env in
                        let _tok = _menhir_env._menhir_token in
                        (match _tok with
                        | LIDENT _v ->
                            _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState76 _v
                        | LPAREN ->
                            _menhir_run59 _menhir_env (Obj.magic _menhir_stack) MenhirState76
                        | UIDENT _v ->
                            _menhir_run57 _menhir_env (Obj.magic _menhir_stack) MenhirState76 _v
                        | _ ->
                            assert (not _menhir_env._menhir_error);
                            _menhir_env._menhir_error <- true;
                            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState76)
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let (_menhir_stack, _menhir_s) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | LIDENT _v ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_s = MenhirState52 in
                let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | LIDENT _v ->
                    _menhir_run46 _menhir_env (Obj.magic _menhir_stack) MenhirState53 _v
                | LPAREN ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_s = MenhirState53 in
                    let _menhir_stack = (_menhir_stack, _menhir_s) in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | LIDENT _v ->
                        _menhir_run60 _menhir_env (Obj.magic _menhir_stack) MenhirState55 _v
                    | LPAREN ->
                        _menhir_run59 _menhir_env (Obj.magic _menhir_stack) MenhirState55
                    | UIDENT _v ->
                        _menhir_run56 _menhir_env (Obj.magic _menhir_stack) MenhirState55 _v
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState55)
                | UIDENT _v ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_s = MenhirState53 in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let (t : (
# 83 "src/parser.mly"
       (string)
# 2922 "src/parser.ml"
                    )) = _v in
                    let _v : (Types.term) = 
# 223 "src/parser.mly"
                 ( TConstr (t, []) )
# 2927 "src/parser.ml"
                     in
                    _menhir_goto_simple_onlyterm _menhir_env _menhir_stack _menhir_s _v
                | ATOM | BAR | END | EOF | HOOK | OR | SEMI | TYPE | VAL ->
                    _menhir_reduce47 _menhir_env (Obj.magic _menhir_stack) MenhirState53
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState53)
            | ATOM | BAR | END | EOF | HOOK | OR | SEMI | TYPE | VAL ->
                _menhir_reduce47 _menhir_env (Obj.magic _menhir_stack) MenhirState52
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState52)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | LIDENT _v ->
        _menhir_run46 _menhir_env (Obj.magic _menhir_stack) MenhirState50 _v
    | ATOM | BAR | END | EOF | HOOK | OR | SEMI | TYPE | VAL ->
        _menhir_reduce47 _menhir_env (Obj.magic _menhir_stack) MenhirState50
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState50

and _menhir_run80 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BRANCH ->
        _menhir_run80 _menhir_env (Obj.magic _menhir_stack) MenhirState80
    | LIDENT _v ->
        _menhir_run50 _menhir_env (Obj.magic _menhir_stack) MenhirState80 _v
    | LPAREN ->
        _menhir_run40 _menhir_env (Obj.magic _menhir_stack) MenhirState80
    | END ->
        _menhir_reduce63 _menhir_env (Obj.magic _menhir_stack) MenhirState80
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState80

and _menhir_goto_separated_nonempty_list_STAR_LIDENT_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (string list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState5 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 2986 "src/parser.ml"
        ))), _, (xs : (string list))) = _menhir_stack in
        let _2 = () in
        let _v : (string list) = 
# 243 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 2992 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_STAR_LIDENT_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState3 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | MINUSGREATER ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState8 _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState8)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState8 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 3023 "src/parser.ml"
        ))), _, (lin : (string list))), _, (lout : (string list))) = _menhir_stack in
        let _5 = () in
        let _3 = () in
        let _1 = () in
        let _v : (decl) = 
# 194 "src/parser.mly"
    ( let lin = if lin = ["unit"] then [] else lin in
      let lout = if lout = ["unit"] then [] else lout in FilterDecl (name, lin, lout) )
# 3032 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (f : (decl)) = _v in
        let _v : (decl) = 
# 176 "src/parser.mly"
                 ( f )
# 3040 "src/parser.ml"
         in
        _menhir_goto_decl _menhir_env _menhir_stack _menhir_s _v
    | MenhirState15 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), (x : (
# 83 "src/parser.mly"
       (string)
# 3049 "src/parser.ml"
        ))), _, (l : (string list))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (string * string list) = 
# 182 "src/parser.mly"
                                                                     ( (x, l) )
# 3056 "src/parser.ml"
         in
        _menhir_goto_constr_decl _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_list_constr_decl_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((string * string list) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState12 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (l : ((string * string list) list)) = _v in
        let ((_menhir_stack, _menhir_s), (x : (
# 82 "src/parser.mly"
       (string)
# 3072 "src/parser.ml"
        ))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (decl) = 
# 175 "src/parser.mly"
                                                     ( ProgramType (x, l) )
# 3079 "src/parser.ml"
         in
        _menhir_goto_decl _menhir_env _menhir_stack _menhir_s _v
    | MenhirState18 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : ((string * string list) list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (string * string list))) = _menhir_stack in
        let _v : ((string * string list) list) = 
# 213 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 3090 "src/parser.ml"
         in
        _menhir_goto_list_constr_decl_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_constr_decl : _menhir_env -> 'ttv_tail -> _menhir_state -> (string * string list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | BAR ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | ATOM | EOF | HOOK | TYPE | VAL ->
        _menhir_reduce53 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState18

and _menhir_goto_list_rule_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((string * Types.var list * Types.skeleton) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState97 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : ((string * Types.var list * Types.skeleton) list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (string * Types.var list * Types.skeleton))) = _menhir_stack in
        let _v : ((string * Types.var list * Types.skeleton) list) = 
# 213 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 3123 "src/parser.ml"
         in
        _menhir_goto_list_rule_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState28 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (l : ((string * Types.var list * Types.skeleton) list)) = _v in
        let (((((_menhir_stack, _menhir_s), (name : (
# 82 "src/parser.mly"
       (string)
# 3133 "src/parser.ml"
        ))), (instate : (
# 82 "src/parser.mly"
       (string)
# 3137 "src/parser.ml"
        ))), (progtype : (
# 82 "src/parser.mly"
       (string)
# 3141 "src/parser.ml"
        ))), (outstate : (
# 82 "src/parser.mly"
       (string)
# 3145 "src/parser.ml"
        ))) = _menhir_stack in
        let _9 = () in
        let _7 = () in
        let _5 = () in
        let _3 = () in
        let _1 = () in
        let _v : (decl) = 
# 244 "src/parser.mly"
      ( HookDef (name, instate, progtype, outstate, l) )
# 3155 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (h : (decl)) = _v in
        let _v : (decl) = 
# 177 "src/parser.mly"
               ( h )
# 3163 "src/parser.ml"
         in
        _menhir_goto_decl _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_const_args : _menhir_env -> 'ttv_tail -> (Types.var list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _v ->
    let _menhir_stack = (_menhir_stack, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | MINUSGREATER ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | BRANCH ->
            _menhir_run80 _menhir_env (Obj.magic _menhir_stack) MenhirState39
        | LIDENT _v ->
            _menhir_run50 _menhir_env (Obj.magic _menhir_stack) MenhirState39 _v
        | LPAREN ->
            _menhir_run40 _menhir_env (Obj.magic _menhir_stack) MenhirState39
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState39)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_list_atom_elem_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (string list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState100 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (l : (string list)) = _v in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        let _1 = () in
        let _v : (decl) = 
# 185 "src/parser.mly"
                                ( Atom (String.concat " " l) )
# 3210 "src/parser.ml"
         in
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (a : (decl)) = _v in
        let _v : (decl) = 
# 178 "src/parser.mly"
               ( a )
# 3218 "src/parser.ml"
         in
        _menhir_goto_decl _menhir_env _menhir_stack _menhir_s _v
    | MenhirState105 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (string list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (string))) = _menhir_stack in
        let _v : (string list) = 
# 213 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 3229 "src/parser.ml"
         in
        _menhir_goto_list_atom_elem_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_atom_elem : _menhir_env -> 'ttv_tail -> _menhir_state -> (string) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | COLON ->
        _menhir_run103 _menhir_env (Obj.magic _menhir_stack) MenhirState105
    | LIDENT _v ->
        _menhir_run102 _menhir_env (Obj.magic _menhir_stack) MenhirState105 _v
    | MINUSGREATER ->
        _menhir_run101 _menhir_env (Obj.magic _menhir_stack) MenhirState105
    | ATOM | EOF | HOOK | TYPE | VAL ->
        _menhir_reduce49 _menhir_env (Obj.magic _menhir_stack) MenhirState105
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState105

and _menhir_fail : unit -> 'a =
  fun () ->
    Printf.fprintf stderr "Internal failure -- please contact the parser generator's developers.\n%!";
    assert false

and _menhir_goto_simple_typedesc : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.typedesc) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState228 | MenhirState154 | MenhirState148 | MenhirState144 | MenhirState118 | MenhirState120 | MenhirState124 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIST ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | PROGRAM_POINT ->
            _menhir_run132 _menhir_env (Obj.magic _menhir_stack)
        | SET ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | STAR ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | EOF | FILTER | HOOK | RPAREN | RULE | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (t : (Types.typedesc))) = _menhir_stack in
            let _v : (Types.typedesc) = 
# 312 "src/parser.mly"
                          ( t )
# 3283 "src/parser.ml"
             in
            _menhir_goto_typedesc _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState129 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIST ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | PROGRAM_POINT ->
            _menhir_run132 _menhir_env (Obj.magic _menhir_stack)
        | SET ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | STAR ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | EOF | FILTER | HOOK | RPAREN | RULE | TYPE | VAL | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (x1 : (Types.typedesc))), _, (x2 : (Types.typedesc))) = _menhir_stack in
            let _2 = () in
            let _v : (Types.typedesc list) = 
# 214 "src/parser.mly"
                          ( [x1; x2] )
# 3312 "src/parser.ml"
             in
            _menhir_goto_separated_list2_STAR_simple_typedesc_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState141 | MenhirState139 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIST ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | PROGRAM_POINT ->
            _menhir_run132 _menhir_env (Obj.magic _menhir_stack)
        | SET ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | STAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState141 _v
            | LPAREN ->
                _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState141
            | PROGRAM_POINT ->
                _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState141
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState141)
        | MINUSGREATER ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (Types.typedesc))) = _menhir_stack in
            let _v : (Types.typedesc list) = 
# 241 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [ x ] )
# 3353 "src/parser.ml"
             in
            _menhir_goto_separated_nonempty_list_STAR_simple_typedesc_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_maybe_annot : _menhir_env -> 'ttv_tail -> _menhir_state -> (string) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState238 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState242 _v
            | RPAREN ->
                _menhir_reduce59 _menhir_env (Obj.magic _menhir_stack) MenhirState242
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState242)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState248 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState250 _v
            | RPAREN ->
                _menhir_reduce59 _menhir_env (Obj.magic _menhir_stack) MenhirState250
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState250)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_loption_separated_nonempty_list_COMMA_LIDENT__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.var list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState40 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LESSMINUS ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | BRANCH ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | BRANCH ->
                        _menhir_run80 _menhir_env (Obj.magic _menhir_stack) MenhirState49
                    | LIDENT _v ->
                        _menhir_run50 _menhir_env (Obj.magic _menhir_stack) MenhirState49 _v
                    | LPAREN ->
                        _menhir_run40 _menhir_env (Obj.magic _menhir_stack) MenhirState49
                    | END ->
                        _menhir_reduce63 _menhir_env (Obj.magic _menhir_stack) MenhirState49
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState49)
                | LIDENT _v ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_stack = (_menhir_stack, _v) in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | LIDENT _v ->
                        _menhir_run46 _menhir_env (Obj.magic _menhir_stack) MenhirState45 _v
                    | ATOM | BAR | END | EOF | HOOK | OR | SEMI | TYPE | VAL ->
                        _menhir_reduce47 _menhir_env (Obj.magic _menhir_stack) MenhirState45
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState45)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState157 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | EQUAL ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | FORALL ->
                    _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState160
                | JOIN ->
                    _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState160
                | LBRACE ->
                    _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState160
                | LBRACK ->
                    _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState160
                | LET ->
                    _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState160
                | LIDENT _v ->
                    _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState160 _v
                | LPAREN ->
                    _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState160
                | TRUE ->
                    _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState160
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState160)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState167 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (xs : (Types.var list))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.var list) = let l = 
# 232 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( xs )
# 3550 "src/parser.ml"
             in
            
# 249 "src/parser.mly"
                                                        ( l )
# 3555 "src/parser.ml"
             in
            _menhir_goto_binder _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState231 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | EQUAL ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | FORALL ->
                    _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState234
                | JOIN ->
                    _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState234
                | LBRACE ->
                    _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState234
                | LBRACK ->
                    _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState234
                | LET ->
                    _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState234
                | LIDENT _v ->
                    _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState234 _v
                | LPAREN ->
                    _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState234
                | TRUE ->
                    _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState234
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState234)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState242 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | EQUAL ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | FORALL ->
                    _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState245
                | JOIN ->
                    _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState245
                | LBRACE ->
                    _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState245
                | LBRACK ->
                    _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState245
                | LET ->
                    _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState245
                | LIDENT _v ->
                    _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState245 _v
                | LPAREN ->
                    _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState245
                | TRUE ->
                    _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState245
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState245)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState250 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | EQUAL ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | FORALL ->
                    _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState253
                | JOIN ->
                    _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState253
                | LBRACE ->
                    _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState253
                | LBRACK ->
                    _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState253
                | LET ->
                    _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState253
                | LIDENT _v ->
                    _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState253 _v
                | LPAREN ->
                    _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState253
                | TRUE ->
                    _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState253
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState253)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState257 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | EQUAL ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | FORALL ->
                    _menhir_run178 _menhir_env (Obj.magic _menhir_stack) MenhirState260
                | JOIN ->
                    _menhir_run175 _menhir_env (Obj.magic _menhir_stack) MenhirState260
                | LBRACE ->
                    _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState260
                | LBRACK ->
                    _menhir_run173 _menhir_env (Obj.magic _menhir_stack) MenhirState260
                | LET ->
                    _menhir_run166 _menhir_env (Obj.magic _menhir_stack) MenhirState260
                | LIDENT _v ->
                    _menhir_run164 _menhir_env (Obj.magic _menhir_stack) MenhirState260 _v
                | LPAREN ->
                    _menhir_run162 _menhir_env (Obj.magic _menhir_stack) MenhirState260
                | TRUE ->
                    _menhir_run161 _menhir_env (Obj.magic _menhir_stack) MenhirState260
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState260)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_separated_nonempty_list_COMMA_LIDENT_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Types.var list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState33 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 3765 "src/parser.ml"
        ))), _, (xs : (Types.var list))) = _menhir_stack in
        let _2 = () in
        let _v : (Types.var list) = 
# 243 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 3771 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_COMMA_LIDENT_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState31 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _, (l : (Types.var list))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Types.var list) = 
# 200 "src/parser.mly"
                                                                 ( l )
# 3789 "src/parser.ml"
             in
            _menhir_goto_const_args _menhir_env _menhir_stack _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState257 | MenhirState250 | MenhirState242 | MenhirState231 | MenhirState167 | MenhirState157 | MenhirState40 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, (x : (Types.var list))) = _menhir_stack in
        let _v : (Types.var list) = 
# 144 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x )
# 3805 "src/parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_COMMA_LIDENT__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_list_decl_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (decl list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState0 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EOF ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (decls : (decl list))) = _menhir_stack in
            let _2 = () in
            let _v : (
# 115 "src/parser.mly"
      (string list * Types.env)
# 3828 "src/parser.ml"
            ) = 
# 128 "src/parser.mly"
    (
      let base, progs, filt, atoms, hooks = split_decl decls in
      let sorts = mkenv_sorts base (List.map fst progs) in
      let type_sort x = SMap.find x sorts in
      let type_sorts = List.map type_sort in
      let const = List.flatten (List.map (fun (o, cst) -> List.map (fun (name, l) -> {
          cs_name = name ;
          cs_input_sorts = type_sorts l ;
          cs_output_sort = o
        }) cst) progs) in
      let env_consts = List.fold_left (fun m c -> SMap.add c.cs_name c m) SMap.empty const in
      let filt = List.map (fun (name, lin, lout) -> {
          fs_name = name ;
          fs_input_sorts = type_sorts lin ;
          fs_output_sorts = type_sorts lout ;
        }) filt in
      let env_filt = List.fold_left (fun m f -> SMap.add f.fs_name f m) SMap.empty filt in
      let hks = SSet.of_list (List.map (fun (name, _, _, _, _) -> name) hooks) in
      let env_hooks = List.fold_left (fun m (name, tin, tterm, tout, rules) ->
        SMap.add name {
            hd_term_sort = tterm ;
            hd_input_sorts = [type_sort tin] ;
            hd_output_sorts = [type_sort tout] ;
            hd_rules =
              List.fold_left (fun mm (c, args, s) ->
                  SMap.add c {
                      r_constructor = c ;
                      r_constructor_arguments = args ;
                      r_skeleton = identify_hooks_skeleton hks s ;
                      r_input_sort = type_sort tin ;
                      r_output_sort = type_sort tout ;
                      r_name = name ^ "_" ^ c
                    } mm
                ) SMap.empty rules
          } m
        ) SMap.empty hooks in
      let env = {
          env_sorts = sorts ;
          env_constr_signatures = env_consts ;
          env_hook_definitions = env_hooks ;
          env_filter_signatures = env_filt
        } in
      (atoms, env)
    )
# 3875 "src/parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_1 : (
# 115 "src/parser.mly"
      (string list * Types.env)
# 3882 "src/parser.ml"
            )) = _v in
            Obj.magic _1
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState112 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (decl))), _, (xs : (decl list))) = _menhir_stack in
        let _v : (decl list) = 
# 213 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 3898 "src/parser.ml"
         in
        _menhir_goto_list_decl_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run4 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 3907 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | STAR ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState5 _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState5)
    | ATOM | BAR | EOF | HOOK | MINUSGREATER | TYPE | VAL ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 3930 "src/parser.ml"
        ))) = _menhir_stack in
        let _v : (string list) = 
# 241 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [ x ] )
# 3935 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_STAR_LIDENT_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_decl : _menhir_env -> 'ttv_tail -> _menhir_state -> (decl) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | ATOM ->
        _menhir_run100 _menhir_env (Obj.magic _menhir_stack) MenhirState112
    | HOOK ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState112
    | TYPE ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState112
    | VAL ->
        _menhir_run1 _menhir_env (Obj.magic _menhir_stack) MenhirState112
    | EOF ->
        _menhir_reduce55 _menhir_env (Obj.magic _menhir_stack) MenhirState112
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState112

and _menhir_reduce53 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : ((string * string list) list) = 
# 211 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 3972 "src/parser.ml"
     in
    _menhir_goto_list_constr_decl_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run13 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | UIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | OF ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState15 _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState15)
        | ATOM | BAR | EOF | HOOK | TYPE | VAL ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), (x : (
# 83 "src/parser.mly"
       (string)
# 4004 "src/parser.ml"
            ))) = _menhir_stack in
            let _1 = () in
            let _v : (string * string list) = 
# 181 "src/parser.mly"
                      ( (x, []) )
# 4010 "src/parser.ml"
             in
            _menhir_goto_constr_decl _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_reduce57 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : ((string * Types.var list * Types.skeleton) list) = 
# 211 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 4031 "src/parser.ml"
     in
    _menhir_goto_list_rule_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run29 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | UIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (x : (
# 82 "src/parser.mly"
       (string)
# 4054 "src/parser.ml"
            )) = _v in
            let _v : (Types.var list) = 
# 199 "src/parser.mly"
                 ( [x] )
# 4059 "src/parser.ml"
             in
            _menhir_goto_const_args _menhir_env _menhir_stack _v
        | LPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState31 _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState31)
        | MINUSGREATER ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _v : (Types.var list) = 
# 198 "src/parser.mly"
                  ( [] )
# 4078 "src/parser.ml"
             in
            _menhir_goto_const_args _menhir_env _menhir_stack _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_reduce49 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (string list) = 
# 211 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 4099 "src/parser.ml"
     in
    _menhir_goto_list_atom_elem_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run101 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (string) = 
# 189 "src/parser.mly"
                   ( "->" )
# 4111 "src/parser.ml"
     in
    _menhir_goto_atom_elem _menhir_env _menhir_stack _menhir_s _v

and _menhir_run102 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 4118 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (x : (
# 82 "src/parser.mly"
       (string)
# 4126 "src/parser.ml"
    )) = _v in
    let _v : (string) = 
# 188 "src/parser.mly"
                 ( x )
# 4131 "src/parser.ml"
     in
    _menhir_goto_atom_elem _menhir_env _menhir_stack _menhir_s _v

and _menhir_run103 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (string) = 
# 190 "src/parser.mly"
            ( ":" )
# 4143 "src/parser.ml"
     in
    _menhir_goto_atom_elem _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_list_cdecl_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (cdecl list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState115 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EOF ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (l : (cdecl list))) = _menhir_stack in
            let _2 = () in
            let _v : (
# 117 "src/parser.mly"
      (Types.pcdecls)
# 4164 "src/parser.ml"
            ) = 
# 343 "src/parser.mly"
      (
        let ts = List.of_seq (Seq.filter_map (function CType t -> Some t | _ -> None) (List.to_seq l)) in
        let vs = List.of_seq (Seq.filter_map (function CVar v -> Some v | _ -> None) (List.to_seq l)) in
        let rivs = List.of_seq (Seq.filter_map (function CRuleInVar v -> Some v | _ -> None) (List.to_seq l)) in
        let rovs = List.of_seq (Seq.filter_map (function CRuleOutVar v -> Some v | _ -> None) (List.to_seq l)) in
        let qs = List.of_seq (Seq.filter_map (function CQuark q -> Some q | _ -> None) (List.to_seq l)) in
        let fs = List.of_seq (Seq.filter_map (function CFilter f -> Some f | _ -> None) (List.to_seq l)) in
        let his = List.of_seq (Seq.filter_map (function CHookIn h -> Some h | _ -> None) (List.to_seq l)) in
        let hos = List.of_seq (Seq.filter_map (function CHookOut h -> Some h | _ -> None) (List.to_seq l)) in
        let ris = List.of_seq (Seq.filter_map (function CRuleIn r -> Some r | _ -> None) (List.to_seq l)) in
        let ros = List.of_seq (Seq.filter_map (function CRuleOut r -> Some r | _ -> None) (List.to_seq l)) in
        {
          pc_types = ts ;
          pc_vars = vs ;
          pc_rulein_vars = rivs ;
          pc_ruleout_vars = rovs ;
          pc_quarks = qs ;
          pc_filters = fs ;
          pc_hookins = his ;
          pc_hookouts = hos ;
          pc_ruleins = ris ;
          pc_ruleouts = ros ;
        }
      )
# 4191 "src/parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_1 : (
# 117 "src/parser.mly"
      (Types.pcdecls)
# 4198 "src/parser.ml"
            )) = _v in
            Obj.magic _1
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState275 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (cdecl))), _, (xs : (cdecl list))) = _menhir_stack in
        let _v : (cdecl list) = 
# 213 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( x :: xs )
# 4214 "src/parser.ml"
         in
        _menhir_goto_list_cdecl_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run119 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (Types.typedesc) = 
# 308 "src/parser.mly"
                    ( Program_point TopType )
# 4228 "src/parser.ml"
     in
    _menhir_goto_simple_typedesc _menhir_env _menhir_stack _menhir_s _v

and _menhir_run120 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState120 _v
    | LPAREN ->
        _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState120
    | PROGRAM_POINT ->
        _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState120
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState120

and _menhir_run121 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 4252 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (x : (
# 82 "src/parser.mly"
       (string)
# 4260 "src/parser.ml"
    )) = _v in
    let _v : (Types.typedesc) = 
# 304 "src/parser.mly"
                 ( BaseType x )
# 4265 "src/parser.ml"
     in
    _menhir_goto_simple_typedesc _menhir_env _menhir_stack _menhir_s _v

and _menhir_reduce69 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (string) = 
# 326 "src/parser.mly"
                    ( "" )
# 4274 "src/parser.ml"
     in
    _menhir_goto_maybe_annot _menhir_env _menhir_stack _menhir_s _v

and _menhir_run239 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (
# 82 "src/parser.mly"
       (string)
# 4291 "src/parser.ml"
        )) = _v in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        let _1 = () in
        let _v : (string) = 
# 327 "src/parser.mly"
                        ( x )
# 4298 "src/parser.ml"
         in
        _menhir_goto_maybe_annot _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_reduce59 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (Types.var list) = 
# 142 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 4313 "src/parser.ml"
     in
    _menhir_goto_loption_separated_nonempty_list_COMMA_LIDENT__ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run32 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 82 "src/parser.mly"
       (string)
# 4320 "src/parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | COMMA ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState33 _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState33)
    | RPAREN ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, (x : (
# 82 "src/parser.mly"
       (string)
# 4343 "src/parser.ml"
        ))) = _menhir_stack in
        let _v : (Types.var list) = 
# 241 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [ x ] )
# 4348 "src/parser.ml"
         in
        _menhir_goto_separated_nonempty_list_COMMA_LIDENT_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_reduce55 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (decl list) = 
# 211 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 4363 "src/parser.ml"
     in
    _menhir_goto_list_decl_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run1 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState3 _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState3)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run10 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EQUAL ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | BAR ->
                _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState12
            | ATOM | EOF | HOOK | TYPE | VAL ->
                _menhir_reduce53 _menhir_env (Obj.magic _menhir_stack) MenhirState12
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState12)
        | ATOM | EOF | HOOK | TYPE | VAL ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), (x : (
# 82 "src/parser.mly"
       (string)
# 4433 "src/parser.ml"
            ))) = _menhir_stack in
            let _1 = () in
            let _v : (decl) = 
# 174 "src/parser.mly"
                       ( BaseType x )
# 4439 "src/parser.ml"
             in
            _menhir_goto_decl _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run20 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_stack = (_menhir_stack, _v) in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | MINUSGREATER ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | LIDENT _v ->
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let _menhir_stack = (_menhir_stack, _v) in
                        let _menhir_env = _menhir_discard _menhir_env in
                        let _tok = _menhir_env._menhir_token in
                        (match _tok with
                        | MINUSGREATER ->
                            let _menhir_stack = Obj.magic _menhir_stack in
                            let _menhir_env = _menhir_discard _menhir_env in
                            let _tok = _menhir_env._menhir_token in
                            (match _tok with
                            | LIDENT _v ->
                                let _menhir_stack = Obj.magic _menhir_stack in
                                let _menhir_stack = (_menhir_stack, _v) in
                                let _menhir_env = _menhir_discard _menhir_env in
                                let _tok = _menhir_env._menhir_token in
                                (match _tok with
                                | EQUAL ->
                                    let _menhir_stack = Obj.magic _menhir_stack in
                                    let _menhir_env = _menhir_discard _menhir_env in
                                    let _tok = _menhir_env._menhir_token in
                                    (match _tok with
                                    | BAR ->
                                        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState28
                                    | ATOM | EOF | HOOK | TYPE | VAL ->
                                        _menhir_reduce57 _menhir_env (Obj.magic _menhir_stack) MenhirState28
                                    | _ ->
                                        assert (not _menhir_env._menhir_error);
                                        _menhir_env._menhir_error <- true;
                                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState28)
                                | _ ->
                                    assert (not _menhir_env._menhir_error);
                                    _menhir_env._menhir_error <- true;
                                    let _menhir_stack = Obj.magic _menhir_stack in
                                    let (((((_menhir_stack, _menhir_s), _), _), _), _) = _menhir_stack in
                                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                            | _ ->
                                assert (not _menhir_env._menhir_error);
                                _menhir_env._menhir_error <- true;
                                let _menhir_stack = Obj.magic _menhir_stack in
                                let ((((_menhir_stack, _menhir_s), _), _), _) = _menhir_stack in
                                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                        | _ ->
                            assert (not _menhir_env._menhir_error);
                            _menhir_env._menhir_error <- true;
                            let _menhir_stack = Obj.magic _menhir_stack in
                            let ((((_menhir_stack, _menhir_s), _), _), _) = _menhir_stack in
                            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run100 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | COLON ->
        _menhir_run103 _menhir_env (Obj.magic _menhir_stack) MenhirState100
    | LIDENT _v ->
        _menhir_run102 _menhir_env (Obj.magic _menhir_stack) MenhirState100 _v
    | MINUSGREATER ->
        _menhir_run101 _menhir_env (Obj.magic _menhir_stack) MenhirState100
    | ATOM | EOF | HOOK | TYPE | VAL ->
        _menhir_reduce49 _menhir_env (Obj.magic _menhir_stack) MenhirState100
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState100

and _menhir_errorcase : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    match _menhir_s with
    | MenhirState275 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState260 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState257 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState253 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState250 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState248 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState245 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState242 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState238 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState234 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState231 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState228 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState217 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState210 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState206 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState198 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState195 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState191 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState188 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState184 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState182 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState180 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState178 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState177 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState175 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState174 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState173 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState172 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState167 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState166 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState165 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState162 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState160 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState157 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState154 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState148 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState144 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState141 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState139 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState129 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState124 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState120 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState118 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState115 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        raise _eRR
    | MenhirState112 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState105 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState100 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState97 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState86 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState82 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState80 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState79 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState76 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState65 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState59 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState58 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState56 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState55 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState53 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState52 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState50 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState49 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState46 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState45 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState40 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState39 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState33 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState31 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        raise _eRR
    | MenhirState28 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((((_menhir_stack, _menhir_s), _), _), _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState18 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState15 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState12 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState8 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState5 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState3 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState0 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        raise _eRR

and _menhir_reduce51 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : (cdecl list) = 
# 211 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
    ( [] )
# 4891 "src/parser.ml"
     in
    _menhir_goto_list_cdecl_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run116 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState118 _v
            | LPAREN ->
                _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | PROGRAM_POINT ->
                _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState118
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState118)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run137 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState139 _v
            | LPAREN ->
                _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState139
            | PROGRAM_POINT ->
                _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState139
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState139)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run146 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EQUAL ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState148 _v
            | LPAREN ->
                _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState148
            | PROGRAM_POINT ->
                _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState148
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState148)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run150 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IN ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LPAREN ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | LIDENT _v ->
                    _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState231 _v
                | RPAREN ->
                    _menhir_reduce59 _menhir_env (Obj.magic _menhir_stack) MenhirState231
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState231)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_stack = (_menhir_stack, _v) in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | COLON ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | LIDENT _v ->
                        _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState228 _v
                    | LPAREN ->
                        _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState228
                    | PROGRAM_POINT ->
                        _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState228
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState228)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | OUT ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LPAREN ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | LIDENT _v ->
                    _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState157 _v
                | RPAREN ->
                    _menhir_reduce59 _menhir_env (Obj.magic _menhir_stack) MenhirState157
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState157)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | VAR ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_stack = (_menhir_stack, _v) in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | COLON ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | LIDENT _v ->
                        _menhir_run121 _menhir_env (Obj.magic _menhir_stack) MenhirState154 _v
                    | LPAREN ->
                        _menhir_run120 _menhir_env (Obj.magic _menhir_stack) MenhirState154
                    | PROGRAM_POINT ->
                        _menhir_run119 _menhir_env (Obj.magic _menhir_stack) MenhirState154
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState154)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run236 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IN ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CARET ->
                _menhir_run239 _menhir_env (Obj.magic _menhir_stack) MenhirState248
            | LPAREN ->
                _menhir_reduce69 _menhir_env (Obj.magic _menhir_stack) MenhirState248
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState248)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | OUT ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LIDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CARET ->
                _menhir_run239 _menhir_env (Obj.magic _menhir_stack) MenhirState238
            | LPAREN ->
                _menhir_reduce69 _menhir_env (Obj.magic _menhir_stack) MenhirState238
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState238)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run255 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LIDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | LPAREN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LIDENT _v ->
                _menhir_run32 _menhir_env (Obj.magic _menhir_stack) MenhirState257 _v
            | RPAREN ->
                _menhir_reduce59 _menhir_env (Obj.magic _menhir_stack) MenhirState257
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState257)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_discard : _menhir_env -> _menhir_env =
  fun _menhir_env ->
    let lexer = _menhir_env._menhir_lexer in
    let lexbuf = _menhir_env._menhir_lexbuf in
    let _tok = lexer lexbuf in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    }

and _menhir_init : (Lexing.lexbuf -> token) -> Lexing.lexbuf -> _menhir_env =
  fun lexer lexbuf ->
    let _tok = Obj.magic () in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    }

and main : (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (
# 115 "src/parser.mly"
      (string list * Types.env)
# 5302 "src/parser.ml"
) =
  fun lexer lexbuf ->
    let _menhir_env = _menhir_init lexer lexbuf in
    Obj.magic (let _menhir_stack = ((), _menhir_env._menhir_lexbuf.Lexing.lex_curr_p) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | ATOM ->
        _menhir_run100 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | HOOK ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | TYPE ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | VAL ->
        _menhir_run1 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | EOF ->
        _menhir_reduce55 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState0)

and main_constr : (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (
# 117 "src/parser.mly"
      (Types.pcdecls)
# 5328 "src/parser.ml"
) =
  fun lexer lexbuf ->
    let _menhir_env = _menhir_init lexer lexbuf in
    Obj.magic (let _menhir_stack = ((), _menhir_env._menhir_lexbuf.Lexing.lex_curr_p) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FILTER ->
        _menhir_run255 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | HOOK ->
        _menhir_run236 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | RULE ->
        _menhir_run150 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | TYPE ->
        _menhir_run146 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | VAL ->
        _menhir_run137 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | VAR ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | EOF ->
        _menhir_reduce51 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState115)

# 269 "/Users/AdamKhayam/.opam/default/lib/menhir/standard.mly"
  

# 5358 "src/parser.ml"
