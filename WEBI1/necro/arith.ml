exception Branch_fail

type 'literal expr =
| C of 'literal
| D of 'literal expr * 'literal expr
| M of 'literal expr * 'literal expr
| P of 'literal expr * 'literal expr
| T of 'literal expr * 'literal expr

module type FLOW = sig
  type literal
  type state
  type value

  val mkstate : unit -> state
  val print_value : value -> unit
  
  val div : literal -> literal -> value option
  val litToVal : literal -> value option
  val minus : literal -> literal -> value option
  val plus : literal -> literal -> value option
  val times : literal -> literal -> value option
  val valToLit : value -> literal option
end

module type INTERPRETER = sig
  type literal
  type state
  type value
  
  val mkstate : unit -> state
  val print_value : value -> unit
  
  val expr : state -> literal expr -> value
end

module MakeInterpreter (F : FLOW) : (INTERPRETER with type literal = F.literal and type state = F.state and type value = F.value) = struct
  include F
  
  let rec __eval_expr_C x_t x_s =
    let (x_o) = match litToVal x_t with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_D x_t1 x_t2 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_t3) = match valToLit x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_f2 = expr x_s x_t2 in
    let (x_t4) = match valToLit x_f2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match div x_t3 x_t4 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_M x_t1 x_t2 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_t3) = match valToLit x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_f2 = expr x_s x_t2 in
    let (x_t4) = match valToLit x_f2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match minus x_t3 x_t4 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_P x_t1 x_t2 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_t3) = match valToLit x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_f2 = expr x_s x_t2 in
    let (x_t4) = match valToLit x_f2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match plus x_t3 x_t4 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_T x_t1 x_t2 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_t3) = match valToLit x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_f2 = expr x_s x_t2 in
    let (x_t4) = match valToLit x_f2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match times x_t3 x_t4 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and expr x_s = function
  | C x_t -> __eval_expr_C x_t  x_s
  | D (x_t1, x_t2) -> __eval_expr_D x_t1 x_t2  x_s
  | M (x_t1, x_t2) -> __eval_expr_M x_t1 x_t2  x_s
  | P (x_t1, x_t2) -> __eval_expr_P x_t1 x_t2  x_s
  | T (x_t1, x_t2) -> __eval_expr_T x_t1 x_t2  x_s
  
  
end
