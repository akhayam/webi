open Pcf_cfa
open Domains

module BaseTypes = struct
  module Ident = struct
    type t = string
    let print ff s = Format.fprintf ff "%s" s
    let compare = compare
  end

  module Globalcontext = struct
    type t = int list list
    let print ff s = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") (pp_pp_ppmap !pp_default_map)) s
    let compare = compare
  end

  module Lit = struct
    type t = int
    let print ff x = Format.fprintf ff "%d" x
  end

  module Int = struct
    type t = Interval.t option
    let print = Interval.pp_print
    let bot = None
    let join v1 v2 = match v1, v2 with
      | None, None -> None
      | None, Some v | Some v, None -> Some v
      | Some v1, Some v2 -> Some (Interval.lub v1 v2)
    let update v1 v2 needs_join =
      match needs_join with
      | JTJoin -> let r = join v1 v2 in (r, r = v1)
      | JTWiden ->
        match v1, v2 with
        | None, None -> (None, true)
        | None, Some v -> (Some v, false)
        | Some v, None -> (Some v, true)
        | Some v1, Some v2 ->
          let r = Interval.widen v1 v2 in
          (* Format.eprintf "old %a new %a res %a stable %b@." print (Some v1) print (Some v2) print (Some r) (r = v1); *)
          (Some r, r = v1)
  end
end

let k = 2
let rec lastk k l =
  match k, l with
  | 0, _ -> []
  | _, [] -> []
  | k, x :: l -> x :: lastk (k - 1) l

module Abstract : MakeTypes(BaseTypes).ABSTRACT = struct
  module X = MakeTypes(BaseTypes)
  open X

  let envset env x v = X.Env1.M.add x v env
  let framepush gc pp = lastk k (pp :: gc)
  let int_bot () = None
  let isBot x = match x with None -> [] | Some _ -> [x]
  let intOfLit x = Some (Interval.Bint x, Interval.Bint x)
  let plus v1 v2 = match v1, v2 with None, _ | _, None -> None | Some v1, Some v2 -> Some (Interval.add v1 v2)
  let isZero sigma x =
    match x with None -> [] | Some x -> if fst (Interval.eq x (Interval.Bint 0, Interval.Bint 0)) then [sigma] else []
  let isNotZero sigma x =
    match x with None -> [] | Some x -> if snd (Interval.eq x (Interval.Bint 0, Interval.Bint 0)) then [sigma] else []
end

module Cstr = MakeCstr(BaseTypes)(Abstract)

let make_store () =
  let init_env = Cstr.Env.M.singleton Cstr.Env1.M.empty in
  let store = Cstr.Store.create_store () in
  let () = Cstr.Store._in_eval_set store ([], []) (make_update Cstr.Env.update init_env) in
  store

let () = Format.set_margin 120

let test t0 =
  fill_default_ppmap gent_term t0;
  let store = make_store () in
  let () = Cstr.run_constraints store t0 in
  Format.printf "%a@." (pp_term Format.pp_print_string Format.pp_print_int [] !pp_default_map) t0;
  Format.printf "%a@." Cstr.Store.print store

let let_ n v body = App (Lam (n, body), v)

let t0 =
  let_ "f" (Lam ("x", App (Var "x", Var "x")))
    (App (Lam ("y", Var "y"), App (Var "f", Var "f")))

let () = test t0

let t1 =
  let_ "Y" (Lam ("f", let u = Lam ("x", App (Var "f", Lam ("v", App (App (Var "x", Var "x"), Var "v")))) in App (u, u)))
    (let_ "z" (App (Var "Y", (Lam ("z", Lam ("x", IfZero (Var "x", Const 0, App (Var "z", Plus (Var "x", Const (-1)))))))))
       (App (Var "z", Const 42)))

let () = test t1

let t2 =
  let_ "Y" (Lam ("f", let u = Lam ("x", App (Var "f", Lam ("v", App (App (Var "x", Var "x"), Var "v")))) in App (u, u)))
    (let_ "z" (App (Var "Y", (Lam ("z", Lam ("x", IfZero (Var "x", Const 0, App (Var "z", Plus (Var "x", Const (-1)))))))))
       (App (Var "z", Const (-42))))

let () = test t2

