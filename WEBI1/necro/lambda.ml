exception Branch_fail

type 'ident lterm =
| App of 'ident lterm * 'ident lterm
| Lam of 'ident * 'ident lterm
| Var of 'ident

module type FLOW = sig
  type clos
  type env
  type ident
  
  val initial_env : unit -> env
  val print_clos : clos -> unit
  
  val extEnv : env -> ident -> clos -> env option
  val getClos : clos -> (ident * ident lterm * env) option
  val getEnv : ident -> env -> clos option
  val mkClos : ident -> ident lterm -> env -> clos option
end

module type INTERPRETER = sig
  type clos
  type env
  type ident
  
  val initial_env : unit -> env
  val print_clos : clos -> unit
  
  val eval : env -> ident lterm -> clos
end

module MakeInterpreter (F : FLOW) : (INTERPRETER with type clos = F.clos and type env = F.env and type ident = F.ident) = struct
  include F
  
  let rec __eval_eval_App x_t1 x_t2 x_s =
    let x_f1 = eval x_s x_t1 in
    let (x_t3, x_t4, x_f2) = match getClos x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_f3 = eval x_s x_t2 in
    let (x_f4) = match extEnv x_f2 x_t3 x_f3 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_o = eval x_f4 x_t4 in
    x_o
  
  and __eval_eval_Lam x_t1 x_t2 x_s =
    let (x_o) = match mkClos x_t1 x_t2 x_s with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_eval_Var x_t x_s =
    let (x_o) = match getEnv x_t x_s with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and eval x_s = function
  | App (x_t1, x_t2) -> __eval_eval_App x_t1 x_t2  x_s
  | Lam (x_t1, x_t2) -> __eval_eval_Lam x_t1 x_t2  x_s
  | Var x_t -> __eval_eval_Var x_t  x_s
  
  
end
