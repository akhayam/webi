exception Branch_fail

type ('ident, 'lit) stat =
| Assign of 'ident * ('ident, 'lit) expr
| If of ('ident, 'lit) expr * ('ident, 'lit) stat * ('ident, 'lit) stat
| Seq of ('ident, 'lit) stat * ('ident, 'lit) stat
| Skip
| While of ('ident, 'lit) expr * ('ident, 'lit) stat

and ('ident, 'lit) expr =
| Bang of ('ident, 'lit) expr
| Const of 'lit
| Equal of ('ident, 'lit) expr * ('ident, 'lit) expr
| Plus of ('ident, 'lit) expr * ('ident, 'lit) expr
| Var of 'ident

module type FLOW = sig
  type ident
  type lit
  type state
  type value
  type vbool
  type vint
  
  val initial_state : unit -> state
  val print_state : state -> unit
  val print_value : value -> unit
  
  val add : vint -> vint -> value option
  val eq : vint -> vint -> value option
  val id : state -> state option
  val isBool : value -> vbool option
  val isFalse : vbool -> unit option
  val isInt : value -> vint option
  val isTrue : vbool -> unit option
  val litToVal : lit -> value option
  val neg : vbool -> value option
  val read : ident -> state -> value option
  val write : ident -> state -> value -> state option
end

module type INTERPRETER = sig
  type ident
  type lit
  type state
  type value
  type vbool
  type vint
  
  val initial_state : unit -> state
  val print_state : state -> unit
  val print_value : value -> unit
  
  val expr : state -> (ident, lit) expr -> value
  val stat : state -> (ident, lit) stat -> state
end

module MakeInterpreter (F : FLOW) : (INTERPRETER with type ident = F.ident and type lit = F.lit and type state = F.state and type value = F.value and type vbool = F.vbool and type vint = F.vint) = struct
  include F
  
  let rec __eval_expr_Bang x_t x_s =
    let x_f1 = expr x_s x_t in
    let (x_f1') = match isBool x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match neg x_f1' with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_Const x_t x_s =
    let (x_o) = match litToVal x_t with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_Equal x_t1 x_t2 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_f1') = match isInt x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_f2 = expr x_s x_t2 in
    let (x_f2') = match isInt x_f2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match eq x_f1' x_f2' with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_Plus x_t1 x_t2 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_f1') = match isInt x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_f2 = expr x_s x_t2 in
    let (x_f2') = match isInt x_f2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match add x_f1' x_f2' with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_expr_Var x_t x_s =
    let (x_o) = match read x_t x_s with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_stat_Assign x_t1 x_t2 x_s =
    let x_f2 = expr x_s x_t2 in
    let (x_o) = match write x_t1 x_s x_f2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_stat_If x_t1 x_t2 x_t3 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_f1') = match isBool x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) =
      try
        
        let () = match isTrue x_f1' with
        | None -> raise Branch_fail
        | Some result -> result
        in
        let x_o = stat x_s x_t2 in
        (x_o)
      with Branch_fail ->
      try
        
        let () = match isFalse x_f1' with
        | None -> raise Branch_fail
        | Some result -> result
        in
        let x_o = stat x_s x_t3 in
        (x_o)
      with Branch_fail ->
      raise Branch_fail
    in
    x_o
  
  and __eval_stat_Seq x_t1 x_t2 x_s =
    let x_f1 = stat x_s x_t1 in
    let x_o = stat x_f1 x_t2 in
    x_o
  
  and __eval_stat_Skip  x_s =
    let (x_o) = match id x_s with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_stat_While x_t1 x_t2 x_s =
    let x_f1 = expr x_s x_t1 in
    let (x_f1') = match isBool x_f1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) =
      try
        
        let () = match isTrue x_f1' with
        | None -> raise Branch_fail
        | Some result -> result
        in
        let x_f2 = stat x_s x_t2 in
        let x_o = stat x_f2 (While (x_t1, x_t2)) in
        (x_o)
      with Branch_fail ->
      try
        
        let () = match isFalse x_f1' with
        | None -> raise Branch_fail
        | Some result -> result
        in
        let (x_o) = match id x_s with
        | None -> raise Branch_fail
        | Some result -> result
        in
        (x_o)
      with Branch_fail ->
      raise Branch_fail
    in
    x_o
  
  and expr x_s = function
  | Bang x_t -> __eval_expr_Bang x_t  x_s
  | Const x_t -> __eval_expr_Const x_t  x_s
  | Equal (x_t1, x_t2) -> __eval_expr_Equal x_t1 x_t2  x_s
  | Plus (x_t1, x_t2) -> __eval_expr_Plus x_t1 x_t2  x_s
  | Var x_t -> __eval_expr_Var x_t  x_s
  
  and stat x_s = function
  | Assign (x_t1, x_t2) -> __eval_stat_Assign x_t1 x_t2  x_s
  | If (x_t1, x_t2, x_t3) -> __eval_stat_If x_t1 x_t2 x_t3  x_s
  | Seq (x_t1, x_t2) -> __eval_stat_Seq x_t1 x_t2  x_s
  | Skip -> __eval_stat_Skip   x_s
  | While (x_t1, x_t2) -> __eval_stat_While x_t1 x_t2  x_s
  
  
end
