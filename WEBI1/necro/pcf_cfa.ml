type join_type = JTJoin | JTWiden
let make_update f v old jt = match old with None -> (v, false) | Some v1 -> f v1 v jt

type ('ident, 'lit) term =
| App of ('ident, 'lit) term * ('ident, 'lit) term
| Const of 'lit
| IfZero of ('ident, 'lit) term * ('ident, 'lit) term * ('ident, 'lit) term
| Lam of 'ident * ('ident, 'lit) term
| Plus of ('ident, 'lit) term * ('ident, 'lit) term
| Var of 'ident

let rec subterm_term_term term pp =
  match pp with
  | [] -> term
  | b :: pp -> match term with
    | App (x_t0, x_t1) -> (match b with | 0 -> subterm_term_term x_t0 pp | 1 -> subterm_term_term x_t1 pp | _ -> assert false)
    | Const x_t0 -> (match b with | 0 -> assert false | _ -> assert false)
    | IfZero (x_t0, x_t1, x_t2) -> (match b with | 0 -> subterm_term_term x_t0 pp | 1 -> subterm_term_term x_t1 pp | 2 -> subterm_term_term x_t2 pp | _ -> assert false)
    | Lam (x_t0, x_t1) -> (match b with | 0 -> assert false | 1 -> subterm_term_term x_t1 pp | _ -> assert false)
    | Plus (x_t0, x_t1) -> (match b with | 0 -> subterm_term_term x_t0 pp | 1 -> subterm_term_term x_t1 pp | _ -> assert false)
    | Var x_t0 -> (match b with | 0 -> assert false | _ -> assert false)

let subterm_term_term term pp = subterm_term_term term (List.rev pp)

let subterm_term = subterm_term_term

let rec subterm_term_ident term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | App (x_t0, x_t1) -> (match b with | 0 -> subterm_term_ident x_t0 pp | 1 -> subterm_term_ident x_t1 pp | _ -> assert false)
    | Const x_t0 -> (match b with | 0 -> assert false | _ -> assert false)
    | IfZero (x_t0, x_t1, x_t2) -> (match b with | 0 -> subterm_term_ident x_t0 pp | 1 -> subterm_term_ident x_t1 pp | 2 -> subterm_term_ident x_t2 pp | _ -> assert false)
    | Lam (x_t0, x_t1) -> (match b with | 0 -> if pp = [] then x_t0 else assert false | 1 -> subterm_term_ident x_t1 pp | _ -> assert false)
    | Plus (x_t0, x_t1) -> (match b with | 0 -> subterm_term_ident x_t0 pp | 1 -> subterm_term_ident x_t1 pp | _ -> assert false)
    | Var x_t0 -> (match b with | 0 -> if pp = [] then x_t0 else assert false | _ -> assert false)

let subterm_term_ident term pp = subterm_term_ident term (List.rev pp)

let subterm_ident = subterm_term_ident

let rec subterm_term_lit term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | App (x_t0, x_t1) -> (match b with | 0 -> subterm_term_lit x_t0 pp | 1 -> subterm_term_lit x_t1 pp | _ -> assert false)
    | Const x_t0 -> (match b with | 0 -> if pp = [] then x_t0 else assert false | _ -> assert false)
    | IfZero (x_t0, x_t1, x_t2) -> (match b with | 0 -> subterm_term_lit x_t0 pp | 1 -> subterm_term_lit x_t1 pp | 2 -> subterm_term_lit x_t2 pp | _ -> assert false)
    | Lam (x_t0, x_t1) -> (match b with | 0 -> assert false | 1 -> subterm_term_lit x_t1 pp | _ -> assert false)
    | Plus (x_t0, x_t1) -> (match b with | 0 -> subterm_term_lit x_t0 pp | 1 -> subterm_term_lit x_t1 pp | _ -> assert false)
    | Var x_t0 -> (match b with | 0 -> assert false | _ -> assert false)

let subterm_term_lit term pp = subterm_term_lit term (List.rev pp)

let subterm_lit = subterm_term_lit

let rec gent_term f_term f_ident f_lit pp term =
  match term with
  | App (x_t0, x_t1) -> f_term pp term; gent_term f_term f_ident f_lit (0 :: pp) x_t0; gent_term f_term f_ident f_lit (1 :: pp) x_t1
  | Const x_t0 -> f_term pp term; f_lit (0 :: pp) x_t0
  | IfZero (x_t0, x_t1, x_t2) -> f_term pp term; gent_term f_term f_ident f_lit (0 :: pp) x_t0; gent_term f_term f_ident f_lit (1 :: pp) x_t1; gent_term f_term f_ident f_lit (2 :: pp) x_t2
  | Lam (x_t0, x_t1) -> f_term pp term; f_ident (0 :: pp) x_t0; gent_term f_term f_ident f_lit (1 :: pp) x_t1
  | Plus (x_t0, x_t1) -> f_term pp term; gent_term f_term f_ident f_lit (0 :: pp) x_t0; gent_term f_term f_ident f_lit (1 :: pp) x_t1
  | Var x_t0 -> f_term pp term; f_ident (0 :: pp) x_t0

module PPMap = Map.Make(struct type t = int list let compare = compare end)
let pp_default_map = ref PPMap.empty
let empty_ppmap () = pp_default_map := PPMap.empty
let fill_default_ppmap gent t0 =
  let c = ref 0 in
  pp_default_map := PPMap.empty;
  gent (fun pp _ -> pp_default_map := PPMap.add pp (incr c; "l" ^ string_of_int !c) !pp_default_map) (fun pp _ -> pp_default_map := PPMap.add pp (incr c; "l" ^ string_of_int !c) !pp_default_map) (fun pp _ -> pp_default_map := PPMap.add pp (incr c; "l" ^ string_of_int !c) !pp_default_map) [] t0
let pp_pp_ppmap ppmap ff pp = match PPMap.find pp ppmap with
| s -> Format.fprintf ff "%s" s
| exception Not_found -> Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Format.pp_print_int) pp

let rec pp_term pp_ident pp_lit pp ppmap ff term =
  match term with
  | App (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(App (@,%a, @,%a)@;<0 -2>)^%a@]" (pp_term pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_term pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp
  | Const x_t0 -> Format.fprintf ff "@[<hv 2>(Const @,%a@;<0 -2>)^%a@]" (fun ff x -> Format.fprintf ff "%a^%a" pp_lit x (pp_pp_ppmap ppmap) (0 :: pp)) x_t0 (pp_pp_ppmap ppmap) pp
  | IfZero (x_t0, x_t1, x_t2) -> Format.fprintf ff "@[<hv 2>(IfZero (@,%a, @,%a, @,%a)@;<0 -2>)^%a@]" (pp_term pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_term pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_term pp_ident pp_lit (2 :: pp) ppmap) x_t2 (pp_pp_ppmap ppmap) pp
  | Lam (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(Lam (@,%a, @,%a)@;<0 -2>)^%a@]" (fun ff x -> Format.fprintf ff "%a^%a" pp_ident x (pp_pp_ppmap ppmap) (0 :: pp)) x_t0 (pp_term pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp
  | Plus (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(Plus (@,%a, @,%a)@;<0 -2>)^%a@]" (pp_term pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_term pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp
  | Var x_t0 -> Format.fprintf ff "@[<hv 2>(Var @,%a@;<0 -2>)^%a@]" (fun ff x -> Format.fprintf ff "%a^%a" pp_ident x (pp_pp_ppmap ppmap) (0 :: pp)) x_t0 (pp_pp_ppmap ppmap) pp

module type BASETYPES = sig
  module Ident : sig
    type t
    val print : Format.formatter -> t -> unit
    val compare : t -> t -> int
  end
  
  module Globalcontext : sig
    type t
    val print : Format.formatter -> t -> unit
    val compare : t -> t -> int
  end
  
  module Int : sig
    type t
    val print : Format.formatter -> t -> unit
    val join : t -> t -> t
    val update : t -> t -> join_type -> t * bool
    val bot : t
  end
  
  module Lit : sig
    type t
    val print : Format.formatter -> t -> unit
  end
end

module MakeTypes(B : BASETYPES) = struct
  open B
  module PP = struct
    type t = int list
    let print ff (p : t) = pp_pp_ppmap !pp_default_map ff p
    let compare : t -> t -> int = compare
  end
  
  module Tuple2_PP_Globalcontext = struct
    type t = PP.t * Globalcontext.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" PP.print x0 Globalcontext.print x1
    let compare ((x0, x1) : t) ((y0, y1) : t) : int =
      let c = PP.compare x0 y0 in
      if c <> 0 then c else
      let c = Globalcontext.compare x1 y1 in
      if c <> 0 then c else
      c
  end
  
  module Env1 = struct
    module M = Map.Make(Ident)
    type t = Tuple2_PP_Globalcontext.t M.t
    let print ff (m : t) = Format.fprintf ff "@[<hv 2>{|@,%a@;<0 -2>|}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") (fun ff (k, v) -> Format.fprintf ff "%a |-> %a" Ident.print k Tuple2_PP_Globalcontext.print v)) (M.bindings m)
    let fold f init (m : t) = M.fold (fun k x acc -> f (k, x) acc) m init
    let find x (m : t) = M.find x m
    let update_elt x f (m : t option) needs_join =
      match m with
      | None -> let (y, stable) = f None needs_join in (M.singleton x y, stable)
      | Some m -> let (y, stable) = f (M.find_opt x m) needs_join in (M.add x y m, stable)
    let compare : t -> t -> int = M.compare Tuple2_PP_Globalcontext.compare
  end
  
  module Tuple3_PP_PP_Env1 = struct
    type t = PP.t * PP.t * Env1.t
    let print ff ((x0, x1, x2) : t) = Format.fprintf ff "(%a, %a, %a)" PP.print x0 PP.print x1 Env1.print x2
    let compare ((x0, x1, x2) : t) ((y0, y1, y2) : t) : int =
      let c = PP.compare x0 y0 in
      if c <> 0 then c else
      let c = PP.compare x1 y1 in
      if c <> 0 then c else
      let c = Env1.compare x2 y2 in
      if c <> 0 then c else
      c
  end
  
  module Clos = struct
    module M = Set.Make(Tuple3_PP_PP_Env1)
    type t = M.t
    let print ff (s : t) = Format.fprintf ff "@[<hv 2>{@,%a@;<0 -2>}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") Tuple3_PP_PP_Env1.print) (M.elements s)
    let fold f init (s : t) = M.fold f s init
    let join (s1 : t) (s2 : t) : t = M.union s1 s2
    let update (s1 : t) (s2 : t) needs_join : (t * bool) = (M.union s1 s2, M.subset s2 s1)
    let bot : t = M.empty
  end
  
  module Env = struct
    module M = Set.Make(Env1)
    type t = M.t
    let print ff (s : t) = Format.fprintf ff "@[<hv 2>{@,%a@;<0 -2>}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") Env1.print) (M.elements s)
    let fold f init (s : t) = M.fold f s init
    let join (s1 : t) (s2 : t) : t = M.union s1 s2
    let update (s1 : t) (s2 : t) needs_join : (t * bool) = (M.union s1 s2, M.subset s2 s1)
    let bot : t = M.empty
  end
  
  module Localcontext = struct
    type t = unit
    let print ff (() : t) = Format.fprintf ff "()"
  end
  
  module Value = struct
    type t = Clos.t * Int.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Clos.print x0 Int.print x1
    let join ((x0, x1) : t) ((y0, y1) : t) : t = (Clos.join x0 y0, Int.join x1 y1)
    let update ((x0, x1) : t) ((y0, y1) : t) needs_join : (t * bool) =
      let (z0, u0) = Clos.update x0 y0 needs_join in
      let (z1, u1) = Int.update x1 y1 needs_join in
    ((z0, z1), u0 && u1)
    let bot : t = (Clos.bot, Int.bot)
  end
  
  module List_Int = struct
    type t = Int.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Int.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
    let join (l1 : t) (l2 : t) : t = l1 @ l2
    let update (l1 : t) (l2 : t) needs_join : (t * bool) = assert false
    let bot : t = []
  end
  
  module List_Localcontext = struct
    type t = Localcontext.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Localcontext.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module Map_Tuple2_PP_Globalcontext_Value = struct
    module M = Map.Make(Tuple2_PP_Globalcontext)
    type t = Value.t M.t
    let print ff (m : t) = Format.fprintf ff "@[<hv 2>{|@,%a@;<0 -2>|}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") (fun ff (k, v) -> Format.fprintf ff "%a |-> %a" Tuple2_PP_Globalcontext.print k Value.print v)) (M.bindings m)
    let fold f init (m : t) = M.fold (fun k x acc -> f (k, x) acc) m init
    let find x (m : t) = try M.find x m with Not_found -> Value.bot
    let update_elt x f (m : t option) needs_join =
      match m with
      | None -> let (y, stable) = f None needs_join in (M.singleton x y, stable)
      | Some m -> let (y, stable) = f (M.find_opt x m) needs_join in (M.add x y m, stable)
  end
  
  module Tuple2_Localcontext_Value = struct
    type t = Localcontext.t * Value.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Localcontext.print x0 Value.print x1
  end
  
  module List_Tuple2_Localcontext_Value = struct
    type t = Tuple2_Localcontext_Value.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Tuple2_Localcontext_Value.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
    let join (l1 : t) (l2 : t) : t = l1 @ l2
    let update (l1 : t) (l2 : t) needs_join : (t * bool) = assert false
    let bot : t = []
  end
  
  module List_List_Tuple2_Localcontext_Value = struct
    type t = List_Tuple2_Localcontext_Value.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") List_Tuple2_Localcontext_Value.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module Tuple2_Localcontext_Int = struct
    type t = Localcontext.t * Int.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Localcontext.print x0 Int.print x1
  end
  
  module List_Tuple2_Localcontext_Int = struct
    type t = Tuple2_Localcontext_Int.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Tuple2_Localcontext_Int.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
    let join (l1 : t) (l2 : t) : t = l1 @ l2
    let update (l1 : t) (l2 : t) needs_join : (t * bool) = assert false
    let bot : t = []
  end
  
  module Set_PP = struct
    module M = Set.Make(PP)
    type t = M.t
    let print ff (s : t) = Format.fprintf ff "@[<hv 2>{@,%a@;<0 -2>}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") PP.print) (M.elements s)
    let fold f init (s : t) = M.fold f s init
  end
  
  module Tuple3_Set_PP_Set_PP_Env = struct
    type t = Set_PP.t * Set_PP.t * Env.t
    let print ff ((x0, x1, x2) : t) = Format.fprintf ff "(%a, %a, %a)" Set_PP.print x0 Set_PP.print x1 Env.print x2
  end
  
  module List_Tuple3_Set_PP_Set_PP_Env = struct
    type t = Tuple3_Set_PP_Set_PP_Env.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Tuple3_Set_PP_Set_PP_Env.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
    let join (l1 : t) (l2 : t) : t = l1 @ l2
    let update (l1 : t) (l2 : t) needs_join : (t * bool) = assert false
    let bot : t = []
  end
  
  module List_Value = struct
    type t = Value.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Value.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module List_Set_PP = struct
    type t = Set_PP.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Set_PP.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
    let join (l1 : t) (l2 : t) : t = l1 @ l2
    let update (l1 : t) (l2 : t) needs_join : (t * bool) = assert false
    let bot : t = []
  end
  
  module List_Env = struct
    type t = Env.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Env.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module Map_Tuple2_PP_Globalcontext_Env = struct
    module M = Map.Make(Tuple2_PP_Globalcontext)
    type t = Env.t M.t
    let print ff (m : t) = Format.fprintf ff "@[<hv 2>{|@,%a@;<0 -2>|}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") (fun ff (k, v) -> Format.fprintf ff "%a |-> %a" Tuple2_PP_Globalcontext.print k Env.print v)) (M.bindings m)
    let fold f init (m : t) = M.fold (fun k x acc -> f (k, x) acc) m init
    let find x (m : t) = try M.find x m with Not_found -> Env.bot
    let update_elt x f (m : t option) needs_join =
      match m with
      | None -> let (y, stable) = f None needs_join in (M.singleton x y, stable)
      | Some m -> let (y, stable) = f (M.find_opt x m) needs_join in (M.add x y m, stable)
  end
  
  module type ABSTRACT = sig
    val envset : Env1.t -> Ident.t -> Tuple2_PP_Globalcontext.t -> Env1.t
    val framepush : Globalcontext.t -> PP.t -> Globalcontext.t
    val intOfLit : Lit.t -> Int.t
    val int_bot : Localcontext.t -> Int.t
    val isBot : Int.t -> List_Int.t
    val isNotZero : Localcontext.t -> Int.t -> List_Localcontext.t
    val isZero : Localcontext.t -> Int.t -> List_Localcontext.t
    val plus : Int.t -> Int.t -> Int.t
  end
end

module MakeCstr(B : BASETYPES)(A : MakeTypes(B).ABSTRACT) = struct
  include B
  include MakeTypes(B)
  include A
  type location =
    | D__in_eval of Tuple2_PP_Globalcontext.t
    | D__out_eval of Tuple2_PP_Globalcontext.t
    | D_vars of Tuple2_PP_Globalcontext.t
  
  module Location = struct
    type t = location
    let compare x y = match x, y with
      | D__in_eval a, D__in_eval b -> Tuple2_PP_Globalcontext.compare a b
      | D__in_eval _, _ -> -1
      | _, D__in_eval _ -> 1
      | D__out_eval a, D__out_eval b -> Tuple2_PP_Globalcontext.compare a b
      | D__out_eval _, _ -> -1
      | _, D__out_eval _ -> 1
      | D_vars a, D_vars b -> Tuple2_PP_Globalcontext.compare a b
  end
  
  module LocationSet = Set.Make(Location)
  module LocationMap = Map.Make(Location)
  
  type cstr = {
    c_run : unit -> unit ;
    c_deps_in : LocationSet.t ;
    c_deps_out : LocationSet.t ;
    c_deps_deps : LocationSet.t ;
  }
  
  module Store = struct
    type t = {
      mutable _stable : bool ;
      mutable _widen_points : LocationSet.t ;
      mutable _deps_stable : bool ;
      mutable _deps_deps : LocationSet.t ;
      mutable _in_eval : Map_Tuple2_PP_Globalcontext_Env.t ;
      mutable _out_eval : Map_Tuple2_PP_Globalcontext_Value.t ;
      mutable vars : Map_Tuple2_PP_Globalcontext_Value.t ;
    }
    
    let create_store () = {
      _stable = true ;
      _widen_points = LocationSet.empty ;
      _deps_stable = true ;
      _deps_deps = LocationSet.empty ;
      _in_eval = Map_Tuple2_PP_Globalcontext_Env.M.empty ;
      _out_eval = Map_Tuple2_PP_Globalcontext_Value.M.empty ;
      vars = Map_Tuple2_PP_Globalcontext_Value.M.empty ;
    }
    
    let print ff (s : t) = Format.fprintf ff "@[<v 2>{@,%a@,%a@,%a@]@,}" (fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff "_in_eval[%a] = %a ;" Tuple2_PP_Globalcontext.print k Env.print v) ff (Map_Tuple2_PP_Globalcontext_Env.M.bindings m)) s._in_eval (fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff "_out_eval[%a] = %a ;" Tuple2_PP_Globalcontext.print k Value.print v) ff (Map_Tuple2_PP_Globalcontext_Value.M.bindings m)) s._out_eval (fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff "vars[%a] = %a ;" Tuple2_PP_Globalcontext.print k Value.print v) ff (Map_Tuple2_PP_Globalcontext_Value.M.bindings m)) s.vars
    
    let _in_eval_get store elt = Map_Tuple2_PP_Globalcontext_Env.find elt store._in_eval
    let _in_eval_set store elt f =
      if not (Map_Tuple2_PP_Globalcontext_Env.M.mem elt store._in_eval) then store._deps_stable <- false;
      let (x, stable) = Map_Tuple2_PP_Globalcontext_Env.update_elt elt f (Some store._in_eval) (if LocationSet.mem (D__in_eval elt) store._widen_points then JTWiden else JTJoin) in
      store._in_eval <- x;
      store._stable <- store._stable && stable;
      if LocationSet.mem (D__in_eval elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable
    let _out_eval_get store elt = Map_Tuple2_PP_Globalcontext_Value.find elt store._out_eval
    let _out_eval_set store elt f =
      let (x, stable) = Map_Tuple2_PP_Globalcontext_Value.update_elt elt f (Some store._out_eval) (if LocationSet.mem (D__out_eval elt) store._widen_points then JTWiden else JTJoin) in
      store._out_eval <- x;
      store._stable <- store._stable && stable;
      if LocationSet.mem (D__out_eval elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable
    let vars_get store elt = Map_Tuple2_PP_Globalcontext_Value.find elt store.vars
    let vars_set store elt f =
      let (x, stable) = Map_Tuple2_PP_Globalcontext_Value.update_elt elt f (Some store.vars) (if LocationSet.mem (D_vars elt) store._widen_points then JTWiden else JTJoin) in
      store.vars <- x;
      store._stable <- store._stable && stable;
      if LocationSet.mem (D_vars elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable
  end
  
  let constraints_eval_App _store _t0 _pp _sigmas =
    let t1 = Set_PP.M.singleton (0 :: _pp) in
    let t2 = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Set_PP.fold (fun (body) -> Value.join (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, (framepush _sigmas _pp)))) Value.bot body)) Value.bot (let (clos, _) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1) in (Clos.fold (fun (n, t, e) -> List_Set_PP.join [(Set_PP.M.of_list [t])]) List_Set_PP.bot clos))), (fun _f -> Store._out_eval_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1) (List_Set_PP.fold (fun (body) -> LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, (framepush _sigmas _pp)))])) LocationSet.empty body)) LocationSet.empty (let (clos, _) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1) in (Clos.fold (fun (n, t, e) -> List_Set_PP.join [(Set_PP.M.of_list [t])]) List_Set_PP.bot clos))))) ;
        c_deps_out = ((LocationSet.of_list [D__out_eval ((_pp, _sigmas))])) ;
        c_deps_deps = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1)) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_eval_get _store (_pp, _sigmas)), (fun _f -> Store._in_eval_set _store (_pp1, _sigmas) _f) in _lv (make_update Env.update _v))) () t1)) ;
        c_deps_in = ((LocationSet.of_list [D__in_eval ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, _sigmas))])) LocationSet.empty t1)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Value.fold (fun (f) () -> (List_Tuple3_Set_PP_Set_PP_Env.fold (fun (x, body, env) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_eval_get _store (_pp, _sigmas)), (fun _f -> Store._in_eval_set _store (_pp1, _sigmas) _f) in _lv (make_update Env.update _v))) () t2)) () (let (clos, _) = f in (Clos.fold (fun (n, t, e) -> List_Tuple3_Set_PP_Set_PP_Env.join [((Set_PP.M.of_list [n]), (Set_PP.M.of_list [t]), (Env.M.of_list [e]))]) List_Tuple3_Set_PP_Set_PP_Env.bot clos)))) () [(Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1)])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1) (LocationSet.of_list [D__in_eval ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, _sigmas))])) LocationSet.empty t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Value.fold (fun (f) () -> (List_Tuple3_Set_PP_Set_PP_Env.fold (fun (x, body, env) () -> (List_Value.fold (fun (v) () -> (Set_PP.fold (fun (n) () -> (let _v, _lv = v, (fun _f -> Store.vars_set _store (n, (framepush _sigmas _pp)) _f) in _lv (make_update Value.update _v))) () x)) () [(Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t2)])) () (let (clos, _) = f in (Clos.fold (fun (n, t, e) -> List_Tuple3_Set_PP_Set_PP_Env.join [((Set_PP.M.of_list [n]), (Set_PP.M.of_list [t]), (Env.M.of_list [e]))]) List_Tuple3_Set_PP_Set_PP_Env.bot clos)))) () [(Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1)])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t2))) ;
        c_deps_out = ((List_Set_PP.fold (fun (x) -> LocationSet.union (Set_PP.fold (fun (n) -> LocationSet.union (LocationSet.of_list [D_vars ((n, (framepush _sigmas _pp)))])) LocationSet.empty x)) LocationSet.empty (let (clos, _) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1) in (Clos.fold (fun (n, t, e) -> List_Set_PP.join [(Set_PP.M.of_list [n])]) List_Set_PP.bot clos)))) ;
        c_deps_deps = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1)) ;
      };
      {
        c_run = (fun () -> (List_Value.fold (fun (f) () -> (List_Tuple3_Set_PP_Set_PP_Env.fold (fun (x, body, env) () -> (List_Value.fold (fun (v) () -> (List_Env.fold (fun (nenv) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = nenv, (fun _f -> Store._in_eval_set _store (_pp1, (framepush _sigmas _pp)) _f) in _lv (make_update Env.update _v))) () body)) () [(Set_PP.fold (fun (n) -> Env.join (Env.fold (fun (e) -> Env.join (Env.M.of_list [(envset e (subterm_ident _t0 n) (n, (framepush _sigmas _pp)))])) Env.bot env)) Env.bot x)])) () [(Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t2)])) () (let (clos, _) = f in (Clos.fold (fun (n, t, e) -> List_Tuple3_Set_PP_Set_PP_Env.join [((Set_PP.M.of_list [n]), (Set_PP.M.of_list [t]), (Env.M.of_list [e]))]) List_Tuple3_Set_PP_Set_PP_Env.bot clos)))) () [(Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1)])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t2))) ;
        c_deps_out = ((List_Set_PP.fold (fun (body) -> LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, (framepush _sigmas _pp)))])) LocationSet.empty body)) LocationSet.empty (let (clos, _) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1) in (Clos.fold (fun (n, t, e) -> List_Set_PP.join [(Set_PP.M.of_list [t])]) List_Set_PP.bot clos)))) ;
        c_deps_deps = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1)) ;
      }
    ]
  
  let constraints_eval_Const _store _t0 _pp _sigmas =
    let n = Set_PP.M.singleton (0 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = ((Clos.M.of_list []), (Set_PP.fold (fun (pp1) -> Int.join (intOfLit (subterm_lit _t0 pp1))) Int.bot n)), (fun _f -> Store._out_eval_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.of_list [])) ;
        c_deps_out = ((LocationSet.of_list [D__out_eval ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_eval_IfZero _store _t0 _pp _sigmas =
    let cond = Set_PP.M.singleton (0 :: _pp) in
    let ifz = Set_PP.M.singleton (1 :: _pp) in
    let ifnz = Set_PP.M.singleton (2 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Tuple2_Localcontext_Int.fold (fun (_sigma, x) -> Value.join (List_Tuple2_Localcontext_Value.fold (fun (_sigma, x_o) -> Value.join x_o) Value.bot (List_List_Tuple2_Localcontext_Value.fold (fun (_l) -> List_Tuple2_Localcontext_Value.join _l) List_Tuple2_Localcontext_Value.bot [(List_Localcontext.fold (fun (_sigma) -> List_Tuple2_Localcontext_Value.join [(_sigma, (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot ifz))]) List_Tuple2_Localcontext_Value.bot (isZero _sigma x)); (List_Localcontext.fold (fun (_sigma) -> List_Tuple2_Localcontext_Value.join [(_sigma, (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot ifnz))]) List_Tuple2_Localcontext_Value.bot (isNotZero _sigma x))]))) Value.bot (let (_, x) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot cond) in (List_Int.fold (fun (y) -> List_Tuple2_Localcontext_Int.join [((), y)]) List_Tuple2_Localcontext_Int.bot (isBot x)))), (fun _f -> Store._out_eval_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty cond) (LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty ifz) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty ifnz)))) ;
        c_deps_out = ((LocationSet.of_list [D__out_eval ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_eval_get _store (_pp, _sigmas)), (fun _f -> Store._in_eval_set _store (_pp1, _sigmas) _f) in _lv (make_update Env.update _v))) () cond)) ;
        c_deps_in = ((LocationSet.of_list [D__in_eval ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, _sigmas))])) LocationSet.empty cond)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, v) () -> (List_Tuple2_Localcontext_Int.fold (fun (_sigma, x) () -> (List_Localcontext.fold (fun (_sigma) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_eval_get _store (_pp, _sigmas)), (fun _f -> Store._in_eval_set _store (_pp1, _sigmas) _f) in _lv (make_update Env.update _v))) () ifz)) () (isZero _sigma x))) () (let (_, x) = v in (List_Int.fold (fun (y) -> List_Tuple2_Localcontext_Int.join [(_sigma, y)]) List_Tuple2_Localcontext_Int.bot (isBot x))))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot cond))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty cond) (LocationSet.of_list [D__in_eval ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, _sigmas))])) LocationSet.empty ifz)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, v) () -> (List_Tuple2_Localcontext_Int.fold (fun (_sigma, x) () -> (List_Localcontext.fold (fun (_sigma) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_eval_get _store (_pp, _sigmas)), (fun _f -> Store._in_eval_set _store (_pp1, _sigmas) _f) in _lv (make_update Env.update _v))) () ifnz)) () (isNotZero _sigma x))) () (let (_, x) = v in (List_Int.fold (fun (y) -> List_Tuple2_Localcontext_Int.join [(_sigma, y)]) List_Tuple2_Localcontext_Int.bot (isBot x))))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot cond))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty cond) (LocationSet.of_list [D__in_eval ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, _sigmas))])) LocationSet.empty ifnz)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_eval_Lam _store _t0 _pp _sigmas =
    let x = Set_PP.M.singleton (0 :: _pp) in
    let body = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = ((Set_PP.fold (fun (n) -> Clos.join (Set_PP.fold (fun (t) -> Clos.join (Env.fold (fun (e) -> Clos.join (Clos.M.of_list [(n, t, e)])) Clos.bot (Store._in_eval_get _store (_pp, _sigmas)))) Clos.bot body)) Clos.bot x), (int_bot ())), (fun _f -> Store._out_eval_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.of_list [D__in_eval ((_pp, _sigmas))])) ;
        c_deps_out = ((LocationSet.of_list [D__out_eval ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_eval_Plus _store _t0 _pp _sigmas =
    let t1 = Set_PP.M.singleton (0 :: _pp) in
    let t2 = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Int.fold (fun (x1) -> Value.join (List_Int.fold (fun (x2) -> Value.join ((Clos.M.of_list []), (plus x1 x2))) Value.bot (let (_, x) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t2) in (List_Int.fold (fun (y) -> List_Int.join [y]) List_Int.bot (isBot x))))) Value.bot (let (_, x) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1) in (List_Int.fold (fun (y) -> List_Int.join [y]) List_Int.bot (isBot x)))), (fun _f -> Store._out_eval_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t2))) ;
        c_deps_out = ((LocationSet.of_list [D__out_eval ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_eval_get _store (_pp, _sigmas)), (fun _f -> Store._in_eval_set _store (_pp1, _sigmas) _f) in _lv (make_update Env.update _v))) () t1)) ;
        c_deps_in = ((LocationSet.of_list [D__in_eval ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, _sigmas))])) LocationSet.empty t1)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Value.fold (fun (v1) () -> (List_Int.fold (fun (x1) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_eval_get _store (_pp, _sigmas)), (fun _f -> Store._in_eval_set _store (_pp1, _sigmas) _f) in _lv (make_update Env.update _v))) () t2)) () (let (_, x) = v1 in (List_Int.fold (fun (y) -> List_Int.join [y]) List_Int.bot (isBot x))))) () [(Set_PP.fold (fun (_pp1) -> Value.join (Store._out_eval_get _store (_pp1, _sigmas))) Value.bot t1)])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_eval ((_pp1, _sigmas))])) LocationSet.empty t1) (LocationSet.of_list [D__in_eval ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_eval ((_pp1, _sigmas))])) LocationSet.empty t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_eval_Var _store _t0 _pp _sigmas =
    let v = Set_PP.M.singleton (0 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (Set_PP.fold (fun (n) -> Value.join (Env.fold (fun (e) -> Value.join (Store.vars_get _store (Env1.find (subterm_ident _t0 n) e))) Value.bot (Store._in_eval_get _store (_pp, _sigmas)))) Value.bot v), (fun _f -> Store._out_eval_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((Set_PP.fold (fun (n) -> LocationSet.union (LocationSet.union (LocationSet.of_list [D__in_eval ((_pp, _sigmas))]) (Env.fold (fun (e) -> LocationSet.union (LocationSet.of_list [D_vars ((Env1.find (subterm_ident _t0 n) e))])) LocationSet.empty (Store._in_eval_get _store (_pp, _sigmas))))) LocationSet.empty v)) ;
        c_deps_out = ((LocationSet.of_list [D__out_eval ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [D__in_eval ((_pp, _sigmas))])) ;
      }
    ]
  
  let constraints_eval store t0 =
    let f pp sigmas = match subterm_term t0 pp with
      | App (t1, t2) -> constraints_eval_App store t0 pp sigmas
      | Const n -> constraints_eval_Const store t0 pp sigmas
      | IfZero (cond, ifz, ifnz) -> constraints_eval_IfZero store t0 pp sigmas
      | Lam (x, body) -> constraints_eval_Lam store t0 pp sigmas
      | Plus (t1, t2) -> constraints_eval_Plus store t0 pp sigmas
      | Var v -> constraints_eval_Var store t0 pp sigmas
    in
    Map_Tuple2_PP_Globalcontext_Env.fold (fun ((pp, sigmas), _) acc -> f pp sigmas @ acc) [] (store._in_eval)
  
  let get_constraints store t0 = constraints_eval store t0
  
  let sort_constraints cstrs =
    let cstrs = Array.of_list cstrs in
    let init_vars = ref LocationMap.empty in
    Array.iteri (fun i c ->
      LocationSet.iter (fun v ->
        init_vars := LocationMap.add v (i :: (try LocationMap.find v !init_vars with Not_found -> [])) !init_vars
      ) c.c_deps_in
    ) cstrs;
    let seen = Array.make (Array.length cstrs) 0 in
    let wid = ref LocationSet.empty in
    let sorted = ref [] in
    let rec dfs v i =
      match seen.(i) with
      | 0 ->
        seen.(i) <- 1;
        let c = cstrs.(i) in
        LocationSet.iter (fun v ->
          let nxt = try LocationMap.find v !init_vars with Not_found -> [] in
          List.iter (dfs (Some v)) nxt
        ) c.c_deps_out;
        seen.(i) <- 2;
        sorted := c :: !sorted
      | 1 -> (match v with None -> assert false | Some v -> wid := LocationSet.add v !wid)
      | _ -> ()
    in
    for i = 0 to Array.length cstrs - 1 do
      dfs None i
    done;
    (!sorted, !wid)
  
  let run_constraints store t0 =
    let open Store in
    while not store._stable do
      store._stable <- true;
      store._deps_stable <- true;
      store._widen_points <- LocationSet.empty;
      let cstr, widen = sort_constraints (get_constraints store t0) in
      store._deps_deps <- List.fold_left LocationSet.union LocationSet.empty (List.map (fun c -> c.c_deps_deps) cstr);
      List.iter (fun c -> c.c_run ()) cstr;
      store._widen_points <- widen;
      while store._deps_stable && not store._stable do
        store._stable <- true;
        List.iter (fun c -> c.c_run ()) cstr
      done
    done
end
