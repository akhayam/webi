exception Branch_fail

type ('ident, 'int) term =
| App of ('ident, 'int) term * ('ident, 'int) term
| Const of 'int
| IfZero of ('ident, 'int) term * ('ident, 'int) term * ('ident, 'int) term
| Lam of 'ident * ('ident, 'int) term
| Plus of ('ident, 'int) term * ('ident, 'int) term
| Var of 'ident

module type FLOW = sig
  type env
  type ident
  type int
  type value
  
  val initial_env : unit -> env
  val print_value : value -> unit
  
  val extEnv : env -> ident -> value -> env option
  val getClos : value -> (ident * (ident, int) term * env) option
  val getEnv : ident -> env -> value option
  val getInt : value -> int option
  val isNotZero : int -> unit option
  val isZero : int -> unit option
  val mkClos : ident -> (ident, int) term -> env -> value option
  val mkInt : int -> value option
  val plus : int -> int -> int option
end

module type INTERPRETER = sig
  type env
  type ident
  type int
  type value
  
  val initial_env : unit -> env
  val print_value : value -> unit
  
  val eval : env -> (ident, int) term -> value
end

module MakeInterpreter (F : FLOW) : (INTERPRETER with type env = F.env and type ident = F.ident and type int = F.int and type value = F.value) = struct
  include F
  
  let rec __eval_eval_App t1 t2 x_s =
    let f = eval x_s t1 in
    let (x, body, env) = match getClos f with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let v = eval x_s t2 in
    let (nenv) = match extEnv env x v with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let x_o = eval nenv body in
    x_o
  
  and __eval_eval_Const n x_s =
    let (x_o) = match mkInt n with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_eval_IfZero cond ifz ifnz x_s =
    let v = eval x_s cond in
    let (x) = match getInt v with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) =
      try
        
        let () = match isZero x with
        | None -> raise Branch_fail
        | Some result -> result
        in
        let x_o = eval x_s ifz in
        (x_o)
      with Branch_fail ->
      try
        
        let () = match isNotZero x with
        | None -> raise Branch_fail
        | Some result -> result
        in
        let x_o = eval x_s ifnz in
        (x_o)
      with Branch_fail ->
      raise Branch_fail
    in
    x_o
  
  and __eval_eval_Lam x body x_s =
    let (x_o) = match mkClos x body x_s with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_eval_Plus t1 t2 x_s =
    let v1 = eval x_s t1 in
    let (x1) = match getInt v1 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let v2 = eval x_s t2 in
    let (x2) = match getInt v2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x) = match plus x1 x2 with
    | None -> raise Branch_fail
    | Some result -> result
    in
    let (x_o) = match mkInt x with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and __eval_eval_Var v x_s =
    let (x_o) = match getEnv v x_s with
    | None -> raise Branch_fail
    | Some result -> result
    in
    x_o
  
  and eval x_s = function
  | App (t1, t2) -> __eval_eval_App t1 t2  x_s
  | Const n -> __eval_eval_Const n  x_s
  | IfZero (cond, ifz, ifnz) -> __eval_eval_IfZero cond ifz ifnz  x_s
  | Lam (x, body) -> __eval_eval_Lam x body  x_s
  | Plus (t1, t2) -> __eval_eval_Plus t1 t2  x_s
  | Var v -> __eval_eval_Var v  x_s
  
  
end
