type join_type = JTJoin | JTWiden
let make_update f v old jt = match old with None -> (v, false) | Some v1 -> f v1 v jt

type ('ident, 'lit) stat =
| Assign of 'ident * ('ident, 'lit) expr
| If of ('ident, 'lit) expr * ('ident, 'lit) stat * ('ident, 'lit) stat
| Seq of ('ident, 'lit) stat * ('ident, 'lit) stat
| Skip
| While of ('ident, 'lit) expr * ('ident, 'lit) stat

and ('ident, 'lit) expr =
| Bang of ('ident, 'lit) expr
| Const of 'lit
| Equal of ('ident, 'lit) expr * ('ident, 'lit) expr
| Plus of ('ident, 'lit) expr * ('ident, 'lit) expr
| Var of 'ident

let rec subterm_stat_expr term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | Assign (x_t0, x_t1) -> (match b with | 0 -> assert false | 1 -> subterm_expr_expr x_t1 pp | _ -> assert false)
    | If (x_t0, x_t1, x_t2) -> (match b with | 0 -> subterm_expr_expr x_t0 pp | 1 -> subterm_stat_expr x_t1 pp | 2 -> subterm_stat_expr x_t2 pp | _ -> assert false)
    | Seq (x_t0, x_t1) -> (match b with | 0 -> subterm_stat_expr x_t0 pp | 1 -> subterm_stat_expr x_t1 pp | _ -> assert false)
    | Skip -> (match b with | _ -> assert false)
    | While (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_expr x_t0 pp | 1 -> subterm_stat_expr x_t1 pp | _ -> assert false)

and subterm_expr_expr term pp =
  match pp with
  | [] -> term
  | b :: pp -> match term with
    | Bang x_t0 -> (match b with | 0 -> subterm_expr_expr x_t0 pp | _ -> assert false)
    | Const x_t0 -> (match b with | 0 -> assert false | _ -> assert false)
    | Equal (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_expr x_t0 pp | 1 -> subterm_expr_expr x_t1 pp | _ -> assert false)
    | Plus (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_expr x_t0 pp | 1 -> subterm_expr_expr x_t1 pp | _ -> assert false)
    | Var x_t0 -> (match b with | 0 -> assert false | _ -> assert false)

let subterm_expr_expr term pp = subterm_expr_expr term (List.rev pp)
let subterm_stat_expr term pp = subterm_stat_expr term (List.rev pp)

let subterm_expr = subterm_stat_expr

let rec subterm_stat_stat term pp =
  match pp with
  | [] -> term
  | b :: pp -> match term with
    | Assign (x_t0, x_t1) -> (match b with | 0 -> assert false | 1 -> subterm_expr_stat x_t1 pp | _ -> assert false)
    | If (x_t0, x_t1, x_t2) -> (match b with | 0 -> subterm_expr_stat x_t0 pp | 1 -> subterm_stat_stat x_t1 pp | 2 -> subterm_stat_stat x_t2 pp | _ -> assert false)
    | Seq (x_t0, x_t1) -> (match b with | 0 -> subterm_stat_stat x_t0 pp | 1 -> subterm_stat_stat x_t1 pp | _ -> assert false)
    | Skip -> (match b with | _ -> assert false)
    | While (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_stat x_t0 pp | 1 -> subterm_stat_stat x_t1 pp | _ -> assert false)

and subterm_expr_stat term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | Bang x_t0 -> (match b with | 0 -> subterm_expr_stat x_t0 pp | _ -> assert false)
    | Const x_t0 -> (match b with | 0 -> assert false | _ -> assert false)
    | Equal (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_stat x_t0 pp | 1 -> subterm_expr_stat x_t1 pp | _ -> assert false)
    | Plus (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_stat x_t0 pp | 1 -> subterm_expr_stat x_t1 pp | _ -> assert false)
    | Var x_t0 -> (match b with | 0 -> assert false | _ -> assert false)

let subterm_expr_stat term pp = subterm_expr_stat term (List.rev pp)
let subterm_stat_stat term pp = subterm_stat_stat term (List.rev pp)

let subterm_stat = subterm_stat_stat

let rec subterm_stat_ident term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | Assign (x_t0, x_t1) -> (match b with | 0 -> if pp = [] then x_t0 else assert false | 1 -> subterm_expr_ident x_t1 pp | _ -> assert false)
    | If (x_t0, x_t1, x_t2) -> (match b with | 0 -> subterm_expr_ident x_t0 pp | 1 -> subterm_stat_ident x_t1 pp | 2 -> subterm_stat_ident x_t2 pp | _ -> assert false)
    | Seq (x_t0, x_t1) -> (match b with | 0 -> subterm_stat_ident x_t0 pp | 1 -> subterm_stat_ident x_t1 pp | _ -> assert false)
    | Skip -> (match b with | _ -> assert false)
    | While (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_ident x_t0 pp | 1 -> subterm_stat_ident x_t1 pp | _ -> assert false)

and subterm_expr_ident term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | Bang x_t0 -> (match b with | 0 -> subterm_expr_ident x_t0 pp | _ -> assert false)
    | Const x_t0 -> (match b with | 0 -> assert false | _ -> assert false)
    | Equal (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_ident x_t0 pp | 1 -> subterm_expr_ident x_t1 pp | _ -> assert false)
    | Plus (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_ident x_t0 pp | 1 -> subterm_expr_ident x_t1 pp | _ -> assert false)
    | Var x_t0 -> (match b with | 0 -> if pp = [] then x_t0 else assert false | _ -> assert false)

let subterm_expr_ident term pp = subterm_expr_ident term (List.rev pp)
let subterm_stat_ident term pp = subterm_stat_ident term (List.rev pp)

let subterm_ident = subterm_stat_ident

let rec subterm_stat_lit term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | Assign (x_t0, x_t1) -> (match b with | 0 -> assert false | 1 -> subterm_expr_lit x_t1 pp | _ -> assert false)
    | If (x_t0, x_t1, x_t2) -> (match b with | 0 -> subterm_expr_lit x_t0 pp | 1 -> subterm_stat_lit x_t1 pp | 2 -> subterm_stat_lit x_t2 pp | _ -> assert false)
    | Seq (x_t0, x_t1) -> (match b with | 0 -> subterm_stat_lit x_t0 pp | 1 -> subterm_stat_lit x_t1 pp | _ -> assert false)
    | Skip -> (match b with | _ -> assert false)
    | While (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_lit x_t0 pp | 1 -> subterm_stat_lit x_t1 pp | _ -> assert false)

and subterm_expr_lit term pp =
  match pp with
  | [] -> assert false
  | b :: pp -> match term with
    | Bang x_t0 -> (match b with | 0 -> subterm_expr_lit x_t0 pp | _ -> assert false)
    | Const x_t0 -> (match b with | 0 -> if pp = [] then x_t0 else assert false | _ -> assert false)
    | Equal (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_lit x_t0 pp | 1 -> subterm_expr_lit x_t1 pp | _ -> assert false)
    | Plus (x_t0, x_t1) -> (match b with | 0 -> subterm_expr_lit x_t0 pp | 1 -> subterm_expr_lit x_t1 pp | _ -> assert false)
    | Var x_t0 -> (match b with | 0 -> assert false | _ -> assert false)

let subterm_expr_lit term pp = subterm_expr_lit term (List.rev pp)
let subterm_stat_lit term pp = subterm_stat_lit term (List.rev pp)

let subterm_lit = subterm_stat_lit

let rec gent_stat f_expr f_stat f_ident f_lit pp term =
  match term with
  | Assign (x_t0, x_t1) -> f_stat pp term; f_ident (0 :: pp) x_t0; gent_expr f_expr f_stat f_ident f_lit (1 :: pp) x_t1
  | If (x_t0, x_t1, x_t2) -> f_stat pp term; gent_expr f_expr f_stat f_ident f_lit (0 :: pp) x_t0; gent_stat f_expr f_stat f_ident f_lit (1 :: pp) x_t1; gent_stat f_expr f_stat f_ident f_lit (2 :: pp) x_t2
  | Seq (x_t0, x_t1) -> f_stat pp term; gent_stat f_expr f_stat f_ident f_lit (0 :: pp) x_t0; gent_stat f_expr f_stat f_ident f_lit (1 :: pp) x_t1
  | Skip -> f_stat pp term
  | While (x_t0, x_t1) -> f_stat pp term; gent_expr f_expr f_stat f_ident f_lit (0 :: pp) x_t0; gent_stat f_expr f_stat f_ident f_lit (1 :: pp) x_t1

and gent_expr f_expr f_stat f_ident f_lit pp term =
  match term with
  | Bang x_t0 -> f_expr pp term; gent_expr f_expr f_stat f_ident f_lit (0 :: pp) x_t0
  | Const x_t0 -> f_expr pp term; f_lit (0 :: pp) x_t0
  | Equal (x_t0, x_t1) -> f_expr pp term; gent_expr f_expr f_stat f_ident f_lit (0 :: pp) x_t0; gent_expr f_expr f_stat f_ident f_lit (1 :: pp) x_t1
  | Plus (x_t0, x_t1) -> f_expr pp term; gent_expr f_expr f_stat f_ident f_lit (0 :: pp) x_t0; gent_expr f_expr f_stat f_ident f_lit (1 :: pp) x_t1
  | Var x_t0 -> f_expr pp term; f_ident (0 :: pp) x_t0

module PPMap = Map.Make(struct type t = int list let compare = compare end)
let pp_default_map = ref PPMap.empty
let empty_ppmap () = pp_default_map := PPMap.empty
let fill_default_ppmap gent t0 =
  let c = ref 0 in
  pp_default_map := PPMap.empty;
  gent (fun pp _ -> pp_default_map := PPMap.add pp (incr c; "l" ^ string_of_int !c) !pp_default_map) (fun pp _ -> pp_default_map := PPMap.add pp (incr c; "l" ^ string_of_int !c) !pp_default_map) (fun pp _ -> pp_default_map := PPMap.add pp (incr c; "l" ^ string_of_int !c) !pp_default_map) (fun pp _ -> pp_default_map := PPMap.add pp (incr c; "l" ^ string_of_int !c) !pp_default_map) [] t0
let pp_pp_ppmap ppmap ff pp = match PPMap.find pp ppmap with
| s -> Format.fprintf ff "%s" s
| exception Not_found -> Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Format.pp_print_int) pp

let rec pp_stat pp_ident pp_lit pp ppmap ff term =
  match term with
  | Assign (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(Assign (@,%a, @,%a)@;<0 -2>)^%a@]" (fun ff x -> Format.fprintf ff "%a^%a" pp_ident x (pp_pp_ppmap ppmap) (0 :: pp)) x_t0 (pp_expr pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp
  | If (x_t0, x_t1, x_t2) -> Format.fprintf ff "@[<hv 2>(If (@,%a, @,%a, @,%a)@;<0 -2>)^%a@]" (pp_expr pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_stat pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_stat pp_ident pp_lit (2 :: pp) ppmap) x_t2 (pp_pp_ppmap ppmap) pp
  | Seq (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(Seq (@,%a, @,%a)@;<0 -2>)^%a@]" (pp_stat pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_stat pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp
  | Skip -> Format.fprintf ff "@[<hv 2>(Skip@;<0 -2>)^%a@]" (pp_pp_ppmap ppmap) pp
  | While (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(While (@,%a, @,%a)@;<0 -2>)^%a@]" (pp_expr pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_stat pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp

and pp_expr pp_ident pp_lit pp ppmap ff term =
  match term with
  | Bang x_t0 -> Format.fprintf ff "@[<hv 2>(Bang @,%a@;<0 -2>)^%a@]" (pp_expr pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_pp_ppmap ppmap) pp
  | Const x_t0 -> Format.fprintf ff "@[<hv 2>(Const @,%a@;<0 -2>)^%a@]" (fun ff x -> Format.fprintf ff "%a^%a" pp_lit x (pp_pp_ppmap ppmap) (0 :: pp)) x_t0 (pp_pp_ppmap ppmap) pp
  | Equal (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(Equal (@,%a, @,%a)@;<0 -2>)^%a@]" (pp_expr pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_expr pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp
  | Plus (x_t0, x_t1) -> Format.fprintf ff "@[<hv 2>(Plus (@,%a, @,%a)@;<0 -2>)^%a@]" (pp_expr pp_ident pp_lit (0 :: pp) ppmap) x_t0 (pp_expr pp_ident pp_lit (1 :: pp) ppmap) x_t1 (pp_pp_ppmap ppmap) pp
  | Var x_t0 -> Format.fprintf ff "@[<hv 2>(Var @,%a@;<0 -2>)^%a@]" (fun ff x -> Format.fprintf ff "%a^%a" pp_ident x (pp_pp_ppmap ppmap) (0 :: pp)) x_t0 (pp_pp_ppmap ppmap) pp

module type BASETYPES = sig
  module Ident : sig
    type t
    val print : Format.formatter -> t -> unit
    val compare : t -> t -> int
  end
  
  module Vint : sig
    type t
    val print : Format.formatter -> t -> unit
    val join : t -> t -> t
    val update : t -> t -> join_type -> t * bool
    val bot : t
  end
  
  module Vbool : sig
    type t
    val print : Format.formatter -> t -> unit
    val join : t -> t -> t
    val update : t -> t -> join_type -> t * bool
    val bot : t
  end
  
  module Lit : sig
    type t
    val print : Format.formatter -> t -> unit
  end
  
  module Globalcontext : sig
    type t
    val print : Format.formatter -> t -> unit
    val compare : t -> t -> int
  end
end

module MakeTypes(B : BASETYPES) = struct
  open B
  module Localcontext = struct
    type t = unit
    let print ff (() : t) = Format.fprintf ff "()"
  end
  
  module Value = struct
    type t = Vint.t * Vbool.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Vint.print x0 Vbool.print x1
    let join ((x0, x1) : t) ((y0, y1) : t) : t = (Vint.join x0 y0, Vbool.join x1 y1)
    let update ((x0, x1) : t) ((y0, y1) : t) needs_join : (t * bool) =
      let (z0, u0) = Vint.update x0 y0 needs_join in
      let (z1, u1) = Vbool.update x1 y1 needs_join in
    ((z0, z1), u0 && u1)
    let bot : t = (Vint.bot, Vbool.bot)
  end
  
  module State = struct
    module M = Map.Make(Ident)
    type t = Value.t M.t
    let print ff (m : t) = Format.fprintf ff "@[<hv 2>{|@,%a@;<0 -2>|}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") (fun ff (k, v) -> Format.fprintf ff "%a |-> %a" Ident.print k Value.print v)) (M.bindings m)
    let fold f init (m : t) = M.fold (fun k x acc -> f (k, x) acc) m init
    let find x (m : t) = try M.find x m with Not_found -> Value.bot
    let update_elt x f (m : t option) needs_join =
      match m with
      | None -> let (y, stable) = f None needs_join in (M.singleton x y, stable)
      | Some m -> let (y, stable) = f (M.find_opt x m) needs_join in (M.add x y m, stable)
    let join (m1 : t) (m2 : t) : t = M.merge (fun _ v1 v2 -> match v1, v2 with None, None -> None | None, Some v | Some v, None -> Some v | Some v1, Some v2 -> Some (Value.join v1 v2)) m1 m2
    let bot : t = M.empty
    let update (m1 : t) (m2 : t) needs_join : (t * bool) =
      let stable = ref true in
      let m = M.merge (fun _ v1 v2 -> match v1, v2 with None, None -> None | None, Some v -> stable := false; Some v | Some v, None -> Some v | Some v1, Some v2 -> let v, st = Value.update v1 v2 needs_join in stable := !stable && st; Some v) m1 m2 in
      (m, !stable)
  end
  
  module Tuple2_Localcontext_Vbool = struct
    type t = Localcontext.t * Vbool.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Localcontext.print x0 Vbool.print x1
  end
  
  module List_Tuple2_Localcontext_Vbool = struct
    type t = Tuple2_Localcontext_Vbool.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Tuple2_Localcontext_Vbool.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module Tuple2_Localcontext_Vint = struct
    type t = Localcontext.t * Vint.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Localcontext.print x0 Vint.print x1
  end
  
  module List_Tuple2_Localcontext_Vint = struct
    type t = Tuple2_Localcontext_Vint.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Tuple2_Localcontext_Vint.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module List_Localcontext = struct
    type t = Localcontext.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Localcontext.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module Tuple2_Localcontext_State = struct
    type t = Localcontext.t * State.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Localcontext.print x0 State.print x1
  end
  
  module List_Tuple2_Localcontext_State = struct
    type t = Tuple2_Localcontext_State.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Tuple2_Localcontext_State.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
    let join (l1 : t) (l2 : t) : t = l1 @ l2
    let update (l1 : t) (l2 : t) needs_join : (t * bool) = assert false
    let bot : t = []
  end
  
  module List_List_Tuple2_Localcontext_State = struct
    type t = List_Tuple2_Localcontext_State.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") List_Tuple2_Localcontext_State.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module Tuple2_Localcontext_Value = struct
    type t = Localcontext.t * Value.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" Localcontext.print x0 Value.print x1
  end
  
  module List_Tuple2_Localcontext_Value = struct
    type t = Tuple2_Localcontext_Value.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") Tuple2_Localcontext_Value.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module List_State = struct
    type t = State.t list
    let print ff (l : t) = Format.fprintf ff "[%a]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "; ") State.print) l
    let fold f init (l : t) = List.fold_left (fun acc x -> f x acc) init l
  end
  
  module PP = struct
    type t = int list
    let print ff (p : t) = pp_pp_ppmap !pp_default_map ff p
    let compare : t -> t -> int = compare
  end
  
  module Set_PP = struct
    module M = Set.Make(PP)
    type t = M.t
    let print ff (s : t) = Format.fprintf ff "@[<hv 2>{@,%a@;<0 -2>}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") PP.print) (M.elements s)
    let fold f init (s : t) = M.fold f s init
  end
  
  module Tuple2_PP_Globalcontext = struct
    type t = PP.t * Globalcontext.t
    let print ff ((x0, x1) : t) = Format.fprintf ff "(%a, %a)" PP.print x0 Globalcontext.print x1
    let compare ((x0, x1) : t) ((y0, y1) : t) : int =
      let c = PP.compare x0 y0 in
      if c <> 0 then c else
      let c = Globalcontext.compare x1 y1 in
      if c <> 0 then c else
      c
  end
  
  module Map_Tuple2_PP_Globalcontext_Value = struct
    module M = Map.Make(Tuple2_PP_Globalcontext)
    type t = Value.t M.t
    let print ff (m : t) = Format.fprintf ff "@[<hv 2>{|@,%a@;<0 -2>|}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") (fun ff (k, v) -> Format.fprintf ff "%a |-> %a" Tuple2_PP_Globalcontext.print k Value.print v)) (M.bindings m)
    let fold f init (m : t) = M.fold (fun k x acc -> f (k, x) acc) m init
    let find x (m : t) = try M.find x m with Not_found -> Value.bot
    let update_elt x f (m : t option) needs_join =
      match m with
      | None -> let (y, stable) = f None needs_join in (M.singleton x y, stable)
      | Some m -> let (y, stable) = f (M.find_opt x m) needs_join in (M.add x y m, stable)
  end
  
  module Map_Tuple2_PP_Globalcontext_State = struct
    module M = Map.Make(Tuple2_PP_Globalcontext)
    type t = State.t M.t
    let print ff (m : t) = Format.fprintf ff "@[<hv 2>{|@,%a@;<0 -2>|}@]" (Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff ";@ ") (fun ff (k, v) -> Format.fprintf ff "%a |-> %a" Tuple2_PP_Globalcontext.print k State.print v)) (M.bindings m)
    let fold f init (m : t) = M.fold (fun k x acc -> f (k, x) acc) m init
    let find x (m : t) = try M.find x m with Not_found -> State.bot
    let update_elt x f (m : t option) needs_join =
      match m with
      | None -> let (y, stable) = f None needs_join in (M.singleton x y, stable)
      | Some m -> let (y, stable) = f (M.find_opt x m) needs_join in (M.add x y m, stable)
  end
  
  module type ABSTRACT = sig
    val add : Vint.t -> Vint.t -> Vint.t
    val bool_bot : Localcontext.t -> Vbool.t
    val bool_is_bot : Localcontext.t -> Vbool.t -> List_Tuple2_Localcontext_Vbool.t
    val eq : Vint.t -> Vint.t -> Vbool.t
    val int_bot : Localcontext.t -> Vint.t
    val int_is_bot : Localcontext.t -> Vint.t -> List_Tuple2_Localcontext_Vint.t
    val isFalse : Localcontext.t -> Vbool.t -> List_Localcontext.t
    val isTrue : Localcontext.t -> Vbool.t -> List_Localcontext.t
    val litToVal : Lit.t -> Value.t
    val neg : Vbool.t -> Vbool.t
    val write : Ident.t -> State.t -> Value.t -> State.t
  end
end

module MakeCstr(B : BASETYPES)(A : MakeTypes(B).ABSTRACT) = struct
  include B
  include MakeTypes(B)
  include A
  type location =
    | D__in_expr of Tuple2_PP_Globalcontext.t
    | D__in_stat of Tuple2_PP_Globalcontext.t
    | D__out_expr of Tuple2_PP_Globalcontext.t
    | D__out_stat of Tuple2_PP_Globalcontext.t
  
  module Location = struct
    type t = location
    let compare x y = match x, y with
      | D__in_expr a, D__in_expr b -> Tuple2_PP_Globalcontext.compare a b
      | D__in_expr _, _ -> -1
      | _, D__in_expr _ -> 1
      | D__in_stat a, D__in_stat b -> Tuple2_PP_Globalcontext.compare a b
      | D__in_stat _, _ -> -1
      | _, D__in_stat _ -> 1
      | D__out_expr a, D__out_expr b -> Tuple2_PP_Globalcontext.compare a b
      | D__out_expr _, _ -> -1
      | _, D__out_expr _ -> 1
      | D__out_stat a, D__out_stat b -> Tuple2_PP_Globalcontext.compare a b
  end
  
  module LocationSet = Set.Make(Location)
  module LocationMap = Map.Make(Location)
  
  type cstr = {
    c_run : unit -> unit ;
    c_deps_in : LocationSet.t ;
    c_deps_out : LocationSet.t ;
    c_deps_deps : LocationSet.t ;
  }
  
  module Store = struct
    type t = {
      mutable _stable : bool ;
      mutable _widen_points : LocationSet.t ;
      mutable _deps_stable : bool ;
      mutable _deps_deps : LocationSet.t ;
      mutable _in_expr : Map_Tuple2_PP_Globalcontext_State.t ;
      mutable _in_stat : Map_Tuple2_PP_Globalcontext_State.t ;
      mutable _out_expr : Map_Tuple2_PP_Globalcontext_Value.t ;
      mutable _out_stat : Map_Tuple2_PP_Globalcontext_State.t ;
    }
    
    let create_store () = {
      _stable = true ;
      _widen_points = LocationSet.empty ;
      _deps_stable = true ;
      _deps_deps = LocationSet.empty ;
      _in_expr = Map_Tuple2_PP_Globalcontext_State.M.empty ;
      _in_stat = Map_Tuple2_PP_Globalcontext_State.M.empty ;
      _out_expr = Map_Tuple2_PP_Globalcontext_Value.M.empty ;
      _out_stat = Map_Tuple2_PP_Globalcontext_State.M.empty ;
    }
    
    let print ff (s : t) = Format.fprintf ff "@[<v 2>{@,%a@,%a@,%a@,%a@]@,}" (fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff "_in_expr[%a] = %a ;" Tuple2_PP_Globalcontext.print k State.print v) ff (Map_Tuple2_PP_Globalcontext_State.M.bindings m)) s._in_expr (fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff "_in_stat[%a] = %a ;" Tuple2_PP_Globalcontext.print k State.print v) ff (Map_Tuple2_PP_Globalcontext_State.M.bindings m)) s._in_stat (fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff "_out_expr[%a] = %a ;" Tuple2_PP_Globalcontext.print k Value.print v) ff (Map_Tuple2_PP_Globalcontext_Value.M.bindings m)) s._out_expr (fun ff m -> Format.pp_print_list (fun ff (k, v) -> Format.fprintf ff "_out_stat[%a] = %a ;" Tuple2_PP_Globalcontext.print k State.print v) ff (Map_Tuple2_PP_Globalcontext_State.M.bindings m)) s._out_stat
    
    let _in_expr_get store elt = Map_Tuple2_PP_Globalcontext_State.find elt store._in_expr
    let _in_expr_set store elt f =
      if not (Map_Tuple2_PP_Globalcontext_State.M.mem elt store._in_expr) then store._deps_stable <- false;
      let (x, stable) = Map_Tuple2_PP_Globalcontext_State.update_elt elt f (Some store._in_expr) (if LocationSet.mem (D__in_expr elt) store._widen_points then JTWiden else JTJoin) in
      store._in_expr <- x;
      store._stable <- store._stable && stable;
      if LocationSet.mem (D__in_expr elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable
    let _in_stat_get store elt = Map_Tuple2_PP_Globalcontext_State.find elt store._in_stat
    let _in_stat_set store elt f =
      if not (Map_Tuple2_PP_Globalcontext_State.M.mem elt store._in_stat) then store._deps_stable <- false;
      let (x, stable) = Map_Tuple2_PP_Globalcontext_State.update_elt elt f (Some store._in_stat) (if LocationSet.mem (D__in_stat elt) store._widen_points then JTWiden else JTJoin) in
      store._in_stat <- x;
      store._stable <- store._stable && stable;
      if LocationSet.mem (D__in_stat elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable
    let _out_expr_get store elt = Map_Tuple2_PP_Globalcontext_Value.find elt store._out_expr
    let _out_expr_set store elt f =
      let (x, stable) = Map_Tuple2_PP_Globalcontext_Value.update_elt elt f (Some store._out_expr) (if LocationSet.mem (D__out_expr elt) store._widen_points then JTWiden else JTJoin) in
      store._out_expr <- x;
      store._stable <- store._stable && stable;
      if LocationSet.mem (D__out_expr elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable
    let _out_stat_get store elt = Map_Tuple2_PP_Globalcontext_State.find elt store._out_stat
    let _out_stat_set store elt f =
      let (x, stable) = Map_Tuple2_PP_Globalcontext_State.update_elt elt f (Some store._out_stat) (if LocationSet.mem (D__out_stat elt) store._widen_points then JTWiden else JTJoin) in
      store._out_stat <- x;
      store._stable <- store._stable && stable;
      if LocationSet.mem (D__out_stat elt) store._deps_deps then store._deps_stable <- store._deps_stable && stable
  end
  
  let constraints_expr_Bang _store _t0 _pp _sigmas =
    let x_t = Set_PP.M.singleton (0 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Tuple2_Localcontext_Vbool.fold (fun (_sigma, x_f1') -> Value.join ((int_bot ()), (neg x_f1'))) Value.bot (let (i, b) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t) in (bool_is_bot () b))), (fun _f -> Store._out_expr_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t)) ;
        c_deps_out = ((LocationSet.of_list [D__out_expr ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_expr_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t)) ;
        c_deps_in = ((LocationSet.of_list [D__in_expr ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_expr_Const _store _t0 _pp _sigmas =
    let x_t = Set_PP.M.singleton (0 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (Set_PP.fold (fun (pp1) -> Value.join (litToVal (subterm_lit _t0 pp1))) Value.bot x_t), (fun _f -> Store._out_expr_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.of_list [])) ;
        c_deps_out = ((LocationSet.of_list [D__out_expr ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_expr_Equal _store _t0 _pp _sigmas =
    let x_t1 = Set_PP.M.singleton (0 :: _pp) in
    let x_t2 = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Tuple2_Localcontext_Vint.fold (fun (_sigma, x_f1') -> Value.join (List_Tuple2_Localcontext_Vint.fold (fun (___sigma_88, x_f2') -> Value.join ((int_bot ()), (eq x_f1' x_f2'))) Value.bot (let (i, b) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t2) in (int_is_bot _sigma i)))) Value.bot (let (i, b) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1) in (int_is_bot () i))), (fun _f -> Store._out_expr_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t2))) ;
        c_deps_out = ((LocationSet.of_list [D__out_expr ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_expr_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t1)) ;
        c_deps_in = ((LocationSet.of_list [D__in_expr ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, x_f1) () -> (List_Tuple2_Localcontext_Vint.fold (fun (_sigma, x_f1') () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_expr_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t2)) () (let (i, b) = x_f1 in (int_is_bot _sigma i)))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (LocationSet.of_list [D__in_expr ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_expr_Plus _store _t0 _pp _sigmas =
    let x_t1 = Set_PP.M.singleton (0 :: _pp) in
    let x_t2 = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Tuple2_Localcontext_Vint.fold (fun (_sigma, x_f1') -> Value.join (List_Tuple2_Localcontext_Vint.fold (fun (___sigma_137, x_f2') -> Value.join ((add x_f1' x_f2'), (bool_bot ()))) Value.bot (let (i, b) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t2) in (int_is_bot _sigma i)))) Value.bot (let (i, b) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1) in (int_is_bot () i))), (fun _f -> Store._out_expr_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t2))) ;
        c_deps_out = ((LocationSet.of_list [D__out_expr ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_expr_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t1)) ;
        c_deps_in = ((LocationSet.of_list [D__in_expr ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, x_f1) () -> (List_Tuple2_Localcontext_Vint.fold (fun (_sigma, x_f1') () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_expr_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t2)) () (let (i, b) = x_f1 in (int_is_bot _sigma i)))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (LocationSet.of_list [D__in_expr ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_expr_Var _store _t0 _pp _sigmas =
    let x_t = Set_PP.M.singleton (0 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (Set_PP.fold (fun (pp1) -> Value.join (State.find (subterm_ident _t0 pp1) (Store._in_expr_get _store (_pp, _sigmas)))) Value.bot x_t), (fun _f -> Store._out_expr_set _store (_pp, _sigmas) _f) in _lv (make_update Value.update _v))) ;
        c_deps_in = ((LocationSet.of_list [D__in_expr ((_pp, _sigmas))])) ;
        c_deps_out = ((LocationSet.of_list [D__out_expr ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_stat_Assign _store _t0 _pp _sigmas =
    let x_t1 = Set_PP.M.singleton (0 :: _pp) in
    let x_t2 = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (Set_PP.fold (fun (pp1) -> State.join (write (subterm_ident _t0 pp1) (Store._in_stat_get _store (_pp, _sigmas)) (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t2))) State.bot x_t1), (fun _f -> Store._out_stat_set _store (_pp, _sigmas) _f) in _lv (make_update State.update _v))) ;
        c_deps_in = ((LocationSet.union (LocationSet.of_list [D__in_stat ((_pp, _sigmas))]) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t2))) ;
        c_deps_out = ((LocationSet.of_list [D__out_stat ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t2)) ;
        c_deps_in = ((LocationSet.of_list [D__in_stat ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_stat_If _store _t0 _pp _sigmas =
    let x_t1 = Set_PP.M.singleton (0 :: _pp) in
    let x_t2 = Set_PP.M.singleton (1 :: _pp) in
    let x_t3 = Set_PP.M.singleton (2 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Tuple2_Localcontext_Vbool.fold (fun (_sigma, x_f1') -> State.join (List_Tuple2_Localcontext_State.fold (fun (_sigma, x_o) -> State.join x_o) State.bot (List_List_Tuple2_Localcontext_State.fold (fun (_l) -> List_Tuple2_Localcontext_State.join _l) List_Tuple2_Localcontext_State.bot [(List_Localcontext.fold (fun (_sigma) -> List_Tuple2_Localcontext_State.join [(_sigma, (Set_PP.fold (fun (_pp1) -> State.join (Store._out_stat_get _store (_pp1, _sigmas))) State.bot x_t2))]) List_Tuple2_Localcontext_State.bot (isTrue _sigma x_f1')); (List_Localcontext.fold (fun (_sigma) -> List_Tuple2_Localcontext_State.join [(_sigma, (Set_PP.fold (fun (_pp1) -> State.join (Store._out_stat_get _store (_pp1, _sigmas))) State.bot x_t3))]) List_Tuple2_Localcontext_State.bot (isFalse _sigma x_f1'))]))) State.bot (let (i, b) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1) in (bool_is_bot () b))), (fun _f -> Store._out_stat_set _store (_pp, _sigmas) _f) in _lv (make_update State.update _v))) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_stat ((_pp1, _sigmas))])) LocationSet.empty x_t2) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_stat ((_pp1, _sigmas))])) LocationSet.empty x_t3)))) ;
        c_deps_out = ((LocationSet.of_list [D__out_stat ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t1)) ;
        c_deps_in = ((LocationSet.of_list [D__in_stat ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, x_f1) () -> (List_Tuple2_Localcontext_Vbool.fold (fun (_sigma, x_f1') () -> (List_Localcontext.fold (fun (_sigma) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._in_stat_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t2)) () (isTrue _sigma x_f1'))) () (let (i, b) = x_f1 in (bool_is_bot _sigma b)))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (LocationSet.of_list [D__in_stat ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_stat ((_pp1, _sigmas))])) LocationSet.empty x_t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, x_f1) () -> (List_Tuple2_Localcontext_Vbool.fold (fun (_sigma, x_f1') () -> (List_Localcontext.fold (fun (_sigma) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._in_stat_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t3)) () (isFalse _sigma x_f1'))) () (let (i, b) = x_f1 in (bool_is_bot _sigma b)))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (LocationSet.of_list [D__in_stat ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_stat ((_pp1, _sigmas))])) LocationSet.empty x_t3)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_stat_Seq _store _t0 _pp _sigmas =
    let x_t1 = Set_PP.M.singleton (0 :: _pp) in
    let x_t2 = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (Set_PP.fold (fun (_pp1) -> State.join (Store._out_stat_get _store (_pp1, _sigmas))) State.bot x_t2), (fun _f -> Store._out_stat_set _store (_pp, _sigmas) _f) in _lv (make_update State.update _v))) ;
        c_deps_in = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_stat ((_pp1, _sigmas))])) LocationSet.empty x_t2)) ;
        c_deps_out = ((LocationSet.of_list [D__out_stat ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._in_stat_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t1)) ;
        c_deps_in = ((LocationSet.of_list [D__in_stat ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_stat ((_pp1, _sigmas))])) LocationSet.empty x_t1)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_State.fold (fun (x_f1) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = x_f1, (fun _f -> Store._in_stat_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t2)) () [(Set_PP.fold (fun (_pp1) -> State.join (Store._out_stat_get _store (_pp1, _sigmas))) State.bot x_t1)])) ;
        c_deps_in = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_stat ((_pp1, _sigmas))])) LocationSet.empty x_t1)) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_stat ((_pp1, _sigmas))])) LocationSet.empty x_t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_stat_Skip _store _t0 _pp _sigmas =
    [
      {
        c_run = (fun () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._out_stat_set _store (_pp, _sigmas) _f) in _lv (make_update State.update _v))) ;
        c_deps_in = ((LocationSet.of_list [D__in_stat ((_pp, _sigmas))])) ;
        c_deps_out = ((LocationSet.of_list [D__out_stat ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_stat_While _store _t0 _pp _sigmas =
    let x_t1 = Set_PP.M.singleton (0 :: _pp) in
    let x_t2 = Set_PP.M.singleton (1 :: _pp) in
    [
      {
        c_run = (fun () -> (let _v, _lv = (List_Tuple2_Localcontext_Vbool.fold (fun (_sigma, x_f1') -> State.join (List_Tuple2_Localcontext_State.fold (fun (_sigma, x_o) -> State.join x_o) State.bot (List_List_Tuple2_Localcontext_State.fold (fun (_l) -> List_Tuple2_Localcontext_State.join _l) List_Tuple2_Localcontext_State.bot [(List_Localcontext.fold (fun (_sigma) -> List_Tuple2_Localcontext_State.join [(_sigma, (Set_PP.fold (fun (_pp1) -> State.join (Store._out_stat_get _store (_pp1, _sigmas))) State.bot (Set_PP.M.of_list [_pp])))]) List_Tuple2_Localcontext_State.bot (isTrue _sigma x_f1')); (List_Localcontext.fold (fun (_sigma) -> List_Tuple2_Localcontext_State.join [(_sigma, (Store._in_stat_get _store (_pp, _sigmas)))]) List_Tuple2_Localcontext_State.bot (isFalse _sigma x_f1'))]))) State.bot (let (i, b) = (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1) in (bool_is_bot () b))), (fun _f -> Store._out_stat_set _store (_pp, _sigmas) _f) in _lv (make_update State.update _v))) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (LocationSet.of_list [D__out_stat ((_pp, _sigmas)); D__in_stat ((_pp, _sigmas))]))) ;
        c_deps_out = ((LocationSet.of_list [D__out_stat ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._in_expr_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t1)) ;
        c_deps_in = ((LocationSet.of_list [D__in_stat ((_pp, _sigmas))])) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, x_f1) () -> (List_Tuple2_Localcontext_Vbool.fold (fun (_sigma, x_f1') () -> (List_Localcontext.fold (fun (_sigma) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = (Store._in_stat_get _store (_pp, _sigmas)), (fun _f -> Store._in_stat_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () x_t2)) () (isTrue _sigma x_f1'))) () (let (i, b) = x_f1 in (bool_is_bot _sigma b)))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (LocationSet.of_list [D__in_stat ((_pp, _sigmas))]))) ;
        c_deps_out = ((Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__in_stat ((_pp1, _sigmas))])) LocationSet.empty x_t2)) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      };
      {
        c_run = (fun () -> (List_Tuple2_Localcontext_Value.fold (fun (_sigma, x_f1) () -> (List_Tuple2_Localcontext_Vbool.fold (fun (_sigma, x_f1') () -> (List_Localcontext.fold (fun (_sigma) () -> (List_Tuple2_Localcontext_State.fold (fun (_sigma, x_f2) () -> (Set_PP.fold (fun (_pp1) () -> (let _v, _lv = x_f2, (fun _f -> Store._in_stat_set _store (_pp1, _sigmas) _f) in _lv (make_update State.update _v))) () (Set_PP.M.of_list [_pp]))) () [(_sigma, (Set_PP.fold (fun (_pp1) -> State.join (Store._out_stat_get _store (_pp1, _sigmas))) State.bot x_t2))])) () (isTrue _sigma x_f1'))) () (let (i, b) = x_f1 in (bool_is_bot _sigma b)))) () [((), (Set_PP.fold (fun (_pp1) -> Value.join (Store._out_expr_get _store (_pp1, _sigmas))) Value.bot x_t1))])) ;
        c_deps_in = ((LocationSet.union (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_expr ((_pp1, _sigmas))])) LocationSet.empty x_t1) (Set_PP.fold (fun (_pp1) -> LocationSet.union (LocationSet.of_list [D__out_stat ((_pp1, _sigmas))])) LocationSet.empty x_t2))) ;
        c_deps_out = ((LocationSet.of_list [D__in_stat ((_pp, _sigmas))])) ;
        c_deps_deps = ((LocationSet.of_list [])) ;
      }
    ]
  
  let constraints_expr store t0 =
    let f pp sigmas = match subterm_expr t0 pp with
      | Bang x_t -> constraints_expr_Bang store t0 pp sigmas
      | Const x_t -> constraints_expr_Const store t0 pp sigmas
      | Equal (x_t1, x_t2) -> constraints_expr_Equal store t0 pp sigmas
      | Plus (x_t1, x_t2) -> constraints_expr_Plus store t0 pp sigmas
      | Var x_t -> constraints_expr_Var store t0 pp sigmas
    in
    Map_Tuple2_PP_Globalcontext_State.fold (fun ((pp, sigmas), _) acc -> f pp sigmas @ acc) [] (store._in_expr)
  
  let constraints_stat store t0 =
    let f pp sigmas = match subterm_stat t0 pp with
      | Assign (x_t1, x_t2) -> constraints_stat_Assign store t0 pp sigmas
      | If (x_t1, x_t2, x_t3) -> constraints_stat_If store t0 pp sigmas
      | Seq (x_t1, x_t2) -> constraints_stat_Seq store t0 pp sigmas
      | Skip -> constraints_stat_Skip store t0 pp sigmas
      | While (x_t1, x_t2) -> constraints_stat_While store t0 pp sigmas
    in
    Map_Tuple2_PP_Globalcontext_State.fold (fun ((pp, sigmas), _) acc -> f pp sigmas @ acc) [] (store._in_stat)
  
  let get_constraints store t0 = constraints_expr store t0 @ constraints_stat store t0
  
  let sort_constraints cstrs =
    let cstrs = Array.of_list cstrs in
    let init_vars = ref LocationMap.empty in
    Array.iteri (fun i c ->
      LocationSet.iter (fun v ->
        init_vars := LocationMap.add v (i :: (try LocationMap.find v !init_vars with Not_found -> [])) !init_vars
      ) c.c_deps_in
    ) cstrs;
    let seen = Array.make (Array.length cstrs) 0 in
    let wid = ref LocationSet.empty in
    let sorted = ref [] in
    let rec dfs v i =
      match seen.(i) with
      | 0 ->
        seen.(i) <- 1;
        let c = cstrs.(i) in
        LocationSet.iter (fun v ->
          let nxt = try LocationMap.find v !init_vars with Not_found -> [] in
          List.iter (dfs (Some v)) nxt
        ) c.c_deps_out;
        seen.(i) <- 2;
        sorted := c :: !sorted
      | 1 -> (match v with None -> assert false | Some v -> wid := LocationSet.add v !wid)
      | _ -> ()
    in
    for i = 0 to Array.length cstrs - 1 do
      dfs None i
    done;
    (!sorted, !wid)
  
  let run_constraints store t0 =
    let open Store in
    while not store._stable do
      store._stable <- true;
      store._deps_stable <- true;
      store._widen_points <- LocationSet.empty;
      let cstr, widen = sort_constraints (get_constraints store t0) in
      store._deps_deps <- List.fold_left LocationSet.union LocationSet.empty (List.map (fun c -> c.c_deps_deps) cstr);
      List.iter (fun c -> c.c_run ()) cstr;
      store._widen_points <- widen;
      while store._deps_stable && not store._stable do
        store._stable <- true;
        List.iter (fun c -> c.c_run ()) cstr
      done
    done
end
