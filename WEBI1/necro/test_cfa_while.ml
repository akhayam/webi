open While_cfa
open Domains

module BaseTypes = struct
  module Ident = struct
    type t = string
    let print ff s = Format.fprintf ff "%s" s
    let compare = compare
  end

  module Globalcontext = struct
    type t = ()
    let print ff () = Format.fprintf ff "()"
    let compare = compare
  end

  module Lit = struct
    type t = int
    let print ff x = Format.fprintf ff "%d" x
  end

  module Vbool = struct
    type t = bool * bool
    let print ff (cbt, cbf) = match cbt, cbf with
      | false, false -> Format.fprintf ff "bot_B"
      | true, false -> Format.fprintf ff "true#"
      | false, true -> Format.fprintf ff "false#"
      | true, true -> Format.fprintf ff "top_B"
    let join (x1, y1) (x2, y2) = (x1 || x2, y1 || y2)
    let update (x1, y1) (x2, y2) jt = ((x1 || x2, y1 || y2), (x1 || not y1) && (x2 || not y2))
    let bot = (false, false)
  end

  module Vint = struct
    type t = Interval.t option
    let print = Interval.pp_print
    let bot = None
    let join v1 v2 = match v1, v2 with
      | None, None -> None
      | None, Some v | Some v, None -> Some v
      | Some v1, Some v2 -> Some (Interval.lub v1 v2)
    let update v1 v2 needs_join =
      match needs_join with
      | JTJoin -> let r = join v1 v2 in (r, r = v1)
      | JTWiden ->
        match v1, v2 with
        | None, None -> (None, true)
        | None, Some v -> (Some v, false)
        | Some v, None -> (Some v, true)
        | Some v1, Some v2 ->
          let r = Interval.widen v1 v2 in
          (Some r, r = v1)
  end
end

let k = 2
let rec lastk k l =
  match k, l with
  | 0, _ -> []
  | _, [] -> []
  | k, x :: l -> x :: lastk (k - 1) l

module Abstract : MakeTypes(BaseTypes).ABSTRACT = struct
  module X = MakeTypes(BaseTypes)
  open X

  let litToVal x = (Some (Interval.Bint x, Interval.Bint x), BaseTypes.Vbool.bot)

  let int_bot () = BaseTypes.Vint.bot
  let bool_bot () = BaseTypes.Vbool.bot
  let int_is_bot sigma x = match x with None -> [] | _ -> [(sigma, x)]
  let bool_is_bot sigma x = if x = BaseTypes.Vbool.bot then [] else [(sigma, x)]
  let write x s v = State.M.add x v s
  let add v1 v2 = match v1, v2 with None, _ | _, None -> None | Some v1, Some v2 -> Some (Interval.add v1 v2)
  let eq x1 x2 = match x1, x2 with Some x1, Some x2 -> Interval.eq x1 x2 | _ -> BaseTypes.Vbool.bot
  let neg (u, v) = (v, u)
  let isTrue sigma (u, v) = if u then [sigma] else []
  let isFalse sigma (u, v) = if v then [sigma] else []
end

module Cstr = MakeCstr(BaseTypes)(Abstract)

let make_store () =
  let init_env = Cstr.State.M.empty in
  let store = Cstr.Store.create_store () in
  let () = Cstr.Store._in_stat_set store ([], ()) (make_update Cstr.State.update init_env) in
  store

let () = Format.set_margin 120

let test t0 =
  fill_default_ppmap gent_stat t0;
  let store = make_store () in
  let () = Cstr.run_constraints store t0 in
  Format.printf "%a@." (pp_stat Format.pp_print_string Format.pp_print_int [] !pp_default_map) t0;
  Format.printf "%a@." Cstr.Store.print store

let t0 =
  Seq (Assign ("x", Const 10),
       While (Bang (Equal (Var "x", Const 0)),
              Assign ("x", Plus (Var "x", Const (-1)))))

let t1 = If (Equal (Const 0, Const 0), (Assign ("x", Const 42)), (Assign ("x", Const 17)))

let () = test t0
let () = test t1
