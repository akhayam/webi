

Set Warnings "-notation-overridden,-parsing".


Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Arith.
Require Import Coq.Arith.EqNat.
Require Import Coq.omega.Omega.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import utils.Maps.

(** First of all we have to define the syntax of the language.
    We choose to include arithmetic and boolean expressions
    
      While 1 syntax is defined as:

        e ::= v | x | op(e1, ..., en) where :
          
          1) v stand for literals
          2) x stands for variables
          3) op(e1, ..., en) stand for operators between expressions. For the sake of simplicity we will
             describe only basilar operators. binary ones for arithmetic expressions and unary/binary ones
             for boolean expressions.

        P ::= x:= e | P;P | if e then P else P fi | while e do P | skip | return e
    
    This simple language is the one that will be evaluated from the client side.
    As whis language is merely a basic WHILE language, this time, the choice is to represent the semantics with
    a big step approach. In While2 we will see the differences between these two languages. 
 *) 



(** These two definitions specify the abstract syntax of
    arithmetic and boolean expressions in Natural Semantics. *)


Definition state := total_map nat. (** import Maps **)

Module NSexpressions.

(** Defining a few variable names as notational shorthands will make
    examples easier to read: *)

Definition W : string := "W".
Definition X : string := "X".
Definition Y : string := "Y".
Definition Z : string := "Z".


Inductive aexp : Type :=
  | ANum : nat -> aexp
  | AId : string -> aexp
  | APlus : aexp -> aexp -> aexp
  | AMinus : aexp -> aexp -> aexp
  | AMult : aexp -> aexp -> aexp.

Inductive bexp : Type :=
  | BTrue : bexp
  | BFalse : bexp
  | BEq : aexp -> aexp -> bexp
  | BLe : aexp -> aexp -> bexp
  | BNot : bexp -> bexp
  | BAnd : bexp -> bexp -> bexp.

(** Evaluation of concrete expressions

Sometimes is quite a good thing in proofs, to have the possibility to move from expression world to propositional one.
With a Relational definition, we can abstract expressions to propositions. it's a way around to make things proovable
in a more easy way.

*)

(** Functional evaluator *)

Coercion AId : string >-> aexp.
Coercion ANum : nat >-> aexp.
Definition bool_to_bexp (b: bool) : bexp :=
  if b then BTrue else BFalse.
Coercion bool_to_bexp : bool >-> bexp.



(** We specialize our notation for total maps to the specific case of states,
    i.e. using [{ !-> 0 }] as empty state. *)

Notation "{ a !-> x }" :=
  (t_update { !-> 0 } a x) (at level 0).
Notation "{ a !-> x ; b !-> y }" :=
  (t_update ({ a !-> x }) b y) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z }" :=
  (t_update ({ a !-> x ; b !-> y }) c z) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z ; d !-> t }" :=
    (t_update ({ a !-> x ; b !-> y ; c !-> z }) d t) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }" :=
  (t_update ({ a !-> x ; b !-> y ; c !-> z ; d !-> t }) e u) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u ; f !-> v }" :=
  (t_update ({ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }) f v) (at level 0).


(** Arithmetic expressions *)

Fixpoint aeval (st : state) (a : aexp) : nat :=
  match a with
  | ANum n => n
  | AId x => st x
  | APlus a1 a2 => (aeval st a1) + (aeval st a2)
  | AMinus a1 a2  => (aeval st a1) - (aeval st a2)
  | AMult a1 a2 => (aeval st a1) * (aeval st a2)
  end.

Example test_aeval1:
  aeval { !-> 0} (APlus (ANum 2) (ANum 2)) = 4.
Proof. simpl. reflexivity. Qed.

Example test_aeval2:
  aeval { !-> 0} (AMult (ANum 2) (ANum 3)) = 6.
Proof. simpl. reflexivity. Qed.

Example test_aeval3:
  aeval { X !-> 2 } (AMult (AId X) (ANum 3)) = 6.
Proof. simpl. reflexivity. Qed.


(** Boolean expressions *)


Fixpoint beval (st : state) (b : bexp) : bool :=
  match b with
  | BTrue       => true
  | BFalse      => false
  | BEq a1 a2   => beq_nat (aeval st a1) (aeval st a2)
  | BLe a1 a2   => leb (aeval st a1) (aeval st a2)
  | BNot b1     => negb (beval st b1)
  | BAnd b1 b2  => andb (beval st b1) (beval st b2)
  end.

Example test_beval1:
  beval { !-> 0} (BNot BTrue) = beval { !-> 0}  BFalse.
Proof. simpl. reflexivity. Qed.

Example test_beval2:
  beval { !-> 0} (BAnd (BNot (BAnd BTrue BFalse)) BTrue) = beval { !-> 0}  BTrue.
Proof. simpl. reflexivity. Qed.

Example test_beval3:
  beval { X !-> 1 ; Y !-> 2}  (BAnd (BNot (BEq (AId X) (AId Y))) BTrue) = beval { !-> 0} BTrue.
Proof. simpl. reflexivity. Qed.

(** Relational evaluator *)

Bind Scope aexp_scope with aexp.
Infix "+" := APlus : aexp_scope.
Infix "-" := AMinus : aexp_scope.
Infix "*" := AMult : aexp_scope.
Bind Scope bexp_scope with bexp.
Infix "<=" := BLe : bexp_scope.
Infix "=" := BEq : bexp_scope.
Infix "&&" := BAnd : bexp_scope.
Notation "'!' b" := (BNot b) (at level 60) : bexp_scope.


(** Arithmetic expressions *)


(** 
                             -----------------                               (E_ANum)
                             st [ ANum n ]a n


                             -------------------                             (E_AId)
                             st [ AId n ]a st x


                              st e1 ]a n1
                              st e2 ]a n2
                         --------------------------                          (E_APlus)
                         st [ APlus e1 e2 ]a n1+n2

                              st e1 ]a n1
                              st e2 ]a n2
                        ---------------------------                          (E_AMinus)
                        st [ AMinus e1 e2 ]a n1-n2

                              st e1 ]a n1
                              st e2 ]a n2
                        ------------------------                             (E_AMult)
                         AMult e1 e2 ]a n1*n2



*)


Reserved Notation "st '[' e ']a' n" (at level 90, left associativity).

Inductive aevalR : state -> aexp -> nat -> Prop :=
  | E_ANum : forall (st : state) (n : nat),
      st [ (ANum n) ]a n
  | E_AId : forall (st : state) (s : string) (n : nat),
      st [ AId s ]a st s
  | E_APlus : forall (e1 e2 : aexp) (n1 n2 : nat) (st : state),
      (st [ e1 ]a n1) -> (st [ e2 ]a n2) -> st [ APlus e1 e2 ]a (n1 + n2)
  | E_AMinus : forall (e1 e2: aexp) (n1 n2 : nat) (st : state),
      (st [ e1 ]a n1) -> (st [ e2 ]a n2) -> st [ AMinus e1 e2 ]a (n1 - n2)
  | E_AMult :  forall (e1 e2: aexp) (n1 n2 : nat) (st : state),
      (st [ e1 ]a n1) -> (st [ e2 ]a n2) -> st [ AMult e1 e2 ]a (n1 * n2)
  where "st '[' e ']a' n" := (aevalR st e n) : type_scope.

(*
Coercion E_AId : string >-> aexp.
Coercion E_ANum : nat >-> aexp.
*)

(** Prooving equality between the two evaluators *)

Theorem aeval_iff_aevalR : forall st a n,
  (st [a]a n) <-> aeval st a = n.
Proof.
 split.
 - (* -> *)
   intros H.
   induction H; simpl.
   + (* E_ANum *)
     reflexivity.
   + (* E_AId *)
     reflexivity.
   + (* E_APlus *)
     rewrite IHaevalR1.  rewrite IHaevalR2.  reflexivity.
   + (* E_AMinus *)
     rewrite IHaevalR1.  rewrite IHaevalR2.  reflexivity.
   + (* E_AMult *)
     rewrite IHaevalR1.  rewrite IHaevalR2.  reflexivity.
 - (* <- *)
    generalize dependent n.
    induction a;
    simpl; intros.
   + (* ANum *)
     subst.
     apply E_ANum.
   + (* AId *)
      inversion H.
      apply E_AId.
      assumption.
   + (* APlus *)
      subst.
      apply E_APlus.
      apply IHa1. reflexivity.
      apply IHa2. reflexivity.
   + (* AMinus *)
      subst.
      apply E_AMinus.
      apply IHa1. reflexivity.
      apply IHa2. reflexivity.
   + (* AMult *)
      subst.
      apply E_AMult.
      apply IHa1. reflexivity.
      apply IHa2. reflexivity.
Qed.

(** Boolean expressions *)

(** 
                             --------------------                              (E_BTrue)
                              st [ BTrue ]a true

                             ---------------------                             (E_BFalse)
                              st [BFalse ]a false


                                st [ e1 ]a n1
                                st [ e2 ]a n2
                         --------------------------                            (E_BEq)
                         BEq [ e1 e2 ]b beq_nat n1 n2

                                    ...

 
*)


Reserved Notation "st '[' e ']b' b" (at level 90, left associativity).

Inductive bevalR: state -> bexp -> bool -> Prop :=
  | E_BTrue : forall (st : state), bevalR st BTrue true
  | E_BFalse : forall (st : state), bevalR st BFalse false
  | B_BEq : forall (st : state) (e1 e2 : aexp) (n1 n2 : nat),
             st [ e1 ]a n1 -> st [ e2 ]a n2 -> st [ (BEq e1 e2) ]b (beq_nat n1 n2)
  | B_LEq : forall (st : state) (e1 e2 : aexp) (n1 n2 : nat),
             st [ e1 ]a n1 -> st [ e2 ]a n2 -> st [ (BLe e1 e2) ]b (leb n1 n2)
  | B_BNot : forall (st : state) (e : bexp) (b : bool),
              st [ e ]b b -> st [ (BNot e) ]b (negb b)
  | B_BAnd : forall (st : state) (e1 e2 : bexp) (b1 b2 : bool),
              st [ e1 ]b b1 -> st [ e2 ]b b2 -> st [ (BAnd e1 e2) ]b (andb b1 b2)
  where "st '[' e ']b' b" := (bevalR st e b) : type_scope.

(** The same thing we have done before with aexps *)

Lemma beval_iff_bevalR : forall st b bv,
 st [ b ]b bv <-> beval st b = bv.
Proof.
  split.
  intros. induction H. 
  (** => *)
    (** BTrue *)
  - reflexivity.
    (** BFalse *)
  - reflexivity.
    (** B_BEq *)
  - rewrite aeval_iff_aevalR in H. rewrite aeval_iff_aevalR in H0. rewrite <- H. rewrite <- H0.
    simpl. reflexivity.
    (** B_LEq *)
  - rewrite aeval_iff_aevalR in H. rewrite aeval_iff_aevalR in H0. rewrite <- H. rewrite <- H0.
    simpl. reflexivity.
    (** B_BNot *)
  - simpl. rewrite IHbevalR. reflexivity.
    (** B_BAnd *)
  - simpl. rewrite IHbevalR1; rewrite IHbevalR2. reflexivity.
    (** <= reversal is more or less the same. it's written in a more compact way*)
  - generalize dependent bv. induction b;simpl;intros bv H; subst; constructor;
    try (rewrite aeval_iff_aevalR); try (apply IHb); try (apply IHb1); try (apply IHb2);
    try reflexivity.
Qed.



End NSexpressions.



Module SOSexpressions.









End SOSexpressions.



























