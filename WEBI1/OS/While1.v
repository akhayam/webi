

Set Warnings "-notation-overridden,-parsing".


Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Arith.
Require Import Coq.Arith.EqNat.
Require Import Coq.omega.Omega.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import utils.Maps.
Require Import Expressions1.

Import NSexpressions.

(** First of all we have to define the syntax of the language.
    We choose to include arithmetic and boolean expressions
    
      While1 syntax is defined as:

        e ::= v | x | op(e1, ..., en) where :
          
          1) v stand for literals
          2) x stands for variables
          3) op(e1, ..., en) stand for operators between expressions. For the sake of simplicity we will
             describe only basilar operators. binary ones for arithmetic expressions and unary/binary ones
             for boolean expressions.

        P ::= x:= e | P;P | if e then P else P fi | while e do P | skip | return e
    
    This simple language is the one that will be evaluated from the client side.
    As whis language is merely a basic WHILE language, this time, the choice is to represent the semantics with
    a big step approach. In While2 we will see the differences between these two languages. 
 *) 



(** In Expressions1.v file, aexps, bexps and their evaluations are defined. *)

Module NSWhile.

(** Commands **)

(** In this module, there is the definition of the syntax and behavior of While1
    Statements. *)

















End NSWhile.

Module SOSWhile1.



End SOSWhile1.


































