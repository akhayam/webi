

Set Warnings "-notation-overridden,-parsing".


Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Arith.
Require Import Coq.Arith.EqNat.
Require Import Coq.omega.Omega.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import Coq.Sets.Ensembles.
Require Import utils.Maps.


(** First of all we have to define the syntax of the language.
    We choose to include arithmetic and boolean expressions
    
      While 1 syntax is defined as:

        e ::= v | x | op(e1, ..., en) where :
          
          1) v stand for literals
          2) x stands for variables
          3) op(e1, ..., en) stand for operators between expressions. For the sake of simplicity we will
             describe only basilar operators. binary ones for arithmetic expressions and unary/binary ones
             for boolean expressions.

        P ::= x:= e | P;P | if e then P else P fi | while e do P | skip | return e
    
    This simple language is the one that will be evaluated from the client side.
    As whis language is merely a basic WHILE language, this time, the choice is to represent the semantics with
    a big step approach. In While2 we will see the differences between these two languages. 
 *) 



(** Polymorphic option value *)

Inductive option (A:Type) : Type :=
  | Some : A -> option A
  | None : option A.

Arguments Some {A} _.
Arguments None {A}.

(** Defining a few variable names as notational shorthands will make
    examples easier to read: *)

Definition W : string := "W".
Definition X : string := "X".
Definition Y : string := "Y".
Definition Z : string := "Z".

(** Defining Syntax *)
(* ################################################################################### *)
Inductive value : Type :=
  | num : nat -> value
  | tru : bool -> value
  | fls : bool -> value
  | id : string -> value
  | NULL : value
.

Inductive vexp : Type :=
  | val : value -> vexp
  | aplus : vexp -> vexp -> vexp
  | aminus : vexp -> vexp -> vexp
  | amult : vexp -> vexp -> vexp
  | bneg : vexp -> vexp
  | band : vexp -> vexp -> vexp
  | bleq : vexp -> vexp -> vexp
  | beq : vexp -> vexp -> vexp
.

(* ################################################################################### *)

(******************** Option Values *)

(** Option values can return an option value that can be a representation of a concrete value of our grammar,
or can be even a value error *)

(******* Option value for Booleans *)

Fixpoint bool_error (v : value)
                   : option bool :=
  match v with
  | num n => None
  | id s => None
  | tru b => if b then Some true  else None
  | fls b => if b then None  else Some false
  | null => None
  end.

Example test_bool_error1 : bool_error (tru true)  = Some true.
Proof. reflexivity. Qed.

Example test_bool_error2 : bool_error (num 5)  = None.
Proof. reflexivity. Qed.

Definition value_to_OPTbexp (b : value) : option bool := bool_error b.
Coercion value_to_OPTbexp : value >-> option.

(******* Option value for Natural numbers *)

Fixpoint nat_error (v : value)
                   : option nat :=
  match v with
  | num n => Some n
  | id s => None 
  | tru b => None
  | fls b => None
  | null => None
  end.

Example test_nat_error1 : nat_error (num 5)  =  Some 5.
Proof. simpl. reflexivity. Qed.

Example test_nat_error2 : nat_error (tru true)  = None.
Proof. reflexivity. Qed.

Definition value_to_OPTaexp (a : value) : option nat := nat_error a.
Coercion value_to_OPTaexp : value >-> option.

(******* Option value for Strings *)

Fixpoint string_error (v : value)
                   : option string :=
  match v with
  | num n => None
  | id s => Some s
  | tru b => None
  | fls b => None
  | null => None
  end.

Example test_string_error1 : string_error (id X)  =  Some X.
Proof. simpl. reflexivity. Qed.

Example test_string_error2 : string_error (num 5)  =  None.
Proof. reflexivity. Qed.

Definition value_to_OPTstring (s : value) : option string := string_error s.
Coercion value_to_OPTstring : value >-> option.

(******* Option value for Values *)

Fixpoint value_error (v : value)
                   : option value :=
  match v with
  | num n => Some v
  | id s => Some v
  | tru b => Some v
  | fls b => Some v (* maybe I've to define only one boolean value! then come easy the evaluation*)
  | null => None
  end.

Example test_value_error1 : value_error (id X)  =  Some (id X).
Proof. simpl. reflexivity. Qed.

Example test_value_error2 : value_error NULL  =  None.
Proof. reflexivity. Qed.

Definition value_to_OPTvalue (v : value) : option value := value_error v.
Coercion value_to_OPTvalue : value >-> option.

Definition state := total_map value.


(******************** Evaluation of expressions

Sometimes is quite a good thing in proofs, to have the possibility to move from semantic expression world to propositional one.
With a Relational definition, we can abstract expressions to propositions. it's a way around to make things proovable
in a more easy way.

*)

(******* Relational evaluator *)

(*** Specifying Maps *)

(** We specialize our notation for total maps to the specific case of states,
    i.e. using [{ !-> 0 }] as empty state. *)

Notation "{ a !-> x }" :=
  (t_update { !-> NULL } a x) (at level 0).
Notation "{ a !-> x ; b !-> y }" :=
  (t_update ({ a !-> x }) b y) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z }" :=
  (t_update ({ a !-> x ; b !-> y }) c z) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z ; d !-> t }" :=
    (t_update ({ a !-> x ; b !-> y ; c !-> z }) d t) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }" :=
  (t_update ({ a !-> x ; b !-> y ; c !-> z ; d !-> t }) e u) (at level 0).
Notation "{ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u ; f !-> v }" :=
  (t_update ({ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }) f v) (at level 0).



Bind Scope vexp_scope with vexp.
Infix "+" := aplus : vexp_scope.
Infix "-" := aminus : vexp_scope.
Infix "*" := amult : vexp_scope.
Infix "<=" := bleq : vexp_scope.
Infix "=" := beq : vexp_scope.
Infix "&&" := band : vexp_scope.
Notation "'!' b" := (bneg b) (at level 60) : vexp_scope.

(*** Value expression evaluator *)

(* TODO very naive implementation. it's only like a note on a workbook

Reserved Notation "st '[' e ']v' n" (at level 90, left associativity).

Fixpoint vaeval (st : state) (v : vexp) : nat :=
  match v with
  | val ve => match ve with
              | (num n) => n
              | (id s) => (vaeval st (val (st s)))
              | _ => 999
              end
  | aplus v1 v2  => (vaeval st v1) + (vaeval st v2)
  | aminus v1 v2  => (vaeval st v1) - (vaeval st v2)
  | amult v1 v2  => (vaeval st v1) * (vaeval st v2)
  | bneg v1 => 999
  | band v1 v2 => 999
  | bleq v1 v2 => 999
  | beq v1 v2 => 999
  end.

Fixpoint vbeval (st : state) (v : vexp) : bool :=
  match v with:
  | val ve => match ve with
              | (tru true) => true
              | (fls false) => false
              | (id s) => (vaeval st (val (st s)))
              | _ => false
              end
  | beq v1 v2 => eqb (vaeval st v1) (vaeval st v2)
  | bneg v1  => negb (vbeval st v1)
  | band v1 v2 => andb (vbeval v1) (vbeval v2)
  | bleq v1 v2 => leb (vaeval v1) (vaeval v2)
  | aplus v1 v2  => false
  | aminus v1 v2  => false
  | amult v1 v2  => false
*)
(*** Arithmetic evaluator *)



Reserved Notation "st '[' e ']a' n" (at level 90, left associativity).

Inductive aevalR : state -> vexp -> nat -> Prop  :=

  | E_ANum : forall (st : state) (ve : vexp) (v : value) (n : nat),
        ve = (val v) ->
        value_error v = Some (num n) -> 
        nat_error v = Some n -> 
        st [ ve ]a n

  | E_AId : forall (st : state) (ve : vexp) (s : string) (n : nat),
        ve = (val (id s)) ->
        value_error (id s) = Some (id s) ->
        string_error (id s) = Some s ->
        nat_error (st s) = Some n ->
        st [ ve ]a n 

  | E_APlus : forall (ve v1 v2 : vexp) (n n1 n2 : nat) (st : state),
      ve = (aplus v1  v2) ->
      (st [ v1 ]a n1) -> 
      (st [ v2 ]a n2) -> 
      (beq_nat n  (n1 + n2)) = true  ->
      st [ ve ]a n

  | E_AMinus : forall (ve v1 v2 : vexp) (n1 n2 n : nat) (st : state),
      ve = (aminus v1  v2) ->
      (st [ v1 ]a n1) -> 
      (st [ v2 ]a n2) -> 
      (beq_nat n  (n1 - n2)) = true ->
      st [ ve ]a n

  | E_AMult :  forall (ve v1 v2 : vexp) (n1 n2 n : nat) (st : state),
      ve = (amult v1  v2) ->
      (st [ v1 ]a n1) -> 
      (st [ v2 ]a n2) ->  
      (beq_nat n  (n1 * n2)) = true ->
      st [ ve ]a n

  where "st '[' e ']a' n" := (aevalR st e n) : type_scope.


(** Arithmetic expressions examples*)


Example test_aeval1:
  forall n1 n2, exists n, { !-> NULL} [val (num n1) + val (num n2) ]a n.
Proof.
  intros.
  eapply ex_intro.
  eapply E_APlus.
  - auto.
  - eapply E_ANum; auto.
    + simpl; auto.
    + reflexivity.
  - eapply E_ANum; auto.
    + simpl; auto.
    + reflexivity.
  - simpl. symmetry. eapply beq_nat_refl. 
Qed.

Example test_aeval2:
  exists n, { X !-> (num 0)} [val (id X) +  val (num 2) ]a n.
Proof.
  eapply ex_intro.
  eapply E_APlus.
  - auto.
  - eapply E_AId; auto.
    + simpl; auto.
  - eapply E_ANum; auto.
    + simpl; auto.
    + reflexivity.
  - symmetry; eapply beq_nat_refl.
Qed.

Lemma nat_equals : forall n,
  beq_nat n n = true.
Proof.
  intros.
  einduction n.
  - simpl. reflexivity.
  - simpl. rewrite IHn0. reflexivity.
Qed.

Theorem add_existance : forall n1 n2, 
  exists n, beq_nat n (n1 + n2) = true.
Proof.
  intros.
  exists (n1 + n2).
  einduction n1.
  - simpl. rewrite nat_equals. reflexivity.
  - simpl. rewrite nat_equals. reflexivity.
Qed.

Example test_aeval3:
  exists n, { X !-> (num 0); Y !-> (num 6); Z !-> (tru true)} [val (id X) + val (id Y) * val (num 2) ]a n.
Proof.
  eapply ex_intro.
  eapply E_APlus.
  - eauto.
  - eapply E_AId; auto.
    + simpl; auto.
  - eapply E_AMult; auto.
    + eapply E_AId; auto.
      * simpl; auto.
    + eapply E_ANum; auto.
      * simpl; auto.
      * reflexivity.
    + simpl. eapply beq_nat_true_iff. eexists.
  - simpl; symmetry; eapply beq_nat_refl.
Qed.


(** Boolean evaluator *)

Reserved Notation "st '[' e ']b' b" (at level 90, left associativity).

Inductive bevalR : state -> vexp -> bool -> Prop :=

  | E_boolT : forall (st : state) (ve : vexp) (v : value) (b : bool),
        ve = (val v) ->
        value_error v = Some (tru b) -> 
        bool_error v = Some b -> 
        st [ ve ]b b

  | E_boolF : forall (st : state) (ve : vexp) (v : value) (b : bool),
        ve = (val v) ->
        value_error v = Some (fls b) -> 
        bool_error v = Some b -> 
        st [ ve ]b b

  | E_BId : forall (st : state) (ve : vexp) (s : string) (vs vb : value) (b : bool),
        ve = (val vs) ->
        value_error vs = Some (id s) ->
        string_error vs = Some s ->
        st s = vb /\ bool_error vb = Some b ->
        st [ ve ]b b

  | E_BEq : forall (ve v1 v2 : vexp) (n1 n2 : nat) (st : state) (b : bool),
      ve = (beq v1  v2) ->
      (st [ v1 ]a n1) ->
      (st [ v2 ]a n2) ->
       b = (beq_nat n1 n2)  ->
      st [ ve ]b b


  | E_BLeq : forall (ve v1 v2 : vexp) (n1 n2 : nat) (st : state) (b : bool),
      ve = (bleq v1  v2) ->
      (st [ v1 ]a n1) ->
      (st [ v2 ]a n2) ->
       b = (leb n1 n2) ->
      st [ ve ]b b

  | E_BAnd :  forall (ve v1 v2 : vexp) (b1 b2 : bool) (st : state) (b : bool),
      ve = (band v1  v2) ->
      (st [ v1 ]b b1) -> 
      (st [ v2 ]b b2) -> 
      b = (andb b1 b2) ->
      st [ ve ]b b
  
  | E_BNeg :  forall (ve v1 v2 : vexp) (b1 : bool) (st : state) (b : bool),
      ve = (bneg v1) ->
      (st [ v1 ]b b1) ->
      b = (negb b1) ->
      st [ ve ]b b

  where "st '[' e ']b' b" := (bevalR st e b) : type_scope.



(** Boolean expressions examples*)

Example test_beval1:
  exists b, { !-> NULL} [val (tru true) && val (fls false) ]b b.
Proof. 
  eapply ex_intro.
  eapply E_BAnd.
  - auto.
  - eapply E_boolT; auto.
    + simpl; eauto.
    + reflexivity.
  - eapply E_boolF; auto.
    + simpl; eauto.
    + reflexivity.
  - reflexivity.
Qed.

Example test_beval2:
  exists b, { X !-> (tru true); Y !-> (num 2); Z !-> (num 2)}
                  [val (tru true) &&  (val (id Y) = val (id Z)) ]b b.
Proof.
  eapply ex_intro.
  eapply E_BAnd.
  - auto.
  - eapply E_boolT; auto.
    + simpl; auto.
    + reflexivity.
  - eapply E_BEq; auto.
    + eapply E_AId; auto.
      * simpl; auto.
    + eapply E_AId; auto.
      * simpl; auto.
  - reflexivity.
Qed.


Example test_beval3:
  exists b, { X !-> (num 0); Y !-> (num 6); Z !-> (tru true)}
                [(val (id X) * (val (id Y) + val (num 2))) = val (num 3) * val (id Z)]b b.
Proof.
  exists false.
  eapply E_BEq.
  - auto.
  - eapply E_AMult; auto.
    + eapply E_AId; auto.
      * simpl; auto.
    + eapply E_APlus; auto.
      * eapply E_AId; auto.
        simpl. auto.
      * eapply E_ANum; auto.
          simpl; auto.
          reflexivity.
      * eapply beq_nat_true_iff; auto.
    + simpl. eapply beq_nat_true_iff; auto.
  - eapply E_AMult; auto.
    + eapply E_ANum.
      * auto.
      * simpl. auto.
      * reflexivity.
    + eapply E_AId.
      * simpl. auto.
      * reflexivity.
      * reflexivity.
      * simpl. (*eexists.*)
Admitted. (* mistake on purpose! I am trying to proof a multiplication between a boolean value and a nat*)

Example test_beval4:
  exists b, { X !-> (num 3); Y !-> (num 4); Z !-> (tru true)}
                  [val (id X) * val (id Y) + val (num 0) * val (id Y) = val (num 3) * val (id Y)]b b.
Proof.
  eapply ex_intro.
  eapply E_BEq.
  - auto.
  - eapply E_APlus; auto.
    + eapply E_AMult; auto.
      * eapply E_AId; simpl. auto. reflexivity. reflexivity.
        simpl. auto.
      * eapply E_AId; simpl. auto. reflexivity. reflexivity.
        simpl. auto.
      * eapply beq_nat_true_iff; auto.
    + eapply E_AMult; auto.
      * eapply E_ANum; auto.
        simpl; auto.
        reflexivity.
      * eapply E_AId; simpl. auto. reflexivity.
         reflexivity.
         simpl. auto.
      * simpl. eapply beq_nat_true_iff; auto.
    + simpl. eapply beq_nat_true_iff; auto.
  - eapply E_AMult; auto.
    + eapply E_ANum.
      * auto.
      * simpl. auto.
      * reflexivity.
    + eapply E_AId.
      * simpl; auto.
      * reflexivity.
      * reflexivity.
      * simpl; auto. 
    + simpl; eapply beq_nat_true_iff; auto.
  - simpl. reflexivity.
Qed.

Check { X !-> (num 3); Y !-> (num 4); Z !-> (tru true)}
            [val (id X) * val (id Y) + val (num 0) * val (id Y) = val (num 3) * val (id Y)]b true.





























