(** An auxiliary file for generic typed environments **)

Require Import LibSet LibHeap.
Require Export Skeletons.

(** * Typed Environments **)

(** Interpretations typically use environments from variables to some
  values, where the values are divided between term values and flow
  values.  This is such a common scheme among interpretations that we
  decided to factorise the definition of such environments. **)

Section Values.

Variables term_value flow_value value : Type.

Variable term_value_to_value : term_value -> value.
Variable flow_value_to_value : flow_value -> value.
Coercion term_value_to_value : term_value >-> value.
Coercion flow_value_to_value : flow_value >-> value.

Hypothesis term_value_to_value_inj : forall t1 t2 : term_value, (t1 : value) = t2 -> t1 = t2.
Hypothesis flow_value_to_value_inj : forall t1 t2 : flow_value, (t1 : value) = t2 -> t1 = t2.

Variable value_rect : forall (P : value -> Type),
  (forall v : term_value, P v) ->
  (forall v : flow_value, P v) ->
  forall v, P v.

Hypothesis value_rect_term : forall P f1 f2 (v : term_value),
  value_rect P f1 f2 v = f1 v.
Hypothesis value_rect_flow : forall P f1 f2 (v : flow_value),
  value_rect P f1 f2 v = f2 v.

Hypothesis term_value_Inhab : Inhab term_value.
Hypothesis flow_value_Inhab : Inhab flow_value.

Variables variable_term variable_flow : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.
Hypothesis variable_flow_Comparable : Comparable variable_flow.

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.

Local Instance variable_flow_extended_Comparable : Comparable variable_flow_extended :=
  variable_flow_extended_Comparable variable_flow_Comparable.

(** ** Definition **)

Record typed_environment := make_typed_environment {
    typed_environment_term : heap variable_term term_value ;
    typed_environment_flow : heap variable_flow_extended flow_value
  }.

(** ** Operations **)

Definition typed_environment_empty :=
  make_typed_environment empty empty.

Definition typed_environment_write_term Gamma x s := {|
    typed_environment_term := write (typed_environment_term Gamma) x s ;
    typed_environment_flow := typed_environment_flow Gamma
  |}.

Definition typed_environment_write_flow Gamma x s := {|
    typed_environment_term := typed_environment_term Gamma ;
    typed_environment_flow :=  write (typed_environment_flow Gamma) x s
  |}.

Definition typed_environment_write_variable Gamma (x : variable) : value -> _ :=
  match x with
  | X_t x =>
    value_rect (fun _ => _) (fun v => Some (typed_environment_write_term Gamma x v)) (fun _ => None)
  | _X_ x =>
    value_rect (fun _ => _) (fun _ => None) (fun v => Some (typed_environment_write_flow Gamma x v))
  end.

Fixpoint typed_environment_write_variables Gamma xs vs :=
  match xs, vs with
  | [], [] => Some Gamma
  | x :: xs, v :: vs =>
    LibOption.apply_on (typed_environment_write_variable Gamma x v)
      (fun Gamma => typed_environment_write_variables Gamma xs vs)
  | _, _ => None
  end.

Definition typed_environment_read_term Gamma x :=
   read_option (typed_environment_term Gamma) x.

Definition typed_environment_read_flow Gamma x :=
   read_option (typed_environment_flow Gamma) x.

Definition typed_environment_read_variable Gamma x :=
  match x with
  | X_t x => LibOption.map (id : term_value -> value) (typed_environment_read_term Gamma x)
  | _X_ x => LibOption.map (id : flow_value -> value) (typed_environment_read_flow Gamma x)
  end.

Definition typed_environment_domain Gamma : set variable :=
  set_st (fun x => typed_environment_read_variable Gamma x <> None).

Definition typed_environment_equivalent Gamma1 Gamma2 :=
  heap_equiv (typed_environment_term Gamma1)
             (typed_environment_term Gamma2) /\
  heap_equiv (typed_environment_flow Gamma1)
             (typed_environment_flow Gamma2).

Definition typed_environment_merge Gamma1 Gamma2 := {|
    typed_environment_term :=
      heap_merge (typed_environment_term Gamma1)
                 (typed_environment_term Gamma2) ;
    typed_environment_flow :=
      heap_merge (typed_environment_flow Gamma1)
                 (typed_environment_flow Gamma2)
  |}.

Fixpoint split_fset_variable (V : list variable) :=
  match V with
  | nil => (nil, nil)
  | x :: V =>
    let (Vterm, Vflow) := split_fset_variable V in
    match x with
    | X_t x => (x :: Vterm, Vflow)
    | _X_ x => (Vterm, x :: Vflow)
    end
  end.

Definition typed_environment_restrict Gamma V :=
  let (Vterm, Vflow) := split_fset_variable V in {|
    typed_environment_term :=
      heap_restrict (typed_environment_term Gamma) Vterm ;
    typed_environment_flow :=
      heap_restrict (typed_environment_flow Gamma) Vflow
  |}.

(** ** Properties **)

Global Instance typed_environment_Inhab : Inhab typed_environment.
  applys prove_Inhab @typed_environment_empty.
Defined.

Lemma typed_environment_equivalent_spec : forall Gamma1 Gamma2 : typed_environment,
  typed_environment_equivalent Gamma1 Gamma2 <-> forall x : variable,
    typed_environment_read_variable Gamma1 x = typed_environment_read_variable Gamma2 x.
Proof.
  iff E.
  - introv. destruct x; simpl; fequals; unfolds; erewrite~ heap_equiv_read_option; apply E.
  - constructors.
    + apply~ read_option_heap_equiv. introv. lets E': (E (X_t k)). simpl in E'.
      unfolds typed_environment_read_term. do 2 destruct read_option; inverts E' as E'; autos~.
      forwards E'': term_value_to_value_inj E'. compute in E''. substs~.
    + apply~ read_option_heap_equiv. introv. lets E': (E (_X_ k)). simpl in E'.
      unfolds typed_environment_read_flow. do 2 destruct read_option; inverts E' as E'; autos~.
      forwards E'': flow_value_to_value_inj E'. compute in E''. substs~.
Qed.

Lemma typed_environment_equivalent_on_domain : forall Gamma1 Gamma2,
  (forall x,
    x \in typed_environment_domain Gamma1 \u typed_environment_domain Gamma2 ->
    typed_environment_read_variable Gamma1 x = typed_environment_read_variable Gamma2 x) ->
  typed_environment_equivalent Gamma1 Gamma2.
Proof.
  introv E. apply typed_environment_equivalent_spec. introv.
  tests I: (x \in typed_environment_domain Gamma1 \u typed_environment_domain Gamma2).
  - applys~ E I.
  - unfold typed_environment_domain in I. rewrite in_union_eq in I.
    repeat rewrite in_set_st_eq in I. rew_logic in I. lets (N1&N2): (rm I).
    rewrite N1, N2. autos~.
Qed.

Lemma typed_environment_equivalent_term : forall (Gamma1 Gamma2 : typed_environment),
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_read_term Gamma1 = typed_environment_read_term Gamma2.
Proof.
  introv E. extens. intro x. rewrite typed_environment_equivalent_spec in E.
  lets E': E (X_t x : variable). simpl in E'.
  do 2 destruct typed_environment_read_term; inverts E' as E'; autos~.
  fequals. apply~ term_value_to_value_inj.
Qed.

Lemma typed_environment_equivalent_flow : forall (Gamma1 Gamma2 : typed_environment),
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_read_flow Gamma1 = typed_environment_read_flow Gamma2.
Proof.
  introv E. extens. intro x. rewrite typed_environment_equivalent_spec in E.
  lets E': E (_X_ x : variable). simpl in E'.
  do 2 destruct typed_environment_read_flow; inverts E' as E'; autos~.
  fequals. apply~ flow_value_to_value_inj.
Qed.

Lemma typed_environment_equivalent_refl : forall Gamma : typed_environment,
  typed_environment_equivalent Gamma Gamma.
Proof. introv. apply~ typed_environment_equivalent_spec. Qed.

Lemma typed_environment_equivalent_sym : forall Gamma1 Gamma2 : typed_environment,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_equivalent Gamma2 Gamma1.
Proof.
  introv E. apply typed_environment_equivalent_spec. introv.
  eapply typed_environment_equivalent_spec in E. autos*.
Qed.

Lemma typed_environment_equivalent_trans : forall Gamma1 Gamma2 Gamma3 : typed_environment,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_equivalent Gamma2 Gamma3 ->
  typed_environment_equivalent Gamma1 Gamma3.
Proof.
  introv E1 E2. apply typed_environment_equivalent_spec. introv.
  eapply typed_environment_equivalent_spec in E1. rewrite E1.
  eapply typed_environment_equivalent_spec in E2. autos*.
Qed.

Definition typed_environment_domain_list (Gamma : typed_environment) : list variable :=
  map (fun xv => X_t (fst xv)) (to_list (typed_environment_term Gamma))
  ++ map (fun xv => _X_ (fst xv)) (to_list (typed_environment_flow Gamma)).

Lemma typed_environment_domain_list_spec : forall Gamma x,
  Mem x (typed_environment_domain_list Gamma) <->
    x \in (typed_environment_domain Gamma : set variable).
Proof.
  introv. unfolds typed_environment_domain_list. rewrite Mem_app_or_eq.
  unfolds typed_environment_domain. rewrite in_set_st_eq.
  destruct x; simpl; unfolds typed_environment_read_term, typed_environment_read_flow;
    (destruct read_option eqn: E; simpl;
      [ apply read_option_indom in E; apply indom_to_list in E;
        forwards (x&M'&E'): Mem_map_inv (rm E);
        iff M; [ discriminate |]; substs
      | apply read_option_not_indom in E; iff M; [| false~ ];
        false E; inverts M as M; lets (x&M'&E'): Mem_map_inv (rm M); inverts E';
        apply to_list_indom; applys~ Mem_map M' ]).
  - left. applys @Mem_map M'.
  - right. applys @Mem_map M'.
Qed.

Lemma typed_environment_write_term_equivalent : forall (Gamma Gamma' : typed_environment) x s,
  typed_environment_equivalent Gamma Gamma' ->
  typed_environment_equivalent
    (typed_environment_write_term Gamma x s)
    (typed_environment_write_term Gamma' x s).
Proof.
  introv E. splits~.
  - applys~ heap_equiv_write. apply E.
  - apply E.
Qed.

Lemma typed_environment_write_flow_equivalent : forall (Gamma Gamma' : typed_environment) x s,
  typed_environment_equivalent Gamma Gamma' ->
  typed_environment_equivalent
    (typed_environment_write_flow Gamma x s)
    (typed_environment_write_flow Gamma' x s).
Proof.
  introv E. splits~.
  - apply E.
  - applys~ heap_equiv_write E.
Qed.

Lemma typed_environment_write_variable_equivalent : forall (Gamma1 Gamma2 Gamma1' Gamma2' : typed_environment) x s,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_write_variable Gamma1 x s = Some Gamma1' ->
  typed_environment_write_variable Gamma2 x s = Some Gamma2' ->
  typed_environment_equivalent Gamma1' Gamma2'.
Proof.
  introv E E1 E2. destruct x;
    gen E1 E2; eapply value_rect with (v := s) (P := fun s => _ s = _ -> _ s = _ -> _);
    introv E1 E2; simpl in E1, E2;
    (rewrite value_rect_term in E1, E2 || rewrite value_rect_flow in E1, E2);
    inverts E1 as E1; inverts E2 as E2.
  - applys~ typed_environment_write_term_equivalent E.
  - applys~ typed_environment_write_flow_equivalent E.
Qed.

Lemma typed_environment_write_variables_equivalent : forall (Gamma1 Gamma2 Gamma1' Gamma2' : typed_environment) xs ss,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_write_variables Gamma1 xs ss = Some Gamma1' ->
  typed_environment_write_variables Gamma2 xs ss = Some Gamma2' ->
  typed_environment_equivalent Gamma1' Gamma2'.
Proof.
  introv E E1 E2. gen Gamma1 Gamma2 ss. induction xs; introv E E1 E2; destruct ss; tryfalse.
  - inverts E1. inverts~ E2.
  - simpls. forwards (Gamma1''&E1a&E1b): LibOption.apply_on_inv E1.
    forwards (Gamma2''&E2a&E2b): LibOption.apply_on_inv E2.
    applys IHxs E1b E2b. applys~ typed_environment_write_variable_equivalent E1a E2a.
Qed.

Lemma typed_environment_write_variable_defined : forall (Gamma1 Gamma2 Gamma1' : typed_environment) x s,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_write_variable Gamma1 x s = Some Gamma1' ->
  typed_environment_write_variable Gamma2 x s <> None.
Proof.
  introv E E1 E2. destruct x;
    gen E1 E2; eapply value_rect with (v := s) (P := fun s => _ s = _ -> _ s = _ -> _);
    introv E1 E2; simpl in E1, E2;
    (rewrite value_rect_term in E1, E2 || rewrite value_rect_flow in E1, E2);
    inverts E1 as E1; inverts E2 as E2.
Qed.

Lemma typed_environment_write_variables_defined : forall (Gamma1 Gamma2 Gamma1' : typed_environment) xs ss,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_write_variables Gamma1 xs ss = Some Gamma1' ->
  typed_environment_write_variables Gamma2 xs ss <> None.
Proof.
  introv E E1 E2. gen Gamma1 Gamma2 ss. induction xs; introv E E1 E2; destruct ss; tryfalse.
  simpls. destruct (typed_environment_write_variable Gamma1) eqn: E3; tryfalse.
  forwards D: typed_environment_write_variable_defined E E3.
  destruct (typed_environment_write_variable Gamma2) eqn: E4; tryfalse.
  applys~ IHxs E1 E2. applys~ typed_environment_write_variable_equivalent E E3 E4.
Qed.

Lemma typed_environment_merge_equivalent : forall Gamma1 Gamma2 Gamma1' Gamma2' : typed_environment,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_equivalent Gamma1' Gamma2' ->
  typed_environment_equivalent
    (typed_environment_merge Gamma1 Gamma1')
    (typed_environment_merge Gamma2 Gamma2').
Proof. introv E1 E2. splits~; (apply~ heap_merge_equiv; [ apply E1 | apply E2 ]). Qed.

Lemma typed_environment_read_write_term_eq : forall Gamma x v,
  typed_environment_read_term (typed_environment_write_term Gamma x v) x = Some v.
Proof. introv. destruct Gamma. apply read_option_write_same. Qed.

Lemma typed_environment_read_write_flow_eq : forall Gamma x v,
  typed_environment_read_flow (typed_environment_write_flow Gamma x v) x = Some v.
Proof. introv. destruct Gamma. apply read_option_write_same. Qed.

Lemma typed_environment_read_write_variable_eq : forall Gamma Gamma' x v,
  typed_environment_write_variable Gamma x v = Some Gamma' ->
  typed_environment_read_variable Gamma' x = Some v.
Proof.
  introv E. destruct x; simpl in E;
    (gen E; eapply value_rect with (v := v) (P := fun v => _ v = _ -> _); introv E;
     [ rewrite value_rect_term in E | rewrite value_rect_flow in E]; inverts E).
  - simpl. rewrite~ typed_environment_read_write_term_eq.
  - simpl. rewrite~ typed_environment_read_write_flow_eq.
Qed.

Lemma typed_environment_read_term_write_flow : forall Gamma x y v,
  typed_environment_read_term (typed_environment_write_flow Gamma x v) y
  = typed_environment_read_term Gamma y.
Proof. introv. destruct~ Gamma. Qed.

Lemma typed_environment_read_flow_write_term : forall Gamma x y v,
  typed_environment_read_flow (typed_environment_write_term Gamma x v) y
  = typed_environment_read_flow Gamma y.
Proof. introv. destruct~ Gamma. Qed.

Lemma typed_environment_read_write_term_neq : forall Gamma x y v,
  x <> y ->
  typed_environment_read_term (typed_environment_write_term Gamma x v) y
  = typed_environment_read_term Gamma y.
Proof. introv D. destruct Gamma. applys read_option_write D. Qed.

Lemma typed_environment_read_write_flow_neq : forall Gamma x y v,
  x <> y ->
  typed_environment_read_flow (typed_environment_write_flow Gamma x v) y
  = typed_environment_read_flow Gamma y.
Proof. introv D. destruct Gamma. applys read_option_write D. Qed.

Lemma typed_environment_read_write_variable_neq : forall Gamma Gamma' x y v,
  x <> y ->
  typed_environment_write_variable Gamma x v = Some Gamma' ->
  typed_environment_read_variable Gamma' y = typed_environment_read_variable Gamma y.
Proof.
  introv D E. destruct x; simpl in E;
    (gen E; eapply value_rect with (v := v) (P := fun v => _ v = _ -> _); introv E;
     [ rewrite value_rect_term in E | rewrite value_rect_flow in E]; inverts E).
  - destruct y; simpl.
    + rewrite~ typed_environment_read_write_term_neq. introv E. false*.
    + rewrite~ typed_environment_read_flow_write_term.
  - destruct y; simpl.
    + rewrite~ typed_environment_read_term_write_flow.
    + rewrite~ typed_environment_read_write_flow_neq. introv E. false*.
Qed.

Lemma typed_environment_read_write_variables_not_in : forall Gamma Gamma' xs y vs,
  ~ Mem y xs ->
  typed_environment_write_variables Gamma xs vs = Some Gamma' ->
  typed_environment_read_variable Gamma' y = typed_environment_read_variable Gamma y.
Proof.
  introv NM E. gen Gamma Gamma' vs. induction xs; introv E; destruct vs; inverts E as E.
  - reflexivity.
  - destruct typed_environment_write_variable eqn: E'; simpl in E; tryfalse.
    rewrites~ <- >> typed_environment_read_write_variable_neq y E'.
    { introv ?.  false NM. substs*. }
    rewrites~ >> IHxs E.
Qed.

Lemma typed_environment_read_write_variables_length : forall Gamma Gamma' xs vs,
  typed_environment_write_variables Gamma xs vs = Some Gamma' ->
  length xs = length vs.
Proof.
  introv E. gen Gamma Gamma' vs. induction xs; introv E; destruct vs; inverts E as E; rew_list~.
  fequals. destruct typed_environment_write_variable eqn: E'; tryfalse. applys~ IHxs E.
Qed.

Lemma typed_environment_read_write_variables_Nth : forall Gamma Gamma' n xs x vs v,
  No_duplicates xs ->
  Nth n xs x ->
  Nth n vs v ->
  typed_environment_write_variables Gamma xs vs = Some Gamma' ->
  typed_environment_read_variable Gamma' x = Some v.
Proof.
  introv ND N1 N2 E. gen Gamma Gamma' vs. induction N1; introv N2 E; destruct vs; inverts E as E.
  - inverts N2. destruct typed_environment_write_variable eqn: E'; simpl in E; tryfalse.
    rewrites >> typed_environment_read_write_variables_not_in E.
    { inverts~ ND. }
    rewrites~ >> typed_environment_read_write_variable_eq E'.
  - inverts N2 as N2. destruct typed_environment_write_variable eqn: E'; simpl in E; tryfalse.
    rewrites* <- >> IHN1 N2. inverts~ ND.
Qed.

Lemma typed_environment_write_variables_last : forall Gamma Gamma' xs vs x v,
  typed_environment_write_variables Gamma xs vs = Some Gamma' ->
  typed_environment_write_variables Gamma (xs & x) (vs & v)
  = typed_environment_write_variable Gamma' x v.
Proof.
  introv E. gen Gamma Gamma' vs. induction xs; introv E; destruct vs as [|v' vs]; tryfalse.
  - inverts~ E. simpl. destruct* typed_environment_write_variable.
  - rew_list. simpls. destruct typed_environment_write_variable; tryfalse. simpls. applys~ IHxs E.
Qed.

Lemma typed_environment_write_variables_last_None : forall Gamma xs vs x v,
  typed_environment_write_variables Gamma xs vs = None ->
  typed_environment_write_variables Gamma (xs & x) (vs & v) = None.
Proof.
  introv E. gen Gamma vs. induction xs; introv E; destruct vs; inverts E as E; rew_list.
  - unfolds. destruct~ typed_environment_write_variable. destruct~ vs.
  - unfolds. destruct~ typed_environment_write_variable. destruct~ xs.
  - simpls. destruct~ typed_environment_write_variable. simpls. rewrite E. applys~ IHxs E.
Qed.

Lemma typed_environment_read_merge : forall Gamma1 Gamma2 x v,
  typed_environment_read_variable (typed_environment_merge Gamma1 Gamma2) x = Some v ->
  typed_environment_read_variable Gamma1 x = Some v
  \/ typed_environment_read_variable Gamma2 x = Some v.
Proof.
  introv E. destruct Gamma1, Gamma2, x as [x|x]; simpls.
  - unfolds typed_environment_read_term. simpls. destruct read_option eqn: E'; inverts E.
    forwards B: @read_option_binds E'.
    forwards [B'|B']: heap_merge_binds B; rewrites* >> (@binds_read_option) B'.
  - unfolds typed_environment_read_flow. simpls. destruct read_option eqn: E'; inverts E.
    forwards B: @read_option_binds E'.
    forwards [B'|B']: heap_merge_binds B; rewrites* >> (@binds_read_option) B'.
Qed.

Lemma typed_environment_read_term_merge_left : forall Gamma1 Gamma2 x,
  ~ X_t x \in typed_environment_domain Gamma2 ->
  typed_environment_read_term (typed_environment_merge Gamma1 Gamma2) x
  = typed_environment_read_term Gamma1 x.
Proof.
  introv E. destruct Gamma1 as [GT1 GF1], Gamma2.
  unfolds typed_environment_read_term. simpls. tests I: (indom GT1 x).
  - forwards (v&B): @indom_binds I. rewrites >> (@binds_read_option) B.
    forwards I': heap_merge_indom_left I. forwards (v'&B'): @indom_binds I'.
    rewrites >> (@binds_read_option) B'. forwards [B0|B0]: heap_merge_binds B'.
    + forwards~: @binds_func B B0. substs~.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_term. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
  - rewrites~ >> (@not_indom_read_option) I. rewrite~ @not_indom_read_option.
    introv A. forwards (v&B): @indom_binds A. forwards [B0|B0]: heap_merge_binds B.
    + false I. applys~ @binds_indom B0.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_term. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
Qed.

Lemma typed_environment_read_flow_merge_left : forall Gamma1 Gamma2 x,
  ~ _X_ x \in typed_environment_domain Gamma2 ->
  typed_environment_read_flow (typed_environment_merge Gamma1 Gamma2) x
  = typed_environment_read_flow Gamma1 x.
Proof.
  introv E. destruct Gamma1 as [GT1 GF1], Gamma2.
  unfolds typed_environment_read_flow. simpls. tests I: (indom GF1 x).
  - forwards (v&B): @indom_binds I. rewrites >> (@binds_read_option) B.
    forwards I': heap_merge_indom_left I. forwards (v'&B'): @indom_binds I'.
    rewrites >> (@binds_read_option) B'. forwards [B0|B0]: heap_merge_binds B'.
    + forwards~: @binds_func B B0; try typeclass. substs~.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_flow. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
  - rewrites~ >> (@not_indom_read_option) I. rewrite~ @not_indom_read_option.
    introv A. forwards (v&B): @indom_binds A. forwards [B0|B0]: heap_merge_binds B.
    + false I. applys~ @binds_indom B0.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_flow. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
Qed.

Lemma typed_environment_read_variable_merge_left : forall Gamma1 Gamma2 x,
  ~ x \in typed_environment_domain Gamma2 ->
  typed_environment_read_variable (typed_environment_merge Gamma1 Gamma2) x
  = typed_environment_read_variable Gamma1 x.
Proof.
  introv E. destruct x.
  - simpl. rewrite~ typed_environment_read_term_merge_left.
  - simpl. rewrite~ typed_environment_read_flow_merge_left.
Qed.

Lemma typed_environment_read_term_merge_right : forall Gamma1 Gamma2 x,
  ~ X_t x \in typed_environment_domain Gamma1 ->
  typed_environment_read_term (typed_environment_merge Gamma1 Gamma2) x
  = typed_environment_read_term Gamma2 x.
Proof.
  introv E. destruct Gamma1, Gamma2 as [GT2 GF2].
  unfolds typed_environment_read_term. simpls. tests I: (indom GT2 x).
  - forwards (v&B): @indom_binds I. rewrites >> (@binds_read_option) B.
    forwards I': heap_merge_indom_right I. forwards (v'&B'): @indom_binds I'.
    rewrites >> (@binds_read_option) B'. forwards [B0|B0]: heap_merge_binds B'.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_term. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
    + forwards~: @binds_func B B0. substs~.
  - rewrites~ >> (@not_indom_read_option) I. rewrite~ @not_indom_read_option.
    introv A. forwards (v&B): @indom_binds A. forwards [B0|B0]: heap_merge_binds B.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_term. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
    + false I. applys~ @binds_indom B0.
Qed.

Lemma typed_environment_read_flow_merge_right : forall Gamma1 Gamma2 x,
  ~ _X_ x \in typed_environment_domain Gamma1 ->
  typed_environment_read_flow (typed_environment_merge Gamma1 Gamma2) x
  = typed_environment_read_flow Gamma2 x.
Proof.
  introv E. destruct Gamma1, Gamma2 as [GT2 GF2].
  unfolds typed_environment_read_flow. simpls. tests I: (indom GF2 x).
  - forwards (v&B): @indom_binds I. rewrites >> (@binds_read_option) B.
    forwards I': heap_merge_indom_right I. forwards (v'&B'): @indom_binds I'.
    rewrites >> (@binds_read_option) B'. forwards [B0|B0]: heap_merge_binds B'.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_flow. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
    + forwards~: @binds_func B B0; try typeclass. substs~.
  - rewrites~ >> (@not_indom_read_option) I. rewrite~ @not_indom_read_option.
    introv A. forwards (v&B): @indom_binds A. forwards [B0|B0]: heap_merge_binds B.
    + false E. unfolds typed_environment_domain. rewrite in_set_st_eq. simpl.
      unfolds typed_environment_read_flow. simpl. rewrites~ >> (@binds_read_option) B0. discriminate.
    + false I. applys~ @binds_indom B0.
Qed.

Lemma typed_environment_read_variable_merge_right : forall Gamma1 Gamma2 x,
  ~ x \in typed_environment_domain Gamma1 ->
  typed_environment_read_variable (typed_environment_merge Gamma1 Gamma2) x
  = typed_environment_read_variable Gamma2 x.
Proof.
  introv E. destruct x.
  - simpl. rewrite~ typed_environment_read_term_merge_right.
  - simpl. rewrite~ typed_environment_read_flow_merge_right.
Qed.

Lemma typed_environment_read_term_merge_right_precise : forall Gamma1 Gamma2 x v,
  typed_environment_read_term Gamma2 x = Some v ->
  typed_environment_read_term (typed_environment_merge Gamma1 Gamma2) x = Some v.
Proof.
  introv E. destruct Gamma1 as [GT1 GF1], Gamma2 as [GT2 GF2].
  unfolds typed_environment_read_term. simpls. destruct (read_option (heap_merge GT1 GT2) x) eqn: E'.
  - forwards B: @read_option_binds (rm E). forwards B': @read_option_binds (rm E').
    forwards~ B2: heap_merge_binds_precise B'.
    { applys~ @binds_indom B. }
    forwards~: @binds_func B B2. substs~.
  - apply read_option_not_indom in E'. false E'. apply heap_merge_indom_right.
    applys~ read_option_indom E.
Qed.

Lemma typed_environment_read_flow_merge_right_precise : forall Gamma1 Gamma2 x v,
  typed_environment_read_flow Gamma2 x = Some v ->
  typed_environment_read_flow (typed_environment_merge Gamma1 Gamma2) x = Some v.
Proof.
  introv E. destruct Gamma1 as [GT1 GF1], Gamma2 as [GT2 GF2].
  unfolds typed_environment_read_flow. simpls. destruct (read_option (heap_merge GF1 GF2) x) eqn: E'.
  - forwards B: @read_option_binds (rm E). forwards B': @read_option_binds (rm E').
    forwards~ B2: heap_merge_binds_precise B'.
    { applys~ @binds_indom B. }
    forwards~: @binds_func B B2; try typeclass. substs~.
  - apply read_option_not_indom in E'. false E'. apply heap_merge_indom_right.
    applys~ read_option_indom E.
Qed.

Lemma typed_environment_read_variable_merge_right_precise : forall Gamma1 Gamma2 x v,
  typed_environment_read_variable Gamma2 x = Some v ->
  typed_environment_read_variable (typed_environment_merge Gamma1 Gamma2) x = Some v.
Proof.
  introv E. destruct x.
  - simpls. destruct typed_environment_read_term eqn: E'; tryfalse.
    rewrites~ >> typed_environment_read_term_merge_right_precise E'.
  - simpls. destruct typed_environment_read_flow eqn: E'; tryfalse.
    rewrites~ >> typed_environment_read_flow_merge_right_precise E'.
Qed.

Lemma split_fset_variable_term_spec : forall L lt lf x,
  split_fset_variable L = (lt, lf) ->
  Mem x lt <-> Mem (X_t x) L.
Proof.
  induction L; introv E.
  - inverts E. iff M; inverts~ M.
  - simpl in E. destruct (split_fset_variable L) as [lt' lf'].
    destruct a; inverts E; iff M; inverts M as M; autos~; forwards*: IHL.
Qed.

Lemma split_fset_variable_flow_spec : forall L lt lf x,
  split_fset_variable L = (lt, lf) ->
  Mem x lf <-> Mem (_X_ x) L.
Proof.
  induction L; introv E.
  - inverts E. iff M; inverts~ M.
  - simpl in E. destruct (split_fset_variable L) as [lt' lf'].
    destruct a; inverts E; iff M; inverts M as M; autos~; forwards*: IHL.
Qed.

Lemma split_fset_variable_equivalent : forall L1 L2 lt1 lf1 lt2 lf2,
  (forall x, Mem x L1 <-> Mem x L2) ->
  split_fset_variable L1 = (lt1, lf1) ->
  split_fset_variable L2 = (lt2, lf2) ->
  (forall x, Mem x lt1 <-> Mem x lt2) /\ (forall x, Mem x lf1 <-> Mem x lf2).
Proof.
  introv E E1 E2. splits; introv.
  - rewrites~ >> split_fset_variable_term_spec E1.
    rewrites~ >> split_fset_variable_term_spec E2.
  - rewrites~ >> split_fset_variable_flow_spec E1.
    rewrites~ >> split_fset_variable_flow_spec E2.
Qed.

Lemma typed_environment_read_restrict_Mem : forall Gamma V x,
  Mem x V ->
  typed_environment_read_variable (typed_environment_restrict Gamma V) x
  = typed_environment_read_variable Gamma x.
Proof.
  introv M. destruct Gamma. unfolds typed_environment_restrict.
  destruct split_fset_variable as [lt lf] eqn: E. destruct x; simpl.
  - unfolds typed_environment_read_term. simpl. destruct read_option eqn: E'.
    + forwards B: @read_option_binds E'. forwards~ B': heap_restrict_binds B.
      rewrites~ >> (@binds_read_option) B'.
    + forwards I: @read_option_not_indom E'. rewrite~ heap_restrict_indom in I.
      rew_logic in I. inverts I as I.
      * rewrites* >> split_fset_variable_term_spec E in I.
      * rewrites~ >> (@not_indom_read_option) I.
  - unfolds typed_environment_read_flow. simpl. destruct read_option eqn: E'.
    + forwards B: @read_option_binds E'. forwards~ B': heap_restrict_binds B.
      rewrites~ >> (@binds_read_option) B'.
    + forwards I: @read_option_not_indom E'. rewrite~ heap_restrict_indom in I.
      rew_logic in I. inverts I as I.
      * unfolds variable. rewrites* <- >> split_fset_variable_flow_spec E in M.
      * rewrites~ >> (@not_indom_read_option) I.
Qed.

Lemma typed_environment_read_restrict_implies_Mem : forall Gamma V x v,
  typed_environment_read_variable (typed_environment_restrict Gamma V) x = Some v ->
  Mem x V.
Proof.
  introv E. destruct Gamma. unfolds typed_environment_restrict.
  destruct split_fset_variable as [lt lf] eqn: E1. destruct x; simpls.
  - unfolds typed_environment_read_term. simpls.
    rewrites <- >> split_fset_variable_term_spec E1. destruct read_option eqn: E2; tryfalse.
    forwards I: @read_option_indom E2. rewrite heap_restrict_indom in I; try typeclass.
    lets~ (M&I'): (rm I).
  - unfolds typed_environment_read_flow. simpls.
    rewrites <- >> split_fset_variable_flow_spec E1. destruct read_option eqn: E2; tryfalse.
    forwards I: @read_option_indom E2. rewrite heap_restrict_indom in I; try typeclass.
    lets~ (M&I'): (rm I).
Qed.

Lemma typed_environment_restrict_equivalent : forall (Gamma1 Gamma2 : typed_environment) L1 L2,
  (forall x, Mem x L1 <-> Mem x L2) ->
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_equivalent
    (typed_environment_restrict Gamma1 L1)
    (typed_environment_restrict Gamma2 L2).
Proof.
  introv E1 E2. unfolds typed_environment_restrict. fold split_fset_variable.
  destruct (split_fset_variable L1) as [lt1 lf1] eqn: E3.
  destruct (split_fset_variable L2) as [lt2 lf2] eqn: E4.
  forwards (E5&E6): split_fset_variable_equivalent E1 E3 E4.
  splits~; apply~ heap_restrict_equiv; apply E2.
Qed.

Lemma typed_environment_domain_equivalent : forall Gamma1 Gamma2 : typed_environment,
  typed_environment_equivalent Gamma1 Gamma2 ->
  typed_environment_domain Gamma1 = typed_environment_domain Gamma2.
Proof.
  introv E. rewrite set_ext_eq. introv. unfolds typed_environment_domain.
  repeat rewrite in_set_st_eq. rewrite typed_environment_equivalent_spec in E.
  rewrite* E.
Qed.

Lemma typed_environment_domain_empty :
  typed_environment_domain typed_environment_empty = \{}.
Proof.
  rewrite is_empty_eq. unfolds typed_environment_domain. introv M.
  rewrite in_set_st_eq in M. applys (rm M).
  destruct x as [x|x]; compute; rewrite~ @not_indom_read_option; apply~ @not_indom_empty.
Qed.

Lemma typed_environment_read_term_empty : forall x,
  typed_environment_read_term typed_environment_empty x = None.
Proof. introv. unfolds. apply not_indom_read_option. apply not_indom_empty. Qed.

Lemma typed_environment_read_flow_empty : forall x,
  typed_environment_read_flow typed_environment_empty x = None.
Proof. introv. unfolds. apply not_indom_read_option. apply not_indom_empty. Qed.

Lemma typed_environment_read_variable_empty : forall x,
  typed_environment_read_variable typed_environment_empty x = None.
Proof.
  introv. destruct x; simpl.
  - rewrite~ typed_environment_read_term_empty.
  - rewrite~ typed_environment_read_flow_empty.
Qed.

Lemma typed_environment_domain_write_term : forall Gamma x v,
  typed_environment_domain (typed_environment_write_term Gamma x v)
  = typed_environment_domain Gamma \u \{ X_t x }.
Proof.
  introv. rewrite set_ext_eq. intro y. unfolds typed_environment_domain.
  rewrite set_in_union_eq. repeat rewrite in_set_st_eq. destruct y as [y|y].
  - destruct Gamma. unfolds typed_environment_write_term. simpl. unfolds typed_environment_read_term.
    simpl. tests: (y = x).
    + rewrite read_option_write_same. iff~ I. discriminate.
    + rewrite~ read_option_write. iff~ I. inverts I as I; autos~. false*.
  - iff I; [ left~ | repeat (inverts I as I; autos~) ].
Qed.

Lemma typed_environment_domain_write_flow : forall Gamma x v,
  typed_environment_domain (typed_environment_write_flow Gamma x v)
  = typed_environment_domain Gamma \u \{ _X_ x }.
Proof.
  introv. rewrite set_ext_eq. intro y. unfolds typed_environment_domain.
  rewrite set_in_union_eq. repeat rewrite in_set_st_eq. destruct y as [y|y].
  - iff I; [ left~ | repeat (inverts I as I; autos~) ].
  - destruct Gamma. unfolds typed_environment_write_flow. simpl. unfolds typed_environment_read_flow.
    simpl. tests: (y = x).
    + rewrite read_option_write_same. iff~ I. discriminate.
    + rewrite~ read_option_write. iff~ I. inverts I as I; autos~. false*.
Qed.

Lemma typed_environment_domain_write_variable : forall Gamma Gamma' x v,
  typed_environment_write_variable Gamma x v = Some Gamma' ->
  typed_environment_domain Gamma' = typed_environment_domain Gamma \u \{ x }.
Proof.
  introv E. destruct x as [x|x]; simpl in E;
    (gen E; eapply value_rect with (v := v) (P := fun v => _ v = _ -> _); introv E;
     [ rewrite value_rect_term in E | rewrite value_rect_flow in E]; inverts E).
  - apply~ typed_environment_domain_write_term.
  - apply~ typed_environment_domain_write_flow.
Qed.

Lemma typed_environment_domain_write_variables : forall Gamma Gamma' xs vs,
  typed_environment_write_variables Gamma xs vs = Some Gamma' ->
  typed_environment_domain Gamma' = typed_environment_domain Gamma \u to_set xs.
Proof.
  introv E. gen Gamma vs. induction xs; introv E; destruct vs; inverts E as E.
  - rewrite to_set_nil. rewrite~ for_set_union_empty_r.
  - rewrite to_set_cons. destruct typed_environment_write_variable eqn: E'; tryfalse.
    simpl in E. rewrites >> (rm IHxs) (rm E).
    rewrites >> typed_environment_domain_write_variable (rm E').
    rewrites~ for_set_union_assoc.
Qed.

Lemma typed_environment_domain_merge : forall Gamma1 Gamma2,
  typed_environment_domain (typed_environment_merge Gamma1 Gamma2)
  = typed_environment_domain Gamma1 \u typed_environment_domain Gamma2.
Proof.
  introv. rewrite set_ext_eq. intro x. unfolds typed_environment_domain.
  rewrite set_in_union_eq. repeat rewrite in_set_st_eq. destruct Gamma1, Gamma2.
  unfolds typed_environment_merge. destruct x as [x|x]; simpl.
  - unfolds typed_environment_read_term. simpl. destruct read_option eqn: E'.
    + iff I; try discriminate. forwards B: @read_option_binds E'.
      forwards [B'|B']: heap_merge_binds B; [ left | right ];
        rewrites~ >> (@binds_read_option) B'.
    + iff I; false~. forwards N: @read_option_not_indom (rm E'). false (rm N).
      inverts I as I; destruct read_option eqn: E; tryfalse~.
      * applys heap_merge_indom_left. applys read_option_indom E.
      * applys heap_merge_indom_right. applys read_option_indom E.
  - unfolds typed_environment_read_flow. simpl. destruct read_option eqn: E'.
    + iff I; try discriminate. forwards B: @read_option_binds E'.
      forwards [B'|B']: heap_merge_binds B; [ left | right ];
        rewrites~ >> (@binds_read_option) B'.
    + iff I; false~. forwards N: @read_option_not_indom (rm E'). false (rm N).
      inverts I as I; destruct read_option eqn: E; tryfalse~.
      * applys heap_merge_indom_left. applys read_option_indom E.
      * applys heap_merge_indom_right. applys read_option_indom E.
Qed.

Lemma typed_environment_domain_restrict : forall Gamma V,
  typed_environment_domain (typed_environment_restrict Gamma V)
  = typed_environment_domain Gamma \n to_set V.
Proof.
  introv. rewrite set_ext_eq. intro x. unfolds typed_environment_domain.
  rewrite in_inter_eq. unfold notin, to_set. repeat rewrite in_set_st_eq.
  destruct Gamma. unfolds typed_environment_restrict. destruct x as [x|x]; simpl.
  - destruct split_fset_variable as [lt lf] eqn: E. unfolds typed_environment_read_term. simpl.
    destruct read_option eqn: E'.
    + iff I; try discriminate. forwards B: @read_option_binds E'. forwards~ B': heap_restrict_binds B.
      rewrites~ >> (@binds_read_option) B'. splits~.
      unfolds variable. rewrites <- >> split_fset_variable_term_spec E.
      forwards I': @binds_indom B. apply heap_restrict_indom in I'; autos~. apply~ I'.
    + iff I; false~. lets (D&M): (rm I). false @read_option_not_indom E'.
      apply~ heap_restrict_indom. splits~.
      * rewrites~ >> split_fset_variable_term_spec E.
      * clear - D. destruct read_option eqn: R; tryfalse~. applys~ @read_option_indom R.
  - destruct split_fset_variable as [lt lf] eqn: E. unfolds typed_environment_read_flow. simpl.
    destruct read_option eqn: E'.
    + iff I; try discriminate. forwards B: @read_option_binds E'. forwards~ B': heap_restrict_binds B.
      rewrites~ >> (@binds_read_option) B'. splits~.
      unfolds variable. rewrites <- >> split_fset_variable_flow_spec E.
      forwards I': @binds_indom B. apply heap_restrict_indom in I'; autos~. apply~ I'.
    + iff I; false~. lets (D&M): (rm I). false @read_option_not_indom E'.
      apply~ heap_restrict_indom. splits~.
      * unfolds variable. rewrites~ <- >> split_fset_variable_flow_spec E in M.
      * clear - D. destruct read_option eqn: R; tryfalse~. applys~ @read_option_indom R.
Qed.

Lemma typed_environment_read_restrict_not_Mem : forall Gamma V x,
  ~ Mem x V ->
  typed_environment_read_variable (typed_environment_restrict Gamma V) x = None.
Proof.
  introv NM. asserts N: (x \notin typed_environment_domain (typed_environment_restrict Gamma V)).
  { rewrite~ typed_environment_domain_restrict. unfold notin. rewrite in_inter_eq. rew_logic*. }
  unfold notin, typed_environment_domain in N. rewrite in_set_st_eq in N. rew_logic~ in N.
Qed.


Lemma indom_typed_environment_term_read_term : forall Gamma x,
  indom (typed_environment_term Gamma) x <-> typed_environment_read_term Gamma x <> None.
Proof.
  introv. rewrite <- iff_not_cancel. rew_logic.
  rewrite not_indom_equiv_read_option. iff I; apply I.
Qed.

Lemma indom_typed_environment_flow_read_flow : forall Gamma x,
  indom (typed_environment_flow Gamma) x <-> typed_environment_read_flow Gamma x <> None.
Proof.
  introv. rewrite <- iff_not_cancel. rew_logic.
  rewrite not_indom_equiv_read_option. iff I; apply I.
Qed.

End Values.

Arguments typed_environment_term [_ _ _ _].
Arguments typed_environment_flow [_ _ _ _].
Arguments typed_environment_empty [_ _ _ _].
Arguments typed_environment_read_term [_ _ _ _] {_}.
Arguments typed_environment_read_flow [_ _ _ _] {_}.
Arguments typed_environment_read_variable [_ _ _] _ _ [_ _] {_ _}.
Arguments typed_environment_write_term [_ _ _ _].
Arguments typed_environment_write_flow [_ _ _ _].
Arguments typed_environment_write_variable [_ _ _] _ _ _ [_ _].
Arguments typed_environment_write_variables [_ _ _] _ _ _ [_ _].
Arguments typed_environment_restrict [_ _ _ _] {_ _}.
Arguments typed_environment_merge [_ _ _ _] {_ _}.
Arguments typed_environment_domain [_ _ _] _ _ [_ _] {_ _}.
Arguments typed_environment_equivalent [_ _ _ _].

Lemma typed_environment_write_variables_Forall3 : forall variable_term variable_flow
    `{Comparable variable_term} `{Comparable variable_flow}
    term_value1 term_value2 flow_value1 flow_value2 value1 value2
    (term_value_to_value1 : term_value1 -> value1)
    (term_value_to_value2 : term_value2 -> value2)
    (flow_value_to_value1 : flow_value1 -> value1)
    (flow_value_to_value2 : flow_value2 -> value2)
    value1_rect value2_rect
    Gamma1 Gamma1' Gamma2 Gamma2' xs vs1 vs2 P
    (x : variable variable_term variable_flow) v1 v2,
  Forall3 P xs vs1 vs2 ->
  typed_environment_write_variables term_value_to_value1 flow_value_to_value1 value1_rect
    Gamma1 xs vs1 = Some Gamma1' ->
  typed_environment_write_variables term_value_to_value2 flow_value_to_value2 value2_rect
    Gamma2 xs vs2 = Some Gamma2' ->
  (forall P f1 f2 (v : term_value1), value1_rect P f1 f2 (term_value_to_value1 v) = f1 v) ->
  (forall P f1 f2 (v : term_value2), value2_rect P f1 f2 (term_value_to_value2 v) = f1 v) ->
  (forall P f1 f2 (v : flow_value1), value1_rect P f1 f2 (flow_value_to_value1 v) = f2 v) ->
  (forall P f1 f2 (v : flow_value2), value2_rect P f1 f2 (flow_value_to_value2 v) = f2 v) ->
  Mem x xs ->
  typed_environment_read_variable term_value_to_value1 flow_value_to_value1 Gamma1' x = Some v1 ->
  typed_environment_read_variable term_value_to_value2 flow_value_to_value2 Gamma2' x = Some v2 ->
  exists y, P y v1 v2.
Proof.
  introv F E1 E2 It1 It2 If1 If2 M Ev1 Ev2. gen vs1 vs2 Gamma1 Gamma2.
  induction xs as [|x' xs]; introv F E1 E2.
  - inverts~ M.
  - inverts F as Px F. simpl in E1, E2.
    destruct (typed_environment_write_variable _ _ _ Gamma1) eqn: E1'; simpl in E1; tryfalse.
    destruct (typed_environment_write_variable _ _ _ Gamma2) eqn: E2'; simpl in E2; tryfalse.
    tests M': (Mem x xs).
    + forwards~ (y&Py): IHxs M' E1 E2. exists~ y.
    + inverts M as M; tryfalse.
      rewrites~ >> typed_environment_read_write_variables_not_in value1_rect M' E1 in Ev1.
      forwards~ R1: typed_environment_read_write_variable_eq E1'.
      rewrites (rm R1) in Ev1. inverts Ev1.
      rewrites~ >> typed_environment_read_write_variables_not_in value2_rect M' E2 in Ev2.
      forwards~ R2: typed_environment_read_write_variable_eq E2'.
      rewrites (rm R2) in Ev2. inverts Ev2. exists* x'.
Qed.

Lemma typed_environment_write_variables_Forall2 : forall variable_term variable_flow
    `{Comparable variable_term} `{Comparable variable_flow}
    term_value1 term_value2 flow_value1 flow_value2 value1 value2
    (term_value_to_value1 : term_value1 -> value1)
    (term_value_to_value2 : term_value2 -> value2)
    (flow_value_to_value1 : flow_value1 -> value1)
    (flow_value_to_value2 : flow_value2 -> value2)
    value1_rect value2_rect
    Gamma1 Gamma1' Gamma2 Gamma2' xs vs1 vs2 P
    (x : variable variable_term variable_flow) v1 v2,
  Forall2 P vs1 vs2 ->
  typed_environment_write_variables term_value_to_value1 flow_value_to_value1 value1_rect
    Gamma1 xs vs1 = Some Gamma1' ->
  typed_environment_write_variables term_value_to_value2 flow_value_to_value2 value2_rect
    Gamma2 xs vs2 = Some Gamma2' ->
  (forall P f1 f2 (v : term_value1), value1_rect P f1 f2 (term_value_to_value1 v) = f1 v) ->
  (forall P f1 f2 (v : term_value2), value2_rect P f1 f2 (term_value_to_value2 v) = f1 v) ->
  (forall P f1 f2 (v : flow_value1), value1_rect P f1 f2 (flow_value_to_value1 v) = f2 v) ->
  (forall P f1 f2 (v : flow_value2), value2_rect P f1 f2 (flow_value_to_value2 v) = f2 v) ->
  Mem x xs ->
  typed_environment_read_variable term_value_to_value1 flow_value_to_value1 Gamma1' x = Some v1 ->
  typed_environment_read_variable term_value_to_value2 flow_value_to_value2 Gamma2' x = Some v2 ->
  P v1 v2.
Proof.
  introv F E1 E2 It1 It2 If1 If2 M Ev1 Ev2.
  forwards* (y&F'): typed_environment_write_variables_Forall3
                      (fun y : variable variable_term variable_flow => P) E1 E2 It1 It2.
  rewrite Forall2_iff_forall_Nth in F. lets (El&F'): (rm F).
  rewrite Forall3_iff_forall_Nth. splits~.
  - rewrites~ >> typed_environment_read_write_variables_length E1.
  - introv N1 N2 N3. applys~ F' N2 N3.
Qed.

(** This definition is useful to transform reading functions defined in
  this file to sets. **)
Definition read_to_set A B f (a : A) : set B :=
  option_to_set (f a).

Arguments read_to_set [_ _].


(** * Tactics **)

Ltac rewrite_typed_environment_domain :=
  repeat match goal with
  | |- context [ typed_environment_domain ?term_value_to_value ?flow_value_to_value
                   typed_environment_empty ] =>
    rewrites >> typed_environment_domain_empty term_value_to_value flow_value_to_value
  | |- context [ typed_environment_domain ?term_value_to_value ?flow_value_to_value
                   (typed_environment_write_term ?Gamma ?x ?v) ] =>
    rewrites >> typed_environment_domain_write_term term_value_to_value flow_value_to_value Gamma x v
  | |- context [ typed_environment_domain ?term_value_to_value ?flow_value_to_value
                   (typed_environment_write_flow ?Gamma ?x ?v) ] =>
    rewrites >> typed_environment_domain_write_flow term_value_to_value flow_value_to_value Gamma x v
  | E: typed_environment_write_variable ?term_value_to_value ?flow_value_to_value ?value_rect
         ?Gamma ?x ?v = Some ?Gamma'
    |- context [ typed_environment_domain ?term_value_to_value ?flow_value_to_value ?Gamma' ] =>
    rewrites >> typed_environment_domain_write_variable term_value_to_value flow_value_to_value
                  value_rect Gamma Gamma' x v E
  | E: typed_environment_write_variables ?term_value_to_value ?flow_value_to_value ?value_rect
         ?Gamma ?xs ?vs = Some ?Gamma'
    |- context [ typed_environment_domain ?term_value_to_value ?flow_value_to_value ?Gamma' ] =>
    rewrites >> typed_environment_domain_write_variables term_value_to_value flow_value_to_value
                  value_rect Gamma Gamma' xs vs E
  | |- context [ typed_environment_domain ?term_value_to_value ?flow_value_to_value
                   (typed_environment_merge ?Gamma1 ?Gamma2) ] =>
    rewrites >> (@typed_environment_domain_merge) term_value_to_value flow_value_to_value Gamma1 Gamma2
  | |- context [ typed_environment_domain ?term_value_to_value ?flow_value_to_value
                   (typed_environment_restrict ?Gamma ?V) ] =>
    rewrites >> (@typed_environment_domain_restrict) term_value_to_value flow_value_to_value Gamma V
  end.

Ltac rewrite_read_typed_environment_basic :=
  let deal_with_merge Gamma1 Gamma2 x lemma :=
    rewrites >> lemma Gamma1 Gamma2 x; try typeclass;
    lazymatch goal with
    | |- ~ _ =>
      rewrite_typed_environment_domain; try typeclass;
      repeat rewrite to_set_cons; repeat rewrite LibSet.to_set_nil;
      solve_in_notin
    | _ => idtac
    end in
  repeat match goal with
  | |- context [ typed_environment_read_term (typed_environment_write_term ?Gamma ?x ?v) ?y ] =>
    first [
        rewrite (typed_environment_read_write_term_eq _ _ _ _ _ Gamma x v)
      | rewrite (typed_environment_read_write_term_neq _ _ _ _ _ Gamma x y v); [| discriminate ] ]
  | |- context [ typed_environment_read_flow (typed_environment_write_flow ?Gamma ?x ?v) ?y ] =>
    first [
        rewrite (typed_environment_read_write_flow_eq _ _ _ _ _ Gamma x v)
      | rewrite (typed_environment_read_write_flow_neq _ _ _ _ _ Gamma x y v); [| discriminate ] ]
  | |- context [ typed_environment_read_term (typed_environment_write_flow ?Gamma ?x ?v) ?y ] =>
    rewrite (typed_environment_read_term_write_flow _ _ _ _ _ Gamma x y v)
  | |- context [ typed_environment_read_flow (typed_environment_write_term ?Gamma ?x ?v) ?y ] =>
    rewrite (typed_environment_read_flow_write_term _ _ _ _ _ Gamma x y v)
  | |- context [ typed_environment_read_term typed_environment_empty ?x ] =>
    rewrite (typed_environment_read_term_empty _ _ _ _ _ _ x)
  | |- context [ typed_environment_read_flow typed_environment_empty ?x ] =>
    rewrite (typed_environment_read_flow_empty _ _ _ _ _ _ x)
  | |- context [ typed_environment_read_variable ?term_value_to_value ?flow_value_to_value
                   typed_environment_empty ?x ] =>
    rewrite (typed_environment_read_flow_empty _ _ _ term_value_to_value flow_value_to_value
               _ _ _ _ _ x)
  | E: typed_environment_write_variable ?term_value_to_value ?flow_value_to_value ?value_rect
         ?Gamma ?x ?v = Some ?Gamma'
    |- context [ typed_environment_read_variable ?term_value_to_value ?flow_value_to_value
                   ?Gamma' ?y ] =>
    first [
        rewrites >> typed_environment_read_write_variable_eq Gamma Gamma' x v E;
        try reflexivity; autos~
      | rewrites >> typed_environment_read_write_variable_neq Gamma Gamma' x y v E;
        try reflexivity; try discriminate; autos~ ]
  | |- context [ typed_environment_read_variable ?term_value_to_value ?flow_value_to_value
                   (typed_environment_restrict ?Gamma ?V) ?x ] =>
    rewrites >> (@typed_environment_read_restrict_Mem) Gamma V x;
    lazymatch goal with
    | |- Mem _ _ => autos~; try Mem_solve
    | _ => idtac
    end
  | |- context [ typed_environment_read_term (typed_environment_merge ?Gamma1 ?Gamma2) ?x ] =>
    first [
        deal_with_merge Gamma1 Gamma2 x (@typed_environment_read_term_merge_left)
      | deal_with_merge Gamma1 Gamma2 x (@typed_environment_read_term_merge_right) ]
  | |- context [ typed_environment_read_flow (typed_environment_merge ?Gamma1 ?Gamma2) ?x ] =>
    first [
        deal_with_merge Gamma1 Gamma2 x (@typed_environment_read_flow_merge_left)
      | deal_with_merge Gamma1 Gamma2 x (@typed_environment_read_flow_merge_right) ]
  | |- context [ typed_environment_read_variable ?term_value_to_value ?flow_value_to_value
                   (typed_environment_merge ?Gamma1 ?Gamma2) ?x ] =>
    first [
        deal_with_merge Gamma1 Gamma2 x (@typed_environment_read_variable_merge_left _ _ _
                                            term_value_to_value flow_value_to_value)
      | deal_with_merge Gamma1 Gamma2 x (@typed_environment_read_variable_merge_right _ _ _
                                            term_value_to_value flow_value_to_value) ]
  | |- context [ typed_environment_read_variable ?term_value_to_value ?flow_value_to_value
                   ?Gamma' ?x ] =>
    (** These cases will then be dealt with the first clauses of the [match]. **)
    lazymatch x with
    | X_t _ => unfold typed_environment_read_variable
    | _X_ _ => unfold typed_environment_read_variable
    end
  end.

Ltac rewrite_read_typed_environment :=
  let rec aux :=
    lazymatch goal with
    | |- indom (typed_environment_term ?Gamma) ?x =>
      let get_variable_term_Comparable Gamma :=
        let t := type of Gamma in
        let t := eval compute in t in
        match t with
        | typed_environment ?term_value ?flow_value ?variable_term ?variable_flow =>
          constr:(_ : Comparable variable_term)
        end in
      let c := get_variable_term_Comparable Gamma in
      rewrite indom_typed_environment_term_read_term with (variable_term_Comparable := c); aux
    | |- indom (typed_environment_flow ?Gamma) ?x =>
      let get_variable_flow_Comparable Gamma :=
        let t := type of Gamma in
        let t := eval compute in t in
        match t with
        | typed_environment ?term_value ?flow_value ?variable_term ?variable_flow =>
          constr:(_ : Comparable variable_flow)
        end in
      let c := get_variable_flow_Comparable Gamma in
      rewrite indom_typed_environment_flow_read_flow with (variable_flow_Comparable := c); aux
    | _ =>
      first [
          progress rewrite_read_typed_environment_basic
        | simpl; progress rewrite_read_typed_environment_basic
        | unfolds; aux ]
    end
  in try aux.

