(** An example of instanciation of the formalism, on the lambda-calculus. **)

(** This file is much shorter than While.v: it is only in the formalisation to show
  an example of a functionnal language.  We redirect the reader to While.v for a
  full example. **)

Set Implicit Arguments.

Require Import TLC.LibString.
Require Export WellFormedness Concrete Abstract.

(** * Language Definition **)

(** ** Definitions of Sorts **)

Inductive base_sort :=
  | Identifier
  .

Inductive program_sort :=
  | Term
  .

Inductive flow_sort :=
  | Closure
  | Environment
  .

Instance base_sort_Comparable : Comparable base_sort.
  prove_comparable.
Defined.

Instance program_sort_Comparable : Comparable program_sort.
  prove_comparable.
Defined.

Instance flow_sort_Comparable : Comparable flow_sort.
  prove_comparable.
Defined.

Instance base_sort_Inhab : Inhab base_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance program_sort_Inhab : Inhab program_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance flow_sort_Inhab : Inhab flow_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Let term_sort : Type := term_sort base_sort program_sort.
Let sort_base : base_sort -> term_sort := @sort_base _ _.
Let sort_program : program_sort -> term_sort := @sort_program _ _.
Coercion sort_base : base_sort >-> term_sort.
Coercion sort_program : program_sort >-> term_sort.
Let sort : Type := sort base_sort program_sort flow_sort.
Let sort_term : term_sort -> sort := @sort_term _ _ _.
Let sort_flow : flow_sort -> sort := @sort_flow _ _ _.
Coercion sort_term : term_sort >-> sort.
Coercion sort_flow : flow_sort >-> sort.

Definition in_sort s :=
  match s with
  | Term => Environment
  end.

Definition out_sort s :=
  match s with
  | Term => Closure
  end.


(** ** Definition of Constructors **)

Inductive constructor :=
  | Lambda
  | Var
  | Application
  .

Instance constructor_Comparable : Comparable constructor.
  prove_comparable.
Defined.

Instance constructor_Inhab : Inhab constructor.
  apply prove_Inhab. repeat constructors~.
Defined.

Definition constructor_signature c :=
  match c with
  | Lambda => ([Identifier : term_sort; Term : term_sort], Term)
  | Var => ([Identifier : term_sort], Term)
  | Application => ([Term : term_sort; Term : term_sort], Term)
  end.

(** ** Definition of Filters **)

Inductive filter :=
  | MakeClosure
  | GetClosure
  | ExtendEnvironment
  | GetEnvironment
  .

Definition filter_signature f :=
  match f with
  | MakeClosure => ([Identifier : sort; Term : sort; Environment : sort], [Closure : sort])
  | GetClosure => ([Closure : sort], [Identifier : sort; Term : sort; Environment : sort])
  | ExtendEnvironment => ([Environment : sort; Identifier : sort; Closure : sort], [Environment : sort])
  | GetEnvironment => ([Identifier : sort; Environment : sort], [Closure : sort])
  end.

(** ** Definition of the Skeletal Semantics **)

Let skeleton_instance : Type := skeleton_instance constructor filter.
Let skeletal_semantics : Type := skeletal_semantics constructor filter.

Definition skeletal_semantics_example : skeletal_semantics := fun c =>
  match c with
  | Lambda =>
    make_skeleton_instance (skeleton_variable_flow := False) ["x"; "e"]
      [F MakeClosure [X_t "x"; X_t "e"; _X_sigma] [_X_o]]
  | Var =>
    make_skeleton_instance (skeleton_variable_flow := False) [tt]
      [F GetEnvironment [X_t tt; _X_sigma] [_X_o]]
  | Application =>
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f 1); F GetClosure [_X_f 1] [X_t 3; X_t 4; _X_f 2];
       H X_sigma (_X_t 2) (X_f 3); F ExtendEnvironment [_X_f 2; X_t 3; _X_f 3] [_X_f 4];
       H (X_f 4) (_X_t 4) X_o]
  end%string.

Instance skeleton_instance_term_example_Comparable : forall c,
    Comparable (skeleton_variable_term (skeletal_semantics_example c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.

Instance skeleton_instance_flow_example_Comparable : forall c,
    Comparable (skeleton_variable_flow (skeletal_semantics_example c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.


(** * Well-formedness **)

Lemma skeletal_semantics_wellformed_example :
  skeletal_semantics_wellformed in_sort out_sort
                                filter_signature constructor_signature
                                skeletal_semantics_example _ _.
Proof. prove_wellformedness. Qed.

