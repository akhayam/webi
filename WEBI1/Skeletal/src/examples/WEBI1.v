(** An example of instanciation of the formalism, on the While language. **)

Set Implicit Arguments.

Require TLC.LibSet.
Require Import TLC.LibInt TLC.LibString.
Require Export WellFormedness Concrete Abstract.

(** * Language Definition **)

(** ** Definitions of Sorts **)

(** ***  base sorts are parametrized by client's and service's laanguage, because 
  WEBI does not provide any of them. *** **)
  
(** *** base sorts *** **)

Inductive base_sort :=
 (*| server's base sorts*)
 (*| client's base sorts*)
   | WEBIconfig
  .


(** *** program sorts  *** **)
Inductive program_sort :=
 | Store
.


(** *** flow sorts *** **)
Inductive flow_sort :=
  | ServerConfig
  | ClientConfig
  | Bool
  | ServerMemory
  | ServerValue
  | ListVarVal
  | WEBIvalue
  | WEBIvariable
  .

(** We instantiate our types for these specific sorts. **)
(** coercions **)
Let term_sort : Type := term_sort base_sort program_sort.
Let sort_base : base_sort -> term_sort := @sort_base _ _.
Let sort_program : program_sort -> term_sort := @sort_program _ _.
Coercion sort_base : base_sort >-> term_sort.
Coercion sort_program : program_sort >-> term_sort.
Let sort : Type := sort base_sort program_sort flow_sort.
Let sort_term : term_sort -> sort := @sort_term _ _ _.
Let sort_flow : flow_sort -> sort := @sort_flow _ _ _.
Coercion sort_term : term_sort >-> sort.
Coercion sort_flow : flow_sort >-> sort.

(** As program sorts are executable, then can be executed in a specific sort
  and return a specific sort when executed.
  The following constructs define these two sorts. **)
  
  Definition in_sort s :=
  match s with
  |  Store=> Store
  end.

Definition out_sort s :=
  match s with
  | Store => Store
  end.


  
(** ** Definition of Constructors **)

(** The term constructors.  Note that they are just symbols with no arity,
  which is given by [constructor_arity], which depends on
  [constructor_signature] defined below . **)

Inductive constructor :=
  | InitService
  | ClientStep
  .

Instance constructor_Comparable : Comparable constructor.
  prove_comparable.
Defined.

Instance constructor_Inhab : Inhab constructor.
  apply prove_Inhab. repeat constructors~.
Defined.

Definition constructor_signature c : list term_sort * program_sort :=
  match c with
  | InitService => ([], Store)
  | ClientService => ([], Store)
  end.
  
Inductive filter :=
  | SplitEnv(* split_sigma in the document*)
  | WcToSc
  | WcToCc
  | ScToValue
  | ScToState
  | Update_wc
  | Update_sc
  | Update_store
  | Update_storeWc
  | Update_serverState
  | IsEmptyConfig
  | Id
  | IsTrue
  | IsFalse
  | PopPair
  | Read_wc
  | GenClientCode
  | ServerEval
  | ClientEval
  .
  
Definition filter_signature (f : filter) : list sort * list sort :=
  match f with
  | SplitEnv => ([Store : sort], [WEBIconfig : sort ; ListVarVal : sort])
  | WcToSc => ([WEBIconfig : sort], [ServerConfig : sort])
  | WcToCc => ([WEBIconfig : sort], [ClientConfig : sort])
  | ScToValue => ([ServerConfig : sort], [ServerValue : sort])
  | ScToState => ([ServerConfig : sort], [ServerMemory : sort])
  | Update_wc => ([WEBIconfig : sort; ClientConfig : sort], [WEBIconfig : sort])
  | Update_sc => ([Store : sort ; WEBIconfig : sort; ListVarVal : sort], [ServerConfig : sort] )
  | Update_store => ([Store : sort ; WEBIconfig : sort; ListVarVal : sort], [Store : sort] )
  | Update_storeWc => ([Store : sort ; WEBIconfig : sort], [Store : sort] )
  | Update_serverState => ([ServerMemory : sort; WEBIvariable : sort; WEBIvalue : sort], 
                                                                                                                          [ServerMemory : sort])
  | IsEmptyConfig => ([ClientConfig : sort], [Bool : sort])
  | Id => ([Store : sort], [Store : sort])
  | IsTrue => ([Bool : sort], [])
  | IsFalse => ([Bool : sort], [])
  | PopPair => ([ListVarVal : sort], [WEBIvariable : sort; WEBIvalue : sort; ListVarVal : sort ])
  | Read_wc => ([Store : sort], [WEBIconfig : sort])
  | GenClientCode => ([ServerValue : sort], [ClientConfig : sort])
  | ServerEval => ([ServerConfig : sort], [ServerConfig : sort])
  | ClientEval => ([ClientConfig : sort], [ClientConfig : sort])
  end.

(** ** Definition of the Skeletal Semantics **)

Let skeleton_instance : Type := skeleton_instance constructor filter.
Let skeletal_semantics : Type := skeletal_semantics constructor filter.

(** To help coercions, we force the first two parameters of [F]. 
Let F : forall variable_term variable_flow, filter -> _ -> _ -> _ := @F constructor filter.**)

Definition skeletal_semantics_WEBI1 : skeletal_semantics := fun c =>
  match c with
  | InitService =>
    make_skeleton_instance (skeleton_variable_term := False) []
     [ F SplitEnv [_X_sigma] [_X_f "wc"; _X_f "list_VarVal"];
       F PopPair [_X_f "list_VarVal"] [_X_f "var"; _X_f "val_s"; _X_f "list_VarVal' "];
       F WcToCc [_X_f "wc"] [_X_f "cc"];
       F IsEmptyConfig [_X_f "cc"] [_X_f "bool"];
       B [
            [ F IsFalse [_X_f "bool"][];
              F Id [_X_sigma] [_X_o]
            ];

            [ F IsTrue [_X_f "bool"][];
              F WcToSc [_X_f "wc"][_X_f "sc"];
              F ScToState [_X_f "sc"][_X_f "serverState"];
              F Update_serverState [_X_f "serverState"; _X_f "var"; _X_f "val"][_X_f "serverState' "];
              F Update_sc [_X_f "sc"; _X_f "serverState' "][_X_f "sc' "];
              F ServerEval [_X_f "sc' "][_X_f "sc'' "];
              F ScToValue [_X_f "sc'' "][_X_f "value_s"];
              F GenClientCode [_X_f "value_s"][_X_f "cc' "];
              F Update_wc [_X_f "wc"; _X_f "cc' "][_X_f "wc' "];
              F Update_store [_X_sigma; _X_f "wc' "; _X_f "list_VarVal' "][_X_o]
            ]
        ] [_X_o]
     ]
  | ClientStep =>
    make_skeleton_instance (skeleton_variable_term := False) []
    [F Read_wc [_X_sigma][_X_f "wc"];
     F WcToCc [_X_f "wc"][_X_f "cc"];
     F ClientEval [_X_f "cc"][_X_f "cc' "];
     F Update_wc [_X_f "wc"; _X_f "cc' "][_X_f "wc' "];
     F Update_storeWc[_X_sigma; _X_f "wc' "][_X_o]
    ]
  end%string.

(**  The following instances check this if the used variables are comparable. **)

Instance skeleton_instance_term_WEBI1_Comparable : forall c,
    Comparable (skeleton_variable_term (skeletal_semantics_WEBI1 c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.

Instance skeleton_instance_flow_WEBI1_Comparable : forall c,
    Comparable (skeleton_variable_flow (skeletal_semantics_WEBI1 c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.

(** ** Well-formedness **)

(** The skeletal semantics of the WEBI is hopefully well-formed.(Let's check) **)

Lemma skeletal_semantics_wellformed_example :
  skeletal_semantics_wellformed in_sort out_sort
                                filter_signature constructor_signature
                                skeletal_semantics_WEBI1 _ _.
Proof. prove_wellformedness. Qed.

Print Assumptions skeletal_semantics_wellformed_example.
