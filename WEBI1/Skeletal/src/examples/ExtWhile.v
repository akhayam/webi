(** This example corresponds to Section 8 of the paper: we extend the While language
  defined in While.v to add exceptions, inputs, outputs, and a heap.
  The comments in this file only focus on the differences with the original While.v file. **)

Set Implicit Arguments.

Require TLC.LibSet.
Require Import Ascii TLC.LibInt TLC.LibString.
Require Export WellFormedness Concrete Abstract.
Require Import While.

(** * Language Definition **)

(** ** Definitions of Sorts **)

(** Base sorts and program sorts are left unchanged. **)

(** We extend the old flow sorts with the new values. **)
Inductive flow_sort :=
  | old_flow_sort : While.flow_sort -> flow_sort
  | In (** Input streams **)
  | Out (** Output streams **)
  | Heap (** Heaps **)
  | Loc (** Locations **)
  | Env (** Environment, that is the combination of input/output streams with a store and a heap **)
  | ValEnv (** Further combination of environment with a value **)
  | ExcEnv (** Environment with a signal indicating whether an exception was raised **)
  .
Coercion old_flow_sort : While.flow_sort >-> flow_sort.


Instance base_sort_Comparable : Comparable base_sort.
  prove_comparable.
Defined.

Instance program_sort_Comparable : Comparable program_sort.
  prove_comparable.
Defined.

Instance flow_sort_Comparable : Comparable flow_sort.
  prove_comparable.
Defined.

Instance base_sort_Inhab : Inhab base_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance program_sort_Inhab : Inhab program_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance flow_sort_Inhab : Inhab flow_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Let term_sort : Type := term_sort base_sort program_sort.
Let sort_base : base_sort -> term_sort := @sort_base _ _.
Let sort_program : program_sort -> term_sort := @sort_program _ _.
Coercion sort_base : base_sort >-> term_sort.
Coercion sort_program : program_sort >-> term_sort.
Let sort : Type := sort base_sort program_sort flow_sort.
Let sort_term : term_sort -> sort := @sort_term _ _ _.
Let sort_flow : flow_sort -> sort := @sort_flow _ _ _.
Coercion sort_term : term_sort >-> sort.
Coercion sort_flow : flow_sort >-> sort.

(** To reuse as many definitions as possible, we define this coercion. **)
Definition While_sort_to_sort (s : While.sort) : sort :=
  match s with
  | WellFormedness.sort_term t => sort_term t
  | WellFormedness.sort_flow s => sort_flow s
  end.
Coercion While_sort_to_sort : While.sort >-> sort.

Definition in_sort (s : program_sort) : flow_sort :=
  match s with
  | Expression => Env
  | Statement => Env
  end.

Definition out_sort (s : program_sort) : flow_sort :=
  match s with
  | Expression => ValEnv
  | Statement => ExcEnv
  end.


(** ** Definition of Constructors **)

(** We extend the constructors of the language. **)

Inductive constructor :=
  | old_constructor : While.constructor -> constructor
  | ReadIn
  | WriteOut
  | Ref
  | Deref (** Corresponds to [!] in the paper. **)
  | AsnRef
  | Throw
  | TryCatch
  .
Coercion old_constructor : While.constructor >-> constructor.

Instance constructor_Comparable : Comparable constructor.
  prove_comparable.
Defined.

Instance constructor_Inhab : Inhab constructor.
  apply prove_Inhab. repeat constructors~.
Defined.

(** This definition corresponds to Figure 14 of the paper. **)

Definition constructor_signature c : list term_sort * program_sort :=
  match c with
  | old_constructor c => constructor_signature c
  | ReadIn => ([], Expression)
  | WriteOut => ([Expression : term_sort], Statement)
  | Ref => ([Expression : term_sort], Expression)
  | Deref => ([Expression : term_sort], Expression)
  | AsnRef => ([Expression : term_sort; Expression : term_sort], Statement)
  | Throw => ([], Statement)
  | TryCatch => ([Statement : term_sort; Statement : term_sort], Statement)
  end.


(** ** Definition of Filters **)

(** We add new filters to extend the language.
  We however made sure not to change the old filters. **)

Inductive filter :=
  | old_filter : While.filter -> filter
  | GetIn
  | SetOut
  | Alloc
  | LocToVal (** [LocVal] in the paper. **)
  | IsLoc
  | GetLoc
  | SetLoc
  | MakeEnv
  | SplitEnv
  | MakeValEnv
  | GetValEnv
  | MakeOK
  | MakeExc
  | IsOK
  | IsExc
  .
Coercion old_filter : While.filter >-> filter.

(** This definition corresponds to Figure 15 of the paper. **)

Definition filter_signature (f : filter) : list sort * list sort :=
  match f with
  | old_filter f =>
    let (li, lo) := filter_signature f in
    (map (id : While.sort -> sort) li, map (id : While.sort -> sort) lo)
  | GetIn => ([In : sort], [Val : sort; In : sort])
  | SetOut => ([Out : sort; Val : sort], [Out : sort])
  | Alloc => ([Heap : sort; Val : sort], [Heap : sort; Loc : sort])
  | LocToVal => ([Loc : sort], [Val : sort])
  | IsLoc => ([Val : sort], [Loc : sort])
  | GetLoc => ([Loc : sort; Heap : sort], [Val : sort])
  | SetLoc => ([Loc : sort; Heap : sort; Val : sort], [Heap : sort])
  | MakeEnv => ([In : sort; Out : sort; Store : sort; Heap : sort], [Env : sort])
  | SplitEnv => ([Env : sort], [In : sort; Out : sort; Store : sort; Heap : sort])
  | MakeValEnv => ([Val : sort; Env : sort], [ValEnv : sort])
  | GetValEnv => ([ValEnv : sort], [Val : sort; Env : sort])
  | MakeOK => ([Env : sort], [ExcEnv : sort])
  | MakeExc => ([Env : sort], [ExcEnv : sort])
  | IsOK => ([ExcEnv : sort], [Env : sort])
  | IsExc => ([ExcEnv : sort], [Env : sort])
  end.

(** ** Definition of the Skeletal Semantics **)

Let skeleton_instance : Type := skeleton_instance constructor filter.
Let skeletal_semantics : Type := skeletal_semantics constructor filter.

(** To help coercions, we force the first two parameters of [F]. **)
Let F : forall variable_term variable_flow, filter -> _ -> _ -> _ := @F constructor filter.

(** We can now define the skeletal semantics.  Note that we also redefine
  the constructors whose skeleton was defined in the original While language.
  Indeed, their semantics has changed in this example: they now take
  environments and no longer stores as input.  We could have reused the old
  semantics for constructors if their associated semantics did not change,
  which is not the case in this example. **)
(** This definition corresponds to Figures 16 and 17 of the paper. **)

Definition skeletal_semantics_example : skeletal_semantics := fun c =>
  match c with

  (** Expressions **)
  | Const =>
    (** [Lit] in the paper.
      We note [X_t tt], [X_f false], and [X_f true] to respectively
      represent [x_t], [x_{f_n}], and [x_{f_v}] of the paper. **)
    make_skeleton_instance [tt]
      [F LitToInt [X_t tt] [_X_f false]; F IntToVal [_X_f false] [_X_f true];
       F MakeValEnv [_X_f true; _X_sigma] [_X_o]]
  | Var =>
    (** We note [X_t tt] and [X_f *] to represent [x_t] and [x_{f_*}] of the paper. **)
    make_skeleton_instance [tt]
      [F SplitEnv [_X_sigma] [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"];
       F Read [X_t tt; _X_f "s"] [_X_f "v"];
       F MakeEnv [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"] [_X_f "sigma"];
       F MakeValEnv [_X_f "v"; _X_f "sigma"] [_X_o]]
  | ReadIn =>
    (** [In] in the paper.
      We note [X_t tt] and [X_f *] to represent [x_t] and [x_{f_*}] of the paper. **)
    make_skeleton_instance (skeleton_variable_term := False) []
      [F SplitEnv [_X_sigma] [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"];
       F GetIn [_X_f "i"] [_X_f "v"; _X_f "i'"];
       F MakeEnv [_X_f "i'"; _X_f "o"; _X_f "s"; _X_f "h"] [_X_f "sigma"];
       F MakeValEnv [_X_f "v"; _X_f "sigma"] [_X_o]]
  | Ref =>
    (** [Alloc] in the paper.
      We note [X_t tt] and [X_f *] to represent [x_t] and [x_{f_*}] of the paper. **)
    make_skeleton_instance [tt]
      [H X_sigma (_X_t tt) (X_f "w"); F GetValEnv [_X_f "w"] [_X_f "v"; _X_f "sigma"];
       F SplitEnv [_X_f "sigma"] [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"];
       F Alloc [_X_f "h"; _X_f "v"] [_X_f "h'"; _X_f "l"]; F LocToVal [_X_f "l"] [_X_f "v'"];
       F MakeEnv [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h'"] [_X_f "sigma'"];
       F MakeValEnv [_X_f "v'"; _X_f "sigma'"] [_X_o]]
  | Deref =>
    (** [Acc] in the paper.
      We note [X_t tt] and [X_f *] to represent [x_t] and [x_{f_*}] of the paper. **)
    make_skeleton_instance [tt]
      [H X_sigma (_X_t tt) (X_f "w"); F GetValEnv [_X_f "w"] [_X_f "v"; _X_f "sigma"];
       F IsLoc [_X_f "v"] [_X_f "l"];
       F SplitEnv [_X_f "sigma"] [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"];
       F GetLoc [_X_f "l"; _X_f "h"] [_X_f "v'"];
       F MakeEnv [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h'"] [_X_f "sigma'"];
       F MakeValEnv [_X_f "v'"; _X_f "sigma'"] [_X_o]]
  | Add =>
    (** We note [X_t n] and [X_f *] to represent [x_{t_n}] and [x_{f_*}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f "w1"); F GetValEnv [_X_f "w1"] [_X_f "v1"; _X_f "sigma1"];
       F IsInt [_X_f "v1"] [_X_f "n1"];
       H (X_f "sigma1") (_X_t 2) (X_f "w2"); F GetValEnv [_X_f "w2"] [_X_f "v2"; _X_f "sigma2"];
       F IsInt [_X_f "v2"] [_X_f "n2"];
       F AddNum [_X_f "n1"; _X_f "n2"] [_X_f "n"]; F IntToVal [_X_f "n"] [_X_f "v"];
       F MakeValEnv [_X_f "v"; _X_f "sigma2"] [_X_o]]
  | Eq =>
    (** We note [X_t n] and [X_f *] to represent [x_{t_n}] and [x_{f_*}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f "w1"); F GetValEnv [_X_f "w1"] [_X_f "v1"; _X_f "sigma1"];
       F IsInt [_X_f "v1"] [_X_f "n1"];
       H (X_f "sigma1") (_X_t 2) (X_f "w2"); F GetValEnv [_X_f "w2"] [_X_f "v2"; _X_f "sigma2"];
       F IsInt [_X_f "v2"] [_X_f "n2"];
       F EqNum [_X_f "n1"; _X_f "n2"] [_X_f "b"]; F BoolToVal [_X_f "b"] [_X_f "v"];
       F MakeValEnv [_X_f "v"; _X_f "sigma2"] [_X_o]]
  | Neg =>
    (** We note [X_t tt], [X_f false], and [X_f true] to respectively represent
      [x_t], [x_{f_1}], and [x_{f_{1'}}] of the paper. **)
    make_skeleton_instance [tt]
      [H X_sigma (_X_t tt) (X_f "w"); F GetValEnv [_X_f "w"] [_X_f "v"; _X_f "sigma"];
       F IsBool [_X_f "v"] [_X_f "b"]; F NegBool [_X_f "b"] [_X_f "b'"];
       F BoolToVal [_X_f "b'"] [_X_f "v'"]; F MakeValEnv [_X_f "v'"; _X_f "sigma"] [_X_o]]

  (** Statements **)
  | Skip =>
    make_skeleton_instance (skeleton_variable_term := False) (skeleton_variable_flow := False) []
      [F MakeOK [_X_sigma] [_X_o]]
  | Throw =>
    make_skeleton_instance (skeleton_variable_term := False) (skeleton_variable_flow := False) []
      [F MakeExc [_X_sigma] [_X_o]]
  | Asn =>
    (** We note [X_f *] and [X_t n] to respectively represent
      [x_{f_*}], [x_{t_n}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 2) (X_f "w"); F GetValEnv [_X_f "w"] [_X_f "v"; _X_f "sigma"];
       F SplitEnv [_X_f "sigma"] [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"];
       F Write [X_t 1; _X_f "s"; _X_f "v"] [_X_f "s'"];
       F MakeEnv [_X_f "i"; _X_f "o"; _X_f "s'"; _X_f "h"] [_X_f "sigma'"];
       F MakeOK [_X_f "sigma'"] [_X_o]]
  | AsnRef =>
    (** [Set] in the paper.
      We note [X_t n] and [X_f *] to represent [x_{t_n}] and [x_{f_*}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f "w1"); F GetValEnv [_X_f "w1"] [_X_f "v1"; _X_f "sigma"];
       F IsLoc [_X_f "v1"] [_X_f "l"];
       H (X_f "sigma") (_X_t 2) (X_f "w2"); F GetValEnv [_X_f "w2"] [_X_f "v2"; _X_f "sigma'"];
       F SplitEnv [_X_f "sigma'"] [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"];
       F SetLoc [_X_f "l"; _X_f "h"; _X_f "v2"] [_X_f "h'"];
       F MakeEnv [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h'"] [_X_f "sigma''"];
       F MakeOK [_X_f "sigma''"] [_X_o]]
  | WriteOut =>
    (** [Out] in the paper.
      We note [X_t tt] and [X_f *] to represent [x_{t_1}] and [x_{f_*}] of the paper. **)
    make_skeleton_instance [tt]
      [H X_sigma (_X_t tt) (X_f "w"); F GetValEnv [_X_f "w"] [_X_f "v"; _X_f "sigma"];
       F SplitEnv [_X_f "sigma'"] [_X_f "i"; _X_f "o"; _X_f "s"; _X_f "h"];
       F SetOut [_X_f "o"; _X_f "v"] [_X_f "o'"];
       F MakeEnv [_X_f "i"; _X_f "o'"; _X_f "s"; _X_f "h"] [_X_f "sigma'"];
       F MakeOK [_X_f "sigma'"] [_X_o]]
  | Seq =>
    (** We note [X_t n] and [X_f *] to respectively represent [x_{s_n}] and
      [x_{f_*}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f "e"); B [
         [F IsOK [_X_f "e"] [_X_f "sigma"]; H (X_f "sigma") (_X_t 2) X_o];
         [F IsExc [_X_f "e"] [_X_f "sigma'"]; F MakeExc [_X_f "sigma'"] [_X_o]]
       ] [_X_o]]
  | TryCatch =>
    (** [Try] in the paper.
      We note [X_t n] and [X_f *] to respectively represent [x_{s_n}] and
      [x_{f_*}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f "e"); B [
         [F IsOK [_X_f "e"] [_X_f "sigma'"]; F MakeOK [_X_f "sigma'"] [_X_o]];
         [F IsExc [_X_f "e"] [_X_f "sigma"]; H (X_f "sigma") (_X_t 2) X_o]
       ] [_X_o]]
  | IfThenElse =>
    (** [If] in the paper.
      We note [X_t n] and [X_f *] to respectively represent [x_{s_n}] and
      [x_{f_*}] of the paper. **)
    make_skeleton_instance [1; 2; 3]
      [H X_sigma (_X_t 1) (X_f "w"); F GetValEnv [_X_f "w"] [_X_f "v"; _X_f "sigma"];
       F IsBool [_X_f "v"] [_X_f "b"]; B [
         [F IsTrue [_X_f "b"] []; H (X_f "sigma") (_X_t 2) X_o];
         [F IsFalse [_X_f "b"] []; H (X_f "sigma") (_X_t 3) X_o]
       ] [_X_o]]
  | While =>
    (** We note [X_t n] and [X_f *] to respectively represent [x_{s_n}] and
      [x_{f_*}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f "w"); F GetValEnv [_X_f "w"] [_X_f "v"; _X_f "sigma"];
       F IsBool [_X_f "v"] [_X_f "b"]; B [
         [F IsTrue [_X_f "b"] []; H (X_f "sigma") (_X_t 2) (X_f "e"); B [
            [F IsOK [_X_f "e"] [_X_f "sigma'"];
             H (X_f "sigma'") (_term_constructor (While : constructor) [_X_t 1; _X_t 2]) (X_o)];
            [F IsExc [_X_f "e"] [_X_f "sigma''"]; F MakeExc [_X_f "sigma''"] [_X_o]]
          ] [_X_o]];
         [F IsFalse [_X_f "b"] []; F MakeOK [_X_f "sigma"] [_X_o]]
       ] [_X_o]]

  end%string.

Instance skeleton_instance_term_example_Comparable : forall c,
    Comparable (skeleton_variable_term (skeletal_semantics_example c)).
  intros c. destruct c as [c| | | | | | |]; [destruct c|..]; simpl; prove_comparable.
Defined.

Instance skeleton_instance_flow_example_Comparable : forall c,
    Comparable (skeleton_variable_flow (skeletal_semantics_example c)).
  intros c. destruct c as [c| | | | | | |]; [destruct c|..]; simpl; prove_comparable.
Defined.
