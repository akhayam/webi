(* Adam Khayam *)

(* General types definition *)
module G =
  struct
    type var = string
    type op =
      | OeqBool
      | OeqNum
      | OleqNum
      | OaddNum
    (* other ops to come  *)
    type const =
      | Cnum of int
      | Cbool of bool
      | Cstr of string
    let eval_op = failwith "eval_op not implemented"
  end

(* memory *) 
module Memory = Map.Make( String )

(* Callback set *)

              
(* Thunks set *)
              
(* follow sintactically this: < P, mu >. 
   It is intended more as a configuration *)
type ('p, 'a) state = 'p * 'a Memory.t (* generic state *)
type ('p, 'a) res_state = (* resulting state after a transition *)
  | ESrun of ('p, 'a) state
  | ESend of 'a  Memory.t

module type LANG =
  (* Components of a language are programs and expressions *)
  sig
    type program
    type baseValue
    val transition = ( program, baseValue) state ->
                     ( program, baseValue) res_state

  end

module type SRV_LANG =
  sig
    include LANG
    type client_program
    val make_client_code = baseValue -> client_program
  end

module Client =
  struct
    (* Syntax of client's expressions  *)
    type expression =
      | Econst of G.const
      | Evar of G.var
      | Eop of G.op * expression list

    (* Syntax of client's programs*)
    type program =
      | Pasgn = G.var * expression
      | Pseq = program * program
      | Pif = expression * program * program
      | Pwhile = expression * program
      | Pskip
      
    (* Client's base values *)
    type baseValue = G.const

                   
    (* Client's transition relation *)
    let transition ((p, m) : (program, baseValue)) :
          (program, baseValue) res_state =
      let rec eval_expr : expression -> baseValue = function
        | Econst c -> c
        | Evar v -> Memory.find v m
        | Eop (o, l) -> G.eval_op o (List.map eval_expr l) in
      match p with
      | Pasgn (v, e) -> ESrun (Pskip, M.add v (eval_expr e) m)
      | Pseq (p0, p1) ->
         begin
           match transition (p0, m) with
           | ESend m0 | ESrun (Pskip, m0) -> ESrun (p1, m0)
           | ESrun (p0n, m0) -> ESrun (Pseq (p0n, p1), m0)
         end
      | Pif (e, p0, p1) ->
         begin
           match eval_expr e with
           | G.Cbool true -> ESrun (p0, m)
           | G.Cbool false -> ESrun (p1, m)
           | _ -> failwith "condition is not a boolean value [if]"
         end
      | Pwhile (e, p0) ->
         begin
           match eval_expr e with
           | G.Cbool true ->
              ESrun (PSeq (p0, p) m)
           | G.Cbool flase -> ESrun (Pskip, m)
           | _ -> failwith "condition is not a boolean value [while]"
         end
      |Pskip -> Esend m
  end
     
module TCclient = (Client : LANG)

module Server =
  struct
    
    (* syntax for ~ expressions and programs *)
    type texpr =
      | TEconst of G.const
      | TEvar of G.var
      | TEop of G.op * texpr list
      | TEdollar of G.var
                  
    (* ~t defined in example 2, page 5*)
    type tprogram =
      | TPasgn of G.var * texpr
      | TPseq of tprogram * tprogram
      | TPif of texpr * tprogram * tprogram
      | TPwhile of texpt * tprogram
      | TPskip
      
    (* e$ in example 2 page 5*)
    type expression =
      | Econst of G.const
      | Evar of G.var
      | Eop of G.op * expression list
      | Etilde of tprogram
                
    (* Server's programming language*)
    type program =
      | Pasgn = G.var * expression
      | Pseq = program * program
      | Pif = expression * program * program
      | Pwhile = expression * program
      | Preturn = expression
      | Pskip
       
    (* Server base values *)
    type baseValue =
      | SVconst of G.const
      | SVprogram of Client.program
                   
    (*  Transition relation *)
    let rec transition ((p, m): (program, baseValue)) :
              (program, baseValue) res_state =

      (* ----------------BEGIN FUNCTIONS DEFINITION---------------- *)

      (* get_val allows to retriev a value in stored in memory/state*)
      let get_val v =
        match Memory.find v m with
        | SVconst c -> c
        | SVprogram _ -> failwith "program under dollar" in

      (* ksi function defined in page 6 *)
      let rec ksi_fun: texpression -> Client.expression = function
        | TEconst c -> Client.Econst
        | TEvar v -> Client.Evar v
        | TEop (o, l) -> Client.Eop (o, List.map ksi_fun l)
        | TEdollar v -> Client.Econst (get_val v) in

      (* fi functin defined in page 6 *)
      let rec fi_fun tprogram -> Client.program = function
        | TPasgn (v, e) -> Client.Pasgn (v, ksi_fun e)
        | TPseq (t0, t1) -> Client.Pseq (ksi_fun t0, ksi_fun t1)
        | TPif (e, t0, t1) -> Client.Pif (ksi_fun e,
                                          fi_fun t0,
                                          fi_fun t1)
        | TPwhile (e, t) -> Client.Pwhile(ksi_fun e,
                                          fi_fun t)
        | TPskip -> Client.Pskip in
      
      (* Server's expression evaluator *)
      let rec eval_expr: expression -> baseValue = function
        | Econst c -> SVconst c
        | Evar v -> Memory.find v m
        | Eop (o, l) ->
           let ldexp = List.map eval_expr l in
           let lval = List.map
                        (function
                         | SVconst c -> c
                         | SVprogram _ ->
                            failwith "program not allowed")
                        ldexp in
           SVconst  (G.eval_op o lval)
        | Etilde p -> SVprogram (fi_fun p) in
      
      (* -----------------END FUNCTIONS DEFINITION----------------- *)

      match p with
      | Pasgn (v, e) -> ESrun (Pskip, Memory.add v (eval_expr e) m)
      | Pseq (p0, p1) ->
         begin
           match transition (p0,m) with
           | ESend m0| ESrun (Pskip, m0) -> ESrun (p1, m0)
           | ESrun (p0n, m0) -> ESrun (Pseq (p0n, p1) m0)
         end
      | Pif (e, t0, t1) ->
         begin
           match eval_expr e with
           | SVconst (G.Cbool true) -> ESrun (p0, m)
           | SVconst (G.Cbool false) -> ESrun (p1, m)
           | _ -> failwith "condition is not a boolean value [if]"
         end
      |Pwhile (e, p0) ->
        begin
          match eval_expr e with
          | SVconst (G.Cbool true) -> ESrun (Pseq (p0, p) m)
          | SVconst (G.Cbool false) -> ESrun (Pskip, m)
          | _ -> failwith "condition is not a boolean value [while]"
        end
      | Pskip -> ESend m
      | Preturn e -> ESend  ((eval_expr e) m)
               
    (* Defining a type for client programs*)
    type Tclient = Client.program

    (* gencc on paper *)
    let make_client_code = function
      | SVconst _ -> failwith "not a server's client program value"
      | SVprogram p -> p
  end

(* Checking types*)
module TCServer = (Server: SRV_LANG)

(* Iterated transition relation *)
let iterator (fnext: ('p, 'a) state -> ('p, 'a) res_state) :
      ('p, 'a) state -> 'a Memory.t =
  let rec aux ini =
    let nxt = fnext ini in
    match nxt with
    | ESend s -> s
    | ESrun (p,s) -> aux (p, s) in
  aux

