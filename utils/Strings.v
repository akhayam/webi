Require Import Coq.Strings.String.


(** Here are defined some properties on strings *)

(** To compare strings, we define the function [beq_string], which
    internally uses the function [string_dec] from Coq's string library.
    We then establish its fundamental properties. *)

Definition beq_string x y :=
  if string_dec x y then true else false.

(** ([string_dec] comes from Coq's string library.
    [string_dec] does not actually return a [bool], but a type that looks
    like [{x = y} + {x <> y}], called a [sumbool].  Formally, an
    element of [sumbool] is either a proof that two things are equal
    or a proof that they are unequal.) *)

Theorem beq_string_refl : forall s, true = beq_string s s.
Proof. intros s. unfold beq_string. destruct (string_dec s s) as [|Hs].
  - reflexivity.
  - destruct Hs. reflexivity.
Qed.

(** The following useful property of [beq_string] follows from an
    analogous lemma about strings: *)

Theorem beq_string_true_iff : forall x y : string,
  beq_string x y = true <-> x = y.
Proof.
   intros x y.
   unfold beq_string.
   destruct (string_dec x y) as [|Hs].
   - subst. split. reflexivity. reflexivity.
   - split.
     + intros contra. inversion contra.
     + intros H.  subst. destruct Hs. reflexivity.
Qed.

(** Similarly: *)

Theorem beq_string_false_iff : forall x y : string,
  beq_string x y = false
  <-> x <> y.
Proof.
  intros x y. rewrite <- beq_string_true_iff.
  unfold not. 
  split.
  - intros. rewrite H0 in H. inversion H.
  - intros. destruct beq_string. destruct H;reflexivity. reflexivity.  
Qed.

(** This useful variant follows just by rewriting: *)

Theorem false_beq_string : forall x y : string,
   x <> y -> beq_string x y = false.
Proof.
  intros x y. rewrite beq_string_false_iff.
  intros H. apply H. Qed.


