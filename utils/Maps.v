Require Import Coq.Arith.Arith.
Require Import Coq.Bool.Bool.
Require Export Coq.Strings.String.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Lists.List.
Require Import utils.Strings.


(** Maps *)



(** Definition of total Maps *)


Definition total_map (A: Type) := string -> A.

(** t_empty yields an empty total map, given a default
    element; this map always returns the default element when applied
    to any string. *)

Definition t_empty {A:Type} (v : A) : total_map A :=
  (fun _ => v).

(** [t_update] takes a _function_ [m] and yields a new function
    [fun x' => ...] that behaves like the desired map. *)

Definition t_update {A:Type} (m : total_map A)
                    (x : string) (v : A) :=
  fun x' => if beq_string x x' then v else m x'. (** Problem with imports on strings.. :/ *)

(** The notation will be the following:*)

(*** i.e. for empty map of int: { !=> 0 } *)

Notation "{ !-> d }" := (t_empty d) (at level 0).

(*** map's update *)

Notation "m '&' { a !-> x }" :=
  (t_update m a x) (at level 20).
Notation "m '&' { a !-> x ; b !-> y }" :=
  (t_update (m & { a !-> x }) b y) (at level 20).
Notation "m '&' { a !-> x ; b !-> y ; c !-> z }" :=
  (t_update (m & { a !-> x ; b !-> y }) c z) (at level 20).
Notation "m '&' { a !-> x ; b !-> y ; c !-> z ; d !-> t }" :=
    (t_update (m & { a !-> x ; b !-> y ; c !-> z }) d t) (at level 20).
Notation "m '&' { a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }" :=
    (t_update (m & { a !-> x ; b !-> y ; c !-> z ; d !-> t }) e u) (at level 20).
Notation "m '&' { a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u ; f !-> v }" :=
    (t_update (m & { a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }) f v) (at level 20).


(* ############################################################################################### *)
(** * Partial maps *)

Definition partial_map (A:Type) := total_map (option A).

Definition empty {A:Type} : partial_map A :=
  t_empty None.

Definition update {A:Type} (m : partial_map A)
           (x : string) (v : A) :=
  m & { x !-> (Some v) }.

(** We introduce a similar notation for partial maps, using double
    curly-brackets.  **)

Notation "m '&' {{ a !-> x }}" :=
  (update m a x) (at level 20).
Notation "m '&' {{ a !-> x ; b !-> y }}" :=
  (update (m & {{ a !-> x }}) b y) (at level 20).
Notation "m '&' {{ a !-> x ; b !-> y ; c !-> z }}" :=
  (update (m & {{ a !-> x ; b !-> y }}) c z) (at level 20).
Notation "m '&' {{ a !-> x ; b !-> y ; c !-> z ; d !-> t }}" :=
    (update (m & {{ a !-> x ; b !-> y ; c !-> z }}) d t) (at level 20).
Notation "m '&' {{ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }}" :=
    (update (m & {{ a !-> x ; b !-> y ; c !-> z ; d !-> t }}) e u) (at level 20).
Notation "m '&' {{ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u ; f !-> v }}" :=
    (update (m & {{ a !-> x ; b !-> y ; c !-> z ; d !-> t ; e !-> u }}) f v) (at level 20).



























