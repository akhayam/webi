# WEBI

Formal operational semantic of WEBI language written in coq.
Purpose of this project is to give a behavioural description of WEBI in COQ, using Small Step and Skeletal Semantics